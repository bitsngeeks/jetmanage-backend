<?php

return [

    /*
    |--------------------------------------------------------------------------
    | CRM User
    |--------------------------------------------------------------------------
    */
    'messages' => [
        'store'   => 'User added successfully.',
        'update'  => 'User updated successfully.',
        'destroy' => [
            'success' => 'User deleted successfully.',
            'error'   => 'Something went wrong while deleting user.',
        ],
        'role'    => 'Roles assigned to user successfully.',
    ],
    'view'     => [
        'list' => [
            'fullName' => 'Full Name',
            'email'    => 'Email',
            'roles'    => 'Roles',
            'username' => 'Username',
        ],
        'form' => [
            'create'             => 'Create User',
            'edit'               => 'Edit User',
            'title'              => 'User Information',
            'fullName'           => 'Full Name',
            'email'              => 'Email',
            'password'           => 'Password',
            'rePassword'         => 'Re-Password',
            'username'           => 'Username',
            'assignedCallCenter' => 'Assigned Call Center',
            'passwordCoupon'     => 'Coupon Password',
            'rePasswordCoupon'   => 'Coupon Re-Password',
        ],
        'role' => [
            'title'  => 'User Role Information',
            'button' => 'Assign Role',
        ],
    ],
    'js'       => [
        'delete'     => [
            'message' => 'Are you sure you want to do delete this user?',
        ],
        'validation' => [
            'name'       => 'The full name is required and cannot be empty',
            'username'   => 'The username is required and cannot be empty',
            'email'      => [
                'notEmpty'     => 'The email address is required and cannot be empty',
                'emailAddress' => 'The email address is not valid',
            ],
            'password'   => [
                'notEmpty'  => 'The password is required',
                'identical' => 'The password and its confirm are not the same',
            ],
            'rePassword' => [
                'notEmpty'  => 'The re-password is required',
                'identical' => 'The password and its confirm are not the same',
            ],
        ],
    ],
];
