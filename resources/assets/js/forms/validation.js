(function (document, window, $) {
    'use strict';


    // Validation for user edit from
    // ---------------------------------
    (function () {
        $('#keyform').formValidation({
            framework: "bootstrap4",
            button: {
                selector: '#sub',
                disabled: 'disabled'
            },
            icon: null,
            fields: {

                name: {
                    validators: {
                        notEmpty: {
                            message: Lang.get('key.js.validation.name')
                        }
                    }
                },

            },
            err: {
                clazz: 'text-help'
            },
            row: {
                invalid: 'has-danger'
            }
        });
    })();



    // Validation for user edit from
    // ---------------------------------
    (function () {

        $('#couponform').formValidation({
            framework: "bootstrap4",
            button: {
                selector: '#sub',
                disabled: 'disabled'
            },
            icon: null,
            fields: {

                name: {
                    validators: {
                        notEmpty: {
                            message: Lang.get('coupon.js.validation.name')
                        }
                    }
                },

                code: {
                    validators: {
                        notEmpty: {
                            message: Lang.get('coupon.js.validation.code.notEmpty')
                        },
                        callback: {
                            message: Lang.get('coupon.js.validation.code.noSpace'),
                            callback: function (value, validator, $field) {
                                console.log(value.indexOf(' '));
                                return value.indexOf(' ') == -1;
                            }
                        }
                    }
                },
                radio: {
                    selector: 'input.fixed, input.percentage',
                    validators: {
                        notEmpty: {
                            message: Lang.get('coupon.js.validation.radio.notEmpty')
                        },
                        numeric: {
                            message: Lang.get('coupon.js.validation.radio.numeric')
                        }
                    }
                }

            },
            err: {
                clazz: 'text-help'
            },
            row: {
                invalid: 'has-danger'
            }
        })

    })();


    // Validation for user edit from
    // ---------------------------------
    (function () {
        $('#customerpasswordform').formValidation({
            framework: "bootstrap4",
            button: {
                selector: '#sub',
                disabled: 'disabled'
            },
            icon: null,
            fields: {

                oldpassword: {
                    validators: {
                        stringLength: {
                            min: 8
                        },
                    }
                },
                password: {
                    validators: {
                        stringLength: {
                            min: 8
                        },
                        identical: {
                            field: 'password_confirmation',
                            message: Lang.get('user.js.validation.password.identical')
                        }
                    }
                },
                password_confirmation: {
                    validators: {
                        stringLength: {
                            min: 8
                        },
                        identical: {
                            field: 'password',
                            message: Lang.get('user.js.validation.rePassword.identical')
                        }
                    }
                },

            },
            err: {
                clazz: 'text-help'
            },
            row: {
                invalid: 'has-danger'
            }
        });
    })();


    (function () {
        $('#caseform').formValidation({
            framework: "bootstrap4",
            button: {
                selector: '#sub',
                disabled: 'disabled'
            },
            icon: null,
            fields: {

                title: {
                    validators: {
                        notEmpty: {
                            message: Lang.get('case.js.validation.title')
                        }
                    }
                },
                user_id: {
                    validators: {
                        notEmpty: {
                            message: Lang.get('case.js.validation.user.notEmpty')
                        },
                        greaterThan: {
                            value: 1,
                            message: Lang.get('case.js.validation.user.notEmpty')
                        }
                    }
                },
            },
            err: {
                clazz: 'text-help'
            },
            row: {
                invalid: 'has-danger'
            }
        });
    })();

    (function () {
        $('#ordercomment').formValidation({
            framework: "bootstrap4",
            button: {
                selector: '#sub',
                disabled: 'disabled'
            },
            icon: null,
            fields: {

                comment: {
                    validators: {
                        notEmpty: {
                            message: Lang.get('order.js.validation.comment')
                        }
                    }
                },

            },
            err: {
                clazz: 'text-help'
            },
            row: {
                invalid: 'has-danger'
            }
        });
    })();

    (function () {
        $('#computerform').formValidation({
            framework: "bootstrap4",
            button: {
                selector: '#sub',
                disabled: 'disabled'
            },
            icon: null,
            fields: {

                name: {
                    validators: {
                        notEmpty: {
                            message: Lang.get('computer.js.validation.name')
                        }
                    }
                },

            },
            err: {
                clazz: 'text-help'
            },
            row: {
                invalid: 'has-danger'
            }
        });
    })();


    // Validation for user edit from
    // ---------------------------------
    (function () {
        $('#planform').formValidation({
            framework: "bootstrap4",
            button: {
                selector: '#sub',
                disabled: 'disabled'
            },
            icon: null,
            fields: {

                name: {
                    validators: {
                        notEmpty: {
                            message: Lang.get('plan.js.validation.name')
                        }
                    }
                },
                eur_initial_amount: {
                    validators: {
                        notEmpty: {
                            message: Lang.get('plan.js.validation.initialAmount.notEmpty')
                        },
                        numeric: {
                            message: Lang.get('plan.js.validation.initialAmount.numeric')
                        }
                    }
                },
                eur_recurring_amount: {
                    validators: {
                        notEmpty: {
                            message: Lang.get('plan.js.validation.recurringAmount.notEmpty')
                        },
                        numeric: {
                            message: Lang.get('plan.js.validation.recurringAmount.numeric')
                        }
                    }
                },
                usd_initial_amount: {
                    validators: {
                        notEmpty: {
                            message: Lang.get('plan.js.validation.initialAmount.notEmpty')
                        },
                        numeric: {
                            message: Lang.get('plan.js.validation.initialAmount.numeric')
                        }
                    }
                },
                usd_recurring_amount: {
                    validators: {
                        notEmpty: {
                            message: Lang.get('plan.js.validation.recurringAmount.notEmpty')
                        },
                        numeric: {
                            message: Lang.get('plan.js.validation.recurringAmount.numeric')
                        }
                    }
                }

            },
            err: {
                clazz: 'text-help'
            },
            row: {
                invalid: 'has-danger'
            }
        });
    })();


    // Validation for user edit from
    // ---------------------------------
    (function () {
        $('#orderadminform').formValidation({
            framework: "bootstrap4",
            button: {
                selector: '#sub',
                disabled: 'disabled'
            },
            icon: null,
            fields: {

                customer_id: {
                    validators: {
                        notEmpty: {
                            message: Lang.get('order.js.validation.id.notEmpty')
                        },
                        greaterThan: {
                            value: 1,
                            message: Lang.get('order.js.validation.id.greater')
                        }
                    }
                },
                products: {
                    validators: {
                        notEmpty: {
                            message: Lang.get('order.js.validation.products')
                        }
                    }
                },
                agent_id: {
                    validators: {
                        notEmpty: {
                            message: Lang.get('order.js.validation.agent.notEmpty')
                        },
                        greaterThan: {
                            value: 1,
                            message: Lang.get('order.js.validation.agent.greater')
                        }
                    }
                },
                payment_type: {
                    validators: {
                        notEmpty: {
                            message: Lang.get('order.js.validation.paymentType')
                        }
                    }
                },
            },
            err: {
                clazz: 'text-help'
            },
            row: {
                invalid: 'has-danger'
            }
        });
    })();

    // ---------------------------------
    (function () {
        $('#orderform').formValidation({
            framework: "bootstrap4",
            button: {
                selector: '#sub',
                disabled: 'disabled'
            },
            icon: null,
            fields: {

                customer_id: {
                    validators: {
                        notEmpty: {
                            message: Lang.get('order.js.validation.id.notEmpty')
                        },
                        greaterThan: {
                            value: 1,
                            message: Lang.get('order.js.validation.id.greater')
                        }
                    }
                },
                products: {
                    validators: {
                        notEmpty: {
                            message: Lang.get('order.js.validation.products')
                        }
                    }
                }

            },
            err: {
                clazz: 'text-help'
            },
            row: {
                invalid: 'has-danger'
            }
        });
    })();


    // Validation for user edit from
    // ---------------------------------
    (function () {
        $('#productform').formValidation({
            framework: "bootstrap4",
            button: {
                selector: '#sub',
                disabled: 'disabled'
            },
            icon: null,
            fields: {

                name: {
                    validators: {
                        notEmpty: {
                            message: Lang.get('product.js.validation.name')
                        }
                    }
                },
                plan_id: {
                    validators: {
                        // notEmpty: {
                        //   message: 'The plan is required and cannot be empty'
                        // },
                        greaterThan: {
                            value: 1,
                            message: Lang.get('product.js.validation.plan')
                        }
                    }
                },
                usd: {
                    validators: {
                        notEmpty: {
                            message: Lang.get('product.js.validation.price.notEmpty')
                        },
                        numeric: {
                            message: Lang.get('product.js.validation.price.numeric')
                        }
                    }
                },
                eur: {
                    validators: {
                        notEmpty: {
                            message: Lang.get('product.js.validation.price.notEmpty')
                        },
                        numeric: {
                            message: Lang.get('product.js.validation.price.numeric')
                        }
                    }
                }

            },
            err: {
                clazz: 'text-help'
            },
            row: {
                invalid: 'has-danger'
            }
        });
    })();


    // Validation for user edit from
    // ---------------------------------
    (function () {
        $('#customerform').formValidation({
            framework: "bootstrap4",
            button: {
                selector: '#sub, #subs',
                disabled: 'disabled'
            },
            icon: null,
            fields: {


                first_name: {
                    validators: {
                        notEmpty: {
                            message: Lang.get('customers.js.validation.firstName')
                        }
                    }
                },
                last_name: {
                    validators: {
                        notEmpty: {
                            message: Lang.get('customers.js.validation.lastName')
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                            message: Lang.get('customers.js.validation.email.notEmpty')
                        },
                        emailAddress: {
                            message: Lang.get('customers.js.validation.email.valid')
                        }
                    }
                },
                phone_no: {
                    validators: {
                        notEmpty: {
                            message: Lang.get('customers.js.validation.phoneNo.notEmpty')
                        }
                    }
                }


            },
            err: {
                clazz: 'text-help'
            },
            row: {
                invalid: 'has-danger'
            }
        });
    })();


    // Validation for user edit from
    // ---------------------------------
    (function () {
        $('#callcenterform').formValidation({
            framework: "bootstrap4",
            button: {
                selector: '#sub',
                disabled: 'disabled'
            },
            icon: null,
            fields: {

                name: {
                    validators: {
                        notEmpty: {
                            message: Lang.get('callcenter.js.validation.name')
                        }
                    }
                }
            },
            err: {
                clazz: 'text-help'
            },
            row: {
                invalid: 'has-danger'
            }
        });
    })();


    // Validation for user edit from
    // ---------------------------------
    (function () {
        $('#userformedit').formValidation({
            framework: "bootstrap4",
            button: {
                selector: '#sub',
                disabled: 'disabled'
            },
            icon: null,
            fields: {

                name: {
                    validators: {
                        notEmpty: {
                            message: Lang.get('user.js.validation.name')
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                            message: Lang.get('user.js.validation.email.notEmpty')
                        },
                        emailAddress: {
                            message: Lang.get('user.js.validation.email.emailAddress')
                        }
                    }
                },
                password: {
                    validators: {
                        stringLength: {
                            min: 8
                        },
                        identical: {
                            field: 'password_confirmation',
                            message: Lang.get('user.js.validation.password.identical')
                        }
                    }
                },
                password_confirmation: {
                    validators: {
                        stringLength: {
                            min: 8
                        },
                        identical: {
                            field: 'password',
                            message: Lang.get('user.js.validation.rePassword.identical')
                        }
                    }
                },

                coupon_password: {
                    validators: {
                        stringLength: {
                            min: 8
                        },
                        identical: {
                            field: 'coupon_password_confirmation',
                            message: Lang.get('user.js.validation.password.identical')
                        }
                    }
                },
                coupon_password_confirmation: {
                    validators: {
                        stringLength: {
                            min: 8
                        },
                        identical: {
                            field: 'coupon_password',
                            message: Lang.get('user.js.validation.rePassword.identical')
                        }
                    }
                },


            },
            err: {
                clazz: 'text-help'
            },
            row: {
                invalid: 'has-danger'
            }
        });
    })();


    // Validation for user Create form
    // ---------------------------------
    (function () {
        $('#userformcreate').formValidation({
            framework: "bootstrap4",
            button: {
                selector: '#sub',
                disabled: 'disabled'
            },
            icon: null,
            fields: {

                name: {
                    validators: {
                        notEmpty: {
                            message: Lang.get('user.js.validation.name')
                        }
                    }
                },
                username: {
                    validators: {
                        notEmpty: {
                            message: Lang.get('user.js.validation.username')
                        },
                        stringLength: {
                            min: 8
                        },
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                            message: Lang.get('user.js.validation.email.notEmpty')
                        },
                        emailAddress: {
                            message: Lang.get('user.js.validation.email.emailAddress')
                        }
                    }
                },
                password: {
                    validators: {
                        notEmpty: {
                            message: Lang.get('user.js.validation.password.notEmpty')
                        },
                        stringLength: {
                            min: 8
                        },
                        identical: {
                            field: 'password_confirmation',
                            message: Lang.get('user.js.validation.password.identical')
                        }
                    }
                },
                password_confirmation: {
                    validators: {
                        notEmpty: {
                            message: Lang.get('user.js.validation.rePassword.notEmpty')
                        },
                        stringLength: {
                            min: 8
                        },
                        identical: {
                            field: 'password',
                            message: Lang.get('user.js.validation.rePassword.identical')
                        }
                    }
                },


            },
            err: {
                clazz: 'text-help'
            },
            row: {
                invalid: 'has-danger'
            }
        });
    })();


    // Validation for user Create form
    // ---------------------------------
    (function () {
        $('#roleform').formValidation({
            framework: "bootstrap4",
            button: {
                selector: '#subButton',
                disabled: 'disabled'
            },
            icon: null,
            fields: {

                label: {
                    validators: {
                        notEmpty: {
                            message: Lang.get('role.js.validation.label')
                        }
                    }
                },
            },
            err: {
                clazz: 'text-help'
            },
            row: {
                invalid: 'has-danger'
            }
        });
    })();


    // ---------------------------------
    (function () {
        $('#reactiveform').formValidation({
            framework: "bootstrap4",
            button: {
                selector: '#subButton',
                disabled: 'disabled'
            },
            icon: null,
            err: {
                clazz: 'text-help'
            },
            row: {
                invalid: 'has-danger'
            }
        }).on('success.form.fv', function (e) {
            // Prevent form submission
            e.preventDefault();

            // Some instances you can use are
            var $form = $(e.target),        // The form instance
                fv = $(e.target).data('formValidation'); // FormValidation instance

            var option = $('input[name="reactive"]:checked').val();
            $.ajax({
                method: "GET",
                url: "/customers/plan/" + $('#pid').val() + "/reactivate",
                type: 'json',
                data: {
                    option: option,
                    reason: $('#reason').val(),
                    comment: $('#comments').val(),
                },
                success: function ($result) {

                    toastr.options = {
                        "debug": false,
                        "positionClass": "toast-top-full-width",
                    };

                    if ($result.code == '200') {
                        if ($result.option == 'restart')
                            toastr.success(Lang.get('customers.js.ajax.restart'));

                        if ($result.option == 'charge')
                            toastr.success(Lang.get('customers.js.ajax.charge'));
                    }

                    //$('#customerPlan').modal('hide');
                    window.location.href = document.URL;
                }
            });
        });
    })();

// Removing validation
    $('#customerPlan').on('hide.bs.modal', function () {
        $('#reason').prop('selectedIndex', 0);
        $('#comments').val('');
        $('#reactiveform').data('formValidation').resetForm();
    })

})(document, window, jQuery);
