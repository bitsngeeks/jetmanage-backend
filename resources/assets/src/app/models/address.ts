export class Address {
  address_1: String;
  address_2: String;
  city: String;
  state: String;
  zip: String;
  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}

