export class Individual {
  id: number;
  first_name = '';
  middle_name = '';
  last_name = '';
  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}
