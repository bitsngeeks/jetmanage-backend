export class Trip {
  id: number;
  aircraft_id: number;
  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}
