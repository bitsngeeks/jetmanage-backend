import { Component, OnInit } from '@angular/core';
import { HttpService } from './../../services/http.service';
import { Router } from '@angular/router';

declare var script: any;
declare var $: any;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  errors : any[];

  constructor(private httpService:HttpService, private router : Router) {}

  ngOnInit() {
    script();
  }

  getLogin(email:string, password:string): void {
    this.httpService.getLogin(email, password)
    .then((result) => {
      result = result.json();
      if(result.success == false){
        var errors_in = JSON.parse(result.error);
        var errores = [];
        for (var key in errors_in) {
          errores.push(errors_in[key][0]);
        }
        this.errors = errores;
      }else{
        localStorage.setItem('token', result.token);
        this.router.navigateByUrl('/admin');
      }
    })
    .catch((err) => {
      this.errors = [];
      this.errors.push('The information you entered is invalid, please try again.');
    });
  }

}
