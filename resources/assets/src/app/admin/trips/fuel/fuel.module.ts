import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FuelRoutingModule } from './fuel-routing.module';
import { FuelComponent } from './fuel.component';

@NgModule({
    imports: [
        CommonModule,
        FuelRoutingModule
    ],
    declarations: [
        FuelComponent
    ]
})
export class FuelModule {}
