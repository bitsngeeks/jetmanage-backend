import { Component } from '@angular/core';

declare var $: any;

@Component({
  selector: 'app-roots',
  templateUrl: './trips.component.html',
  styleUrls: ['./trips.component.scss'],

})
export class TripsComponent {
  title = 'app';
  public getRouterOutletState(outlet) {
    return outlet.isActivated ? outlet.activatedRoute : '';
  }
}
 