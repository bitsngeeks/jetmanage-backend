import { TestBed, async } from '@angular/core/testing';
import { TripsComponent } from './trips.component';
describe('TripsComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        TripsComponent
      ],
    }).compileComponents();
  }));
  it('should create the trips', async(() => {
    const fixture = TestBed.createComponent(TripsComponent);
    const trips = fixture.debugElement.componentInstance;
    expect(trips).toBeTruthy();
  }));
  it(`should have as title 'trips'`, async(() => {
    const fixture = TestBed.createComponent(TripsComponent);
    const trips = fixture.debugElement.componentInstance;
    expect(trips.title).toEqual('trips');
  }));
  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(TripsComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Welcome to trips!');
  }));
});
