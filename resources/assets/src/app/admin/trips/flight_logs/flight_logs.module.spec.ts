import { FlightLogsModule } from './flight_logs.module';

describe('FlightLogsModule', () => {
  let flight_logsModule: FlightLogsModule;

  beforeEach(() => {
    flight_logsModule = new FlightLogsModule();
  });

  it('should create an instance', () => {
    expect(flight_logsModule).toBeTruthy();
  });
});
