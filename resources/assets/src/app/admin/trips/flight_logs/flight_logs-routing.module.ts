import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FlightLogsComponent } from './flight_logs.component';

const routes: Routes = [
    {
        path: '', component: FlightLogsComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FlightLogsRoutingModule {
}
