import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FlightLogsRoutingModule } from './flight_logs-routing.module';
import { FlightLogsComponent } from './flight_logs.component';

@NgModule({
    imports: [
        CommonModule,
        FlightLogsRoutingModule
    ],
    declarations: [
        FlightLogsComponent
    ]
})
export class FlightLogsModule {}
