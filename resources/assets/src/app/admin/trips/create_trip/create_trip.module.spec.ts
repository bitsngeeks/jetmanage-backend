import { CreateTripModule } from './create_trip.module';

describe('CreateTripModule', () => {
  let create_tripModule: CreateTripModule;

  beforeEach(() => {
    create_tripModule = new CreateTripModule();
  });

  it('should create an instance', () => {
    expect(create_tripModule).toBeTruthy();
  });
});
