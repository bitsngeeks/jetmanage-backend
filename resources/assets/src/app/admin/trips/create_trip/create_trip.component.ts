import { Component, OnInit,ViewEncapsulation} from '@angular/core';
import { TripsService } from './../../../services/trips.service';
import { AircraftService } from './../../../services/aircraft.service';
import { AirportService } from './../../../services/airport.service';
import { Router } from '@angular/router';
import { Trip } from '../../../models/trip';
import {FormControl} from '@angular/forms';



declare var select,google: any;
declare var script: any;
declare var $: any;

@Component({
    selector: 'app-create_trip',
    templateUrl: './create_trip.component.html',
	styleUrls: ['./create_trip.component.scss']
})

export class CreateTripComponent {
	public itemsOut: any[] = [];
	public items: any = []; 
	public aircrafts: any = []; 
    public ngxValue: any[] = [];
    public ngxValueAircraft: any = [];
	public forArray:any =["0"];
	private trip: Object = {
    	aircraft_id: String
  	};
	private aircraftsList = [];
	private airportsList = [];
	private airportData=[];
	private placeholder:string[]=["No airport selected"];



	constructor(private aircraftService:AircraftService, private airportService:AirportService, private tripsService:TripsService, private router:Router) {}

	ngOnInit(){
		script();
		this.getAircrafts();
	}
/* For dinamic legs form Start*/
	addNewRow() {
		if(this.ngxValue[2*this.forArray.length-1]){
			this.placeholder[this.forArray.length]=this.ngxValue[2*this.forArray.length-1].split("/")[3];
		} else {
			this.placeholder[this.forArray.length]="No airports available"
		}
		this.ngxValue[2*this.forArray.length]=this.ngxValue[2*this.forArray.length-1];
        this.forArray.push("0");
		script();
    }
    deleteRow(index: number) {
        this.forArray.pop();
		script();
	}
	clearArray(index){
		this.ngxValue[index]=0;
	}
/* For dinamic legs form End */
	getAircrafts() :void{
		this.aircraftService.getAllAircrafts().subscribe(
	        (rows) => {
	        	for(var i in rows.json().data){
					this.aircraftsList.push(rows.json().data[i]);

				}
				this.aircraftsList.forEach((aircraft:
					{
						name:string, 
						id:number, 
						serial_number:number,
					}) => {
					this.aircrafts.push({
					  id: aircraft.id, 
					  text: aircraft.name + ' - ' + aircraft.serial_number ,
					});
				  });
	        },
            e  => console.log(e),
            () => setTimeout(()=>{ 
			})
        );
	}
	updateAirports(event,index){
		if(event.length>=3){
			this.airportService.searchAirports(event).subscribe(
				(rows) => {
							this.items=[];
							this.airportsList=rows.json();
							this.airportsList.forEach((airport:
								{
									formattedName:string, 
									id:number,
									latitude:number,
									longitude:number,
								}) => {
								this.items.push({
								  id: airport.id+"/"+airport.latitude+"/"+airport.longitude+"/"+airport.formattedName, 
								  text: airport.formattedName,
								  options: [{latitude: airport.latitude,longitude: airport.longitude}]
								});
							  });
							  this.itemsOut[index]=this.items;
				},
				e  => console.log(e),
				() => setTimeout(()=>{ 
				})
			);
		} else {
				this.airportsList=[];		
		}		
	}

	getMapModal(){
		if(this.forArray.length==(this.ngxValue.length/2) && 
		!(this.ngxValue.includes(0)) && 
		Object.keys(this.ngxValue).length==this.ngxValue.length){
			var airport = {lat:0, lng: 0};
		var latlngbounds = new google.maps.LatLngBounds();
		var centerLat=0;
		var centerLng=0;
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 2,
          center: airport
        });   	
		for (var i in this.ngxValue) {
			airport = {lat: parseInt(this.ngxValue[i].split("/")[1]), lng: parseInt(this.ngxValue[i].split("/")[2])};			
			var marker = new google.maps.Marker({
				position: airport,
				map: map,
				icon: "../assets/images/plane.png",
			  });	
			  centerLat+=parseInt(this.ngxValue[i].split("/")[1]);
			  centerLng+=parseInt(this.ngxValue[i].split("/")[2]);
			if((parseInt(i)%2)==0){				
			}else{
				var flightPath = new google.maps.Polyline({
					path:[
						{lat: parseInt(this.ngxValue[parseInt(i)-1].split("/")[1]), lng: parseInt(this.ngxValue[parseInt(i)-1].split("/")[2])},
						{lat: parseInt(this.ngxValue[i].split("/")[1]), lng: parseInt(this.ngxValue[i].split("/")[2])}				
						],
					geodesic: true,
					strokeColor: '#47c9af',
					strokeOpacity: 1.0,
					strokeWeight: 2
				  });		  
				  flightPath.setMap(map);
			}
		}
		centerLat=centerLat/(parseInt(i)+1);
		centerLng=centerLng/(parseInt(i)+1);
		map.setCenter({lat: centerLat, lng: centerLng})
		$('#airportDetail').modal({}).on('shown.bs.modal', function() {
			google.maps.event.trigger(map, 'resize');
		});
		} else {
			alert("Please fill all the airports")
		}
	}

	createTrip(value,value2){
		var departDate=[];
		$('.departDate').each(function(index){
			departDate.push($( this ).val());
		})
		var arrivalDate=[];
		$('.arrivalDate').each(function(index){
			arrivalDate.push($( this ).val());
		})
		if(
			value && 
			this.forArray.length==(value2.length/2) && 
			!(value2.includes(0)) && 
			Object.keys(value2).length==value2.length &&
			!(arrivalDate.includes("")) && 
			!(departDate.includes(""))
		){
			this.tripsService.createTrip({aircraft_id:value})
			.then((result) => {
			this.tripsService.createLegs(result.json().data.id,value,value2,departDate,arrivalDate)
			.then((result) => {
				alert("Trip Created")
				this.router.navigateByUrl('/admin/trips/scheduled_trips');
							
			  })
						
		  })
		} else{
			alert("please fill all required fields")
		}
	}
	


}