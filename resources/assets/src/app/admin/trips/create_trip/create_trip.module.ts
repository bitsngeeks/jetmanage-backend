import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateTripRoutingModule } from './create_trip-routing.module';
import { CreateTripComponent } from './create_trip.component';
import { SelectControlValueAccessor } from '@angular/forms';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { forwardRef } from '@angular/core';
import { NgxSelectModule } from 'ngx-select-ex';


@NgModule({
    imports: [
        CommonModule,
        CreateTripRoutingModule,
        FormsModule,
        NgxSelectModule,
        ReactiveFormsModule
    ],
    declarations: [
        CreateTripComponent
    ],
    providers: [
        { 
          provide: NG_VALUE_ACCESSOR,
          multi: true,
          useExisting: forwardRef(() => CreateTripComponent),
        }
      ]
})
export class CreateTripModule {}
