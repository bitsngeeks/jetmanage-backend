import { ScheduledTripsModule } from './scheduled_trips.module';

describe('ScheduledTripsModule', () => {
  let scheduled_tripsModule: ScheduledTripsModule;

  beforeEach(() => {
    scheduled_tripsModule = new ScheduledTripsModule();
  });

  it('should create an instance', () => {
    expect(scheduled_tripsModule).toBeTruthy();
  });
});
