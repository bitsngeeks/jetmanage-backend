import { NgxSelectModule } from 'ngx-select-ex';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ScheduledTripsRoutingModule } from './scheduled_trips-routing.module';
import { ScheduledTripsComponent } from './scheduled_trips.component';

@NgModule({
    imports: [
        CommonModule,
        ScheduledTripsRoutingModule,
        FormsModule,
        NgxSelectModule
    ],
    declarations: [
        ScheduledTripsComponent
    ]
})
export class ScheduledTripsModule {}
 