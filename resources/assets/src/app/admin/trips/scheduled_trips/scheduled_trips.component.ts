import { Component, OnInit } from '@angular/core';
import { TripsService } from './../../../services/trips.service';
import { Router } from '@angular/router';

declare var select: any;
declare var script: any;
declare var map: any;
declare var google: any;
declare var $: any;
declare var moment:any;

@Component({
    selector: 'app-scheduled_trips',
    templateUrl: './scheduled_trips.component.html',
    styleUrls: ['./scheduled_trips.component.scss']
})

export class ScheduledTripsComponent {
    currentTrips   = [];
    pastTrips      = [];
    airportDetail  = [];
    aircraftDetail = [];

    public trips: any = []; 
    public tripId: string; 

    ngOnInit() {
       
        
        script();
        this.getTrips();

    }

    constructor(private tripsService: TripsService) {} 
    Changetrips(type){
        if(type==0){
            var tripLegs="";
            var dateAux=true;
            this.trips=[]
            this.currentTrips.forEach((trip:
                {
                    id:number, 
                    legs:Object,
                }) => {
                    
                   
                    if(tripLegs==""&&dateAux){
                        dateAux=false;
                        tripLegs=trip.legs[0].depart_date+" ";
                    }
                
                for (var i in trip.legs){
                    tripLegs=tripLegs+trip.legs[i].from_airport[0].code.toUpperCase() + '-'+trip.legs[i].to_airport[0].code.toUpperCase() +" "
                }
                this.trips.push({
                    id: trip.id, 
                    text: '('+trip.id +')'+ tripLegs
                });
                tripLegs="";
                dateAux=true;
               
                });
        }else{
            var tripLegs="";
            var dateAux=true;
            this.trips=[]
            this.pastTrips.forEach((trip:
                {
                    id:number, 
                    legs:Object,
                }) => {
                    
               
                    if(tripLegs==""&&dateAux){
                        dateAux=false;
                        tripLegs=trip.legs[0].depart_date+" ";
                    }
                   
                for (var i in trip.legs){
                    tripLegs=tripLegs+trip.legs[i].from_airport[0].code.toUpperCase() + '-'+trip.legs[i].to_airport[0].code.toUpperCase() +" "
                }
                this.trips.push({
                    id: trip.id, 
                    text: '('+trip.id +')'+ tripLegs
                });
                tripLegs="";
                dateAux=true;
               
                });
        }
    }
    getTrips() {
        this.tripsService.getAllTrips().subscribe(
            (rows) => {
				var now = moment();
                for (var i in rows.json().data) {
                    if(rows.json().data[i].legs[rows.json().data[i].legs.length - 1]){
                        var date = moment(rows.json().data[i].legs[rows.json().data[i].legs.length - 1].depart_date);
                        if(date < now){
                            this.pastTrips.push(rows.json().data[i]);
                        }else{
                            this.currentTrips.push(rows.json().data[i]);
                            var tripLegs="";
                            var dateAux=true;
                            this.trips=[]
                            this.currentTrips.forEach((trip:
                                {
                                    id:number, 
                                    legs:Object,
                                }) => {
                                   
                                   
                                    if(tripLegs==""&&dateAux){
                                        dateAux=false;
                                        tripLegs=trip.legs[0].depart_date+" ";
                                    }
                            
                                for (var i in trip.legs){
                                    tripLegs=tripLegs+trip.legs[i].from_airport[0].code.toUpperCase() + '-'+trip.legs[i].to_airport[0].code.toUpperCase() +" "
                                }
                                this.trips.push({
                                  id: trip.id, 
                                  text: '('+trip.id +')'+ tripLegs
                                });
                                tripLegs="";
                                dateAux=true;

                              });
                             
                        }
                        
                    }
                	
                }
            },
            e => console.log(e),
            () => setTimeout(() => {
                $('[data-toggle="tooltip"]').tooltip()
            })
        );
    }

    getAirportDetailModal(idTrip, idAirport, type, arrayType) {
        var airports = this.getAirports(idTrip, idAirport, type, arrayType);
        this.getAirportSelected(idTrip, idAirport, type, arrayType);
        this.initMapModal("map_airport", airports, true, arrayType);
    }

    getAirports(idTrip, idAirport, type, arrayType) {
        var trip = { legs : [] };
        var trips = [];
        if(arrayType == 1){ //CURRENT TRIPS
    		trips = this.currentTrips;
    	}else{//PAST TRIPS
			trips = this.pastTrips;
    	}
        for(var i in trips){
        	if(trips[i].id == idTrip){
        		trip = trips[i];
        	}
        }
        var airports = [];
        for (var i in trip.legs) {
            if (trip.legs[i].from_airport[0].airport_entry_id == idAirport && type == "0") {
                airports.push([trip.legs[i].from_airport[0].latitude, trip.legs[i].from_airport[0].longitude, idTrip, trip.legs[i].from_airport[0].airport_entry_id, 0, 1]);
            } else {
                airports.push([trip.legs[i].from_airport[0].latitude, trip.legs[i].from_airport[0].longitude, idTrip, trip.legs[i].from_airport[0].airport_entry_id, 0, 0]);
            }

            if (trip.legs[i].to_airport[0].airport_entry_id == idAirport && type == "1") {
                airports.push([trip.legs[i].to_airport[0].latitude, trip.legs[i].to_airport[0].longitude, idTrip, trip.legs[i].to_airport[0].airport_entry_id, 1, 1]);
            } else {
                airports.push([trip.legs[i].to_airport[0].latitude, trip.legs[i].to_airport[0].longitude, idTrip, trip.legs[i].to_airport[0].airport_entry_id, 1, 0]);
            }
        }
        return airports;
    }

    getAirportSelected(idTrip, idAirport, type, arrayType){
    	var trip  = { legs : [] };
    	var trips = [];
        if(arrayType == 1){ //CURRENT TRIPS
    		trips = this.currentTrips;
    	}else{//PAST TRIPS
			trips = this.pastTrips;
    	}
        for(var i in trips){
        	if(trips[i].id == idTrip){
        		trip = trips[i];
        	}
        }
        var legs = trip.legs;
        for(var i in legs){
        	if (type == "0") { //FROM_AIRPORT
            	if(legs[i].from_airport_id == idAirport){
            		this.airportDetail = legs[i].from_airport[0];
            	}
	        }else{ //TO_AIRPORT
	        	if(legs[i].to_airport_id == idAirport){
	        		this.airportDetail = legs[i].to_airport[0];
	        	}
	        }
        }
    }

    initMapModal(id, airports, modal, arrayType) {
        var latlngbounds = new google.maps.LatLngBounds();
        var infowindow = new google.maps.InfoWindow();
        var map = new google.maps.Map(document.getElementById(id), { zoom: 10 });

        for (var i in airports) {
            var icon = "../assets/images/no_plane.png";
            if (airports[i][5] == 1) {
                var center;
                if(modal){
                    center={lat: airports[i][0]+0.1, lng: airports[i][1]-0.18}
                } else {
                    center={lat: airports[i][0], lng: airports[i][1]}
                }
                
                var icon = "../assets/images/plane.png";
            }

            var marker = new google.maps.Marker({
                position: {
                    lat: airports[i][0],
                    lng: airports[i][1]
                },
                map: map,
                icon: icon,
            });

            latlngbounds.extend(marker.position);

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                	var contentString = `<p class="mb-0"> <strong class="clicktext" idtrip="` + airports[i][2] + `" idAirport="` + airports[i][3] + `" type="` + airports[i][4] + `" arraytype = "` +arrayType+ `" id="check_details_airport" >Check details</strong></p>`;
                    infowindow.setContent(contentString);
                    infowindow.open(map, marker);
                }
            })(marker, i));
        }

        google.maps.event.addListener(infowindow, 'domready', () => {
            var clickableItem = document.getElementById('check_details_airport');
            clickableItem.addEventListener('click', () => {
                this.clickAirportDetail(clickableItem.getAttribute("idtrip"), clickableItem.getAttribute("idAirport"), clickableItem.getAttribute("type"), clickableItem.getAttribute("arraytype"));
            });
        });

       
        map.setCenter(center);
         /* map.setCenter(latlngbounds.getCenter()); */
        /* map.fitBounds(latlngbounds); */
        if (modal) {
            $('#airportDetail').modal({}).on('shown.bs.modal', function() {
                google.maps.event.trigger(map, 'resize');
            });
        }
        google.maps.event.trigger(map, 'resize');
    }

    clickAirportDetail(idTrip, idAirport, type, arraytype) {
        var airports = this.getAirports(idTrip, idAirport, type, arraytype);
        this.initMapModal("map_airport", airports, false, arraytype);
        this.getAirportSelected(idTrip, idAirport, type, arraytype);
    }

    getAircraftDetailModal(idTrip, idAircraft, arrayType){
    	var trip = { aircraft : [] };
    	var trips = [];
    	if(arrayType == 1){ //CURRENT TRIPS
    		trips = this.currentTrips;
    	}else{//PAST TRIPS
			trips = this.pastTrips;
    	}
        for(var i in trips){
        	if(trips[i].id == idTrip){
        		trip = trips[i];
        	}
        }
        this.aircraftDetail = trip.aircraft;
        $('#aircraftDetail').modal();
    }
    
}