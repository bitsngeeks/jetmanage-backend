import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ScheduledTripsComponent } from './scheduled_trips.component';

const routes: Routes = [
    {
        path: '', component: ScheduledTripsComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ScheduledTripsRoutingModule {
}
