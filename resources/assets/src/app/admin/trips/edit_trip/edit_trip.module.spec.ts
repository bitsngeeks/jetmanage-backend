import { EditTripModule } from './edit_trip.module';

describe('EditTripModule', () => {
  let edit_tripModule: EditTripModule;

  beforeEach(() => {
    edit_tripModule = new EditTripModule();
  });

  it('should create an instance', () => {
    expect(edit_tripModule).toBeTruthy();
  });
});
