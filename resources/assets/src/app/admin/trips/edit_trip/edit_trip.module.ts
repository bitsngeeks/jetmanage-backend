import { FormsModule } from '@angular/forms';
import { NgxSelectModule } from 'ngx-select-ex';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';

import { EditTripRoutingModule } from './edit_trip-routing.module';
import { EditTripComponent } from './edit_trip.component';

@NgModule({
    imports: [
        CommonModule,
        EditTripRoutingModule,
        HttpModule,
        NgxSelectModule,
        FormsModule
    ],
    declarations: [
        EditTripComponent
    ]
})
export class EditTripModule {}
