import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditTripComponent } from './edit_trip.component';

const routes: Routes = [
    {
        path: '', component: EditTripComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class EditTripRoutingModule {
}
