import { IndividualsService } from './../../../services/individuals.service';
import { AircraftService } from './../../../services/aircraft.service';
import { TripsService } from './../../../services/trips.service';
import {
    Component
} from '@angular/core';
import  {Http, Response } from '@angular/http';
import  {ActivatedRoute} from '@angular/router'
import 'rxjs/add/operator/map';
declare var select: any;
declare var script: any;
declare var $: any;
@Component({
    selector: 'app-edit_trip',
    templateUrl: './edit_trip.component.html',
    styleUrls: ['./edit_trip.component.scss']
})
export class EditTripComponent {
    public id;
    public customerData:any=[];
    public passengerData:any=[];
    public legsAux=[];
    public aircrafts: any = []; 
    public aircraft: any = [];
    public aircraftName: any = [];
    public legs;
    public individuals: any = [];
    public individualId;
    public passenger: any = [];
    public passengerId;
    public crew: any = [];
    public passengers: any = [];
    public tripData:any={"aircraft":{0:""}};
    public aircraftBool=false;
    private aircraftsList = [];
    private legCheckbox: any[]=[];
    constructor(private tripsService:TripsService, private route:ActivatedRoute,private aircraftService:AircraftService,private individualsService:IndividualsService) {}

    getAircrafts() :void{
		this.aircraftService.getAllAircrafts().subscribe(
	        (rows) => {
	        	for(var i in rows.json().data){
					this.aircraftsList.push(rows.json().data[i]);

				}
				this.aircraftsList.forEach((aircraft:
					{
						name:string, 
						id:number, 
						serial_number:number,
					}) => {
					this.aircrafts.push({
					  id: aircraft.id+'-'+aircraft.name, 
					  text: aircraft.name + ' - ' + aircraft.serial_number ,
					});
				  });
	        },
            e  => console.log(e),
            () => setTimeout(()=>{ 
			})
        );
    }
    getIndividuals(){
        this.individualsService.getAllIndividuals()
        .subscribe(
            (rows) => {
                this.customerData=[];
                for (var i in rows.json().data){
                    for(var j in rows.json().data[i].tags){
                        if(rows.json().data[i].tags[j].id==1){
                            this.customerData.push(rows.json().data[i]);                            
                        }
                        if(rows.json().data[i].tags[j].id==4){
                            this.passengerData.push(rows.json().data[i]);                            
                        }

                    }
                    
                }
                this.customerData.forEach((individual:
					{
						id:number, 
						first_name:string,
						middle_name:string,
						last_name:string,
					}) => {
					this.individuals.push({
					  id: individual.id, 
					  text: individual.first_name +" "+ individual.middle_name +" " +individual.last_name ,
					});
				  });
                this.passengerData.forEach((individual:
					{
						id:number, 
						first_name:string,
						middle_name:string,
						last_name:string,
					}) => {
					this.passenger.push({
					  id: individual.id, 
					  text: individual.first_name +" "+ individual.middle_name +" " +individual.last_name ,
					});
				  });
            },
            e  => console.log(e),
            () => setTimeout(()=>{ 
                $('[data-toggle="tooltip"]').tooltip();
            })
        );
        }

    updateAircraft(){
        this.aircraftBool=!this.aircraftBool
    }
    updateAircraftInfo(id){
        this.tripsService.updateAircraft(id,this.aircraft.split("-")[0])
        this.aircraftName=this.aircraft.split("-")[1]

    }
    addCrew(event){
        this.individualsService.getIndividual(event)
        .subscribe(
            (rows) => {
                var notDuplicate=true;
                this.crew.forEach((crew:
					{
						name:number, 
					}) => {
					if(crew.name == rows.json().data.first_name){
                        notDuplicate=false;
                    }
                  });
                  
                if(notDuplicate){
                    this.crew.push({
                        name:rows.json().data.first_name,
                        last_name:rows.json().data.last_name,
                        dob:rows.json().data.dob                
                    })
                }
                
            },
            e  => console.log(e),
            () => setTimeout(()=>{ 
                $('[data-toggle="tooltip"]').tooltip();
            })
        );
       
    }
    addPassenger(event){
        this.individualsService.getIndividual(event)
        .subscribe(
            (rows) => {
                var notDuplicate=true;
                this.passengers.forEach((passengers:
					{
						name:number, 
					},index) => {
                        
					if(passengers.name == rows.json().data.first_name){
                
                        notDuplicate=false;
                    }
                  });
                  
                if(notDuplicate){
                    this.passengers.push({
                        name:rows.json().data.first_name,
                        last_name:rows.json().data.last_name,
                        dob:rows.json().data.dob                
                    })
                }
                
            },
            e  => console.log(e),
            () => setTimeout(()=>{ 
                $('[data-toggle="tooltip"]').tooltip();
            })
        );
       
    }
    deletePassenger(name){
        console.log("gg")
        this.passengers.forEach((passengers:
            {
                name:number, 
            },index) => {
                
            if(passengers.name == name){
                if (index > -1) {
                    this.passengers.splice(index, 1);
                }
               
            }
          });
          $('[data-toggle="tooltip"]').tooltip();
    }
    deleteCrew(name){
        console.log("gg")
        this.crew.forEach((passengers:
            {
                name:number, 
            },index) => {
                
            if(passengers.name == name){
                if (index > -1) {
                    this.crew.splice(index, 1);
                }
               
            }
          });
          $('[data-toggle="tooltip"]').tooltip();
    }

	ngOnInit(){
        /* select.selectAll(); */
        this.getAircrafts();
        this.getIndividuals();
        this.route.paramMap
            .subscribe(params=>{
                this.id=params.get('id');
            });

        this.tripsService.getTripById(this.id)
        .subscribe(
	        (rows) => {
                this.tripData=rows.json().data;
                this.aircraft=rows.json().data.aircraft[0].id+'-'+rows.json().data.aircraft[0].name
                this.aircraftName=rows.json().data.aircraft[0].name
                this.legsAux=[];
                this.legs="";
                for (var i in rows.json().data.legs ){
                    if(this.legsAux.indexOf(rows.json().data.legs[i].from_airport[0].code)==-1 ){
                        this.legsAux.push(rows.json().data.legs[i].from_airport[0].code)
                    }
                    if(this.legsAux.indexOf(rows.json().data.legs[i].to_airport[0].code)==-1 ){
                        this.legsAux.push(rows.json().data.legs[i].to_airport[0].code)
                    }
                }
                for (let i = 0; i < this.legsAux.length; i++) {
                    
                    if(i<(this.legsAux.length-1)){
                        this.legs=this.legs+this.legsAux[i]+",";
                    }else{
                        this.legs = this.legs+ this.legsAux[i];
                    }
                    
                }
	        },
            e  => console.log(e),
            () => setTimeout(()=>{ 
			})
        );
		script();
    }
    

}
