
import { NgModule } from '@angular/core';

import { TripsComponent } from './trips.component';

import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
    {
        path: '',
        component: TripsComponent,
        children: [
            { path: '', redirectTo: 'create_trip' },
            { path: 'create_trip', loadChildren: './create_trip/create_trip.module#CreateTripModule' },
            { path: 'edit_trip/:id', loadChildren: './edit_trip/edit_trip.module#EditTripModule' },
            { path: 'fuel', loadChildren: './fuel/fuel.module#FuelModule' },
            { path: 'scheduled_trips', loadChildren: './scheduled_trips/scheduled_trips.module#ScheduledTripsModule' },
            { path: 'flight_logs', loadChildren: './flight_logs/flight_logs.module#FlightLogsModule' },
        ]
    }
];

@NgModule({
  declarations: [
    TripsComponent,

  ],
  imports: [
    RouterModule.forChild(routes)
  ],
  providers: [],
  bootstrap: [TripsComponent]
})
export class TripsModule { }
