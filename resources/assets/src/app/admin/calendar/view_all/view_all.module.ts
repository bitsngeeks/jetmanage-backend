import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';

import { ViewAllRoutingModule } from './view_all-routing.module';
import { ViewAllComponent } from './view_all.component';

@NgModule({
    imports: [
        CommonModule,
        ViewAllRoutingModule,
        HttpModule
    ],
    declarations: [
        ViewAllComponent
    ]
})
export class ViewAllModule {}
