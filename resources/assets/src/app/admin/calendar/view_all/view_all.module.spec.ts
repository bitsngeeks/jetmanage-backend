import { ViewAllModule } from './view_all.module';

describe('DashboardModule', () => {
  let view_allModule: ViewAllModule;

  beforeEach(() => {
    view_allModule = new ViewAllModule();
  });

  it('should create an instance', () => {
    expect(view_allModule).toBeTruthy();
  });
});
