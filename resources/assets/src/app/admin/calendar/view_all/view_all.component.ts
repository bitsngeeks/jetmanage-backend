import {
    Component
} from '@angular/core';
import  {Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
declare var select: any;
declare var script: any;
declare var $: any;
@Component({
    selector: 'app-view_all',
    templateUrl: './view_all.component.html',
    styleUrls: ['./view_all.component.scss']
})
export class ViewAllComponent {
	ngOnInit(){
		select.selectAll();
		script();
	}
}
