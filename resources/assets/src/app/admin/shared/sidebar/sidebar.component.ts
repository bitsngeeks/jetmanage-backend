import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})

export class SidebarComponent implements OnInit{

	ngOnInit() {
		$('.tabs-sub-menu-item a').click(function(){
			$('.tabs-sub-menu-item a').removeClass('active');
			$(this).addClass('active');
		})
		// $('.side-menu-list li.with-sub').each(function(){
		// 	var parent = $(this),
		// 		clickLink = parent.find('>span'),
		// 		subMenu = parent.find('>ul');

		// 	clickLink.click(function() {
		// 		if (parent.hasClass('opened')) {
		// 			parent.removeClass('opened');
		// 			subMenu.slideUp();
		// 			subMenu.find('.opened').removeClass('opened');
		// 		} else {
		// 			if (clickLink.parents('.with-sub').length == 1) {
		// 				$('.side-menu-list .opened').removeClass('opened').find('ul').slideUp();
		// 			}
		// 			parent.addClass('opened');
		// 			subMenu.slideDown();
		// 		}
		// 	});
		// });
	}
}
