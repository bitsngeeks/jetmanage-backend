import { NgModule } from '@angular/core';

import { AircraftComponent } from './aircraft.component';

import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
    {
        path: '',
        component: AircraftComponent,
        children: [
            { path: '', redirectTo: 'fleet' },
            { path: 'fleet_edit/:id', loadChildren: './fleet_edit/fleet_edit.module#FleetEditModule' },
/*             { path: 'fleet_edit', loadChildren: './fleet_edit/fleet_edit.module#FleetEditModule' }, */
            { path: 'fleet', loadChildren: './fleet/fleet.module#FleetModule' },
            { path: 'performance_profiles', loadChildren: './performance_profiles/performance_profiles.module#PerformanceProfilesModule' },
            { path: 'performance_profiles_edit', loadChildren: './performance_profiles_edit/performance_edit.module#PerformanceEditModule' }
        ]
    }
];

@NgModule({
  declarations: [
    AircraftComponent,

  ],
  imports: [
    RouterModule.forChild(routes)
  ],
  providers: [],
  bootstrap: [AircraftComponent]
})
export class AircraftModule { }
