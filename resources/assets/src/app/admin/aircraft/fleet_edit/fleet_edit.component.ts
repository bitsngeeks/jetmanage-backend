import {
    Component,
    OnInit
} from '@angular/core';
import  {ActivatedRoute} from '@angular/router'
import { AircraftService } from './../../../services/aircraft.service';

declare var select: any;
declare var script: any;
declare var $: any;
@Component({
    selector: 'app-fleet_edit',
    templateUrl: './fleet_edit.component.html',
    styleUrls: ['./fleet_edit.component.scss']
})
export class FleetEditComponent {
    public regNum;
    public serial_number;
    public name;
    public type=1;
    public aircraftType: any = [{id:1,text:"Pilatus PC 12"}]; 
    public id;
    public aircraftData:any=[];

    constructor(private aircraftService:AircraftService, private route:ActivatedRoute) {}

    
	ngOnInit(){
        this.route.paramMap
            .subscribe(params=>{
                this.id=params.get('id');
            });
        this.aircraftService.getAircraft(this.id)
        .subscribe(
	        (rows) => {
                console.log(rows.json().data)
                this.aircraftData=rows.json().data;
                this.regNum=rows.json().data.registration;
                /* this.type=rows.json().data.type_id; */
                this.serial_number=rows.json().data.serial_number;
                if(this.serial_number =="0"){
                    this.serial_number="";
                }
	        },
            e  => console.log(e),
            () => setTimeout(()=>{ 
			})
        );
		script();
    }

    updateAircraft(id){
        console.log(this.regNum);
        console.log(this.type);
        console.log(this.serial_number);
        if(this.regNum!="" && this.type){
            this.aircraftService.updateAircraft(id,this.regNum,this.serial_number,this.type)
            /* .subscribe(
                (rows) => {
                    console.log(rows.json().data)
                },
                e  => console.log(e),
                () => setTimeout(()=>{ 
                })
            ); */
        }else{
            alert("Please fill all required fields")
        }
       
    }
    

	}
