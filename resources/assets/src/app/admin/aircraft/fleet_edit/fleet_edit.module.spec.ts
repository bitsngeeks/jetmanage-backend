import { FleetEditModule } from './fleet_edit.module';

describe('FleetEditModule', () => {
  let fleet_editModule: FleetEditModule;

  beforeEach(() => {
    fleet_editModule = new FleetEditModule();
  });

  it('should create an instance', () => {
    expect(fleet_editModule).toBeTruthy();
  });
});
