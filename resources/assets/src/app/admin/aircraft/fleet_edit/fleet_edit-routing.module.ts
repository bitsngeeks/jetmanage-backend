import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FleetEditComponent } from './fleet_edit.component';

const routes: Routes = [
    {
        path: '', component: FleetEditComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FleetEditRoutingModule {
}
