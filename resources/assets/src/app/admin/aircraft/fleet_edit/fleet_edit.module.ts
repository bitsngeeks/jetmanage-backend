import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FleetEditRoutingModule } from './fleet_edit-routing.module';
import { FleetEditComponent } from './fleet_edit.component';
import { FormsModule } from '@angular/forms';
import { NgxSelectModule } from 'ngx-select-ex';

@NgModule({
    imports: [
        CommonModule,
        FleetEditRoutingModule,
        FormsModule,
        NgxSelectModule
    ],
    declarations: [
        FleetEditComponent
    ]
})
export class FleetEditModule {}
