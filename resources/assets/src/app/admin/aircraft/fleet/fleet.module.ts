import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FleetRoutingModule } from './fleet-routing.module';
import { FleetComponent } from './fleet.component';
import { FormsModule } from '@angular/forms';
import { NgxSelectModule } from 'ngx-select-ex';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NgxSelectModule,
        FleetRoutingModule
    ],
    declarations: [
        FleetComponent
    ]
})
export class FleetModule {}
