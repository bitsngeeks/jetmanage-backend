import {
    Component,
    OnInit
} from '@angular/core';
import {FormControl, FormGroup, FormArray, FormBuilder, Validators} from '@angular/forms';
import {Router} from '@angular/router'
import { AircraftService } from './../../../services/aircraft.service';

declare var select: any;
declare var script: any;
declare var $: any;


@Component({
    selector: 'app-fleet',
    templateUrl: './fleet.component.html',
    styleUrls: ['./fleet.component.scss']
})
export class FleetComponent {
    public regNum = ""; 
    public serial = "";
    public year = "";
    public type;
    public id;
    public aircraftsList = [];
    public aircraftType: any = [{id:1,text:"Pilatus PC 12"}]; 
    constructor(private aircraftService:AircraftService, private router:Router) {}

	ngOnInit(){
        this.getAircrafts();
        
		script();
    }
    
    test(event){
        console.log(event)
    }
    getAircrafts() :void{
		this.aircraftService.getAllAircrafts().subscribe(
	        (rows) => {
                this.aircraftsList=[];
	        	for(var i in rows.json().data){
					this.aircraftsList.push(rows.json().data[i]);

                }
	        },
            e  => console.log(e),
            () => setTimeout(()=>{ 
                $('[data-toggle="tooltip"]').tooltip()
			})
        );
    }
    createAircraft(){       
        /* $('#manufacturingYear').val() */
        if(this.regNum && this.type ){
            if(this.serial){
                this.aircraftService.CreateAircraft(this.regNum,this.type,this.serial)
                .subscribe(
                    (rows) => {
                        console.log(rows.json())
                        this.id = rows.json().data.id
                    },
                    e  => alert(e),
                    () => setTimeout(()=>{ 
                        $('#ModalAddFleet').modal('hide');
                        this.router.navigateByUrl('/admin/aircraft/fleet_edit/'+this.id);
                    })
                );
            } else{
                this.aircraftService.CreateAircraft(this.regNum,this.type)
                .subscribe(
                    (rows) => {
                        console.log(rows.json())
                        this.id = rows.json().data.id
                    },
                    e  => alert(e),
                    () => setTimeout(()=>{ 
                        $('#ModalAddFleet').modal('hide');
                        this.router.navigateByUrl('/admin/aircraft/fleet_edit/'+this.id);
                    })
                );
            }
           
            
        }else{
            alert("Please fill all required fields");
        }           
    }
    deleteAircraft(id){
        this.aircraftService.deleteAircraft(id)
        .subscribe(
            (rows) => {
                alert("aircraft deleted")
            },
            e  => alert(e),
            () => setTimeout(()=>{ 
                this.getAircrafts();
            })
        );;
        
    }
}
