import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PerformanceEditComponent } from './performance_edit.component';

const routes: Routes = [
    {
        path: '', component: PerformanceEditComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PerformanceEditRoutingModule {
}
