import { PerformanceEditModule } from './performance_edit.module';

describe('PerformanceEditModule', () => {
  let performance_editModule: PerformanceEditModule;

  beforeEach(() => {
    performance_editModule = new PerformanceEditModule();
  });

  it('should create an instance', () => {
    expect(performance_editModule).toBeTruthy();
  });
});
