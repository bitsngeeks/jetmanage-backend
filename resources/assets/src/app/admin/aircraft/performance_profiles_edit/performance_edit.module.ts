import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PerformanceEditRoutingModule } from './performance_edit-routing.module';
import { PerformanceEditComponent } from './performance_edit.component';

@NgModule({
    imports: [
        CommonModule,
        PerformanceEditRoutingModule
    ],
    declarations: [
        PerformanceEditComponent
    ]
})
export class PerformanceEditModule {}
