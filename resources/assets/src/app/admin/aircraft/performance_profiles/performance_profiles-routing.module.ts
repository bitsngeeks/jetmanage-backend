import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PerformanceProfilesComponent } from './performance_profiles.component';

const routes: Routes = [
    {
        path: '', component: PerformanceProfilesComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PerformanceProfilesRoutingModule {
}
