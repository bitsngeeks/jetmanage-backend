import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PerformanceProfilesRoutingModule } from './performance_profiles-routing.module';
import { PerformanceProfilesComponent } from './performance_profiles.component';

@NgModule({
    imports: [
        CommonModule,
        PerformanceProfilesRoutingModule
    ],
    declarations: [
        PerformanceProfilesComponent
    ]
})
export class PerformanceProfilesModule {}
