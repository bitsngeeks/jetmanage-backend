import { PerformanceProfilesModule } from './performance_profiles.module';

describe('PerformanceProfilesModule', () => {
  let performance_profilesModule: PerformanceProfilesModule;

  beforeEach(() => {
    performance_profilesModule = new PerformanceProfilesModule();
  });

  it('should create an instance', () => {
    expect(performance_profilesModule).toBeTruthy();
  });
});
