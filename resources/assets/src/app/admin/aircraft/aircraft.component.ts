import { Component } from '@angular/core';

declare var $: any;

@Component({
  selector: 'app-roots',
  templateUrl: './aircraft.component.html',
  styleUrls: ['./aircraft.component.scss']

})
export class AircraftComponent {
  title = 'app';
  public getRouterOutletState(outlet) {
    return outlet.isActivated ? outlet.activatedRoute : '';
  }
}
 