import { Component } from '@angular/core';

declare var $: any;

@Component({
  selector: 'app-roots',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss'],

})
export class CompanyComponent {
  title = 'app';
  public getRouterOutletState(outlet) {
    return outlet.isActivated ? outlet.activatedRoute : '';
  }
}
 