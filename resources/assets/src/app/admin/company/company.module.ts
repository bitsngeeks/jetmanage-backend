
import { NgModule } from '@angular/core';

import { CompanyComponent } from './company.component';

import {RouterModule, Routes} from '@angular/router';


const routes: Routes = [
    {
        path: '',
        component: CompanyComponent,
        children: [
            { path: '', redirectTo: 'company_details' },
            { path: 'company_details', loadChildren: './company_details/company_details.module#CompanyDetailsModule' },
            { path: 'customers', loadChildren: './customers/customers.module#CustomersModule' },
            { path: 'edit_customer/:id', loadChildren: './edit_customer/edit_customer.module#EditCustomerModule' },
            { path: 'pilots', loadChildren: './pilots/pilots.module#PilotsModule' },
            { path: 'edit_pilot', loadChildren: './edit_pilot/edit_pilot.module#EditPilotModule' },
            { path: 'users', loadChildren: './users/users.module#UsersModule' },
            { path: 'edit_user', loadChildren: './edit_user/edit_user.module#EditUserModule' },
            { path: 'settings', loadChildren: './settings/settings.module#SettingsModule' },
        ]
    }
];

@NgModule({
  declarations: [
    CompanyComponent,

  ],
  imports: [
    RouterModule.forChild(routes)
  ],
  providers: [],
  bootstrap: [CompanyComponent]
})
export class CompanyModule { }
