import { EditUserModule } from './edit_user.module';

describe('EditUserModule', () => {
  let edit_userModule: EditUserModule;

  beforeEach(() => {
    edit_userModule = new EditUserModule();
  });

  it('should create an instance', () => {
    expect(edit_userModule).toBeTruthy();
  });
});
