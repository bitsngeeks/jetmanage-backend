import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditUserRoutingModule } from './edit_user-routing.module';
import { EditUserComponent } from './edit_user.component';

@NgModule({
    imports: [
        CommonModule,
        EditUserRoutingModule
    ],
    declarations: [
        EditUserComponent
    ]
})
export class EditUserModule {}
