import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CompanyDetailsRoutingModule } from './company_details-routing.module';
import { CompanyDetailsComponent } from './company_details.component';

@NgModule({
    imports: [
        CommonModule,
        CompanyDetailsRoutingModule
    ],
    declarations: [
        CompanyDetailsComponent
    ]
})
export class CompanyDetailsModule {}
