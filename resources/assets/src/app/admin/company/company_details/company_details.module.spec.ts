import { CompanyDetailsModule } from './company_details.module';

describe('CompanyDetailsModule', () => {
  let company_detailsModule: CompanyDetailsModule;

  beforeEach(() => {
    company_detailsModule = new CompanyDetailsModule();
  });

  it('should create an instance', () => {
    expect(company_detailsModule).toBeTruthy();
  });
});
