import { CountriesService } from './../../../services/countries.service';
import { ActivatedRoute } from '@angular/router';
import { IndividualsService } from './../../../services/individuals.service';
import {
    Component,
    OnInit
} from '@angular/core';
declare var select: any;
declare var script: any;
declare var $: any;
@Component({
    selector: 'app-edit_customer',
    templateUrl: './edit_customer.component.html',
    styleUrls: ['./edit_customer.component.scss']
})
export class EditCustomerComponent {

    constructor(private individualsService:IndividualsService, private route:ActivatedRoute, private countriesService:CountriesService) {}

    public id;
    public customerData:any=[];
    public customerPhone:any=[];
    public customerMail:any=[];
    public birthCountryData:any=[];
    public resCountryData:any=[];
    public first_name:string;
    public middle_name:string;
    public last_name:string;
    public resCountry:string;    
    public gender:boolean;
    public birCountry:string;
    public Countries = [];
    public Genders = [{id:true,text:"male"},{id:false,text:"female"}];
    public weightUnits = [{id:1,text:"Pounds"},{id:2,text:"Kg"}];
    public heightUnits = [{id:1,text:"Inches"},{id:2,text:"Cm"}];
    public weightUnit;
    public heightUnit;
    public weight:number;
    public height:number;  
    public phone;  
    public email;  
    public dob:number;  
    updateCustomer(){
        this.dob=$('#dob').val();
        console.log(this.dob)
        this.individualsService.updateIndividual(this.id,this.first_name,this.middle_name,this.last_name,this.gender,this.weight,this.height,this.dob,1,this.resCountry,this.birCountry)
        .subscribe(
            (rows) => {
                console.log(rows.json().data)
            },
            e  => console.log(e),
            () => setTimeout(()=>{ 
            })
        );	

        
    }
    updateAirports(){
        this.countriesService.getAllCountries().subscribe(
            (rows) => {
                rows.json().data.forEach((country:
                    {
                        full_name:string, 
                        id:number,
                        iso_3166_2:string,
                    }) => {
                    this.Countries.push({
                      id: country.id, 
                      text: country.full_name
                    });
                  });
            },
            e  => console.log(e),
            () => setTimeout(()=>{ 
            })
        );	
}
	ngOnInit(){
        this.updateAirports();
		this.route.paramMap
            .subscribe(params=>{
                this.id=params.get('id');
            });
        this.individualsService.getIndividual(this.id)
        .subscribe(
            (rows) => {
                
                this.customerData=rows.json().data;
                this.first_name=rows.json().data.first_name;
                this.middle_name=rows.json().data.middle_name;
                this.last_name=rows.json().data.last_name;
                this.weight=rows.json().data.weight;
                this.height=rows.json().data.height;
                this.phone=rows.json().data.phone[0].phone;
                this.email=rows.json().data.email[0].email;
                this.gender=rows.json().data.gender;
                this.weightUnit=1;
                this.heightUnit=1;
                if(this.customerData.phone[0]){
                    this.customerPhone=this.customerData.phone[0];
                } else{
                    this.customerPhone=[]
                }
                if(this.customerData.email[0]){
                    this.customerMail=this.customerData.email[0]
                } else{
                    this.customerMail=[]
                }
            },
            e  => console.log(e),
            () => setTimeout(()=>{ 
                if(this.customerData.res_country_id!=0){
                    this.countriesService.getCountry(this.customerData.res_country_id)
                .subscribe(
                    (rows) => {
                        this.birCountry=rows.json().data.id;
                        this.resCountryData=rows.json().data;

                    },
                    e  => console.log(e),
                    () => setTimeout(()=>{ 
                    })
                );
                }
                if(this.customerData.birth_country_id!=0){
                    this.countriesService.getCountry(this.customerData.birth_country_id)
                .subscribe(
                    (rows) => {
                        this.resCountry=rows.json().data.id;
                        this.birthCountryData=rows.json().data;

                    },
                    e  => console.log(e),
                    () => setTimeout(()=>{ 

                    })
                );
                }
                
                
            })
        );
		script();
	}
	}
