import { NgxSelectModule } from 'ngx-select-ex';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditCustomerRoutingModule } from './edit_customer-routing.module';
import { EditCustomerComponent } from './edit_customer.component';

@NgModule({
    imports: [
        CommonModule,
        EditCustomerRoutingModule,
        FormsModule,
        NgxSelectModule
    ],
    declarations: [
        EditCustomerComponent
    ]
})
export class EditCustomerModule {


    

}
