import { EditCustomerModule } from './edit_customer.module';

describe('EditCustomerModule', () => {
  let edit_customerModule: EditCustomerModule;

  beforeEach(() => {
    edit_customerModule = new EditCustomerModule();
  });

  it('should create an instance', () => {
    expect(edit_customerModule).toBeTruthy();
  });
});
