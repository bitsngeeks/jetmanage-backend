import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomersRoutingModule } from './customers-routing.module';
import { CustomersComponent } from './customers.component';
import { FormsModule } from '@angular/forms';
import { NgxSelectModule } from 'ngx-select-ex';

@NgModule({
    imports: [
        CommonModule,
        CustomersRoutingModule,
        FormsModule,
        NgxSelectModule
    ],
    declarations: [
        CustomersComponent
    ]
})
export class CustomersModule {}
