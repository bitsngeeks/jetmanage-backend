import { CountriesService } from './../../../services/countries.service';
import { IndividualsService } from './../../../services/individuals.service';
import {
    Component,
    OnInit
} from '@angular/core';



declare var select: any;
declare var script: any;
declare var $: any;
@Component({
    selector: 'app-customers',
    templateUrl: './customers.component.html',
    styleUrls: ['./customers.component.scss']
})
export class CustomersComponent {
    public customerData:any=[];
    public firstName:string;
    public middleName:string;
    public lastName:string;
    public gender:boolean;
    public weight:number;
    public weightAux:number;
    public weightUnit:number;
    public height:number;    
    public heightAux:number;    
    public heightUnit:number;    
    public dob;    
    public resCountry:string;    
    public birCountry:string;    
    public indId:number;    
    public Countries = [];
    public Genders = [{id:true,text:"male"},{id:false,text:"female"}];
    public weightUnits = [{id:1,text:"Pounds"},{id:2,text:"Kg"}];
    public heightUnits = [{id:1,text:"Inches"},{id:2,text:"Cm"}];



    constructor(private individualsService: IndividualsService,private countriesService:CountriesService) {}


	ngOnInit(){
        this.getIndividuals();
        this.updateAirports();
		script();
    }

    getIndividuals(){
    this.individualsService.getAllIndividuals()
    .subscribe(
        (rows) => {
            this.customerData=[];
            for (var i in rows.json().data){
                for(var j in rows.json().data[i].tags){
                    if(rows.json().data[i].tags[j].id==4){
                        this.customerData.push(rows.json().data[i]);
                    }
                }
                
            }
        },
        e  => console.log(e),
        () => setTimeout(()=>{ 
            $('[data-toggle="tooltip"]').tooltip();
        })
    );
    }

    updateAirports(){
			this.countriesService.getAllCountries().subscribe(
				(rows) => {
                    rows.json().data.forEach((country:
                        {
                            full_name:string, 
                            id:number,
                            iso_3166_2:string,
                        }) => {
                        this.Countries.push({
                          id: country.id, 
                          text: country.full_name
                        });
                      });
				},
				e  => console.log(e),
				() => setTimeout(()=>{ 
				})
			);	
	}
    deleteCustomer(id){
        console.log(id)
        this.individualsService.deleteTag(id,1)
        .subscribe(
            (rows) => {
            },
            e  => {this.individualsService.getAllIndividuals();},
            () => setTimeout(()=>{ 
                this.getIndividuals();
            })
        );
        alert("Deleted Customer")
        this.individualsService.deleteIndividual(id)
        .subscribe(
            (rows) => {
                console.log(rows.json())
            },
            e  => {this.individualsService.getAllIndividuals()},
            () => setTimeout(()=>{ 
                this.getIndividuals();
            })
        );
        
        
    }
    createCustomer(){
        this.dob=$('#dob').val();
        loop1:
        if(this.firstName && this.lastName){
            if(this.height){
                switch (this.heightUnit) {
                    case 1:
                        this.heightAux=this.height
                        break;
                    case 2:
                        this.heightAux=parseInt((this.height*0.393701).toFixed(0));
                        break;
                    default:
                        alert("Please select a unit of measurement")
                        break loop1;
                }
            }
            if(this.weight){
                switch (this.weightUnit) {
                    case 1:
                        this.weightAux=this.weight
                        break;
                    case 2:
                        this.weightAux=parseInt((this.weight/0.453592).toFixed(0));
                        break;
                    default:
                        alert("Please select a unit of measurement")
                        break loop1;
                }
            }
            this.individualsService.createIndividual(this.firstName,this.lastName,this.middleName,this.gender,this.weightAux,this.heightAux,this.dob,1,this.resCountry,this.birCountry)
            .subscribe(
                (rows) => {
                    alert("Customer Created")
                    $('#createTab').removeClass('active')
                    $('#addPerson').removeClass('active')
                    $('#searchTab').addClass('active')
                    $('#search').addClass('active')
                    this.indId=rows.json().data.id
                },
                e  => console.log(e),
                () => setTimeout(()=>{ 
                    this.individualsService.addTag(this.indId,1)
                    .subscribe(
                        (rows) => {
                        },
                        e  => {this.getIndividuals();},
                        () => setTimeout(()=>{ 

                            this.getIndividuals();
                        })
                    );
                })
            );
        } else {
            alert("Please fill all the required fields")
            
        }

    }

	}
