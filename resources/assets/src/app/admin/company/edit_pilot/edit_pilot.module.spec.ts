import { EditPilotModule } from './edit_pilot.module';

describe('EditPilotModule', () => {
  let edit_pilotModule: EditPilotModule;

  beforeEach(() => {
    edit_pilotModule = new EditPilotModule();
  });

  it('should create an instance', () => {
    expect(edit_pilotModule).toBeTruthy();
  });
});
