import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditPilotRoutingModule } from './edit_pilot-routing.module';
import { EditPilotComponent } from './edit_pilot.component';

@NgModule({
    imports: [
        CommonModule,
        EditPilotRoutingModule
    ],
    declarations: [
        EditPilotComponent
    ]
})
export class EditPilotModule {}
