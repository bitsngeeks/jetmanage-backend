import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditPilotComponent } from './edit_pilot.component';

const routes: Routes = [
    {
        path: '', component: EditPilotComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class EditPilotRoutingModule {
}
