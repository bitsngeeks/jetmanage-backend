import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PilotsRoutingModule } from './pilots-routing.module';
import { PilotsComponent } from './pilots.component';

@NgModule({
    imports: [
        CommonModule,
        PilotsRoutingModule
    ],
    declarations: [
        PilotsComponent
    ]
})
export class PilotsModule {}
