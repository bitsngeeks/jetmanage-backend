
import { NgModule } from '@angular/core';

import { SidebarComponent } from './shared/sidebar/sidebar.component';
import { LayoutComponent } from './layout.component';

import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  { path: '',
    component:LayoutComponent,
    children: [
    { path: '', loadChildren: './calendar/view_all/view_all.module#ViewAllModule'},
    { path: 'calendar', loadChildren: './calendar/view_all/view_all.module#ViewAllModule'},
    { path: 'trips', loadChildren: './trips/trips.module#TripsModule'},
    { path: 'aircraft', loadChildren: './aircraft/aircraft.module#AircraftModule'},
    { path: 'company', loadChildren: './company/company.module#CompanyModule'},
    ]
  }
];

@NgModule({
  declarations: [
    LayoutComponent,
    SidebarComponent
  ],
  imports: [
    RouterModule.forChild(routes)
  ],
  providers: [],
  bootstrap: [LayoutComponent]
})
export class LayoutModule { }
