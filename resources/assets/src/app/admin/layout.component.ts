import { Component, AfterViewInit } from '@angular/core';
import { fadeAnimation } from './shared/utils/fade.animation';
declare var select: any;
declare var script: any;
declare var $: any;
var Aux =true;
var prevId = "";
@Component({
  selector: 'app-roots2',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
  animations: [fadeAnimation]
})
    
 export class LayoutComponent {

    public getRouterOutletState(outlet) {
        return outlet.isActivated ? outlet.activatedRoute : '';
    };


    
};
