import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';

const API_URL = 'http://localhost:8000/api';

@Injectable()
export class IndividualsService {

  token: string = '';
  twoToken: string = '';
  headers: Headers;

  constructor(private http:Http) { 
  	this.token    = localStorage.getItem('token');
    this.twoToken = localStorage.getItem('twoToken');

    this.headers = new Headers({
      'Content-Type': 'application/json',
      Authorization: `Bearer ${this.token}`,
      'twoToken': this.twoToken,
    });
  }

  public getAllIndividuals(){
    return this.http.get(API_URL + '/individuals/', {headers: this.headers});
  }
  public getIndividual(id){
    return this.http.get(API_URL + `/individuals/${id}`, {headers: this.headers});
  }
  public addTag(id,tagId){
    return this.http.post(API_URL + `/individuals/tag/${id}/${tagId}/`, {},{headers: this.headers});
  }
  public deleteTag(id,tagId){
    return this.http.delete(API_URL + `/individuals/tag/${id}/${tagId}/`, {headers: this.headers});
  }

  public deleteIndividual(id){
    return this.http.delete(API_URL + `/individuals/${id}`, {headers: this.headers});
  }
  public createIndividual(firstName,lastName,middleName,gender,weight,height,dob,companyId,resId,birthId){
    if(!middleName){
      middleName="Null";
    }
    if(!weight){
      weight=0;
    }
    if(!height){
      height=0;
    }
    if(!dob){
      dob="0000-01-01";
    }
    if(!companyId){
      companyId=1;
    }
    if(!resId){
      resId=0;
    }
    if(!birthId){
      birthId=0;
    }
    return this.http.post(API_URL + '/individuals/',
    {
      "first_name":firstName,
      "middle_name":middleName,
      "last_name":lastName,
      "gender":gender,
      "weight":weight,
      "height":height,
      "dob": dob,
      "company_id":companyId,
      "res_country_id":resId,
      "birth_country_id":birthId
    },
    {headers: this.headers});
  }
 
  public addPassengers(){
    return this.http.post(API_URL + `/passengers/`, 
    {
      "leg_id": 1,
      "trip_id": 1,
      "ind_id":1
    }, {headers: this.headers});
  }

  public updateIndividual(id,firstName,middleName,lastName,gender,weight,height,
  dob,companyId,resId,birthId){
    return this.http.put(API_URL + `/individuals/${id}`,
    {
      "first_name":firstName,
      "middle_name":middleName,
      "last_name":lastName,
      "gender":gender,
      "weight":weight,
      "height":height,
      "dob": dob,
      "company_id":companyId,
      "res_country_id":resId,
      "birth_country_id":birthId
    },
    {headers: this.headers});
  }
}
