import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';

const API_URL = 'http://localhost:8000/api';

@Injectable()
export class TripsService {
  token: string = '';
  twoToken: string = '';
  headers: Headers;

  constructor(private http:Http) { 
  	this.token    = localStorage.getItem('token');
    this.twoToken = localStorage.getItem('twoToken');

    this.headers = new Headers({
      'Content-Type': 'application/json',
      Authorization: `Bearer ${this.token}`,
      'twoToken': this.twoToken,
    });
  }

  public getAllTrips() {
    return this.http.get(API_URL + '/trips/', {headers: this.headers});
  }

  public getTripById(tripId: number) {
    return this.http.get(API_URL + '/trips/' + tripId, {headers: this.headers});
  }

  public createTrip(dataTrip) {
    return this.http.post(API_URL + '/trips/', dataTrip, {headers: this.headers}).toPromise();
  }

  public createLegs(tripid,aircraftid,dataLegs,depart,arrival){
    for (let index = 0; index < depart.length; index++) {
      if(index<(depart.length-1)){
        this.createLeg(tripid,aircraftid,dataLegs[2*index].split("/")[0],dataLegs[2*index+1].split("/")[0],depart[index],arrival[index]);
      }else{
        return this.createLeg(tripid,aircraftid,dataLegs[2*index].split("/")[0],dataLegs[2*index+1].split("/")[0],depart[index],arrival[index]);
      }
      
    }
}

public createLeg(tripid,aircraftid,dataLegFrom,dataLegTo,depart,arrival){
  return this.http.post(API_URL + '/legs/', 
    {
      "trip_id": tripid,
      "trip_leg_order": 1,
      "from_airport_id": dataLegFrom,
      "to_airport_id": dataLegTo,
      "depart_date": depart,
      "arrival_date": arrival,
      "out_time": "2017-10-31 07:00:00",
      "on_time": "2017-10-31 09:00:00",
      "in_time": "2017-10-31 09:20:00",
      "off_time": "2017-10-31 07:10:00",
      "out_fuel": 500,
      "in_fuel": 300,
      "out_hobbs": 500,
      "in_hobbs": 300,
      "out_tach": 1000,
      "in_tach": 1300,
      "aircraft_id": aircraftid
    }
    , {headers: this.headers}).toPromise();
}

public updateAircraft(id,aircraftId){
  return this.http.put(API_URL + `/trips/${id}`, 
  {
    "aircraft_id": aircraftId
  }, 
  {headers: this.headers}).toPromise();
}
}

  
