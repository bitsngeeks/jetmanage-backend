import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';

@Injectable()
export class HttpService {
  private BASE_URL: string = 'http://localhost:8000/api';
  private headers: Headers = new Headers({'Content-Type': 'application/json'});
  results:Object[];

  constructor(private http:Http) { 
    this.results=[];
  }

  getLogin(email, password): Promise<any> {
    var url = `${this.BASE_URL}/login`;
    return this.http.post(url, {
          "email" : email,
          "password": password
        }, {headers: this.headers}).toPromise();
  }

}
