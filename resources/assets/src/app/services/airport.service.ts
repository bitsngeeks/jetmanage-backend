import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';

const API_URL = 'http://localhost:8000/api';

@Injectable()
export class AirportService {
  token: string = '';
  twoToken: string = '';
  headers: Headers;

  constructor(private http:Http) { 
  	this.token    = localStorage.getItem('token');
    this.twoToken = localStorage.getItem('twoToken');

    this.headers = new Headers({
      'Content-Type': 'application/json',
      Authorization: `Bearer ${this.token}`,
      'twoToken': this.twoToken,
    });
  }

  public getAllAirports() {
    return this.http.get(API_URL + '/airports/', {headers: this.headers});
  }
  public searchAirports(query){
    return this.http.get(API_URL + `/airports/search/${query}`, {headers: this.headers});
  }

}
