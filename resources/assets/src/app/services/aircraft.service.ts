import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';

const API_URL = 'http://localhost:8000/api';

@Injectable()
export class AircraftService {

  token: string = '';
  twoToken: string = '';
  headers: Headers;

  constructor(private http:Http) { 
  	this.token    = localStorage.getItem('token');
    this.twoToken = localStorage.getItem('twoToken');

    this.headers = new Headers({
      'Content-Type': 'application/json',
      Authorization: `Bearer ${this.token}`,
      'twoToken': this.twoToken,
    });
  }

  public getAllAircrafts() {
    return this.http.get(API_URL + '/aircrafts/', {headers: this.headers});
  }
  public getAircraft(id) {
    return this.http.get(API_URL + `/aircrafts/${id}`, {headers: this.headers});
  }
  public updateAircraft(id,registration,serial_number,type) {
    var jsonAux=[];
    var json='{';        
    if(registration){
      jsonAux.push('"registration":"'+registration+'"')
    }
    if(serial_number!=""){
      jsonAux.push('"serial_number":"'+serial_number+'"')
    }
    if(type){
      jsonAux.push('"type_id":'+type)
    }

    for (let i = 0; i < jsonAux.length; i++) {
      json=json+jsonAux[i];
      if(i<(jsonAux.length-1)){
        json=json+','
      }      
    }
    json=json+'}';
    json=JSON.parse(json);
    return this.http.put(API_URL + `/aircrafts/${id}`,json,{headers: this.headers});
    
  }
  public deleteAircraft(id) {
    return this.http.delete(API_URL + `/aircrafts/${id}`, {headers: this.headers});
  }


  public CreateAircraft(registration,type,serial_number="0") {
    return this.http.post(API_URL + '/aircrafts/',
    {
        "registration": registration,
        "type_id": type,
        "serial_number": serial_number,
        "name": "Pilatus PC 12"
    },    
    {headers: this.headers});
  }

}
