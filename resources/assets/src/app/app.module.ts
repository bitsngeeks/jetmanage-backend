import { CountriesService } from './services/countries.service';
import { IndividualsService } from './services/individuals.service';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { LoginRedirect } from './services/login-redirect.service';
import { LoginComponent } from './login/login/login.component';
import { HttpService } from './services/http.service';
import { TripsService } from './services/trips.service';
import { AircraftService } from './services/aircraft.service';
import { AirportService } from './services/airport.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';



const routes: Routes = [
  { path: '', component : LoginComponent, canActivate: [LoginRedirect]},
  { path: 'login', component : LoginComponent, canActivate: [LoginRedirect]},
  { path: 'admin', loadChildren: './admin/layout.module#LayoutModule'},
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    HttpModule
  ],
  providers: [
    HttpService, 
    TripsService, 
    AircraftService,
    AirportService,
    IndividualsService,
    CountriesService,
    LoginRedirect
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
