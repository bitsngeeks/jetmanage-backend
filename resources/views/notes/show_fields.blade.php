<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $note->id !!}</p>
</div>

<!-- Ind Id Field -->
<div class="form-group">
    {!! Form::label('ind_id', 'Ind Id:') !!}
    <p>{!! $note->ind_id !!}</p>
</div>

<!-- Note Type Field -->
<div class="form-group">
    {!! Form::label('note_type', 'Note Type:') !!}
    <p>{!! $note->note_type !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $note->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $note->updated_at !!}</p>
</div>

