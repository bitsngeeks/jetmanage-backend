@extends('layouts.main')


@section("content")
  @yield('roles')
@stop


@section('bottom_js')
<script>
// Bootstrap-datatable method to show edit, delete icons.
function actionRoleFormatter(value, row, index) {
  return [
  	@can('role-update', 'test')
      '<div class="edit icondemo vertical-align-middle">',
        '<i class="icon fa-edit" aria-hidden="true"></i>',
      '</div>',
    @endcan

    @can('role-delete', 'test')
      '<div class="remove icondemo vertical-align-middle">',
        '<i class="icon fa-trash" aria-hidden="true"></i>',
      '</div>'
    @endcan
  ].join('');
}
</script>
@stop
