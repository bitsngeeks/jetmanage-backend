@extends('admin.role.view')


@section("roles")

  {{ Form::open(['route' => ['roles.store'], 'method' => 'post', 'id' => 'roleform', 'name' => 'roleform', "class" => "form-horizontal"]) }}

    {{ Form::hidden('jsfields', null, ["id" => "jsfields"]) }}

    @include('admin.role.partials.form', ['submitButtonText'=>  trans('role.view.form.create')])

  {{ Form::close() }}

@stop
