@include('errors.formerror')

<div class="panel panel-primary panel-line">
  <div class="panel-heading">
    <h3 class="panel-title">@lang('role.view.form.title')</h3>
  </div>
  <div class="panel-body">
    <div class="row">
      <div class="col-sm-12">
        <div class="form-group">
          {{ Form::label(trans('role.view.form.roleName').':') }}
          {{ Form::text('label',null , ['class' => 'form-control', 'required' => "required"]) }}
        </div>
      </div>
    </div>
  </div>
</div>


<div class="panel panel-primary panel-line">
  <div class="panel-heading">
    <h3 class="panel-title">@lang('role.view.form.title2')</h3>
  </div>
  <div class="panel-body">
    <div class="row">

      <div class="col-sm-12">
        <div class="form-group">
           {{ Form::label(trans('role.view.form.permissionsTree').':') }}
           <div id="treeview"></div>
        </div>
      </div>

      <div class="col-sm-12">
        <div class="form-group">
          {{ Form::button($submitButtonText, ['type' => 'submit', 'class' => "form-control btn btn-block btn-primary waves-effect",  'id' => "subButton", "onclick" => "submitMe()"]) }}
        </div>
      </div>

    </div>
  </div>
</div>


