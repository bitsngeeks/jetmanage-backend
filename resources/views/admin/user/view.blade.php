@extends('layouts.main')


@section("content")
    @yield('users')
@stop


@section('bottom_js')

<script>
jQuery(function($) {
    $('select').selectpicker();
});
// Bootstrap-datatable method to show edit, delete and role icons.
function actionFormatter(value, row, index) {
  return [
  	@can('user-role', '')
      '<div class="assign icondemo vertical-align-middle">',
        '<i class="icon fa-plus-square" aria-hidden="true"></i>',
      '</div>',
    @endcan

    @can('user-update', '')
      '<div class="edit icondemo vertical-align-middle">',
        '<i class="icon fa-edit" aria-hidden="true"></i>',
      '</div>',
    @endcan

    @can('user-delete', '')
      '<div class="remove icondemo vertical-align-middle">',
        '<i class="icon fa-trash" aria-hidden="true"></i>',
      '</div>'
    @endcan
  ].join('');
}
</script>
@stop
