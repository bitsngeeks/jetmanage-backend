@extends('admin.user.view')


@section("users")
  {{ Form::model($user, ['route' => ['users.saverole', $user->id], 'method' => 'patch', 'id' => 'userformrole', 'name' => 'userformrole', "class" => "form-horizontal"]) }}

  @include('errors.formerror')

  <div class="panel panel-primary panel-line">
    <div class="panel-heading">
      <h3 class="panel-title">@lang('user.view.role.title')</h3>
    </div>
    <div class="panel-body">
      <div class="row">
        <div class="col-sm-6">

          <div class="form-group">
            {{ Form::label(trans('user.view.form.fullName').':') }}
            <p class="form-control-static bg-blue-grey-50 grey-800 card-block">{{ $user->name }}</p>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            {{ Form::label(trans('user.view.form.email').':') }}
            <p class="form-control-static bg-blue-grey-50 grey-800 card-block">{{ $user->email }}</p>
          </div>
        </div>
        <div class="col-sm-12">
          <div class="form-group">
            {{ Form::label(trans('user.view.list.roles').':') }}
            {{ Form::select('roles[]', $roles, $selectedRoles, ['multiple' => true, "data-plugin" =>"selectpicker", 'class' => 'form-control' ]) }}
          </div>
        </div>

        <div class="col-sm-12">
          <div class="form-group">
            {{ Form::button(trans('user.view.role.button'), ['type' => 'submit', 'class' => "form-control btn btn-block btn-primary waves-effect",  'id' => "sub"]) }}
          </div>
        </div>

      </div>
    </div>
  </div>

  {{ Form::close() }}
@stop

