@extends('admin.user.view')


@section("users")
    {{ Form::open(['route' => ['users.store'], 'method' => 'post', 'id' => 'userformcreate', 'name' => 'userformcreate', "class" => "form-horizontal"]) }}

    @include('admin.user.partials.form', [
        'submitButtonText' => trans('user.view.form.create'),
        'type' => 'create',
    ])

    {{ Form::close() }}
@stop
