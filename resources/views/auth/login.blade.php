@extends('layouts.login')

@section('content')
    <div class="page" data-animsition-in="fade-in" data-animsition-out="fade-out">
        <div class="page-content">
            <div class="page-brand-info">
                <div class="brand">
                </div>
            </div>
            <div class="page-login-main">
                <div class="brand hidden-md-up">
                    <h3 class="brand-text font-size-40">@lang('site.name')</h3>
                </div>
                <h3 class="font-size-24">@lang('site.login.title')</h3>
                <form method="post" action="{{ url('/login') }}" autocomplete="off">
                    {{ csrf_field() }}
                    <div class="form-group form-material floating" data-plugin="formMaterial">
                        <input class="form-control" id="email" name="email" placeholder="@lang('site.login.email')" type="text"  required autofocus />
                    </div>
                    <div class="form-group form-material floating" data-plugin="formMaterial">
                        <input type="password" class="form-control empty" id="inputPassword" name="password" placeholder="@lang('site.login.password')">
                    </div>
                    <div class="form-group clearfix">
                        <div class="checkbox-custom checkbox-inline checkbox-primary pull-xs-left">
                            <input type="checkbox" id="remember" name="remember">
                            <label for="inputCheckbox">@lang('site.login.rememberMe')</label>
                        </div>
                        {{-- <a class="pull-xs-right" href="{{ url('/password/reset') }}">@lang('site.login.forgot')?</a> --}}
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">@lang('site.login.signIn')</button>
                </form>
                <footer class="page-copyright">
                    <p>@lang('site.login.copyRightSite')</p>
                    <p>© 2017</p>
                    <div class="social">
                        <a class="btn btn-icon btn-round social-twitter" href="javascript:void(0)">
                            <i class="icon bd-twitter" aria-hidden="true"></i>
                        </a>
                        <a class="btn btn-icon btn-round social-facebook" href="javascript:void(0)">
                            <i class="icon bd-facebook" aria-hidden="true"></i>
                        </a>
                        <a class="btn btn-icon btn-round social-google-plus" href="javascript:void(0)">
                            <i class="icon bd-google-plus" aria-hidden="true"></i>
                        </a>
                    </div>
                </footer>
            </div>
        </div>
    </div>
@endsection
