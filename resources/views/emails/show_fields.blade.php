<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $email->id !!}</p>
</div>

<!-- Ind Id Field -->
<div class="form-group">
    {!! Form::label('ind_id', 'Ind Id:') !!}
    <p>{!! $email->ind_id !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $email->email !!}</p>
</div>

<!-- Is Verified Field -->
<div class="form-group">
    {!! Form::label('is_verified', 'Is Verified:') !!}
    <p>{!! $email->is_verified !!}</p>
</div>

<!-- Is Primary Field -->
<div class="form-group">
    {!! Form::label('is_primary', 'Is Primary:') !!}
    <p>{!! $email->is_primary !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $email->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $email->updated_at !!}</p>
</div>

