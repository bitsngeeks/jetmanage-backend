<!-- Leg Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('leg_id', 'Leg Id:') !!}
    {!! Form::number('leg_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Trip Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('trip_id', 'Trip Id:') !!}
    {!! Form::number('trip_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Ind Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ind_id', 'Ind Id:') !!}
    {!! Form::number('ind_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('passengers.index') !!}" class="btn btn-default">Cancel</a>
</div>
