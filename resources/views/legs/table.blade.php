<table class="table table-responsive" id="legs-table">
    <thead>
        <tr>
            <th>Trip Id</th>
        <th>Trip Leg Order</th>
        <th>From Airport Id</th>
        <th>To Airport Id</th>
        <th>Depart Date</th>
        <th>Out Time</th>
        <th>Off Time</th>
        <th>Arrival Date</th>
        <th>On Time</th>
        <th>In Time</th>
        <th>Out Fuel</th>
        <th>In Fuel</th>
        <th>Out Hobbs</th>
        <th>In Hobbs</th>
        <th>Out Tach</th>
        <th>In Tach</th>
        <th>Regs</th>
        <th>Aircraft Id</th>
        <th>Registration</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($legs as $leg)
        <tr>
            <td>{!! $leg->trip_id !!}</td>
            <td>{!! $leg->trip_leg_order !!}</td>
            <td>{!! $leg->from_airport_id !!}</td>
            <td>{!! $leg->to_airport_id !!}</td>
            <td>{!! $leg->depart_date !!}</td>
            <td>{!! $leg->out_time !!}</td>
            <td>{!! $leg->off_time !!}</td>
            <td>{!! $leg->arrival_date !!}</td>
            <td>{!! $leg->on_time !!}</td>
            <td>{!! $leg->in_time !!}</td>
            <td>{!! $leg->out_fuel !!}</td>
            <td>{!! $leg->in_fuel !!}</td>
            <td>{!! $leg->out_hobbs !!}</td>
            <td>{!! $leg->in_hobbs !!}</td>
            <td>{!! $leg->out_tach !!}</td>
            <td>{!! $leg->in_tach !!}</td>
            <td>{!! $leg->regs !!}</td>
            <td>{!! $leg->aircraft_id !!}</td>
            <td>{!! $leg->registration !!}</td>
            <td>
                {!! Form::open(['route' => ['legs.destroy', $leg->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('legs.show', [$leg->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('legs.edit', [$leg->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>