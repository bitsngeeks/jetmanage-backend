<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $leg->id !!}</p>
</div>

<!-- Trip Id Field -->
<div class="form-group">
    {!! Form::label('trip_id', 'Trip Id:') !!}
    <p>{!! $leg->trip_id !!}</p>
</div>

<!-- Trip Leg Order Field -->
<div class="form-group">
    {!! Form::label('trip_leg_order', 'Trip Leg Order:') !!}
    <p>{!! $leg->trip_leg_order !!}</p>
</div>

<!-- From Airport Id Field -->
<div class="form-group">
    {!! Form::label('from_airport_id', 'From Airport Id:') !!}
    <p>{!! $leg->from_airport_id !!}</p>
</div>

<!-- To Airport Id Field -->
<div class="form-group">
    {!! Form::label('to_airport_id', 'To Airport Id:') !!}
    <p>{!! $leg->to_airport_id !!}</p>
</div>

<!-- Depart Date Field -->
<div class="form-group">
    {!! Form::label('depart_date', 'Depart Date:') !!}
    <p>{!! $leg->depart_date !!}</p>
</div>

<!-- Out Time Field -->
<div class="form-group">
    {!! Form::label('out_time', 'Out Time:') !!}
    <p>{!! $leg->out_time !!}</p>
</div>

<!-- Off Time Field -->
<div class="form-group">
    {!! Form::label('off_time', 'Off Time:') !!}
    <p>{!! $leg->off_time !!}</p>
</div>

<!-- Arrival Date Field -->
<div class="form-group">
    {!! Form::label('arrival_date', 'Arrival Date:') !!}
    <p>{!! $leg->arrival_date !!}</p>
</div>

<!-- On Time Field -->
<div class="form-group">
    {!! Form::label('on_time', 'On Time:') !!}
    <p>{!! $leg->on_time !!}</p>
</div>

<!-- In Time Field -->
<div class="form-group">
    {!! Form::label('in_time', 'In Time:') !!}
    <p>{!! $leg->in_time !!}</p>
</div>

<!-- Out Fuel Field -->
<div class="form-group">
    {!! Form::label('out_fuel', 'Out Fuel:') !!}
    <p>{!! $leg->out_fuel !!}</p>
</div>

<!-- In Fuel Field -->
<div class="form-group">
    {!! Form::label('in_fuel', 'In Fuel:') !!}
    <p>{!! $leg->in_fuel !!}</p>
</div>

<!-- Out Hobbs Field -->
<div class="form-group">
    {!! Form::label('out_hobbs', 'Out Hobbs:') !!}
    <p>{!! $leg->out_hobbs !!}</p>
</div>

<!-- In Hobbs Field -->
<div class="form-group">
    {!! Form::label('in_hobbs', 'In Hobbs:') !!}
    <p>{!! $leg->in_hobbs !!}</p>
</div>

<!-- Out Tach Field -->
<div class="form-group">
    {!! Form::label('out_tach', 'Out Tach:') !!}
    <p>{!! $leg->out_tach !!}</p>
</div>

<!-- In Tach Field -->
<div class="form-group">
    {!! Form::label('in_tach', 'In Tach:') !!}
    <p>{!! $leg->in_tach !!}</p>
</div>

<!-- Regs Field -->
<div class="form-group">
    {!! Form::label('regs', 'Regs:') !!}
    <p>{!! $leg->regs !!}</p>
</div>

<!-- Aircraft Id Field -->
<div class="form-group">
    {!! Form::label('aircraft_id', 'Aircraft Id:') !!}
    <p>{!! $leg->aircraft_id !!}</p>
</div>

<!-- Registration Field -->
<div class="form-group">
    {!! Form::label('registration', 'Registration:') !!}
    <p>{!! $leg->registration !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $leg->deleted_at !!}</p>
</div>

