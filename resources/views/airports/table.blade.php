<table class="table table-responsive" id="airports-table">
    <thead>
        <tr>
            <th>Airport Entry Id</th>
        <th>Code</th>
        <th>City</th>
        <th>Country</th>
        <th>Faacode</th>
        <th>Iata</th>
        <th>Icao</th>
        <th>Landingfacilityid</th>
        <th>Latitude</th>
        <th>Longitude</th>
        <th>Typicallyhasmetar</th>
        <th>Typicallyhastaf</th>
        <th>Name</th>
        <th>State</th>
        <th>Zip</th>
        <th>Fullcountrynamelowercase</th>
        <th>Magvarwest</th>
        <th>Access</th>
        <th>Type</th>
        <th>Hasfuel</th>
        <th>Fueltypes</th>
        <th>Longestrunwaylength</th>
        <th>Longestrunwaysurfacetype</th>
        <th>Nearestlandingfacilityids</th>
        <th>Elevationft</th>
        <th>Proceduretypes</th>
        <th>Towered</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($airports as $airport)
        <tr>
            <td>{!! $airport->airport_entry_id !!}</td>
            <td>{!! $airport->code !!}</td>
            <td>{!! $airport->city !!}</td>
            <td>{!! $airport->country !!}</td>
            <td>{!! $airport->faaCode !!}</td>
            <td>{!! $airport->iata !!}</td>
            <td>{!! $airport->icao !!}</td>
            <td>{!! $airport->landingFacilityId !!}</td>
            <td>{!! $airport->latitude !!}</td>
            <td>{!! $airport->longitude !!}</td>
            <td>{!! $airport->typicallyHasMetar !!}</td>
            <td>{!! $airport->typicallyHasTaf !!}</td>
            <td>{!! $airport->name !!}</td>
            <td>{!! $airport->state !!}</td>
            <td>{!! $airport->zip !!}</td>
            <td>{!! $airport->fullCountryNameLowerCase !!}</td>
            <td>{!! $airport->magVarWest !!}</td>
            <td>{!! $airport->access !!}</td>
            <td>{!! $airport->type !!}</td>
            <td>{!! $airport->hasFuel !!}</td>
            <td>{!! $airport->fuelTypes !!}</td>
            <td>{!! $airport->longestRunwayLength !!}</td>
            <td>{!! $airport->longestRunwaySurfaceType !!}</td>
            <td>{!! $airport->nearestlandingFacilityIds !!}</td>
            <td>{!! $airport->elevationFt !!}</td>
            <td>{!! $airport->procedureTypes !!}</td>
            <td>{!! $airport->towered !!}</td>
            <td>
                {!! Form::open(['route' => ['airports.destroy', $airport->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('airports.show', [$airport->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('airports.edit', [$airport->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>