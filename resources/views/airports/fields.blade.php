<!-- Airport Entry Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('airport_entry_id', 'Airport Entry Id:') !!}
    {!! Form::number('airport_entry_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('code', 'Code:') !!}
    {!! Form::text('code', null, ['class' => 'form-control']) !!}
</div>

<!-- City Field -->
<div class="form-group col-sm-6">
    {!! Form::label('city', 'City:') !!}
    {!! Form::text('city', null, ['class' => 'form-control']) !!}
</div>

<!-- Country Field -->
<div class="form-group col-sm-6">
    {!! Form::label('country', 'Country:') !!}
    {!! Form::text('country', null, ['class' => 'form-control']) !!}
</div>

<!-- Faacode Field -->
<div class="form-group col-sm-6">
    {!! Form::label('faaCode', 'Faacode:') !!}
    {!! Form::text('faaCode', null, ['class' => 'form-control']) !!}
</div>

<!-- Iata Field -->
<div class="form-group col-sm-6">
    {!! Form::label('iata', 'Iata:') !!}
    {!! Form::text('iata', null, ['class' => 'form-control']) !!}
</div>

<!-- Icao Field -->
<div class="form-group col-sm-6">
    {!! Form::label('icao', 'Icao:') !!}
    {!! Form::text('icao', null, ['class' => 'form-control']) !!}
</div>

<!-- Landingfacilityid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('landingFacilityId', 'Landingfacilityid:') !!}
    {!! Form::text('landingFacilityId', null, ['class' => 'form-control']) !!}
</div>

<!-- Latitude Field -->
<div class="form-group col-sm-6">
    {!! Form::label('latitude', 'Latitude:') !!}
    {!! Form::number('latitude', null, ['class' => 'form-control']) !!}
</div>

<!-- Longitude Field -->
<div class="form-group col-sm-6">
    {!! Form::label('longitude', 'Longitude:') !!}
    {!! Form::number('longitude', null, ['class' => 'form-control']) !!}
</div>

<!-- Typicallyhasmetar Field -->
<div class="form-group col-sm-6">
    {!! Form::label('typicallyHasMetar', 'Typicallyhasmetar:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('typicallyHasMetar', false) !!}
        {!! Form::checkbox('typicallyHasMetar', '1', null) !!} 1
    </label>
</div>

<!-- Typicallyhastaf Field -->
<div class="form-group col-sm-6">
    {!! Form::label('typicallyHasTaf', 'Typicallyhastaf:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('typicallyHasTaf', false) !!}
        {!! Form::checkbox('typicallyHasTaf', '1', null) !!} 1
    </label>
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- State Field -->
<div class="form-group col-sm-6">
    {!! Form::label('state', 'State:') !!}
    {!! Form::text('state', null, ['class' => 'form-control']) !!}
</div>

<!-- Zip Field -->
<div class="form-group col-sm-6">
    {!! Form::label('zip', 'Zip:') !!}
    {!! Form::text('zip', null, ['class' => 'form-control']) !!}
</div>

<!-- Fullcountrynamelowercase Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fullCountryNameLowerCase', 'Fullcountrynamelowercase:') !!}
    {!! Form::text('fullCountryNameLowerCase', null, ['class' => 'form-control']) !!}
</div>

<!-- Magvarwest Field -->
<div class="form-group col-sm-6">
    {!! Form::label('magVarWest', 'Magvarwest:') !!}
    {!! Form::number('magVarWest', null, ['class' => 'form-control']) !!}
</div>

<!-- Access Field -->
<div class="form-group col-sm-6">
    {!! Form::label('access', 'Access:') !!}
    {!! Form::text('access', null, ['class' => 'form-control']) !!}
</div>

<!-- Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type', 'Type:') !!}
    {!! Form::text('type', null, ['class' => 'form-control']) !!}
</div>

<!-- Hasfuel Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hasFuel', 'Hasfuel:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('hasFuel', false) !!}
        {!! Form::checkbox('hasFuel', '1', null) !!} 1
    </label>
</div>

<!-- Fueltypes Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fuelTypes', 'Fueltypes:') !!}
    {!! Form::text('fuelTypes', null, ['class' => 'form-control']) !!}
</div>

<!-- Longestrunwaylength Field -->
<div class="form-group col-sm-6">
    {!! Form::label('longestRunwayLength', 'Longestrunwaylength:') !!}
    {!! Form::number('longestRunwayLength', null, ['class' => 'form-control']) !!}
</div>

<!-- Longestrunwaysurfacetype Field -->
<div class="form-group col-sm-6">
    {!! Form::label('longestRunwaySurfaceType', 'Longestrunwaysurfacetype:') !!}
    {!! Form::text('longestRunwaySurfaceType', null, ['class' => 'form-control']) !!}
</div>

<!-- Nearestlandingfacilityids Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nearestlandingFacilityIds', 'Nearestlandingfacilityids:') !!}
    {!! Form::text('nearestlandingFacilityIds', null, ['class' => 'form-control']) !!}
</div>

<!-- Elevationft Field -->
<div class="form-group col-sm-6">
    {!! Form::label('elevationFt', 'Elevationft:') !!}
    {!! Form::number('elevationFt', null, ['class' => 'form-control']) !!}
</div>

<!-- Proceduretypes Field -->
<div class="form-group col-sm-6">
    {!! Form::label('procedureTypes', 'Proceduretypes:') !!}
    {!! Form::text('procedureTypes', null, ['class' => 'form-control']) !!}
</div>

<!-- Towered Field -->
<div class="form-group col-sm-6">
    {!! Form::label('towered', 'Towered:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('towered', false) !!}
        {!! Form::checkbox('towered', '1', null) !!} 1
    </label>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('airports.index') !!}" class="btn btn-default">Cancel</a>
</div>
