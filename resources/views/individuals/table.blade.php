<table class="table table-responsive" id="individuals-table">
    <thead>
        <tr>
            <th>First Name</th>
        <th>Middle Name</th>
        <th>Last Name</th>
        <th>Gender</th>
        <th>Dob</th>
        <th>Weight</th>
        <th>Height</th>
        <th>Res Country Id</th>
        <th>Birth Country Id</th>
        <th>Company Id</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($individuals as $individual)
        <tr>
            <td>{!! $individual->first_name !!}</td>
            <td>{!! $individual->middle_name !!}</td>
            <td>{!! $individual->last_name !!}</td>
            <td>{!! $individual->gender !!}</td>
            <td>{!! $individual->dob !!}</td>
            <td>{!! $individual->weight !!}</td>
            <td>{!! $individual->height !!}</td>
            <td>{!! $individual->res_country_id !!}</td>
            <td>{!! $individual->birth_country_id !!}</td>
            <td>{!! $individual->company_id !!}</td>
            <td>
                {!! Form::open(['route' => ['individuals.destroy', $individual->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('individuals.show', [$individual->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('individuals.edit', [$individual->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>