<!-- Aircraft Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('aircraft_id', 'Aircraft Id:') !!}
    {!! Form::number('aircraft_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('trips.index') !!}" class="btn btn-default">Cancel</a>
</div>
