<nav class="site-navbar navbar navbar-default navbar-inverse navbar-fixed-top navbar-mega" role="navigation">
    <div class="navbar-header">
    </div>
    <div class="navbar-container container-fluid">

      <!-- Navbar Collapse -->
      <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">

        <!-- Navbar Toolbar Right -->
        <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">
          <li class="nav-item dropdown">
            <a class="nav-link icon md-search" data-toggle="collapse" href="#" data-target="#site-navbar-search"
            role="button">
              <span class="sr-only">Toggle Search</span>
            </a>
          </li>
          <li class="nav-item dropdown"> &nbsp; </li>
        </ul>
        <!-- End Navbar Toolbar Right -->

      </div>


      <!-- End Navbar Collapse -->
      <!-- Site Navbar Seach -->
      {{--<div class="collapse navbar-search-overlap" id="site-navbar-search">--}}
        {{--{{ Form::open(['route' => ['search.live'], 'method' => 'post', 'id' => 'liveform', 'name' => 'liveform', "class" => "form-horizontal"]) }}--}}
          {{--<div class="form-group">--}}
            {{--<div class="input-search">--}}
              {{--<i class="input-search-icon md-search" aria-hidden="true"></i>--}}
              {{--<input type="text" name="site-search" class='form-control' id="liveSearch" placeholder="Type to start searching ...">--}}
              {{--<button type="button" class="input-search-close icon md-close" id="searchClose" data-target="#site-navbar-search"--}}
              {{--data-toggle="collapse" aria-label="Close"></button>--}}
            {{--</div>--}}
          {{--</div>--}}
        {{--{{ Form::close() }}--}}
        {{--<div class="ls_div"></div>--}}
      {{--</div>--}}
      <!-- End Site Navbar Seach -->
    </div>
</nav>


<div class="site-menubar">
    <div class="site-menubar-body">
        <div>
            <div>
                <ul class="site-menu" data-plugin="menu">
                    <li class="site-menu-category">AD Management</li>
                    <li class="site-menu-item">
                        <a href="javascript:void(0)">
                            <a class="animsition-link" href="{{ url('dashboard') }}">
                                <i class="site-menu-icon md-home" aria-hidden="true"></i>
                                <span class="site-menu-title">Dashboard</span>
                            </a>
                        </a>
                    </li>



                    <li class="site-menu-category">Admin</li>
                    @can('user-display', '')
                        <li class="site-menu-item">
                            <a class="animsition-link" href="{{ url('users') }}">
                                <i class="site-menu-icon md-settings" aria-hidden="true"></i>
                                <span class="site-menu-title">@lang('site.leftMenu.users')</span>
                            </a>
                        </li>
                    @endcan

                    @can('role-display', '')
                        <li class="site-menu-item">
                            <a class="animsition-link" href="{{ url('roles') }}">
                                <i class="site-menu-icon md-settings" aria-hidden="true"></i>
                                <span class="site-menu-title">@lang('site.leftMenu.roles')</span>
                            </a>
                        </li>
                    @endcan
                </ul>
            </div>
        </div>
    </div>
    <div class="site-menubar-footer">
        <a href="/settings/" class="fold-show" data-placement="top" data-toggle="tooltip"
           data-original-title="Settings">
            <span class="icon md-settings" aria-hidden="true"></span>
        </a>
        <a  href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"  data-placement="top" data-toggle="tooltip" data-original-title="Logout">
            <span class="icon md-power" aria-hidden="true"></span>
        </a>
        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
          {{ csrf_field() }}
        </form>
    </div>
</div>
