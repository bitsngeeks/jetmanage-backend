function script() {
    $("html").addClass('js');
    var fileInput = $(".input-file"),
        button = $(".input-file-trigger"),
        the_return = $(".file-return");


    button.on("keydown", function(event) {
        if (event.keyCode == 13 || event.keyCode == 32) {
            fileInput.focus();
        }
    });
    button.on("click", function(event) {
        fileInput.focus();
        return false;
    });
    fileInput.on("change", function(event) {
        the_return[$(this).data("id")].innerHTML = this.value;
    });

    $('.md-form label').on('click', function() {
        $(this).parent().find('input').focus();
    });
    $('.md-form input, .md-form textarea').on('focus', function() {


        $(this).next('label').addClass('active');
    });
    $('.md-form input, .md-form textarea').focusout(function() {
        $(this).next('label').removeClass('active');
    })


    $('[data-toggle="tooltip"]').tooltip();


    $('textarea, input[type="text"]').on('focusout', function() {
        if ($(this).val() == "") {
            $(this).next('label').show();
        } else {
            $(this).next('label').hide();
        }
    })
    $('textarea, input[type="number"]').on('focusout', function() {
        if ($(this).val() == "") {
            $(this).next('label').show();
        } else {
            $(this).next('label').hide();
        }
    })
    $(document).ready(function() {
        $('input[type="text"]').each(function(index, item) {

            if ($(item).val() !== '') {
                $(this).next('label').hide();
            } else {

                $(item).next('label').show();
            }
        });
        $('input[type="number"]').each(function(index, item) {

            if ($(item).val() !== '') {
                $(this).next('label').hide();
            } else {

                $(item).next('label').show();
            }
        });
        

        $('input[data-daterange="true"]').datepicker({
            startDate: new Date(),
            orientation: "auto",
            templates: {
                leftArrow: '<i class="flaticon--basic-left-arrow"></i>',
                rightArrow: '<i class="flaticon--basic-right-arrow"></i>'
            },
            toggleActive: true,
            todayHighlight: true
        });

        $('input[data-daterange="true"]').datepicker().on("changeDate", function(e) {
                $(this).next().css('display', 'none')
                $(this).datepicker("hide");
        });
        $('input[data-daterange="past"]').datepicker({
            format: " yyyy-mm-dd",
            orientation: "auto",
            templates: {
                leftArrow: '<i class="flaticon--basic-left-arrow"></i>',
                rightArrow: '<i class="flaticon--basic-right-arrow"></i>'
            },
            toggleActive: true,
            todayHighlight: true
        });

        $('input[data-daterange="past"]').datepicker().on("changeDate", function(e) {
                $(this).next().css('display', 'none')
                $(this).datepicker("hide");
        });

        $('input[data-daterange="year"]').datepicker({
            format: " yyyy", // Notice the Extra space at the beginning
            viewMode: "years", 
            minViewMode: "years",
            templates: {
                leftArrow: '<i class="flaticon--basic-left-arrow"></i>',
                rightArrow: '<i class="flaticon--basic-right-arrow"></i>'
            },
            toggleActive: true,
            todayHighlight: true
        });

        $('input[data-daterange="year"]').datepicker().on("changeDate", function(e) {
                $(this).next().css('display', 'none')
                $(this).datepicker("hide");
        });


        $('.tooltip').attr('hidden', true);
        $('a.routerLink').click(function() {
            $('.tooltip').attr('hidden', true);
        })

        $('input[data-datetimerange="true"]').datetimepicker();

    });
}