<!-- Ind Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ind_id', 'Ind Id:') !!}
    {!! Form::number('ind_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone', 'Phone:') !!}
    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Phone Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone_type', 'Phone Type:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('phone_type', false) !!}
        {!! Form::checkbox('phone_type', '1', null) !!} 1
    </label>
</div>

<!-- Is Textable Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_textable', 'Is Textable:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_textable', false) !!}
        {!! Form::checkbox('is_textable', '1', null) !!} 1
    </label>
</div>

<!-- Is Verified Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_verified', 'Is Verified:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_verified', false) !!}
        {!! Form::checkbox('is_verified', '1', null) !!} 1
    </label>
</div>

<!-- Is Primary Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_primary', 'Is Primary:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('is_primary', false) !!}
        {!! Form::checkbox('is_primary', '1', null) !!} 1
    </label>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('phones.index') !!}" class="btn btn-default">Cancel</a>
</div>
