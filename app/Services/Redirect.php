<?php

namespace App\Services;

trait Redirect
{

    /**
     * Sending redirect value.
     *
     * @author Jesse V
     * @param  string $type    Which type of flash message
     * @param  string $message Flash message text
     * @param  string $url
     */
    public function send($type = "", $message = "", $url = "")
    {
        if (!isset($this->controllerRoute)) {
            $send = 'dashboard';
            $type = 'error';
            $message = 'Redirect controller route is not set';
        } else {
            $send = $this->controllerRoute;
        }

        $send = ($url != "") ? $url : $send;

        return \Redirect::to($send)->with($type, $message);
    }

    /**
     * Setting error message with redirect url.
     *
     * @author Jesse V
     * @param  string $message
     * @param  string $url
     */
    public function error($message = "", $url = "")
    {
        return $this->send('error', $message, $url);
    }

    /**
     * Setting success message with redirect url.
     *
     * @author Jesse V
     * @param  string $message
     * @param  string $url
     */
    public function success($message = "", $url = "")
    {
        return $this->send('success', $message, $url);
    }

    /**
     * Sending back is success
     *
     * @author Jesse V
     * @param  string $message
     * @return \Illuminate\Http\RedirectResponse
     */
    public function backSuccess($message = "")
    {
        return back()->with('success', $message);
    }

    /**
     * Sending back is error
     *
     * @author Jesse V
     * @param  string $message
     * @return \Illuminate\Http\RedirectResponse
     */
    public function backError($message = "")
    {
        return back()->with('error', $message);
    }
}
