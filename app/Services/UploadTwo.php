<?php
namespace App\Library\Services;

use App\Services\Uploads\UploadServiceInterface;

class UploadTwo implements UploadServiceInterface
{
    public function upload()
    {
        return 'Output from TWO';
    }
}