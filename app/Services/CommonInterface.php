<?php

namespace App\Services;

Interface CommonInterface
{

    /**
     * Setthing the current controller for service class.
     *
     * @author Jesse V
     * @param  string $route
     * @return void
     */
    public function setControllerRoute($route);

    /**
     * Getting the current controller.
     *
     * @author Jesse V
     * @return string
     */
    public function getControllerRoute();

    /**
     * Setting the model for current service class.
     *
     * @author Jesse V
     * @param  $model
     * @return void
     */
    public function setModel($model);

    /**
     * Getting the model.
     *
     * @author Jesse V
     * @return string
     */
    public function getModel();

    /**
     * Common method to find rows or row of any model by id.
     *
     * @author Jesse V
     * @param  integer $id
     * @return collection
     */
    public function findById($id);
}
