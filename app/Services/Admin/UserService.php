<?php

namespace App\Services\Admin;

use App\Models\User;
use App\Services\Common;
use App\Services\Redirect;
use App\Services\CommonInterface;

class UserService implements CommonInterface
{
    use Redirect, Common;

    public function __construct()
    {
        // Setting up current model.
        $this->setModel(new User);

        // Setting up route redirect for the controller.
        $this->setControllerRoute('users');
    }

    /**
     * Getting list of user with their role.
     *
     * @author Jesse V
     * @param  integer $page
     * @return collection
     */
    public function getUserWithRoles($page = 20)
    {
        return User::getAllUsersAndRoles();
    }

    /**
     * Creating user.
     *
     * @author Jesse V
     * @param  array $request
     * @return boolean
     */
    public function create($request)
    {
        $user = new User;

        $user = assignRequestValue($request, $user, [
            'name',
            'email'
        ]);
        $user->password = bcrypt($request['password']);
        $user->save();

        return true;
    }

    /**
     * Updating user.
     *
     * @author Jesse V
     * @param  array   $request
     * @param  integer $userId
     * @return boolean
     */
    public function update($request, $userId)
    {
        $user = $this->findById($userId);

        $user = assignRequestValue($request, $user, [
            'name',
        ]);

        if ($request['password'] != "")
            $user->password = bcrypt($request['password']);

        $user->save();

        return true;
    }

    /**
     * Assigning roles to user.
     *
     * @author Jesse V
     * @param  array   $roles
     * @param  integer $userId
     * @return boolean
     */
    public function assignRolesToUser($roles, $userId)
    {
        $user = $this->findById($userId);
        $user->assignRolesToUser($roles);

        return true;
    }

}
