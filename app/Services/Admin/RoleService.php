<?php

namespace App\Services\Admin;

use App\Models\Role;
use App\Models\Permission;
use App\Services\Common;
use App\Services\Redirect;
use App\Services\CommonInterface;

class RoleService implements CommonInterface
{
    use Redirect, Common;

    public function __construct()
    {
        // Setting up current model.
        $this->setModel(new Role);

        // Setting up route redirect for the controller.
        $this->setControllerRoute('roles');
    }

    /**
     * Getting list of roles.
     *
     * @author Jesse V
     * @param  integer $page
     * @return collection
     */
    public function getRoleList($page = 20)
    {
        return Role::oldest('label')->get();
    }

    /**
     * Ajax call to get permission json.
     *
     * @author Jesse V
     * @param  integer $id
     * @return collection
     */
    public function getPermssions($id)
    {
        return Permission::treeJson($id);
    }

    /**
     * Create new role
     *
     * @author Jesse V
     * @param  array $request
     * @return bool
     */
    public function create($request)
    {
        $role = Role::createRole(str_slug($request['label'], '-'), $request['label']);

        // Assigning permission to roles.
        $role->assignPermissions($request['jsfields']);

        return true;
    }

    /**
     * Updating products.
     *
     * @author Jesse V
     * @param  array   $request
     * @param  integer $permissionId
     * @return boolean
     */
    public function update($request, $permissionId)
    {
        $role = $this->findById($permissionId);
        $role->name = str_slug($request['label'], '-');
        $role->label = $request['label'];
        $role->save();

        // Assigning permission to roles.
        $role->assignPermissions($request['jsfields']);

        return true;
    }
}
