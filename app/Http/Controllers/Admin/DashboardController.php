<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @author Jesse V
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // For header.
        $page = [trans('breadcrumbs.dashboard'), 'dashboardIndex'];

        return view('admin.dashboard', compact('page'));
    }
}
