<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateLegRequest;
use App\Http\Requests\UpdateLegRequest;
use App\Repositories\LegRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class LegController extends AppBaseController
{
    /** @var  LegRepository */
    private $legRepository;

    public function __construct(LegRepository $legRepo)
    {
        $this->legRepository = $legRepo;
    }

    /**
     * Display a listing of the Leg.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->legRepository->pushCriteria(new RequestCriteria($request));
        $legs = $this->legRepository->all();

        return view('legs.index')
            ->with('legs', $legs);
    }

    /**
     * Show the form for creating a new Leg.
     *
     * @return Response
     */
    public function create()
    {
        return view('legs.create');
    }

    /**
     * Store a newly created Leg in storage.
     *
     * @param CreateLegRequest $request
     *
     * @return Response
     */
    public function store(CreateLegRequest $request)
    {
        $input = $request->all();

        $leg = $this->legRepository->create($input);

        Flash::success('Leg saved successfully.');

        return redirect(route('legs.index'));
    }

    /**
     * Display the specified Leg.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $leg = $this->legRepository->findWithoutFail($id);

        if (empty($leg)) {
            Flash::error('Leg not found');

            return redirect(route('legs.index'));
        }

        return view('legs.show')->with('leg', $leg);
    }

    /**
     * Show the form for editing the specified Leg.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $leg = $this->legRepository->findWithoutFail($id);

        if (empty($leg)) {
            Flash::error('Leg not found');

            return redirect(route('legs.index'));
        }

        return view('legs.edit')->with('leg', $leg);
    }

    /**
     * Update the specified Leg in storage.
     *
     * @param  int              $id
     * @param UpdateLegRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLegRequest $request)
    {
        $leg = $this->legRepository->findWithoutFail($id);

        if (empty($leg)) {
            Flash::error('Leg not found');

            return redirect(route('legs.index'));
        }

        $leg = $this->legRepository->update($request->all(), $id);

        Flash::success('Leg updated successfully.');

        return redirect(route('legs.index'));
    }

    /**
     * Remove the specified Leg from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $leg = $this->legRepository->findWithoutFail($id);

        if (empty($leg)) {
            Flash::error('Leg not found');

            return redirect(route('legs.index'));
        }

        $this->legRepository->delete($id);

        Flash::success('Leg deleted successfully.');

        return redirect(route('legs.index'));
    }
}
