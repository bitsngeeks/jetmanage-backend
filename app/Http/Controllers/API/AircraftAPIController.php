<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAircraftAPIRequest;
use App\Http\Requests\API\UpdateAircraftAPIRequest;
use App\Models\Aircraft;
use App\Repositories\AircraftRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class AircraftController
 * @package App\Http\Controllers\API
 */

class AircraftAPIController extends AppBaseController
{
    /** @var  AircraftRepository */
    private $aircraftRepository;

    public function __construct(AircraftRepository $aircraftRepo)
    {
        $this->aircraftRepository = $aircraftRepo;
    }

    /**
     * Display a listing of the Aircraft.
     * GET|HEAD /aircrafts
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->aircraftRepository->pushCriteria(new RequestCriteria($request));
        $this->aircraftRepository->pushCriteria(new LimitOffsetCriteria($request));
        $aircrafts = $this->aircraftRepository->all();

        return $this->sendResponse($aircrafts->toArray(), 'Aircrafts retrieved successfully');
    }

    /**
     * Store a newly created Aircraft in storage.
     * POST /aircrafts
     *
     * @param CreateAircraftAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateAircraftAPIRequest $request)
    {
        $input = $request->all();

        $aircrafts = $this->aircraftRepository->create($input);

        return $this->sendResponse($aircrafts->toArray(), 'Aircraft saved successfully');
    }

    /**
     * Display the specified Aircraft.
     * GET|HEAD /aircrafts/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Aircraft $aircraft */
        $aircraft = $this->aircraftRepository->findWithoutFail($id);

        if (empty($aircraft)) {
            return $this->sendError('Aircraft not found');
        }

        return $this->sendResponse($aircraft->toArray(), 'Aircraft retrieved successfully');
    }

    /**
     * Update the specified Aircraft in storage.
     * PUT/PATCH /aircrafts/{id}
     *
     * @param  int $id
     * @param UpdateAircraftAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAircraftAPIRequest $request)
    {
        $input = $request->all();

        /** @var Aircraft $aircraft */
        $aircraft = $this->aircraftRepository->findWithoutFail($id);

        if (empty($aircraft)) {
            return $this->sendError('Aircraft not found');
        }

        $aircraft = $this->aircraftRepository->update($input, $id);

        return $this->sendResponse($aircraft->toArray(), 'Aircraft updated successfully');
    }

    /**
     * Remove the specified Aircraft from storage.
     * DELETE /aircrafts/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Aircraft $aircraft */
        $aircraft = $this->aircraftRepository->findWithoutFail($id);

        if (empty($aircraft)) {
            return $this->sendError('Aircraft not found');
        }

        $aircraft->delete();

        return $this->sendResponse($id, 'Aircraft deleted successfully');
    }
}
