<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateLegAPIRequest;
use App\Http\Requests\API\UpdateLegAPIRequest;
use App\Models\Leg;
use App\Repositories\LegRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class LegController
 * @package App\Http\Controllers\API
 */

class LegAPIController extends AppBaseController
{
    /** @var  LegRepository */
    private $legRepository;

    public function __construct(LegRepository $legRepo)
    {
        $this->legRepository = $legRepo;
    }

    /**
     * Display a listing of the Leg.
     * GET|HEAD /legs
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->legRepository->pushCriteria(new RequestCriteria($request));
        $this->legRepository->pushCriteria(new LimitOffsetCriteria($request));
        $legs = $this->legRepository->all();

        return $this->sendResponse($legs->toArray(), 'Legs retrieved successfully');
    }

    /**
     * Store a newly created Leg in storage.
     * POST /legs
     *
     * @param CreateLegAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateLegAPIRequest $request)
    {
        $input = $request->all();

        $legs = $this->legRepository->create($input);

        return $this->sendResponse($legs->toArray(), 'Leg saved successfully');
    }

    /**
     * Display the specified Leg.
     * GET|HEAD /legs/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Leg $leg */
        $leg = $this->legRepository->findWithoutFail($id);

        if (empty($leg)) {
            return $this->sendError('Leg not found');
        }

        return $this->sendResponse($leg->toArray(), 'Leg retrieved successfully');
    }

    /**
     * Update the specified Leg in storage.
     * PUT/PATCH /legs/{id}
     *
     * @param  int $id
     * @param UpdateLegAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLegAPIRequest $request)
    {
        $input = $request->all();

        /** @var Leg $leg */
        $leg = $this->legRepository->findWithoutFail($id);

        if (empty($leg)) {
            return $this->sendError('Leg not found');
        }
        $input['depart_date'] = substr(str_replace('T', ' ', $input['depart_date']), 0, -5);
        $input['arrival_date'] = substr(str_replace('T', ' ', $input['arrival_date']), 0, -5);

        $leg = $this->legRepository->update($input, $id);

        return $this->sendResponse($leg->toArray(), 'Leg updated successfully');
    }

    /**
     * Remove the specified Leg from storage.
     * DELETE /legs/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Leg $leg */
        $leg = $this->legRepository->findWithoutFail($id);

        if (empty($leg)) {
            return $this->sendError('Leg not found');
        }

        $leg->delete();

        return $this->sendResponse($id, 'Leg deleted successfully');
    }
}
