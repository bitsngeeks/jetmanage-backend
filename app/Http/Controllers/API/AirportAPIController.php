<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAirportAPIRequest;
use App\Http\Requests\API\UpdateAirportAPIRequest;
use App\Models\Airport;
use App\Repositories\AirportRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class AirportController
 * @package App\Http\Controllers\API
 */

class AirportAPIController extends AppBaseController
{
    /** @var  AirportRepository */
    private $airportRepository;

    public function __construct(AirportRepository $airportRepo)
    {
        $this->airportRepository = $airportRepo;
    }

    /**
     * Display a listing of the Airport.
     * GET|HEAD /airports
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->airportRepository->pushCriteria(new RequestCriteria($request));
        $this->airportRepository->pushCriteria(new LimitOffsetCriteria($request));
        $airports = $this->airportRepository->all();

        return $this->sendResponse($airports->toArray(), 'Airports retrieved successfully');
    }

    /**
     * Store a newly created Airport in storage.
     * POST /airports
     *
     * @param CreateAirportAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateAirportAPIRequest $request)
    {
        $input = $request->all();

        $airports = $this->airportRepository->create($input);

        return $this->sendResponse($airports->toArray(), 'Airport saved successfully');
    }

    /**
     * Display the specified Airport.
     * GET|HEAD /airports/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Airport $airport */
        $airport = $this->airportRepository->findWithoutFail($id);
        if (empty($airport)) {
            return $this->sendError('Airport not found');
        }

        return $this->sendResponse($airport->toArray(), 'Airport retrieved successfully');
    }

    /**
     * Update the specified Airport in storage.
     * PUT/PATCH /airports/{id}
     *
     * @param  int $id
     * @param UpdateAirportAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAirportAPIRequest $request)
    {
        $input = $request->all();

        /** @var Airport $airport */
        $airport = $this->airportRepository->findWithoutFail($id);

        if (empty($airport)) {
            return $this->sendError('Airport not found');
        }

        $airport = $this->airportRepository->update($input, $id);

        return $this->sendResponse($airport->toArray(), 'Airport updated successfully');
    }

    /**
     * Remove the specified Airport from storage.
     * DELETE /airports/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Airport $airport */
        $airport = $this->airportRepository->findWithoutFail($id);

        if (empty($airport)) {
            return $this->sendError('Airport not found');
        }

        $airport->delete();

        return $this->sendResponse($id, 'Airport deleted successfully');
    }


    public function search($query) {
        if(strlen($query) < 3)
            return $this->sendResponse($query, 'Too Short');

        $drivers = Airport::where('icao', 'like', "%{$query}%")->orWhere('city', 'like', "%{$query}%")->get();
        $formattedData = [];

        foreach($drivers as $driver) {
            if(empty($driver['icao']))
                continue;
            $driver['formattedName'] = strtoupper($driver['icao']). '-' .strtoupper($driver['name']);
            $formattedData[] = $driver;
        }
        return Response::json($formattedData);
    }
}
