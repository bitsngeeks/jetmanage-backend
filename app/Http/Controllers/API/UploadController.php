<?php

namespace App\Http\Controllers\API;

use App\Providers\UploadServiceProvider;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Token;
use Uploader;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\AppBaseController;
use App\Services\Zipper;
use App\Services\Uploads\UploadServiceInterface;
use App\Repositories\IndividualRepository as IdvRepo;
use App\Repositories\CompanyRepository as CompanyRepo;
use App\Repositories\FileUploadRepository as FileUploadRepository;

class UploadController extends AppBaseController
{

    private $userInfo;
    private $idvRepo;
    private $companyRepo;
    private $fileUploadRepository;

    public function __construct(AuthController $authController, Request $request, IdvRepo $idvRepo, CompanyRepo $companyRepo, FileUploadRepository $fileUploadRepository)
    {
        $this->userInfo             = $authController->getUserObject($request->input('token'));
        $this->idvRepo              = $idvRepo;
        $this->companyRepo          = $companyRepo;
        $this->fileUploadRepository = $fileUploadRepository;
    }

    public function uploadDocument(Request $request, AuthController $authController) {
        Uploader::from('request')->uploadTo('local')->upload('file', function ($filename) {
            global $request;

            $orgFileName = $request->file('file')->getClientOriginalName();
            $request->file('file')->move(base_path() . '/storage/upload/', $orgFileName);
            $individual = $this->idvRepo->findWithoutFail($this->userInfo->id);
            $company = $this->companyRepo->findWithoutFail($individual->company_id);
            $zipper = new Zipper;
            $files = glob(base_path() . '/storage/app/*');

            $zipPassword = $company->hash;

            $uploadedFilename = md5(rand(0000,9999)).time();
            $zipper->make(base_path() . '/storage/app/'.$uploadedFilename.'.zip')->add($files)->close();

            $fileData = array();
            $fileData['document_id'] = 0;
            $fileData['filename'] = $filename;
            $fileData['mime'] = '';
            $fileData['original_filename'] = $orgFileName;

            $this->fileUploadRepository->create($fileData);

        });
    }
}