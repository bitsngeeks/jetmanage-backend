<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAircraftRequest;
use App\Http\Requests\UpdateAircraftRequest;
use App\Repositories\AircraftRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class AircraftController extends AppBaseController
{
    /** @var  AircraftRepository */
    private $aircraftRepository;

    public function __construct(AircraftRepository $aircraftRepo)
    {
        $this->aircraftRepository = $aircraftRepo;
    }

    /**
     * Display a listing of the Aircraft.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->aircraftRepository->pushCriteria(new RequestCriteria($request));
        $aircrafts = $this->aircraftRepository->all();

        return view('aircrafts.index')
            ->with('aircrafts', $aircrafts);
    }

    /**
     * Show the form for creating a new Aircraft.
     *
     * @return Response
     */
    public function create()
    {
        return view('aircrafts.create');
    }

    /**
     * Store a newly created Aircraft in storage.
     *
     * @param CreateAircraftRequest $request
     *
     * @return Response
     */
    public function store(CreateAircraftRequest $request)
    {
        $input = $request->all();

        $aircraft = $this->aircraftRepository->create($input);

        Flash::success('Aircraft saved successfully.');

        return redirect(route('aircrafts.index'));
    }

    /**
     * Display the specified Aircraft.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $aircraft = $this->aircraftRepository->findWithoutFail($id);

        if (empty($aircraft)) {
            Flash::error('Aircraft not found');

            return redirect(route('aircrafts.index'));
        }

        return view('aircrafts.show')->with('aircraft', $aircraft);
    }

    /**
     * Show the form for editing the specified Aircraft.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $aircraft = $this->aircraftRepository->findWithoutFail($id);

        if (empty($aircraft)) {
            Flash::error('Aircraft not found');

            return redirect(route('aircrafts.index'));
        }

        return view('aircrafts.edit')->with('aircraft', $aircraft);
    }

    /**
     * Update the specified Aircraft in storage.
     *
     * @param  int              $id
     * @param UpdateAircraftRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAircraftRequest $request)
    {
        $aircraft = $this->aircraftRepository->findWithoutFail($id);

        if (empty($aircraft)) {
            Flash::error('Aircraft not found');

            return redirect(route('aircrafts.index'));
        }

        $aircraft = $this->aircraftRepository->update($request->all(), $id);

        Flash::success('Aircraft updated successfully.');

        return redirect(route('aircrafts.index'));
    }

    /**
     * Remove the specified Aircraft from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $aircraft = $this->aircraftRepository->findWithoutFail($id);

        if (empty($aircraft)) {
            Flash::error('Aircraft not found');

            return redirect(route('aircrafts.index'));
        }

        $this->aircraftRepository->delete($id);

        Flash::success('Aircraft deleted successfully.');

        return redirect(route('aircrafts.index'));
    }
}
