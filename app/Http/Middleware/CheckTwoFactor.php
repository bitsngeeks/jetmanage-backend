<?php
namespace App\Http\Middleware;
use Closure;
use JWTAuth;
use App\Http\Controllers\API\AuthController;
use App\Token;

class CheckTwoFactor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $requestCopy = $request;
        // Create MD5 Hash from Salt and User Agent
        $twoToken = md5(env('TWO_TOKEN_SALT', null) . str_replace(' ', '', $requestCopy->server('HTTP_USER_AGENT')));
        // Check if twotoken cookie is set
        if($request->header('twotoken'))
        {
            $twoTokenCookie = $request->header('twotoken');
            // If twotoken cookie is valid, continue with new request
            if($twoTokenCookie == $twoToken) {
                return $next($request);
            }
            else {
                // Invalid Two Token
                return response()->json(['status' => 'invalid_2fa'], '200')->withCookie(cookie()->forget('twotoken'));
            }
        }
        else
        {
            $twoFactorPost = $requestCopy->input('twotoken');
            // If Two Factor POST var is set
            if(isset($twoFactorPost)) {
                // If Two Factor var is numeric
                if(!is_numeric($requestCopy->input('twotoken')))
                    return response()->json(['token_invalid' => 'TwoFactor not Numeric'], '200');
                // If Two Token matches a database entry, set cookie and move on
                if(Token::where('code', $requestCopy->input('twotoken') )->first() != null) {
                    return response()->json(['twotoken' => $twoToken], '200');
                }
                else {
                    // Send token and return json message
                    $authController = new AuthController();
                    $userObject = $authController->getUserObject($requestCopy->input('token'));
                    $token = Token::create([
                        'user_id' => $userObject->id
                    ]);
                    $token->sendCode();
                    return response()->json(['status' => '2fa_1'], '200');
                }
            }
            else {
                // Send token and return json message
                $authController = new AuthController();
                $userObject = $authController->getUserObject($requestCopy->input('token'));
                $token = Token::create([
                    'user_id' => $userObject->id
                ]);
                $token->sendCode();
                return response()->json(['status' => '2fa_2'], '200');
            }
        }
    }
}