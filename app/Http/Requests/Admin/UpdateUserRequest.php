<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return boolean
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if($this->request->all() ['password'] == "" && $this->request->all() ['password_confirmation'] == ""){
            $rule = [];
        }
        else{
            $rule = [
                'password' => 'required|min:6|confirmed',
            ];
        }

        return $rule;
    }
}
