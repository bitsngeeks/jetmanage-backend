<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Passenger
 * @package App\Models
 * @version October 31, 2017, 2:27 pm UTC
 *
 * @property \App\Models\Individual individual
 * @property \App\Models\Leg leg
 * @property \App\Models\Trip trip
 * @property \Illuminate\Database\Eloquent\Collection legs
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection roleUser
 * @property integer leg_id
 * @property integer trip_id
 * @property integer ind_id
 */
class Passenger extends Model
{
    use SoftDeletes;

    public $table = 'passengers';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'leg_id',
        'trip_id',
        'ind_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'leg_id' => 'integer',
        'trip_id' => 'integer',
        'ind_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function individual()
    {
        return $this->belongsTo(\App\Models\Individual::class, 'ind_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function leg()
    {
        return $this->belongsTo(\App\Models\Leg::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function trip()
    {
        return $this->belongsTo(\App\Models\Trip::class);
    }
}
