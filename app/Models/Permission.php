<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    use Common;

    /**
     * Many roles are associated to many permissions.
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }


    /**
     * Getting permission by name.
     *
     * @author Jesse V
     * @param  string $name
     * @return collection
     */
    public static function getPermission($name = "")
    {
        return self::where('name', $name)->first();
    }

    /**
     * Creating permission.
     *
     * @author Jesse V
     * @param  string $name
     * @param  string $label
     * @return collection
     */
    public static function createPermission($name, $label)
    {
        if (strpos($name, "-") === false)
            $name .= "-index";

        $permissionId = self::createRow('permissions', $name, $label);

        return $permission = Permission::find($permissionId);
    }

    /**
     * Create permission from array.
     *
     * @author Jesse V
     * @param  array $permission Array containing permission values
     * @return array             Returning array with permission object
     */
    public static function createPermissions($permission)
    {
        $permissions = [];
        foreach ($permission as $name => $label) {
            $permissions[$name] = self::createPermission($name, $label);
        }

        return $permissions;
    }

    /**
     * Adding CRUD permissions for a single role.
     *
     * @author Jesse V
     * @param  array  $crud  Array containing which CRUD permission is needed
     * @param  string $name  Actual name or slug for permission
     * @param  string $label Label for the permission
     * @return array/void        Array containing permission object
     */
    public static function createPermissionCRUD($crud, $name, $label)
    {
        $labels = [
            'c' => "Create",
            'r' => "Display",
            'u' => "Update",
            'd' => "Delete",
        ];

        if (is_array($crud) && count($crud) >= 1) {

            // Only getting selected values for crud operation.
            $crudArray = array_intersect(['c','r','u','d'], $crud);
            foreach ($crudArray as $operation) {
                $slug = strtolower($name."-".$labels[$operation]);
                $ret[$slug] = self::createPermission(
                    $slug,
                    $label." ".$labels[$operation]
                );
            }

            return $ret;
        }
    }

    /**
     * Method to create CRUD for a specific permission.
     *
     * @author Jesse V
     * @param  string $name  Actual name or slug for permission
     * @param  string $label Label for the permission
     * @return array         Array containing permission object
     */
    public static function createPermissionCRUDComplete($name, $label)
    {
        return self::createPermissionCRUD(['c', 'r', 'u', 'd'], $name, $label);
    }

    /**
     * Creating CRUD for controller.
     *
     * @author Jesse V
     * @param  array $crud Array with index as slug and value as label
     * @return array       Array containing permission object
     */
    public static function createCruds($crud)
    {
        $permssionObjects = [];
        foreach ($crud as $name => $label) {
            $permssionObjects[$name] = self::createPermissionCRUDComplete($name, $label);
        }

        return $permssionObjects;
    }

    /**
     * Getting all permission related to a specific role.
     *
     * @author Jesse V
     * @param  integer $permissionId Permission id
     * @return array/null
     */
    public static function getPermissionIds($permissionId)
    {
        if ($permissionId) {
            $allPermissions = Role::with('permissions')->find($permissionId);
            $permissionId = [];
            foreach ($allPermissions->permissions as $permission) {
                $permissionId[] = $permission->id;
            }

            return $permissionId;
        }

        return null;
    }

    /**
     * Creating json format for jtree ajax call.
     *
     * @author Jesse V
     * @param  integer $permissionId Permission id
     * @return array                 Array for jtree ajax object
     */
    public static function treeJson($permissionId = null)
    {
        // Get all permissions.
        $permissions = self::all();

        // Getting role of permission ids.
        $rolePermission = self::getPermissionIds($permissionId);

        $treeArray = [];
        foreach ($permissions as $key => $permission) {
            list($mainkey, $val) = explodePermissionName($permission->name);

            // Getting only children.
            $treeArray[$mainkey]['children'][] = self::getTreeNode(
                $permission->label,
                $permission->id,
                false,
                $rolePermission
            );
        }

        $retArray = [];
        foreach ($treeArray as $nodeKey => $nodeArray) {
            $result = self::getTreeNode(ucfirst($nodeKey), $nodeKey, true);
            $result['children'] = $nodeArray['children'];
            $retArray[] = $result;
        }

        return $retArray;
    }

    /**
     * Getting single tree note, which can be open or close.
     *
     * @author Jesse V
     * @param  string  $label           Label to show on web page
     * @param  integer $id              Permission id
     * @param  boolean $open            Should be opened or closed
     * @param  array   $permissionArray Array of all permissions
     * @return array
     */
    public static function getTreeNode(
        $label,
        $id = null,
        $open = false,
        $permissionArray = null
    )
    {
        $node = [
            'id'   => $id,
            'text' => $label,
            'icon' => 'wb-folder',
        ];

        if (is_array($permissionArray) && in_array($id, $permissionArray)) {
            $node['state'] = ['selected' => true];
        }

        if ($open === true) {
            $node['state'] = ['opened' => $open];
        }

        return $node;
    }
}
