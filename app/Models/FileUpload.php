<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class FileUpload
 * @package App\Models
 * @version October 10, 2017, 9:24 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection Document
 * @property \Illuminate\Database\Eloquent\Collection Individual
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection roleUser
 * @property string full_name
 * @property string iso_3166_2
 */
class FileUpload extends Model
{
    use SoftDeletes;

    public $table = 'file_uploads';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'id',
        'document_id',
        'filename',
        'mime',
        'original_filename'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */

    protected $casts = [
        'id' => 'integer',
        'document_id' => 'integer',
        'filename'  => 'string',
        'mime'  => 'string',
        'original_filename'  => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */

    public static $rules = [
    ];

}
