<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, UserRoles, Attribute, Common;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    /**
     * Getting user roles in an array.
     *
     * @author Jesse V
     * @return array
     */
    public function getRoleDropdownArray()
    {
        return createDropdownArray($this->roles, null, 'id');
    }

    /**
     * Getting a user by their role.
     *
     * @author Jesse V
     * @param  array      $roleName
     * @return collection
     */
    static function getUserByRole($roleName) {
        return self::whereHas('roles', function ($query) use ($roleName) {
            $query->where('name', $roleName);
        })->first();
    }

    /**
     * Getting a users by a single role.
     *
     * @author Jesse V
     * @param  array      $roleName
     * @return collection
     */
    static function getUsersByRole($roleName) {
        return self::whereHas('roles', function ($query) use ($roleName) {
            $query->where('name', $roleName);
        })->get();
    }

    /**
     * Getting all users with their role name.
     *
     * @author Jesse V
     * @return collection
     */
    public static function getAllUsersAndRoles()
    {
        $userRoles = self::with('roles')->latest()->get();//->paginate($page);

        return addNewAttributeToCollection($userRoles, 'roles', 'label', 'role_labels');
    }

    /**
     * Getting a single user with role as admin.
     *
     * @author Jesse V
     * @return collection
     */
    public static function getSuperAdmin()
    {
        return self::getRoles('super-admin')->first();
    }

    /**
     * Getting a single user with role as admin.
     *
     * @author Jesse V
     * @return collection
     */
    public static function getAdmin()
    {
        return self::getRoles('admin')->first();
    }

    /**
     * Creating user dropdown array.
     *
     * @author Jesse V
     * @return array
     */
    public static function createUserDropdownArray()
    {
        return createDropdownArray(self::all());
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function individual()
    {
        return $this->belongsTo(\App\Models\Individual::class);
    }
}
