<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Individual
 * @package App\Models
 * @version October 10, 2017, 9:24 am UTC
 *
 * @property \App\Models\Country country
 * @property \App\Models\Company company
 * @property \Illuminate\Database\Eloquent\Collection Address
 * @property \Illuminate\Database\Eloquent\Collection Email
 * @property \Illuminate\Database\Eloquent\Collection Note
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection Phone
 * @property \Illuminate\Database\Eloquent\Collection roleUser
 * @property string first_name
 * @property string middle_name
 * @property string last_name
 * @property boolean gender
 * @property string|\Carbon\Carbon dob
 * @property integer weight
 * @property integer height
 * @property integer res_country_id
 * @property integer birth_country_id
 * @property integer company_id
 */

class Individual extends Model
{
    use SoftDeletes;

    public $table = 'individuals';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'first_name',
        'middle_name',
        'last_name',
        'gender',
        'dob',
        'weight',
        'height',
        'res_country_id',
        'birth_country_id',
        'company_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'first_name' => 'string',
        'middle_name' => 'string',
        'last_name' => 'string',
        'gender' => 'boolean',
        'weight' => 'integer',
        'height' => 'integer',
        'res_country_id' => 'integer',
        'birth_country_id' => 'integer',
        'company_id' => 'integer'
    ];

    protected $eagerload = ["tags"];


    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function country()
    {
        return $this->belongsTo(\App\Models\Country::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function company()
    {
        return $this->belongsTo(\App\Models\Company::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function addresses()
    {
        return $this->hasMany(\App\Models\Address::class, 'ind_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function emails()
    {
        return $this->hasMany(\App\Models\Email::class, 'ind_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function notes()
    {
        return $this->hasMany(\App\Models\Note::class, 'ind_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function phones()
    {
        return $this->hasMany(\App\Models\Phone::class, 'ind_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function tags()
    {
        return $this->belongsToMany(\App\Models\Tag::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function users()
    {
        return $this->hasOne(\App\Models\User::class, 'ind_id');
    }
}
