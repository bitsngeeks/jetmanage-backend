<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Aircraft
 * @package App\Models
 * @version October 31, 2017, 2:25 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection Leg
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection roleUser
 * @property \Illuminate\Database\Eloquent\Collection Trip
 * @property string registration
 * @property string type_id
 * @property string serial_number
 * @property string name
 */
class Aircraft extends Model
{
    use SoftDeletes;

    public $table = 'aircrafts';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'registration',
        'type_id',
        'serial_number',
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'registration' => 'string',
        'type_id' => 'string',
        'serial_number' => 'string',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function legs()
    {
        return $this->hasMany(\App\Models\Leg::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function trips()
    {
        return $this->hasMany(\App\Models\Trip::class);
    }
}
