<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Leg
 * @package App\Models
 * @version October 31, 2017, 2:26 pm UTC
 *
 * @property \App\Models\Aircraft aircraft
 * @property \App\Models\Trip trip
 * @property \Illuminate\Database\Eloquent\Collection Passenger
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection roleUser
 * @property integer trip_id
 * @property integer trip_leg_order
 * @property integer from_airport_id
 * @property integer to_airport_id
 * @property string|\Carbon\Carbon depart_date
 * @property string|\Carbon\Carbon out_time
 * @property string|\Carbon\Carbon off_time
 * @property string|\Carbon\Carbon arrival_date
 * @property string|\Carbon\Carbon on_time
 * @property string|\Carbon\Carbon in_time
 * @property integer out_fuel
 * @property integer in_fuel
 * @property integer out_hobbs
 * @property integer in_hobbs
 * @property integer out_tach
 * @property integer in_tach
 * @property string regs
 * @property integer aircraft_id
 * @property string registration
 */
class Leg extends Model
{
    use SoftDeletes;

    public $table = 'legs';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'trip_id',
        'trip_leg_order',
        'from_airport_id',
        'to_airport_id',
        'depart_date',
        'out_time',
        'off_time',
        'arrival_date',
        'on_time',
        'in_time',
        'out_fuel',
        'in_fuel',
        'out_hobbs',
        'in_hobbs',
        'out_tach',
        'in_tach',
        'regs',
        'aircraft_id',
        'registration'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'trip_id' => 'integer',
        'trip_leg_order' => 'integer',
        'from_airport_id' => 'integer',
        'to_airport_id' => 'integer',
        'out_fuel' => 'integer',
        'in_fuel' => 'integer',
        'out_hobbs' => 'integer',
        'in_hobbs' => 'integer',
        'out_tach' => 'integer',
        'in_tach' => 'integer',
        'regs' => 'string',
        'aircraft_id' => 'integer',
        'registration' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function aircraft()
    {
        return $this->belongsTo(\App\Models\Aircraft::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function trip()
    {
        return $this->belongsTo(\App\Models\Trip::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function passengers()
    {
        return $this->hasMany(\App\Models\Passenger::class);
    }
    public function fromAirport()
    {
        return $this->hasMany(\App\Models\Airport::class, 'id', 'from_airport_id');
    }
    public function toAirport()
    {
        return $this->hasMany(\App\Models\Airport::class, 'id', 'to_airport_id');
    }

}
