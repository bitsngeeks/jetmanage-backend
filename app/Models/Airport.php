<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Airport
 * @package App\Models
 * @version November 8, 2017, 8:41 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection legs
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection roleUser
 * @property integer airport_entry_id
 * @property string code
 * @property string city
 * @property string country
 * @property string faaCode
 * @property string iata
 * @property string icao
 * @property string landingFacilityId
 * @property float latitude
 * @property float longitude
 * @property boolean typicallyHasMetar
 * @property boolean typicallyHasTaf
 * @property string name
 * @property string state
 * @property string zip
 * @property string fullCountryNameLowerCase
 * @property float magVarWest
 * @property string access
 * @property string type
 * @property boolean hasFuel
 * @property string fuelTypes
 * @property integer longestRunwayLength
 * @property string longestRunwaySurfaceType
 * @property string nearestlandingFacilityIds
 * @property float elevationFt
 * @property string procedureTypes
 * @property boolean towered
 */
class Airport extends Model
{

    public $table = 'airports';
    
    //const CREATED_AT = 'created_at';
    //const UPDATED_AT = 'updated_at';
    public $timestamps = false;

    public $fillable = [
        'airport_entry_id',
        'code',
        'city',
        'country',
        'faaCode',
        'iata',
        'icao',
        'landingFacilityId',
        'latitude',
        'longitude',
        'typicallyHasMetar',
        'typicallyHasTaf',
        'name',
        'state',
        'zip',
        'fullCountryNameLowerCase',
        'magVarWest',
        'access',
        'type',
        'hasFuel',
        'fuelTypes',
        'longestRunwayLength',
        'longestRunwaySurfaceType',
        'nearestlandingFacilityIds',
        'elevationFt',
        'procedureTypes',
        'towered'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'airport_entry_id' => 'integer',
        'code' => 'string',
        'city' => 'string',
        'country' => 'string',
        'faaCode' => 'string',
        'iata' => 'string',
        'icao' => 'string',
        'landingFacilityId' => 'string',
        'latitude' => 'float',
        'longitude' => 'float',
        'typicallyHasMetar' => 'boolean',
        'typicallyHasTaf' => 'boolean',
        'name' => 'string',
        'state' => 'string',
        'zip' => 'string',
        'fullCountryNameLowerCase' => 'string',
        'magVarWest' => 'float',
        'access' => 'string',
        'type' => 'string',
        'hasFuel' => 'boolean',
        'fuelTypes' => 'string',
        'longestRunwayLength' => 'integer',
        'longestRunwaySurfaceType' => 'string',
        'nearestlandingFacilityIds' => 'string',
        'elevationFt' => 'float',
        'procedureTypes' => 'string',
        'towered' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
