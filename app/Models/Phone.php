<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Phone
 * @package App\Models
 * @version October 12, 2017, 12:30 pm UTC
 *
 * @property \App\Models\Individual individual
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection roleUser
 * @property integer ind_id
 * @property string phone
 * @property boolean phone_type
 * @property boolean is_textable
 * @property boolean is_verified
 * @property boolean is_primary
 */
class Phone extends Model
{
    use SoftDeletes;

    public $table = 'phones';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'ind_id',
        'phone',
        'phone_type',
        'is_textable',
        'is_verified',
        'is_primary'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'ind_id' => 'integer',
        'phone' => 'string',
        'phone_type' => 'boolean',
        'is_textable' => 'boolean',
        'is_verified' => 'boolean',
        'is_primary' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function individual()
    {
        return $this->belongsTo(\App\Models\Individual::class);
    }
}
