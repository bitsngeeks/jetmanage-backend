<?php

namespace App\Repositories;

use App\Models\Individual;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class IndividualRepository
 * @package App\Repositories
 * @version October 10, 2017, 9:24 am UTC
 *
 * @method Individual findWithoutFail($id, $columns = ['*'])
 * @method Individual find($id, $columns = ['*'])
 * @method Individual first($columns = ['*'])
*/
class IndividualRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'first_name',
        'middle_name',
        'last_name',
        'gender',
        'dob',
        'weight',
        'height',
        'res_country_id',
        'birth_country_id',
        'company_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Individual::class;
    }

    /**
     * @param array $columns
     * @return mixed
     */
    public function all($columns = array('*')) {
        return $this->model->get($columns);
    }
}
