<?php

namespace App\Repositories;

use App\Models\FileUpload;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class FileUpload
 * @package App\Repositories
 * @version October 10, 2017, 9:24 am UTC
 *
 * @method Company findWithoutFail($id, $columns = ['*'])
 * @method Company find($id, $columns = ['*'])
 * @method Company first($columns = ['*'])
*/
class FileUploadRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'name',
        'hash'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return FileUpload::class;
    }
}
