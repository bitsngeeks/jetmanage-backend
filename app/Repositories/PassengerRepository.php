<?php

namespace App\Repositories;

use App\Models\Passenger;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PassengerRepository
 * @package App\Repositories
 * @version October 31, 2017, 2:27 pm UTC
 *
 * @method Passenger findWithoutFail($id, $columns = ['*'])
 * @method Passenger find($id, $columns = ['*'])
 * @method Passenger first($columns = ['*'])
*/
class PassengerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'leg_id',
        'trip_id',
        'ind_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Passenger::class;
    }
}
