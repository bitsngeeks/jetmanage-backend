<?php

class SSREquipment
{

    /**
     * @var ADSCapabilities $ADSCapabilities
     */
    protected $ADSCapabilities = null;

    /**
     * @var boolean $ModeA
     */
    protected $ModeA = null;

    /**
     * @var boolean $ModeAC
     */
    protected $ModeAC = null;

    /**
     * @var ModeSCapabilities $ModeS
     */
    protected $ModeS = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ADSCapabilities
     */
    public function getADSCapabilities()
    {
      return $this->ADSCapabilities;
    }

    /**
     * @param ADSCapabilities $ADSCapabilities
     * @return SSREquipment
     */
    public function setADSCapabilities($ADSCapabilities)
    {
      $this->ADSCapabilities = $ADSCapabilities;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getModeA()
    {
      return $this->ModeA;
    }

    /**
     * @param boolean $ModeA
     * @return SSREquipment
     */
    public function setModeA($ModeA)
    {
      $this->ModeA = $ModeA;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getModeAC()
    {
      return $this->ModeAC;
    }

    /**
     * @param boolean $ModeAC
     * @return SSREquipment
     */
    public function setModeAC($ModeAC)
    {
      $this->ModeAC = $ModeAC;
      return $this;
    }

    /**
     * @return ModeSCapabilities
     */
    public function getModeS()
    {
      return $this->ModeS;
    }

    /**
     * @param ModeSCapabilities $ModeS
     * @return SSREquipment
     */
    public function setModeS($ModeS)
    {
      $this->ModeS = $ModeS;
      return $this;
    }

}
