<?php

class ArrayOfAltitudeChangeInformation implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var AltitudeChangeInformation[] $AltitudeChangeInformation
     */
    protected $AltitudeChangeInformation = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return AltitudeChangeInformation[]
     */
    public function getAltitudeChangeInformation()
    {
      return $this->AltitudeChangeInformation;
    }

    /**
     * @param AltitudeChangeInformation[] $AltitudeChangeInformation
     * @return ArrayOfAltitudeChangeInformation
     */
    public function setAltitudeChangeInformation(array $AltitudeChangeInformation = null)
    {
      $this->AltitudeChangeInformation = $AltitudeChangeInformation;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->AltitudeChangeInformation[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return AltitudeChangeInformation
     */
    public function offsetGet($offset)
    {
      return $this->AltitudeChangeInformation[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param AltitudeChangeInformation $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->AltitudeChangeInformation[] = $value;
      } else {
        $this->AltitudeChangeInformation[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->AltitudeChangeInformation[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return AltitudeChangeInformation Return the current element
     */
    public function current()
    {
      return current($this->AltitudeChangeInformation);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->AltitudeChangeInformation);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->AltitudeChangeInformation);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->AltitudeChangeInformation);
    }

    /**
     * Countable implementation
     *
     * @return AltitudeChangeInformation Return count of elements
     */
    public function count()
    {
      return count($this->AltitudeChangeInformation);
    }

}
