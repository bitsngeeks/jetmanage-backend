<?php

class ArrayOfFlightATCEETElement implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var FlightATCEETElement[] $FlightATCEETElement
     */
    protected $FlightATCEETElement = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return FlightATCEETElement[]
     */
    public function getFlightATCEETElement()
    {
      return $this->FlightATCEETElement;
    }

    /**
     * @param FlightATCEETElement[] $FlightATCEETElement
     * @return ArrayOfFlightATCEETElement
     */
    public function setFlightATCEETElement(array $FlightATCEETElement = null)
    {
      $this->FlightATCEETElement = $FlightATCEETElement;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->FlightATCEETElement[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return FlightATCEETElement
     */
    public function offsetGet($offset)
    {
      return $this->FlightATCEETElement[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param FlightATCEETElement $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->FlightATCEETElement[] = $value;
      } else {
        $this->FlightATCEETElement[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->FlightATCEETElement[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return FlightATCEETElement Return the current element
     */
    public function current()
    {
      return current($this->FlightATCEETElement);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->FlightATCEETElement);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->FlightATCEETElement);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->FlightATCEETElement);
    }

    /**
     * Countable implementation
     *
     * @return FlightATCEETElement Return count of elements
     */
    public function count()
    {
      return count($this->FlightATCEETElement);
    }

}
