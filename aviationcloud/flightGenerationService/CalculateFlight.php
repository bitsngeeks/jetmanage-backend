<?php

class CalculateFlight
{

    /**
     * @var FlightCalculationRequest $request
     */
    protected $request = null;

    /**
     * @param FlightCalculationRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return FlightCalculationRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param FlightCalculationRequest $request
     * @return CalculateFlight
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
