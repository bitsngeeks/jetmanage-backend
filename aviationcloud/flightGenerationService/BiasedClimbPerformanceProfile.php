<?php

class BiasedClimbPerformanceProfile extends ClimbProfile
{

    /**
     * @var int $DistanceDeviation
     */
    protected $DistanceDeviation = null;

    /**
     * @var int $FuelDeviation
     */
    protected $FuelDeviation = null;

    /**
     * @var int $ReferencedProfile
     */
    protected $ReferencedProfile = null;

    /**
     * @var int $TimeDeviation
     */
    protected $TimeDeviation = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return int
     */
    public function getDistanceDeviation()
    {
      return $this->DistanceDeviation;
    }

    /**
     * @param int $DistanceDeviation
     * @return BiasedClimbPerformanceProfile
     */
    public function setDistanceDeviation($DistanceDeviation)
    {
      $this->DistanceDeviation = $DistanceDeviation;
      return $this;
    }

    /**
     * @return int
     */
    public function getFuelDeviation()
    {
      return $this->FuelDeviation;
    }

    /**
     * @param int $FuelDeviation
     * @return BiasedClimbPerformanceProfile
     */
    public function setFuelDeviation($FuelDeviation)
    {
      $this->FuelDeviation = $FuelDeviation;
      return $this;
    }

    /**
     * @return int
     */
    public function getReferencedProfile()
    {
      return $this->ReferencedProfile;
    }

    /**
     * @param int $ReferencedProfile
     * @return BiasedClimbPerformanceProfile
     */
    public function setReferencedProfile($ReferencedProfile)
    {
      $this->ReferencedProfile = $ReferencedProfile;
      return $this;
    }

    /**
     * @return int
     */
    public function getTimeDeviation()
    {
      return $this->TimeDeviation;
    }

    /**
     * @param int $TimeDeviation
     * @return BiasedClimbPerformanceProfile
     */
    public function setTimeDeviation($TimeDeviation)
    {
      $this->TimeDeviation = $TimeDeviation;
      return $this;
    }

}
