<?php

class ArrayOfProcedureIdentifier implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var ProcedureIdentifier[] $ProcedureIdentifier
     */
    protected $ProcedureIdentifier = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ProcedureIdentifier[]
     */
    public function getProcedureIdentifier()
    {
      return $this->ProcedureIdentifier;
    }

    /**
     * @param ProcedureIdentifier[] $ProcedureIdentifier
     * @return ArrayOfProcedureIdentifier
     */
    public function setProcedureIdentifier(array $ProcedureIdentifier = null)
    {
      $this->ProcedureIdentifier = $ProcedureIdentifier;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->ProcedureIdentifier[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return ProcedureIdentifier
     */
    public function offsetGet($offset)
    {
      return $this->ProcedureIdentifier[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param ProcedureIdentifier $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->ProcedureIdentifier[] = $value;
      } else {
        $this->ProcedureIdentifier[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->ProcedureIdentifier[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return ProcedureIdentifier Return the current element
     */
    public function current()
    {
      return current($this->ProcedureIdentifier);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->ProcedureIdentifier);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->ProcedureIdentifier);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->ProcedureIdentifier);
    }

    /**
     * Countable implementation
     *
     * @return ProcedureIdentifier Return count of elements
     */
    public function count()
    {
      return count($this->ProcedureIdentifier);
    }

}
