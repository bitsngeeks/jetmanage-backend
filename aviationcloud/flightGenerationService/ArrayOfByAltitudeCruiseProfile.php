<?php

class ArrayOfByAltitudeCruiseProfile implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var ByAltitudeCruiseProfile[] $ByAltitudeCruiseProfile
     */
    protected $ByAltitudeCruiseProfile = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ByAltitudeCruiseProfile[]
     */
    public function getByAltitudeCruiseProfile()
    {
      return $this->ByAltitudeCruiseProfile;
    }

    /**
     * @param ByAltitudeCruiseProfile[] $ByAltitudeCruiseProfile
     * @return ArrayOfByAltitudeCruiseProfile
     */
    public function setByAltitudeCruiseProfile(array $ByAltitudeCruiseProfile = null)
    {
      $this->ByAltitudeCruiseProfile = $ByAltitudeCruiseProfile;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->ByAltitudeCruiseProfile[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return ByAltitudeCruiseProfile
     */
    public function offsetGet($offset)
    {
      return $this->ByAltitudeCruiseProfile[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param ByAltitudeCruiseProfile $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->ByAltitudeCruiseProfile[] = $value;
      } else {
        $this->ByAltitudeCruiseProfile[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->ByAltitudeCruiseProfile[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return ByAltitudeCruiseProfile Return the current element
     */
    public function current()
    {
      return current($this->ByAltitudeCruiseProfile);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->ByAltitudeCruiseProfile);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->ByAltitudeCruiseProfile);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->ByAltitudeCruiseProfile);
    }

    /**
     * Countable implementation
     *
     * @return ByAltitudeCruiseProfile Return count of elements
     */
    public function count()
    {
      return count($this->ByAltitudeCruiseProfile);
    }

}
