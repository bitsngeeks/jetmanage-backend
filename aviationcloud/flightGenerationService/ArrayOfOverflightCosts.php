<?php

class ArrayOfOverflightCosts implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var OverflightCosts[] $OverflightCosts
     */
    protected $OverflightCosts = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return OverflightCosts[]
     */
    public function getOverflightCosts()
    {
      return $this->OverflightCosts;
    }

    /**
     * @param OverflightCosts[] $OverflightCosts
     * @return ArrayOfOverflightCosts
     */
    public function setOverflightCosts(array $OverflightCosts = null)
    {
      $this->OverflightCosts = $OverflightCosts;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->OverflightCosts[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return OverflightCosts
     */
    public function offsetGet($offset)
    {
      return $this->OverflightCosts[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param OverflightCosts $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->OverflightCosts[] = $value;
      } else {
        $this->OverflightCosts[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->OverflightCosts[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return OverflightCosts Return the current element
     */
    public function current()
    {
      return current($this->OverflightCosts);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->OverflightCosts);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->OverflightCosts);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->OverflightCosts);
    }

    /**
     * Countable implementation
     *
     * @return OverflightCosts Return count of elements
     */
    public function count()
    {
      return count($this->OverflightCosts);
    }

}
