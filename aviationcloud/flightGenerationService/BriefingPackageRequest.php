<?php

class BriefingPackageRequest extends RequestBase
{

    /**
     * @var AdditionalBriefingData $AdditionalData
     */
    protected $AdditionalData = null;

    /**
     * @var ETPWindChartDocumentSpecification $ETPWindChart
     */
    protected $ETPWindChart = null;

    /**
     * @var EnrouteChargeDocumentSpecification $EnrouteChargeSpecification
     */
    protected $EnrouteChargeSpecification = null;

    /**
     * @var ATCFPLMessage $FPLMessage
     */
    protected $FPLMessage = null;

    /**
     * @var FlightSpecification $FlightSpecification
     */
    protected $FlightSpecification = null;

    /**
     * @var ICAOFlightPlanDocumentSpecification $ICAOFlightPlanSpecification
     */
    protected $ICAOFlightPlanSpecification = null;

    /**
     * @var boolean $MergeDocuments
     */
    protected $MergeDocuments = null;

    /**
     * @var NatTrackDocumentSpecification $NatTrackSpecification
     */
    protected $NatTrackSpecification = null;

    /**
     * @var NavLogDocumentSpecification $NavLogSpecification
     */
    protected $NavLogSpecification = null;

    /**
     * @var NotamDocumentSpecification $NotamDocumentSpecification
     */
    protected $NotamDocumentSpecification = null;

    /**
     * @var PDFFlightlogGenerationOptions $PDFFlightlogSpecification
     */
    protected $PDFFlightlogSpecification = null;

    /**
     * @var RAIMPredictionOptions $RAIMPredictionOptions
     */
    protected $RAIMPredictionOptions = null;

    /**
     * @var SingleEngineRiskChartDocumentSpecification $RiskAnalysis
     */
    protected $RiskAnalysis = null;

    /**
     * @var SignifantWeatherDocumentSpecification $SignifantWeather
     */
    protected $SignifantWeather = null;

    /**
     * @var boolean $UseFileDownload
     */
    protected $UseFileDownload = null;

    /**
     * @var boolean $UseV2Briefing
     */
    protected $UseV2Briefing = null;

    /**
     * @var CrossSectionDocumentSpecification $VerticalCrossSection
     */
    protected $VerticalCrossSection = null;

    /**
     * @var WindChartDocumentSpecification $WindChart
     */
    protected $WindChart = null;

    /**
     * @var WxDocumentSpecification $WxDocumentSpecification
     */
    protected $WxDocumentSpecification = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return AdditionalBriefingData
     */
    public function getAdditionalData()
    {
      return $this->AdditionalData;
    }

    /**
     * @param AdditionalBriefingData $AdditionalData
     * @return BriefingPackageRequest
     */
    public function setAdditionalData($AdditionalData)
    {
      $this->AdditionalData = $AdditionalData;
      return $this;
    }

    /**
     * @return ETPWindChartDocumentSpecification
     */
    public function getETPWindChart()
    {
      return $this->ETPWindChart;
    }

    /**
     * @param ETPWindChartDocumentSpecification $ETPWindChart
     * @return BriefingPackageRequest
     */
    public function setETPWindChart($ETPWindChart)
    {
      $this->ETPWindChart = $ETPWindChart;
      return $this;
    }

    /**
     * @return EnrouteChargeDocumentSpecification
     */
    public function getEnrouteChargeSpecification()
    {
      return $this->EnrouteChargeSpecification;
    }

    /**
     * @param EnrouteChargeDocumentSpecification $EnrouteChargeSpecification
     * @return BriefingPackageRequest
     */
    public function setEnrouteChargeSpecification($EnrouteChargeSpecification)
    {
      $this->EnrouteChargeSpecification = $EnrouteChargeSpecification;
      return $this;
    }

    /**
     * @return ATCFPLMessage
     */
    public function getFPLMessage()
    {
      return $this->FPLMessage;
    }

    /**
     * @param ATCFPLMessage $FPLMessage
     * @return BriefingPackageRequest
     */
    public function setFPLMessage($FPLMessage)
    {
      $this->FPLMessage = $FPLMessage;
      return $this;
    }

    /**
     * @return FlightSpecification
     */
    public function getFlightSpecification()
    {
      return $this->FlightSpecification;
    }

    /**
     * @param FlightSpecification $FlightSpecification
     * @return BriefingPackageRequest
     */
    public function setFlightSpecification($FlightSpecification)
    {
      $this->FlightSpecification = $FlightSpecification;
      return $this;
    }

    /**
     * @return ICAOFlightPlanDocumentSpecification
     */
    public function getICAOFlightPlanSpecification()
    {
      return $this->ICAOFlightPlanSpecification;
    }

    /**
     * @param ICAOFlightPlanDocumentSpecification $ICAOFlightPlanSpecification
     * @return BriefingPackageRequest
     */
    public function setICAOFlightPlanSpecification($ICAOFlightPlanSpecification)
    {
      $this->ICAOFlightPlanSpecification = $ICAOFlightPlanSpecification;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getMergeDocuments()
    {
      return $this->MergeDocuments;
    }

    /**
     * @param boolean $MergeDocuments
     * @return BriefingPackageRequest
     */
    public function setMergeDocuments($MergeDocuments)
    {
      $this->MergeDocuments = $MergeDocuments;
      return $this;
    }

    /**
     * @return NatTrackDocumentSpecification
     */
    public function getNatTrackSpecification()
    {
      return $this->NatTrackSpecification;
    }

    /**
     * @param NatTrackDocumentSpecification $NatTrackSpecification
     * @return BriefingPackageRequest
     */
    public function setNatTrackSpecification($NatTrackSpecification)
    {
      $this->NatTrackSpecification = $NatTrackSpecification;
      return $this;
    }

    /**
     * @return NavLogDocumentSpecification
     */
    public function getNavLogSpecification()
    {
      return $this->NavLogSpecification;
    }

    /**
     * @param NavLogDocumentSpecification $NavLogSpecification
     * @return BriefingPackageRequest
     */
    public function setNavLogSpecification($NavLogSpecification)
    {
      $this->NavLogSpecification = $NavLogSpecification;
      return $this;
    }

    /**
     * @return NotamDocumentSpecification
     */
    public function getNotamDocumentSpecification()
    {
      return $this->NotamDocumentSpecification;
    }

    /**
     * @param NotamDocumentSpecification $NotamDocumentSpecification
     * @return BriefingPackageRequest
     */
    public function setNotamDocumentSpecification($NotamDocumentSpecification)
    {
      $this->NotamDocumentSpecification = $NotamDocumentSpecification;
      return $this;
    }

    /**
     * @return PDFFlightlogGenerationOptions
     */
    public function getPDFFlightlogSpecification()
    {
      return $this->PDFFlightlogSpecification;
    }

    /**
     * @param PDFFlightlogGenerationOptions $PDFFlightlogSpecification
     * @return BriefingPackageRequest
     */
    public function setPDFFlightlogSpecification($PDFFlightlogSpecification)
    {
      $this->PDFFlightlogSpecification = $PDFFlightlogSpecification;
      return $this;
    }

    /**
     * @return RAIMPredictionOptions
     */
    public function getRAIMPredictionOptions()
    {
      return $this->RAIMPredictionOptions;
    }

    /**
     * @param RAIMPredictionOptions $RAIMPredictionOptions
     * @return BriefingPackageRequest
     */
    public function setRAIMPredictionOptions($RAIMPredictionOptions)
    {
      $this->RAIMPredictionOptions = $RAIMPredictionOptions;
      return $this;
    }

    /**
     * @return SingleEngineRiskChartDocumentSpecification
     */
    public function getRiskAnalysis()
    {
      return $this->RiskAnalysis;
    }

    /**
     * @param SingleEngineRiskChartDocumentSpecification $RiskAnalysis
     * @return BriefingPackageRequest
     */
    public function setRiskAnalysis($RiskAnalysis)
    {
      $this->RiskAnalysis = $RiskAnalysis;
      return $this;
    }

    /**
     * @return SignifantWeatherDocumentSpecification
     */
    public function getSignifantWeather()
    {
      return $this->SignifantWeather;
    }

    /**
     * @param SignifantWeatherDocumentSpecification $SignifantWeather
     * @return BriefingPackageRequest
     */
    public function setSignifantWeather($SignifantWeather)
    {
      $this->SignifantWeather = $SignifantWeather;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUseFileDownload()
    {
      return $this->UseFileDownload;
    }

    /**
     * @param boolean $UseFileDownload
     * @return BriefingPackageRequest
     */
    public function setUseFileDownload($UseFileDownload)
    {
      $this->UseFileDownload = $UseFileDownload;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUseV2Briefing()
    {
      return $this->UseV2Briefing;
    }

    /**
     * @param boolean $UseV2Briefing
     * @return BriefingPackageRequest
     */
    public function setUseV2Briefing($UseV2Briefing)
    {
      $this->UseV2Briefing = $UseV2Briefing;
      return $this;
    }

    /**
     * @return CrossSectionDocumentSpecification
     */
    public function getVerticalCrossSection()
    {
      return $this->VerticalCrossSection;
    }

    /**
     * @param CrossSectionDocumentSpecification $VerticalCrossSection
     * @return BriefingPackageRequest
     */
    public function setVerticalCrossSection($VerticalCrossSection)
    {
      $this->VerticalCrossSection = $VerticalCrossSection;
      return $this;
    }

    /**
     * @return WindChartDocumentSpecification
     */
    public function getWindChart()
    {
      return $this->WindChart;
    }

    /**
     * @param WindChartDocumentSpecification $WindChart
     * @return BriefingPackageRequest
     */
    public function setWindChart($WindChart)
    {
      $this->WindChart = $WindChart;
      return $this;
    }

    /**
     * @return WxDocumentSpecification
     */
    public function getWxDocumentSpecification()
    {
      return $this->WxDocumentSpecification;
    }

    /**
     * @param WxDocumentSpecification $WxDocumentSpecification
     * @return BriefingPackageRequest
     */
    public function setWxDocumentSpecification($WxDocumentSpecification)
    {
      $this->WxDocumentSpecification = $WxDocumentSpecification;
      return $this;
    }

}
