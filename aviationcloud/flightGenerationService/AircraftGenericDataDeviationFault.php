<?php

class AircraftGenericDataDeviationFault
{

    /**
     * @var ArrayOfAircraftGenericDataMaxDeviatioNExceedError $MaxDeviationExceededErrors
     */
    protected $MaxDeviationExceededErrors = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfAircraftGenericDataMaxDeviatioNExceedError
     */
    public function getMaxDeviationExceededErrors()
    {
      return $this->MaxDeviationExceededErrors;
    }

    /**
     * @param ArrayOfAircraftGenericDataMaxDeviatioNExceedError $MaxDeviationExceededErrors
     * @return AircraftGenericDataDeviationFault
     */
    public function setMaxDeviationExceededErrors($MaxDeviationExceededErrors)
    {
      $this->MaxDeviationExceededErrors = $MaxDeviationExceededErrors;
      return $this;
    }

}
