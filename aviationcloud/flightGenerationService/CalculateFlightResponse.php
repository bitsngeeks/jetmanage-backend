<?php

class CalculateFlightResponse
{

    /**
     * @var FlightLog $CalculateFlightResult
     */
    protected $CalculateFlightResult = null;

    /**
     * @param FlightLog $CalculateFlightResult
     */
    public function __construct($CalculateFlightResult)
    {
      $this->CalculateFlightResult = $CalculateFlightResult;
    }

    /**
     * @return FlightLog
     */
    public function getCalculateFlightResult()
    {
      return $this->CalculateFlightResult;
    }

    /**
     * @param FlightLog $CalculateFlightResult
     * @return CalculateFlightResponse
     */
    public function setCalculateFlightResult($CalculateFlightResult)
    {
      $this->CalculateFlightResult = $CalculateFlightResult;
      return $this;
    }

}
