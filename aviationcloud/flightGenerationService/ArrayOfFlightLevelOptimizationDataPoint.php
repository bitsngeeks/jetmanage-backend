<?php

class ArrayOfFlightLevelOptimizationDataPoint implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var FlightLevelOptimizationDataPoint[] $FlightLevelOptimizationDataPoint
     */
    protected $FlightLevelOptimizationDataPoint = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return FlightLevelOptimizationDataPoint[]
     */
    public function getFlightLevelOptimizationDataPoint()
    {
      return $this->FlightLevelOptimizationDataPoint;
    }

    /**
     * @param FlightLevelOptimizationDataPoint[] $FlightLevelOptimizationDataPoint
     * @return ArrayOfFlightLevelOptimizationDataPoint
     */
    public function setFlightLevelOptimizationDataPoint(array $FlightLevelOptimizationDataPoint = null)
    {
      $this->FlightLevelOptimizationDataPoint = $FlightLevelOptimizationDataPoint;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->FlightLevelOptimizationDataPoint[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return FlightLevelOptimizationDataPoint
     */
    public function offsetGet($offset)
    {
      return $this->FlightLevelOptimizationDataPoint[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param FlightLevelOptimizationDataPoint $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->FlightLevelOptimizationDataPoint[] = $value;
      } else {
        $this->FlightLevelOptimizationDataPoint[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->FlightLevelOptimizationDataPoint[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return FlightLevelOptimizationDataPoint Return the current element
     */
    public function current()
    {
      return current($this->FlightLevelOptimizationDataPoint);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->FlightLevelOptimizationDataPoint);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->FlightLevelOptimizationDataPoint);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->FlightLevelOptimizationDataPoint);
    }

    /**
     * Countable implementation
     *
     * @return FlightLevelOptimizationDataPoint Return count of elements
     */
    public function count()
    {
      return count($this->FlightLevelOptimizationDataPoint);
    }

}
