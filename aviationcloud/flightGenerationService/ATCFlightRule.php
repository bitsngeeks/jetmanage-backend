<?php

class ATCFlightRule
{
    const __default = 'IFR';
    const IFR = 'IFR';
    const VFR = 'VFR';
    const IFR_THEN_VFR = 'IFR_THEN_VFR';
    const VFR_THEN_IFR = 'VFR_THEN_IFR';


}
