<?php

class ValidateAircraftRequest extends RequestBase
{

    /**
     * @var AircraftSpecification $AircraftDefinition
     */
    protected $AircraftDefinition = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return AircraftSpecification
     */
    public function getAircraftDefinition()
    {
      return $this->AircraftDefinition;
    }

    /**
     * @param AircraftSpecification $AircraftDefinition
     * @return ValidateAircraftRequest
     */
    public function setAircraftDefinition($AircraftDefinition)
    {
      $this->AircraftDefinition = $AircraftDefinition;
      return $this;
    }

}
