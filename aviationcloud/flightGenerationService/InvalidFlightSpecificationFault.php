<?php

class InvalidFlightSpecificationFault
{

    /**
     * @var ArrayOfFlightValidationError $ValidationErrors
     */
    protected $ValidationErrors = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfFlightValidationError
     */
    public function getValidationErrors()
    {
      return $this->ValidationErrors;
    }

    /**
     * @param ArrayOfFlightValidationError $ValidationErrors
     * @return InvalidFlightSpecificationFault
     */
    public function setValidationErrors($ValidationErrors)
    {
      $this->ValidationErrors = $ValidationErrors;
      return $this;
    }

}
