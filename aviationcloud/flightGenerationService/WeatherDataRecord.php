<?php

class WeatherDataRecord
{

    /**
     * @var int $Altitude
     */
    protected $Altitude = null;

    /**
     * @var float $Direction
     */
    protected $Direction = null;

    /**
     * @var float $ISA
     */
    protected $ISA = null;

    /**
     * @var SphereicPoint $Position
     */
    protected $Position = null;

    /**
     * @var float $Speed
     */
    protected $Speed = null;

    /**
     * @var float $TempCelcius
     */
    protected $TempCelcius = null;

    /**
     * @var \DateTime $Time
     */
    protected $Time = null;

    /**
     * @var float $WindComponent
     */
    protected $WindComponent = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getAltitude()
    {
      return $this->Altitude;
    }

    /**
     * @param int $Altitude
     * @return WeatherDataRecord
     */
    public function setAltitude($Altitude)
    {
      $this->Altitude = $Altitude;
      return $this;
    }

    /**
     * @return float
     */
    public function getDirection()
    {
      return $this->Direction;
    }

    /**
     * @param float $Direction
     * @return WeatherDataRecord
     */
    public function setDirection($Direction)
    {
      $this->Direction = $Direction;
      return $this;
    }

    /**
     * @return float
     */
    public function getISA()
    {
      return $this->ISA;
    }

    /**
     * @param float $ISA
     * @return WeatherDataRecord
     */
    public function setISA($ISA)
    {
      $this->ISA = $ISA;
      return $this;
    }

    /**
     * @return SphereicPoint
     */
    public function getPosition()
    {
      return $this->Position;
    }

    /**
     * @param SphereicPoint $Position
     * @return WeatherDataRecord
     */
    public function setPosition($Position)
    {
      $this->Position = $Position;
      return $this;
    }

    /**
     * @return float
     */
    public function getSpeed()
    {
      return $this->Speed;
    }

    /**
     * @param float $Speed
     * @return WeatherDataRecord
     */
    public function setSpeed($Speed)
    {
      $this->Speed = $Speed;
      return $this;
    }

    /**
     * @return float
     */
    public function getTempCelcius()
    {
      return $this->TempCelcius;
    }

    /**
     * @param float $TempCelcius
     * @return WeatherDataRecord
     */
    public function setTempCelcius($TempCelcius)
    {
      $this->TempCelcius = $TempCelcius;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getTime()
    {
      if ($this->Time == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->Time);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $Time
     * @return WeatherDataRecord
     */
    public function setTime(\DateTime $Time = null)
    {
      if ($Time == null) {
       $this->Time = null;
      } else {
        $this->Time = $Time->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return float
     */
    public function getWindComponent()
    {
      return $this->WindComponent;
    }

    /**
     * @param float $WindComponent
     * @return WeatherDataRecord
     */
    public function setWindComponent($WindComponent)
    {
      $this->WindComponent = $WindComponent;
      return $this;
    }

}
