<?php

class ATCMessageBase
{

    /**
     * @var string $Content
     */
    protected $Content = null;

    /**
     * @var boolean $IsSimulated
     */
    protected $IsSimulated = null;

    /**
     * @var \DateTime $MessageDate
     */
    protected $MessageDate = null;

    /**
     * @var string $Type
     */
    protected $Type = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getContent()
    {
      return $this->Content;
    }

    /**
     * @param string $Content
     * @return ATCMessageBase
     */
    public function setContent($Content)
    {
      $this->Content = $Content;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsSimulated()
    {
      return $this->IsSimulated;
    }

    /**
     * @param boolean $IsSimulated
     * @return ATCMessageBase
     */
    public function setIsSimulated($IsSimulated)
    {
      $this->IsSimulated = $IsSimulated;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getMessageDate()
    {
      if ($this->MessageDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->MessageDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $MessageDate
     * @return ATCMessageBase
     */
    public function setMessageDate(\DateTime $MessageDate = null)
    {
      if ($MessageDate == null) {
       $this->MessageDate = null;
      } else {
        $this->MessageDate = $MessageDate->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
      return $this->Type;
    }

    /**
     * @param string $Type
     * @return ATCMessageBase
     */
    public function setType($Type)
    {
      $this->Type = $Type;
      return $this;
    }

}
