<?php

class NatTrackDirection
{
    const __default = 'East';
    const East = 'East';
    const West = 'West';
    const EastAndWest = 'EastAndWest';
    const Auto = 'Auto';
    const AutoBoth = 'AutoBoth';


}
