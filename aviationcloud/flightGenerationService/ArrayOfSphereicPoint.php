<?php

class ArrayOfSphereicPoint implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var SphereicPoint[] $SphereicPoint
     */
    protected $SphereicPoint = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return SphereicPoint[]
     */
    public function getSphereicPoint()
    {
      return $this->SphereicPoint;
    }

    /**
     * @param SphereicPoint[] $SphereicPoint
     * @return ArrayOfSphereicPoint
     */
    public function setSphereicPoint(array $SphereicPoint = null)
    {
      $this->SphereicPoint = $SphereicPoint;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->SphereicPoint[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return SphereicPoint
     */
    public function offsetGet($offset)
    {
      return $this->SphereicPoint[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param SphereicPoint $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->SphereicPoint[] = $value;
      } else {
        $this->SphereicPoint[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->SphereicPoint[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return SphereicPoint Return the current element
     */
    public function current()
    {
      return current($this->SphereicPoint);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->SphereicPoint);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->SphereicPoint);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->SphereicPoint);
    }

    /**
     * Countable implementation
     *
     * @return SphereicPoint Return count of elements
     */
    public function count()
    {
      return count($this->SphereicPoint);
    }

}
