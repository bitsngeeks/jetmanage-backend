<?php

class TripPackageRequest
{

    /**
     * @var FuelTankeringDocumentSpecification $FuelTankering
     */
    protected $FuelTankering = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return FuelTankeringDocumentSpecification
     */
    public function getFuelTankering()
    {
      return $this->FuelTankering;
    }

    /**
     * @param FuelTankeringDocumentSpecification $FuelTankering
     * @return TripPackageRequest
     */
    public function setFuelTankering($FuelTankering)
    {
      $this->FuelTankering = $FuelTankering;
      return $this;
    }

}
