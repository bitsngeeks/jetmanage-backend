<?php

class FuelTankListDefinition extends FuelTankDefinition
{

    /**
     * @var ArrayOfFuelTank $FuelTanks
     */
    protected $FuelTanks = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfFuelTank
     */
    public function getFuelTanks()
    {
      return $this->FuelTanks;
    }

    /**
     * @param ArrayOfFuelTank $FuelTanks
     * @return FuelTankListDefinition
     */
    public function setFuelTanks($FuelTanks)
    {
      $this->FuelTanks = $FuelTanks;
      return $this;
    }

}
