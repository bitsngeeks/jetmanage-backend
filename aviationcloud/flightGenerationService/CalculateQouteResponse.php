<?php

class CalculateQouteResponse
{

    /**
     * @var QouteCalculationResponse $CalculateQouteResult
     */
    protected $CalculateQouteResult = null;

    /**
     * @param QouteCalculationResponse $CalculateQouteResult
     */
    public function __construct($CalculateQouteResult)
    {
      $this->CalculateQouteResult = $CalculateQouteResult;
    }

    /**
     * @return QouteCalculationResponse
     */
    public function getCalculateQouteResult()
    {
      return $this->CalculateQouteResult;
    }

    /**
     * @param QouteCalculationResponse $CalculateQouteResult
     * @return CalculateQouteResponse
     */
    public function setCalculateQouteResult($CalculateQouteResult)
    {
      $this->CalculateQouteResult = $CalculateQouteResult;
      return $this;
    }

}
