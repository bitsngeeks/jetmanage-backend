<?php

class ValidateFlightResponse
{

    /**
     * @var CFMUValidationResult $ValidateFlightResult
     */
    protected $ValidateFlightResult = null;

    /**
     * @param CFMUValidationResult $ValidateFlightResult
     */
    public function __construct($ValidateFlightResult)
    {
      $this->ValidateFlightResult = $ValidateFlightResult;
    }

    /**
     * @return CFMUValidationResult
     */
    public function getValidateFlightResult()
    {
      return $this->ValidateFlightResult;
    }

    /**
     * @param CFMUValidationResult $ValidateFlightResult
     * @return ValidateFlightResponse
     */
    public function setValidateFlightResult($ValidateFlightResult)
    {
      $this->ValidateFlightResult = $ValidateFlightResult;
      return $this;
    }

}
