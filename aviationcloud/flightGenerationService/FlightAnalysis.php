<?php

class FlightAnalysis
{

    /**
     * @var RiskAnalysis $RiskAnalysis
     */
    protected $RiskAnalysis = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return RiskAnalysis
     */
    public function getRiskAnalysis()
    {
      return $this->RiskAnalysis;
    }

    /**
     * @param RiskAnalysis $RiskAnalysis
     * @return FlightAnalysis
     */
    public function setRiskAnalysis($RiskAnalysis)
    {
      $this->RiskAnalysis = $RiskAnalysis;
      return $this;
    }

}
