<?php

class CrewCompartment extends AircraftStructureUnit
{

    /**
     * @var CrewType $CrewType
     */
    protected $CrewType = null;

    /**
     * @var Weight $DefaultCrewWeight
     */
    protected $DefaultCrewWeight = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return CrewType
     */
    public function getCrewType()
    {
      return $this->CrewType;
    }

    /**
     * @param CrewType $CrewType
     * @return CrewCompartment
     */
    public function setCrewType($CrewType)
    {
      $this->CrewType = $CrewType;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getDefaultCrewWeight()
    {
      return $this->DefaultCrewWeight;
    }

    /**
     * @param Weight $DefaultCrewWeight
     * @return CrewCompartment
     */
    public function setDefaultCrewWeight($DefaultCrewWeight)
    {
      $this->DefaultCrewWeight = $DefaultCrewWeight;
      return $this;
    }

}
