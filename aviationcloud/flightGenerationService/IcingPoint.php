<?php

class IcingPoint
{

    /**
     * @var Altitude $Altitude
     */
    protected $Altitude = null;

    /**
     * @var SphereicPoint $Position
     */
    protected $Position = null;

    /**
     * @var IcingSeverity $Severity
     */
    protected $Severity = null;

    /**
     * @var \DateTime $Time
     */
    protected $Time = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Altitude
     */
    public function getAltitude()
    {
      return $this->Altitude;
    }

    /**
     * @param Altitude $Altitude
     * @return IcingPoint
     */
    public function setAltitude($Altitude)
    {
      $this->Altitude = $Altitude;
      return $this;
    }

    /**
     * @return SphereicPoint
     */
    public function getPosition()
    {
      return $this->Position;
    }

    /**
     * @param SphereicPoint $Position
     * @return IcingPoint
     */
    public function setPosition($Position)
    {
      $this->Position = $Position;
      return $this;
    }

    /**
     * @return IcingSeverity
     */
    public function getSeverity()
    {
      return $this->Severity;
    }

    /**
     * @param IcingSeverity $Severity
     * @return IcingPoint
     */
    public function setSeverity($Severity)
    {
      $this->Severity = $Severity;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getTime()
    {
      if ($this->Time == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->Time);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $Time
     * @return IcingPoint
     */
    public function setTime(\DateTime $Time = null)
    {
      if ($Time == null) {
       $this->Time = null;
      } else {
        $this->Time = $Time->format(\DateTime::ATOM);
      }
      return $this;
    }

}
