<?php

class ATCData
{

    /**
     * @var ATCFlightRule $FilghtRule
     */
    protected $FilghtRule = null;

    /**
     * @var boolean $IncludeEETInField18
     */
    protected $IncludeEETInField18 = null;

    /**
     * @var boolean $IncludeSupplementaryInfo
     */
    protected $IncludeSupplementaryInfo = null;

    /**
     * @var string $Item19Remarks
     */
    protected $Item19Remarks = null;

    /**
     * @var int $NumberOfAircrafts
     */
    protected $NumberOfAircrafts = null;

    /**
     * @var ArrayOfATCOtherItemField $OtherItemFields
     */
    protected $OtherItemFields = null;

    /**
     * @var SpecialHandlingReason $ReasonForSpecialHandling
     */
    protected $ReasonForSpecialHandling = null;

    /**
     * @var Length $RunwayVisualRange
     */
    protected $RunwayVisualRange = null;

    /**
     * @var ATCSupplementaryInfo $SupplementaryInfo
     */
    protected $SupplementaryInfo = null;

    /**
     * @var ATCFlightType $TypeOfFlight
     */
    protected $TypeOfFlight = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ATCFlightRule
     */
    public function getFilghtRule()
    {
      return $this->FilghtRule;
    }

    /**
     * @param ATCFlightRule $FilghtRule
     * @return ATCData
     */
    public function setFilghtRule($FilghtRule)
    {
      $this->FilghtRule = $FilghtRule;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIncludeEETInField18()
    {
      return $this->IncludeEETInField18;
    }

    /**
     * @param boolean $IncludeEETInField18
     * @return ATCData
     */
    public function setIncludeEETInField18($IncludeEETInField18)
    {
      $this->IncludeEETInField18 = $IncludeEETInField18;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIncludeSupplementaryInfo()
    {
      return $this->IncludeSupplementaryInfo;
    }

    /**
     * @param boolean $IncludeSupplementaryInfo
     * @return ATCData
     */
    public function setIncludeSupplementaryInfo($IncludeSupplementaryInfo)
    {
      $this->IncludeSupplementaryInfo = $IncludeSupplementaryInfo;
      return $this;
    }

    /**
     * @return string
     */
    public function getItem19Remarks()
    {
      return $this->Item19Remarks;
    }

    /**
     * @param string $Item19Remarks
     * @return ATCData
     */
    public function setItem19Remarks($Item19Remarks)
    {
      $this->Item19Remarks = $Item19Remarks;
      return $this;
    }

    /**
     * @return int
     */
    public function getNumberOfAircrafts()
    {
      return $this->NumberOfAircrafts;
    }

    /**
     * @param int $NumberOfAircrafts
     * @return ATCData
     */
    public function setNumberOfAircrafts($NumberOfAircrafts)
    {
      $this->NumberOfAircrafts = $NumberOfAircrafts;
      return $this;
    }

    /**
     * @return ArrayOfATCOtherItemField
     */
    public function getOtherItemFields()
    {
      return $this->OtherItemFields;
    }

    /**
     * @param ArrayOfATCOtherItemField $OtherItemFields
     * @return ATCData
     */
    public function setOtherItemFields($OtherItemFields)
    {
      $this->OtherItemFields = $OtherItemFields;
      return $this;
    }

    /**
     * @return SpecialHandlingReason
     */
    public function getReasonForSpecialHandling()
    {
      return $this->ReasonForSpecialHandling;
    }

    /**
     * @param SpecialHandlingReason $ReasonForSpecialHandling
     * @return ATCData
     */
    public function setReasonForSpecialHandling($ReasonForSpecialHandling)
    {
      $this->ReasonForSpecialHandling = $ReasonForSpecialHandling;
      return $this;
    }

    /**
     * @return Length
     */
    public function getRunwayVisualRange()
    {
      return $this->RunwayVisualRange;
    }

    /**
     * @param Length $RunwayVisualRange
     * @return ATCData
     */
    public function setRunwayVisualRange($RunwayVisualRange)
    {
      $this->RunwayVisualRange = $RunwayVisualRange;
      return $this;
    }

    /**
     * @return ATCSupplementaryInfo
     */
    public function getSupplementaryInfo()
    {
      return $this->SupplementaryInfo;
    }

    /**
     * @param ATCSupplementaryInfo $SupplementaryInfo
     * @return ATCData
     */
    public function setSupplementaryInfo($SupplementaryInfo)
    {
      $this->SupplementaryInfo = $SupplementaryInfo;
      return $this;
    }

    /**
     * @return ATCFlightType
     */
    public function getTypeOfFlight()
    {
      return $this->TypeOfFlight;
    }

    /**
     * @param ATCFlightType $TypeOfFlight
     * @return ATCData
     */
    public function setTypeOfFlight($TypeOfFlight)
    {
      $this->TypeOfFlight = $TypeOfFlight;
      return $this;
    }

}
