<?php

class NatTrackDocumentSpecification extends DocumentSpecification
{

    /**
     * @var NatTrackDirection $Direction
     */
    protected $Direction = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return NatTrackDirection
     */
    public function getDirection()
    {
      return $this->Direction;
    }

    /**
     * @param NatTrackDirection $Direction
     * @return NatTrackDocumentSpecification
     */
    public function setDirection($Direction)
    {
      $this->Direction = $Direction;
      return $this;
    }

}
