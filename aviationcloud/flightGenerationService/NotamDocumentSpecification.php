<?php

class NotamDocumentSpecification extends WxNotamDocumentSpecification
{

    /**
     * @var boolean $ExcludeETPAirportNotams
     */
    protected $ExcludeETPAirportNotams = null;

    /**
     * @var boolean $ExcludeFIRNOTAMs
     */
    protected $ExcludeFIRNOTAMs = null;

    /**
     * @var boolean $ExcludeFIRNotams
     */
    protected $ExcludeFIRNotams = null;

    /**
     * @var boolean $IncludeTFRs
     */
    protected $IncludeTFRs = null;

    /**
     * @var boolean $IncludeUSDomesticNOTAMs
     */
    protected $IncludeUSDomesticNOTAMs = null;

    /**
     * @var boolean $UseV2Document
     */
    protected $UseV2Document = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return boolean
     */
    public function getExcludeETPAirportNotams()
    {
      return $this->ExcludeETPAirportNotams;
    }

    /**
     * @param boolean $ExcludeETPAirportNotams
     * @return NotamDocumentSpecification
     */
    public function setExcludeETPAirportNotams($ExcludeETPAirportNotams)
    {
      $this->ExcludeETPAirportNotams = $ExcludeETPAirportNotams;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getExcludeFIRNOTAMs()
    {
      return $this->ExcludeFIRNOTAMs;
    }

    /**
     * @param boolean $ExcludeFIRNOTAMs
     * @return NotamDocumentSpecification
     */
    public function setExcludeFIRNOTAMs($ExcludeFIRNOTAMs)
    {
      $this->ExcludeFIRNOTAMs = $ExcludeFIRNOTAMs;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getExcludeFIRNotams()
    {
      return $this->ExcludeFIRNotams;
    }

    /**
     * @param boolean $ExcludeFIRNotams
     * @return NotamDocumentSpecification
     */
    public function setExcludeFIRNotams($ExcludeFIRNotams)
    {
      $this->ExcludeFIRNotams = $ExcludeFIRNotams;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIncludeTFRs()
    {
      return $this->IncludeTFRs;
    }

    /**
     * @param boolean $IncludeTFRs
     * @return NotamDocumentSpecification
     */
    public function setIncludeTFRs($IncludeTFRs)
    {
      $this->IncludeTFRs = $IncludeTFRs;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIncludeUSDomesticNOTAMs()
    {
      return $this->IncludeUSDomesticNOTAMs;
    }

    /**
     * @param boolean $IncludeUSDomesticNOTAMs
     * @return NotamDocumentSpecification
     */
    public function setIncludeUSDomesticNOTAMs($IncludeUSDomesticNOTAMs)
    {
      $this->IncludeUSDomesticNOTAMs = $IncludeUSDomesticNOTAMs;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUseV2Document()
    {
      return $this->UseV2Document;
    }

    /**
     * @param boolean $UseV2Document
     * @return NotamDocumentSpecification
     */
    public function setUseV2Document($UseV2Document)
    {
      $this->UseV2Document = $UseV2Document;
      return $this;
    }

}
