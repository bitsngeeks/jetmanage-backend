<?php

class Speed
{

    /**
     * @var SpeedUnit $Unit
     */
    protected $Unit = null;

    /**
     * @var int $Value
     */
    protected $Value = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return SpeedUnit
     */
    public function getUnit()
    {
      return $this->Unit;
    }

    /**
     * @param SpeedUnit $Unit
     * @return Speed
     */
    public function setUnit($Unit)
    {
      $this->Unit = $Unit;
      return $this;
    }

    /**
     * @return int
     */
    public function getValue()
    {
      return $this->Value;
    }

    /**
     * @param int $Value
     * @return Speed
     */
    public function setValue($Value)
    {
      $this->Value = $Value;
      return $this;
    }

}
