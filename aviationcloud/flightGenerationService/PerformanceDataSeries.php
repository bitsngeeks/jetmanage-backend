<?php

class PerformanceDataSeries
{

    /**
     * @var Weight $Weight
     */
    protected $Weight = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Weight
     */
    public function getWeight()
    {
      return $this->Weight;
    }

    /**
     * @param Weight $Weight
     * @return PerformanceDataSeries
     */
    public function setWeight($Weight)
    {
      $this->Weight = $Weight;
      return $this;
    }

}
