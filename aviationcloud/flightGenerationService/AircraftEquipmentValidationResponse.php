<?php

class AircraftEquipmentValidationResponse
{

    /**
     * @var string $Item10aString
     */
    protected $Item10aString = null;

    /**
     * @var string $Item10bString
     */
    protected $Item10bString = null;

    /**
     * @var string $PBNString
     */
    protected $PBNString = null;

    /**
     * @var PerformanceBasedNavigationCapabilities $SuggestedPBNEquipment
     */
    protected $SuggestedPBNEquipment = null;

    /**
     * @var string $SuggestedPBNString
     */
    protected $SuggestedPBNString = null;

    /**
     * @var ArrayOfAircraftValidationError $ValidationErrors
     */
    protected $ValidationErrors = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getItem10aString()
    {
      return $this->Item10aString;
    }

    /**
     * @param string $Item10aString
     * @return AircraftEquipmentValidationResponse
     */
    public function setItem10aString($Item10aString)
    {
      $this->Item10aString = $Item10aString;
      return $this;
    }

    /**
     * @return string
     */
    public function getItem10bString()
    {
      return $this->Item10bString;
    }

    /**
     * @param string $Item10bString
     * @return AircraftEquipmentValidationResponse
     */
    public function setItem10bString($Item10bString)
    {
      $this->Item10bString = $Item10bString;
      return $this;
    }

    /**
     * @return string
     */
    public function getPBNString()
    {
      return $this->PBNString;
    }

    /**
     * @param string $PBNString
     * @return AircraftEquipmentValidationResponse
     */
    public function setPBNString($PBNString)
    {
      $this->PBNString = $PBNString;
      return $this;
    }

    /**
     * @return PerformanceBasedNavigationCapabilities
     */
    public function getSuggestedPBNEquipment()
    {
      return $this->SuggestedPBNEquipment;
    }

    /**
     * @param PerformanceBasedNavigationCapabilities $SuggestedPBNEquipment
     * @return AircraftEquipmentValidationResponse
     */
    public function setSuggestedPBNEquipment($SuggestedPBNEquipment)
    {
      $this->SuggestedPBNEquipment = $SuggestedPBNEquipment;
      return $this;
    }

    /**
     * @return string
     */
    public function getSuggestedPBNString()
    {
      return $this->SuggestedPBNString;
    }

    /**
     * @param string $SuggestedPBNString
     * @return AircraftEquipmentValidationResponse
     */
    public function setSuggestedPBNString($SuggestedPBNString)
    {
      $this->SuggestedPBNString = $SuggestedPBNString;
      return $this;
    }

    /**
     * @return ArrayOfAircraftValidationError
     */
    public function getValidationErrors()
    {
      return $this->ValidationErrors;
    }

    /**
     * @param ArrayOfAircraftValidationError $ValidationErrors
     * @return AircraftEquipmentValidationResponse
     */
    public function setValidationErrors($ValidationErrors)
    {
      $this->ValidationErrors = $ValidationErrors;
      return $this;
    }

}
