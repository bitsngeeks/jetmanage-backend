<?php

class AircraftEquipment
{

    /**
     * @var PerformanceBasedNavigationCapabilities $PerformanceBasedNavigation
     */
    protected $PerformanceBasedNavigation = null;

    /**
     * @var RadioAndNavigationEquipment $RadioAndNavEquipment
     */
    protected $RadioAndNavEquipment = null;

    /**
     * @var string $SELCALCode
     */
    protected $SELCALCode = null;

    /**
     * @var SSREquipment $SurveillanceEquipment
     */
    protected $SurveillanceEquipment = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return PerformanceBasedNavigationCapabilities
     */
    public function getPerformanceBasedNavigation()
    {
      return $this->PerformanceBasedNavigation;
    }

    /**
     * @param PerformanceBasedNavigationCapabilities $PerformanceBasedNavigation
     * @return AircraftEquipment
     */
    public function setPerformanceBasedNavigation($PerformanceBasedNavigation)
    {
      $this->PerformanceBasedNavigation = $PerformanceBasedNavigation;
      return $this;
    }

    /**
     * @return RadioAndNavigationEquipment
     */
    public function getRadioAndNavEquipment()
    {
      return $this->RadioAndNavEquipment;
    }

    /**
     * @param RadioAndNavigationEquipment $RadioAndNavEquipment
     * @return AircraftEquipment
     */
    public function setRadioAndNavEquipment($RadioAndNavEquipment)
    {
      $this->RadioAndNavEquipment = $RadioAndNavEquipment;
      return $this;
    }

    /**
     * @return string
     */
    public function getSELCALCode()
    {
      return $this->SELCALCode;
    }

    /**
     * @param string $SELCALCode
     * @return AircraftEquipment
     */
    public function setSELCALCode($SELCALCode)
    {
      $this->SELCALCode = $SELCALCode;
      return $this;
    }

    /**
     * @return SSREquipment
     */
    public function getSurveillanceEquipment()
    {
      return $this->SurveillanceEquipment;
    }

    /**
     * @param SSREquipment $SurveillanceEquipment
     * @return AircraftEquipment
     */
    public function setSurveillanceEquipment($SurveillanceEquipment)
    {
      $this->SurveillanceEquipment = $SurveillanceEquipment;
      return $this;
    }

}
