<?php

class SingleEngineRiskChartDocumentSpecification extends DocumentSpecification
{

    /**
     * @var DocumentFormat $Format
     */
    protected $Format = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return DocumentFormat
     */
    public function getFormat()
    {
      return $this->Format;
    }

    /**
     * @param DocumentFormat $Format
     * @return SingleEngineRiskChartDocumentSpecification
     */
    public function setFormat($Format)
    {
      $this->Format = $Format;
      return $this;
    }

}
