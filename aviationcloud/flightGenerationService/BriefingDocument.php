<?php

class BriefingDocument
{

    /**
     * @var base64Binary $Data
     */
    protected $Data = null;

    /**
     * @var string $DownloadURL
     */
    protected $DownloadURL = null;

    /**
     * @var DocumentFormat $Format
     */
    protected $Format = null;

    /**
     * @var ArrayOfBriefingDocumentMetaData $MetaDataCollection
     */
    protected $MetaDataCollection = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return base64Binary
     */
    public function getData()
    {
      return $this->Data;
    }

    /**
     * @param base64Binary $Data
     * @return BriefingDocument
     */
    public function setData($Data)
    {
      $this->Data = $Data;
      return $this;
    }

    /**
     * @return string
     */
    public function getDownloadURL()
    {
      return $this->DownloadURL;
    }

    /**
     * @param string $DownloadURL
     * @return BriefingDocument
     */
    public function setDownloadURL($DownloadURL)
    {
      $this->DownloadURL = $DownloadURL;
      return $this;
    }

    /**
     * @return DocumentFormat
     */
    public function getFormat()
    {
      return $this->Format;
    }

    /**
     * @param DocumentFormat $Format
     * @return BriefingDocument
     */
    public function setFormat($Format)
    {
      $this->Format = $Format;
      return $this;
    }

    /**
     * @return ArrayOfBriefingDocumentMetaData
     */
    public function getMetaDataCollection()
    {
      return $this->MetaDataCollection;
    }

    /**
     * @param ArrayOfBriefingDocumentMetaData $MetaDataCollection
     * @return BriefingDocument
     */
    public function setMetaDataCollection($MetaDataCollection)
    {
      $this->MetaDataCollection = $MetaDataCollection;
      return $this;
    }

}
