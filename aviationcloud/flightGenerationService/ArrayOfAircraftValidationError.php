<?php

class ArrayOfAircraftValidationError implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var AircraftValidationError[] $AircraftValidationError
     */
    protected $AircraftValidationError = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return AircraftValidationError[]
     */
    public function getAircraftValidationError()
    {
      return $this->AircraftValidationError;
    }

    /**
     * @param AircraftValidationError[] $AircraftValidationError
     * @return ArrayOfAircraftValidationError
     */
    public function setAircraftValidationError(array $AircraftValidationError = null)
    {
      $this->AircraftValidationError = $AircraftValidationError;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->AircraftValidationError[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return AircraftValidationError
     */
    public function offsetGet($offset)
    {
      return $this->AircraftValidationError[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param AircraftValidationError $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->AircraftValidationError[] = $value;
      } else {
        $this->AircraftValidationError[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->AircraftValidationError[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return AircraftValidationError Return the current element
     */
    public function current()
    {
      return current($this->AircraftValidationError);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->AircraftValidationError);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->AircraftValidationError);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->AircraftValidationError);
    }

    /**
     * Countable implementation
     *
     * @return AircraftValidationError Return count of elements
     */
    public function count()
    {
      return count($this->AircraftValidationError);
    }

}
