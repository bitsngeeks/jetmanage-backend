<?php

class AircraftMaxWeightExceededError
{

    /**
     * @var float $MaxValue
     */
    protected $MaxValue = null;

    /**
     * @var float $SpecifiedValue
     */
    protected $SpecifiedValue = null;

    /**
     * @var string $ValueName
     */
    protected $ValueName = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getMaxValue()
    {
      return $this->MaxValue;
    }

    /**
     * @param float $MaxValue
     * @return AircraftMaxWeightExceededError
     */
    public function setMaxValue($MaxValue)
    {
      $this->MaxValue = $MaxValue;
      return $this;
    }

    /**
     * @return float
     */
    public function getSpecifiedValue()
    {
      return $this->SpecifiedValue;
    }

    /**
     * @param float $SpecifiedValue
     * @return AircraftMaxWeightExceededError
     */
    public function setSpecifiedValue($SpecifiedValue)
    {
      $this->SpecifiedValue = $SpecifiedValue;
      return $this;
    }

    /**
     * @return string
     */
    public function getValueName()
    {
      return $this->ValueName;
    }

    /**
     * @param string $ValueName
     * @return AircraftMaxWeightExceededError
     */
    public function setValueName($ValueName)
    {
      $this->ValueName = $ValueName;
      return $this;
    }

}
