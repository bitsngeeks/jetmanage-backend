<?php

class BriefingDocumentMetaData
{

    /**
     * @var string $DocumentID
     */
    protected $DocumentID = null;

    /**
     * @var string $DocumentTitle
     */
    protected $DocumentTitle = null;

    /**
     * @var string $FlightLevel
     */
    protected $FlightLevel = null;

    /**
     * @var \DateTime $ValidFrom
     */
    protected $ValidFrom = null;

    /**
     * @var \DateTime $ValidTo
     */
    protected $ValidTo = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getDocumentID()
    {
      return $this->DocumentID;
    }

    /**
     * @param string $DocumentID
     * @return BriefingDocumentMetaData
     */
    public function setDocumentID($DocumentID)
    {
      $this->DocumentID = $DocumentID;
      return $this;
    }

    /**
     * @return string
     */
    public function getDocumentTitle()
    {
      return $this->DocumentTitle;
    }

    /**
     * @param string $DocumentTitle
     * @return BriefingDocumentMetaData
     */
    public function setDocumentTitle($DocumentTitle)
    {
      $this->DocumentTitle = $DocumentTitle;
      return $this;
    }

    /**
     * @return string
     */
    public function getFlightLevel()
    {
      return $this->FlightLevel;
    }

    /**
     * @param string $FlightLevel
     * @return BriefingDocumentMetaData
     */
    public function setFlightLevel($FlightLevel)
    {
      $this->FlightLevel = $FlightLevel;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getValidFrom()
    {
      if ($this->ValidFrom == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->ValidFrom);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $ValidFrom
     * @return BriefingDocumentMetaData
     */
    public function setValidFrom(\DateTime $ValidFrom = null)
    {
      if ($ValidFrom == null) {
       $this->ValidFrom = null;
      } else {
        $this->ValidFrom = $ValidFrom->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getValidTo()
    {
      if ($this->ValidTo == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->ValidTo);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $ValidTo
     * @return BriefingDocumentMetaData
     */
    public function setValidTo(\DateTime $ValidTo = null)
    {
      if ($ValidTo == null) {
       $this->ValidTo = null;
      } else {
        $this->ValidTo = $ValidTo->format(\DateTime::ATOM);
      }
      return $this;
    }

}
