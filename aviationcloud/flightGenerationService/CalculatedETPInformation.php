<?php

class CalculatedETPInformation
{

    /**
     * @var ArrayOfAirport $AdequateAirports
     */
    protected $AdequateAirports = null;

    /**
     * @var ArrayOfAirportTimeWindow $AirportTimeWindows
     */
    protected $AirportTimeWindows = null;

    /**
     * @var ArrayOfETOPSSettingSpecification $ETOPSCertification
     */
    protected $ETOPSCertification = null;

    /**
     * @var ArrayOfAirport $ETPAirports
     */
    protected $ETPAirports = null;

    /**
     * @var ArrayOfETPPoint $ETPPoints
     */
    protected $ETPPoints = null;

    /**
     * @var ArrayOfETPScenario $ETPScenarios
     */
    protected $ETPScenarios = null;

    /**
     * @var ETPPoint $PointOfSafeReturn
     */
    protected $PointOfSafeReturn = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfAirport
     */
    public function getAdequateAirports()
    {
      return $this->AdequateAirports;
    }

    /**
     * @param ArrayOfAirport $AdequateAirports
     * @return CalculatedETPInformation
     */
    public function setAdequateAirports($AdequateAirports)
    {
      $this->AdequateAirports = $AdequateAirports;
      return $this;
    }

    /**
     * @return ArrayOfAirportTimeWindow
     */
    public function getAirportTimeWindows()
    {
      return $this->AirportTimeWindows;
    }

    /**
     * @param ArrayOfAirportTimeWindow $AirportTimeWindows
     * @return CalculatedETPInformation
     */
    public function setAirportTimeWindows($AirportTimeWindows)
    {
      $this->AirportTimeWindows = $AirportTimeWindows;
      return $this;
    }

    /**
     * @return ArrayOfETOPSSettingSpecification
     */
    public function getETOPSCertification()
    {
      return $this->ETOPSCertification;
    }

    /**
     * @param ArrayOfETOPSSettingSpecification $ETOPSCertification
     * @return CalculatedETPInformation
     */
    public function setETOPSCertification($ETOPSCertification)
    {
      $this->ETOPSCertification = $ETOPSCertification;
      return $this;
    }

    /**
     * @return ArrayOfAirport
     */
    public function getETPAirports()
    {
      return $this->ETPAirports;
    }

    /**
     * @param ArrayOfAirport $ETPAirports
     * @return CalculatedETPInformation
     */
    public function setETPAirports($ETPAirports)
    {
      $this->ETPAirports = $ETPAirports;
      return $this;
    }

    /**
     * @return ArrayOfETPPoint
     */
    public function getETPPoints()
    {
      return $this->ETPPoints;
    }

    /**
     * @param ArrayOfETPPoint $ETPPoints
     * @return CalculatedETPInformation
     */
    public function setETPPoints($ETPPoints)
    {
      $this->ETPPoints = $ETPPoints;
      return $this;
    }

    /**
     * @return ArrayOfETPScenario
     */
    public function getETPScenarios()
    {
      return $this->ETPScenarios;
    }

    /**
     * @param ArrayOfETPScenario $ETPScenarios
     * @return CalculatedETPInformation
     */
    public function setETPScenarios($ETPScenarios)
    {
      $this->ETPScenarios = $ETPScenarios;
      return $this;
    }

    /**
     * @return ETPPoint
     */
    public function getPointOfSafeReturn()
    {
      return $this->PointOfSafeReturn;
    }

    /**
     * @param ETPPoint $PointOfSafeReturn
     * @return CalculatedETPInformation
     */
    public function setPointOfSafeReturn($PointOfSafeReturn)
    {
      $this->PointOfSafeReturn = $PointOfSafeReturn;
      return $this;
    }

}
