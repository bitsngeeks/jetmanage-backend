<?php

class ArrayOfByAltitudeClimbProfile implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var ByAltitudeClimbProfile[] $ByAltitudeClimbProfile
     */
    protected $ByAltitudeClimbProfile = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ByAltitudeClimbProfile[]
     */
    public function getByAltitudeClimbProfile()
    {
      return $this->ByAltitudeClimbProfile;
    }

    /**
     * @param ByAltitudeClimbProfile[] $ByAltitudeClimbProfile
     * @return ArrayOfByAltitudeClimbProfile
     */
    public function setByAltitudeClimbProfile(array $ByAltitudeClimbProfile = null)
    {
      $this->ByAltitudeClimbProfile = $ByAltitudeClimbProfile;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->ByAltitudeClimbProfile[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return ByAltitudeClimbProfile
     */
    public function offsetGet($offset)
    {
      return $this->ByAltitudeClimbProfile[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param ByAltitudeClimbProfile $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->ByAltitudeClimbProfile[] = $value;
      } else {
        $this->ByAltitudeClimbProfile[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->ByAltitudeClimbProfile[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return ByAltitudeClimbProfile Return the current element
     */
    public function current()
    {
      return current($this->ByAltitudeClimbProfile);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->ByAltitudeClimbProfile);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->ByAltitudeClimbProfile);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->ByAltitudeClimbProfile);
    }

    /**
     * Countable implementation
     *
     * @return ByAltitudeClimbProfile Return count of elements
     */
    public function count()
    {
      return count($this->ByAltitudeClimbProfile);
    }

}
