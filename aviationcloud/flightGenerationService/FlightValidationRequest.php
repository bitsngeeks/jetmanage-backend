<?php

class FlightValidationRequest extends RequestBase
{

    /**
     * @var string $ATCFplMessageText
     */
    protected $ATCFplMessageText = null;

    /**
     * @var ATCFPLMessage $FPLMessage
     */
    protected $FPLMessage = null;

    /**
     * @var FlightLog $FlightLog
     */
    protected $FlightLog = null;

    /**
     * @var FlightSpecification $FlightSpecification
     */
    protected $FlightSpecification = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return string
     */
    public function getATCFplMessageText()
    {
      return $this->ATCFplMessageText;
    }

    /**
     * @param string $ATCFplMessageText
     * @return FlightValidationRequest
     */
    public function setATCFplMessageText($ATCFplMessageText)
    {
      $this->ATCFplMessageText = $ATCFplMessageText;
      return $this;
    }

    /**
     * @return ATCFPLMessage
     */
    public function getFPLMessage()
    {
      return $this->FPLMessage;
    }

    /**
     * @param ATCFPLMessage $FPLMessage
     * @return FlightValidationRequest
     */
    public function setFPLMessage($FPLMessage)
    {
      $this->FPLMessage = $FPLMessage;
      return $this;
    }

    /**
     * @return FlightLog
     */
    public function getFlightLog()
    {
      return $this->FlightLog;
    }

    /**
     * @param FlightLog $FlightLog
     * @return FlightValidationRequest
     */
    public function setFlightLog($FlightLog)
    {
      $this->FlightLog = $FlightLog;
      return $this;
    }

    /**
     * @return FlightSpecification
     */
    public function getFlightSpecification()
    {
      return $this->FlightSpecification;
    }

    /**
     * @param FlightSpecification $FlightSpecification
     * @return FlightValidationRequest
     */
    public function setFlightSpecification($FlightSpecification)
    {
      $this->FlightSpecification = $FlightSpecification;
      return $this;
    }

}
