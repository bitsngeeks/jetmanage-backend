<?php

class ValueFuelPolicy extends FuelPolicy
{

    /**
     * @var Weight $Value
     */
    protected $Value = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return Weight
     */
    public function getValue()
    {
      return $this->Value;
    }

    /**
     * @param Weight $Value
     * @return ValueFuelPolicy
     */
    public function setValue($Value)
    {
      $this->Value = $Value;
      return $this;
    }

}
