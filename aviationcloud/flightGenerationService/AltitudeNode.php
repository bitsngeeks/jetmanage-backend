<?php

class AltitudeNode
{

    /**
     * @var Altitude $Height
     */
    protected $Height = null;

    /**
     * @var RouteNodeIdentifier $Waypoint
     */
    protected $Waypoint = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Altitude
     */
    public function getHeight()
    {
      return $this->Height;
    }

    /**
     * @param Altitude $Height
     * @return AltitudeNode
     */
    public function setHeight($Height)
    {
      $this->Height = $Height;
      return $this;
    }

    /**
     * @return RouteNodeIdentifier
     */
    public function getWaypoint()
    {
      return $this->Waypoint;
    }

    /**
     * @param RouteNodeIdentifier $Waypoint
     * @return AltitudeNode
     */
    public function setWaypoint($Waypoint)
    {
      $this->Waypoint = $Waypoint;
      return $this;
    }

}
