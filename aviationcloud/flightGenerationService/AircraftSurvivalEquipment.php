<?php

class AircraftSurvivalEquipment
{

    /**
     * @var boolean $Desert
     */
    protected $Desert = null;

    /**
     * @var boolean $Maritime
     */
    protected $Maritime = null;

    /**
     * @var boolean $Other
     */
    protected $Other = null;

    /**
     * @var boolean $Polar
     */
    protected $Polar = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return boolean
     */
    public function getDesert()
    {
      return $this->Desert;
    }

    /**
     * @param boolean $Desert
     * @return AircraftSurvivalEquipment
     */
    public function setDesert($Desert)
    {
      $this->Desert = $Desert;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getMaritime()
    {
      return $this->Maritime;
    }

    /**
     * @param boolean $Maritime
     * @return AircraftSurvivalEquipment
     */
    public function setMaritime($Maritime)
    {
      $this->Maritime = $Maritime;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getOther()
    {
      return $this->Other;
    }

    /**
     * @param boolean $Other
     * @return AircraftSurvivalEquipment
     */
    public function setOther($Other)
    {
      $this->Other = $Other;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getPolar()
    {
      return $this->Polar;
    }

    /**
     * @param boolean $Polar
     * @return AircraftSurvivalEquipment
     */
    public function setPolar($Polar)
    {
      $this->Polar = $Polar;
      return $this;
    }

}
