<?php

class ProcedureOptions
{

    /**
     * @var ArrayOfProcedureIdentifier $ProcedureIdentifiers
     */
    protected $ProcedureIdentifiers = null;

    /**
     * @var boolean $UseRelaxedProcedureMatching
     */
    protected $UseRelaxedProcedureMatching = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfProcedureIdentifier
     */
    public function getProcedureIdentifiers()
    {
      return $this->ProcedureIdentifiers;
    }

    /**
     * @param ArrayOfProcedureIdentifier $ProcedureIdentifiers
     * @return ProcedureOptions
     */
    public function setProcedureIdentifiers($ProcedureIdentifiers)
    {
      $this->ProcedureIdentifiers = $ProcedureIdentifiers;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUseRelaxedProcedureMatching()
    {
      return $this->UseRelaxedProcedureMatching;
    }

    /**
     * @param boolean $UseRelaxedProcedureMatching
     * @return ProcedureOptions
     */
    public function setUseRelaxedProcedureMatching($UseRelaxedProcedureMatching)
    {
      $this->UseRelaxedProcedureMatching = $UseRelaxedProcedureMatching;
      return $this;
    }

}
