<?php

class ModeSCapabilities
{

    /**
     * @var boolean $AircraftIdentification
     */
    protected $AircraftIdentification = null;

    /**
     * @var boolean $EnhancedSurveillance
     */
    protected $EnhancedSurveillance = null;

    /**
     * @var boolean $ExtendedSquitterADSB
     */
    protected $ExtendedSquitterADSB = null;

    /**
     * @var boolean $ModeSCapable
     */
    protected $ModeSCapable = null;

    /**
     * @var boolean $PreassureAltitude
     */
    protected $PreassureAltitude = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return boolean
     */
    public function getAircraftIdentification()
    {
      return $this->AircraftIdentification;
    }

    /**
     * @param boolean $AircraftIdentification
     * @return ModeSCapabilities
     */
    public function setAircraftIdentification($AircraftIdentification)
    {
      $this->AircraftIdentification = $AircraftIdentification;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getEnhancedSurveillance()
    {
      return $this->EnhancedSurveillance;
    }

    /**
     * @param boolean $EnhancedSurveillance
     * @return ModeSCapabilities
     */
    public function setEnhancedSurveillance($EnhancedSurveillance)
    {
      $this->EnhancedSurveillance = $EnhancedSurveillance;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getExtendedSquitterADSB()
    {
      return $this->ExtendedSquitterADSB;
    }

    /**
     * @param boolean $ExtendedSquitterADSB
     * @return ModeSCapabilities
     */
    public function setExtendedSquitterADSB($ExtendedSquitterADSB)
    {
      $this->ExtendedSquitterADSB = $ExtendedSquitterADSB;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getModeSCapable()
    {
      return $this->ModeSCapable;
    }

    /**
     * @param boolean $ModeSCapable
     * @return ModeSCapabilities
     */
    public function setModeSCapable($ModeSCapable)
    {
      $this->ModeSCapable = $ModeSCapable;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getPreassureAltitude()
    {
      return $this->PreassureAltitude;
    }

    /**
     * @param boolean $PreassureAltitude
     * @return ModeSCapabilities
     */
    public function setPreassureAltitude($PreassureAltitude)
    {
      $this->PreassureAltitude = $PreassureAltitude;
      return $this;
    }

}
