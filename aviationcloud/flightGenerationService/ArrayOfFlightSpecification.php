<?php

class ArrayOfFlightSpecification implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var FlightSpecification[] $FlightSpecification
     */
    protected $FlightSpecification = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return FlightSpecification[]
     */
    public function getFlightSpecification()
    {
      return $this->FlightSpecification;
    }

    /**
     * @param FlightSpecification[] $FlightSpecification
     * @return ArrayOfFlightSpecification
     */
    public function setFlightSpecification(array $FlightSpecification = null)
    {
      $this->FlightSpecification = $FlightSpecification;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->FlightSpecification[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return FlightSpecification
     */
    public function offsetGet($offset)
    {
      return $this->FlightSpecification[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param FlightSpecification $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->FlightSpecification[] = $value;
      } else {
        $this->FlightSpecification[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->FlightSpecification[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return FlightSpecification Return the current element
     */
    public function current()
    {
      return current($this->FlightSpecification);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->FlightSpecification);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->FlightSpecification);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->FlightSpecification);
    }

    /**
     * Countable implementation
     *
     * @return FlightSpecification Return count of elements
     */
    public function count()
    {
      return count($this->FlightSpecification);
    }

}
