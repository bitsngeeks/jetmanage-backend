<?php

class SimpleCruiseProfile extends CruiseProfile
{

    /**
     * @var Weight $CruiseFuelPrHour
     */
    protected $CruiseFuelPrHour = null;

    /**
     * @var Speed $CruiseTrueAirspeed
     */
    protected $CruiseTrueAirspeed = null;

    /**
     * @var Altitude $DefaultCruiseAltitude
     */
    protected $DefaultCruiseAltitude = null;

    /**
     * @var Altitude $MaxAltitude
     */
    protected $MaxAltitude = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return Weight
     */
    public function getCruiseFuelPrHour()
    {
      return $this->CruiseFuelPrHour;
    }

    /**
     * @param Weight $CruiseFuelPrHour
     * @return SimpleCruiseProfile
     */
    public function setCruiseFuelPrHour($CruiseFuelPrHour)
    {
      $this->CruiseFuelPrHour = $CruiseFuelPrHour;
      return $this;
    }

    /**
     * @return Speed
     */
    public function getCruiseTrueAirspeed()
    {
      return $this->CruiseTrueAirspeed;
    }

    /**
     * @param Speed $CruiseTrueAirspeed
     * @return SimpleCruiseProfile
     */
    public function setCruiseTrueAirspeed($CruiseTrueAirspeed)
    {
      $this->CruiseTrueAirspeed = $CruiseTrueAirspeed;
      return $this;
    }

    /**
     * @return Altitude
     */
    public function getDefaultCruiseAltitude()
    {
      return $this->DefaultCruiseAltitude;
    }

    /**
     * @param Altitude $DefaultCruiseAltitude
     * @return SimpleCruiseProfile
     */
    public function setDefaultCruiseAltitude($DefaultCruiseAltitude)
    {
      $this->DefaultCruiseAltitude = $DefaultCruiseAltitude;
      return $this;
    }

    /**
     * @return Altitude
     */
    public function getMaxAltitude()
    {
      return $this->MaxAltitude;
    }

    /**
     * @param Altitude $MaxAltitude
     * @return SimpleCruiseProfile
     */
    public function setMaxAltitude($MaxAltitude)
    {
      $this->MaxAltitude = $MaxAltitude;
      return $this;
    }

}
