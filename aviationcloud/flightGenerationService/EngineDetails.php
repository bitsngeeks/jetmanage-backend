<?php

class EngineDetails
{

    /**
     * @var string $EngineName
     */
    protected $EngineName = null;

    /**
     * @var AircraftEngineType $EngineType
     */
    protected $EngineType = null;

    /**
     * @var int $NumberOfEngines
     */
    protected $NumberOfEngines = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getEngineName()
    {
      return $this->EngineName;
    }

    /**
     * @param string $EngineName
     * @return EngineDetails
     */
    public function setEngineName($EngineName)
    {
      $this->EngineName = $EngineName;
      return $this;
    }

    /**
     * @return AircraftEngineType
     */
    public function getEngineType()
    {
      return $this->EngineType;
    }

    /**
     * @param AircraftEngineType $EngineType
     * @return EngineDetails
     */
    public function setEngineType($EngineType)
    {
      $this->EngineType = $EngineType;
      return $this;
    }

    /**
     * @return int
     */
    public function getNumberOfEngines()
    {
      return $this->NumberOfEngines;
    }

    /**
     * @param int $NumberOfEngines
     * @return EngineDetails
     */
    public function setNumberOfEngines($NumberOfEngines)
    {
      $this->NumberOfEngines = $NumberOfEngines;
      return $this;
    }

}
