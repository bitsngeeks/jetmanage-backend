<?php

class SimpleDescentProfile extends DescentProfile
{

    /**
     * @var int $DescentFeetPrMinute
     */
    protected $DescentFeetPrMinute = null;

    /**
     * @var Weight $DescentFuelPrHour
     */
    protected $DescentFuelPrHour = null;

    /**
     * @var Speed $DescentTrueAirspeed
     */
    protected $DescentTrueAirspeed = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return int
     */
    public function getDescentFeetPrMinute()
    {
      return $this->DescentFeetPrMinute;
    }

    /**
     * @param int $DescentFeetPrMinute
     * @return SimpleDescentProfile
     */
    public function setDescentFeetPrMinute($DescentFeetPrMinute)
    {
      $this->DescentFeetPrMinute = $DescentFeetPrMinute;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getDescentFuelPrHour()
    {
      return $this->DescentFuelPrHour;
    }

    /**
     * @param Weight $DescentFuelPrHour
     * @return SimpleDescentProfile
     */
    public function setDescentFuelPrHour($DescentFuelPrHour)
    {
      $this->DescentFuelPrHour = $DescentFuelPrHour;
      return $this;
    }

    /**
     * @return Speed
     */
    public function getDescentTrueAirspeed()
    {
      return $this->DescentTrueAirspeed;
    }

    /**
     * @param Speed $DescentTrueAirspeed
     * @return SimpleDescentProfile
     */
    public function setDescentTrueAirspeed($DescentTrueAirspeed)
    {
      $this->DescentTrueAirspeed = $DescentTrueAirspeed;
      return $this;
    }

}
