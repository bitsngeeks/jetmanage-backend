<?php

class TripCalculationRequest extends RequestBase
{

    /**
     * @var Weight $InitialFuelOnBoard
     */
    protected $InitialFuelOnBoard = null;

    /**
     * @var TripPackageRequest $TripPackage
     */
    protected $TripPackage = null;

    /**
     * @var TripSpecification $Trips
     */
    protected $Trips = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return Weight
     */
    public function getInitialFuelOnBoard()
    {
      return $this->InitialFuelOnBoard;
    }

    /**
     * @param Weight $InitialFuelOnBoard
     * @return TripCalculationRequest
     */
    public function setInitialFuelOnBoard($InitialFuelOnBoard)
    {
      $this->InitialFuelOnBoard = $InitialFuelOnBoard;
      return $this;
    }

    /**
     * @return TripPackageRequest
     */
    public function getTripPackage()
    {
      return $this->TripPackage;
    }

    /**
     * @param TripPackageRequest $TripPackage
     * @return TripCalculationRequest
     */
    public function setTripPackage($TripPackage)
    {
      $this->TripPackage = $TripPackage;
      return $this;
    }

    /**
     * @return TripSpecification
     */
    public function getTrips()
    {
      return $this->Trips;
    }

    /**
     * @param TripSpecification $Trips
     * @return TripCalculationRequest
     */
    public function setTrips($Trips)
    {
      $this->Trips = $Trips;
      return $this;
    }

}
