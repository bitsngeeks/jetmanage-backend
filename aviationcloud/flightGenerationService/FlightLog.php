<?php

class FlightLog
{

    /**
     * @var ATCFPLMessage $ATCData
     */
    protected $ATCData = null;

    /**
     * @var string $ATCSpeed
     */
    protected $ATCSpeed = null;

    /**
     * @var ArrayOfAirport $AdequateAirports
     */
    protected $AdequateAirports = null;

    /**
     * @var Currency $AircraftCost
     */
    protected $AircraftCost = null;

    /**
     * @var CalculatedAircraftWeights $AircraftWeights
     */
    protected $AircraftWeights = null;

    /**
     * @var ArrayOfIntersectionPoint $AirspaceIntersectionPoints
     */
    protected $AirspaceIntersectionPoints = null;

    /**
     * @var string $Alternate2CruiseProfile
     */
    protected $Alternate2CruiseProfile = null;

    /**
     * @var string $AlternateCruiseProfile
     */
    protected $AlternateCruiseProfile = null;

    /**
     * @var Route $CalcualtedAlternate1Route
     */
    protected $CalcualtedAlternate1Route = null;

    /**
     * @var Route $CalcualtedAlternate2Route
     */
    protected $CalcualtedAlternate2Route = null;

    /**
     * @var Route $CalculatedRoute
     */
    protected $CalculatedRoute = null;

    /**
     * @var Route $CalculatedSecondDestinationAlternateRoute
     */
    protected $CalculatedSecondDestinationAlternateRoute = null;

    /**
     * @var Route $CalculatedSecondDestinationRoute
     */
    protected $CalculatedSecondDestinationRoute = null;

    /**
     * @var Route $CalculatedTakeOffAlternateRoute
     */
    protected $CalculatedTakeOffAlternateRoute = null;

    /**
     * @var CalculationInformation $CalculationReport
     */
    protected $CalculationReport = null;

    /**
     * @var string $ClimbProfile
     */
    protected $ClimbProfile = null;

    /**
     * @var string $CruiseProfile
     */
    protected $CruiseProfile = null;

    /**
     * @var string $DescentProfile
     */
    protected $DescentProfile = null;

    /**
     * @var CalculatedETPInformation $ETPInformation
     */
    protected $ETPInformation = null;

    /**
     * @var OverflightCalculationInformation $EnrouteCharges
     */
    protected $EnrouteCharges = null;

    /**
     * @var ArrayOfAFTNAddress $FileToAddresses
     */
    protected $FileToAddresses = null;

    /**
     * @var FlightAnalysis $FlightAnalysis
     */
    protected $FlightAnalysis = null;

    /**
     * @var FlightDistance $FlightDistances
     */
    protected $FlightDistances = null;

    /**
     * @var FLightATCEET $FlightEETData
     */
    protected $FlightEETData = null;

    /**
     * @var FlightFuelValues $FlightFuel
     */
    protected $FlightFuel = null;

    /**
     * @var FlightLevelOptimizationData $FlightLevelOptimzationData
     */
    protected $FlightLevelOptimzationData = null;

    /**
     * @var FlightMetaData $FlightMetaData
     */
    protected $FlightMetaData = null;

    /**
     * @var FlightSpecification $FlightSpecification
     */
    protected $FlightSpecification = null;

    /**
     * @var FlightTimes $FlightTimes
     */
    protected $FlightTimes = null;

    /**
     * @var Currency $FuelCost
     */
    protected $FuelCost = null;

    /**
     * @var string $ICAOFlightPlanString
     */
    protected $ICAOFlightPlanString = null;

    /**
     * @var FlightMetrologicalData $MetrologicalData
     */
    protected $MetrologicalData = null;

    /**
     * @var ArrayOfOverflightCosts $OverflightCosts
     */
    protected $OverflightCosts = null;

    /**
     * @var ArrayOfFIR $OverflownFIRs
     */
    protected $OverflownFIRs = null;

    /**
     * @var PathwayData $PathwayData
     */
    protected $PathwayData = null;

    /**
     * @var WaypointData $ReducedContingencyDecisionPoint
     */
    protected $ReducedContingencyDecisionPoint = null;

    /**
     * @var string $TakeoffAlternateCruiseProfile
     */
    protected $TakeoffAlternateCruiseProfile = null;

    /**
     * @var Currency $TotalCost
     */
    protected $TotalCost = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ATCFPLMessage
     */
    public function getATCData()
    {
      return $this->ATCData;
    }

    /**
     * @param ATCFPLMessage $ATCData
     * @return FlightLog
     */
    public function setATCData($ATCData)
    {
      $this->ATCData = $ATCData;
      return $this;
    }

    /**
     * @return string
     */
    public function getATCSpeed()
    {
      return $this->ATCSpeed;
    }

    /**
     * @param string $ATCSpeed
     * @return FlightLog
     */
    public function setATCSpeed($ATCSpeed)
    {
      $this->ATCSpeed = $ATCSpeed;
      return $this;
    }

    /**
     * @return ArrayOfAirport
     */
    public function getAdequateAirports()
    {
      return $this->AdequateAirports;
    }

    /**
     * @param ArrayOfAirport $AdequateAirports
     * @return FlightLog
     */
    public function setAdequateAirports($AdequateAirports)
    {
      $this->AdequateAirports = $AdequateAirports;
      return $this;
    }

    /**
     * @return Currency
     */
    public function getAircraftCost()
    {
      return $this->AircraftCost;
    }

    /**
     * @param Currency $AircraftCost
     * @return FlightLog
     */
    public function setAircraftCost($AircraftCost)
    {
      $this->AircraftCost = $AircraftCost;
      return $this;
    }

    /**
     * @return CalculatedAircraftWeights
     */
    public function getAircraftWeights()
    {
      return $this->AircraftWeights;
    }

    /**
     * @param CalculatedAircraftWeights $AircraftWeights
     * @return FlightLog
     */
    public function setAircraftWeights($AircraftWeights)
    {
      $this->AircraftWeights = $AircraftWeights;
      return $this;
    }

    /**
     * @return ArrayOfIntersectionPoint
     */
    public function getAirspaceIntersectionPoints()
    {
      return $this->AirspaceIntersectionPoints;
    }

    /**
     * @param ArrayOfIntersectionPoint $AirspaceIntersectionPoints
     * @return FlightLog
     */
    public function setAirspaceIntersectionPoints($AirspaceIntersectionPoints)
    {
      $this->AirspaceIntersectionPoints = $AirspaceIntersectionPoints;
      return $this;
    }

    /**
     * @return string
     */
    public function getAlternate2CruiseProfile()
    {
      return $this->Alternate2CruiseProfile;
    }

    /**
     * @param string $Alternate2CruiseProfile
     * @return FlightLog
     */
    public function setAlternate2CruiseProfile($Alternate2CruiseProfile)
    {
      $this->Alternate2CruiseProfile = $Alternate2CruiseProfile;
      return $this;
    }

    /**
     * @return string
     */
    public function getAlternateCruiseProfile()
    {
      return $this->AlternateCruiseProfile;
    }

    /**
     * @param string $AlternateCruiseProfile
     * @return FlightLog
     */
    public function setAlternateCruiseProfile($AlternateCruiseProfile)
    {
      $this->AlternateCruiseProfile = $AlternateCruiseProfile;
      return $this;
    }

    /**
     * @return Route
     */
    public function getCalcualtedAlternate1Route()
    {
      return $this->CalcualtedAlternate1Route;
    }

    /**
     * @param Route $CalcualtedAlternate1Route
     * @return FlightLog
     */
    public function setCalcualtedAlternate1Route($CalcualtedAlternate1Route)
    {
      $this->CalcualtedAlternate1Route = $CalcualtedAlternate1Route;
      return $this;
    }

    /**
     * @return Route
     */
    public function getCalcualtedAlternate2Route()
    {
      return $this->CalcualtedAlternate2Route;
    }

    /**
     * @param Route $CalcualtedAlternate2Route
     * @return FlightLog
     */
    public function setCalcualtedAlternate2Route($CalcualtedAlternate2Route)
    {
      $this->CalcualtedAlternate2Route = $CalcualtedAlternate2Route;
      return $this;
    }

    /**
     * @return Route
     */
    public function getCalculatedRoute()
    {
      return $this->CalculatedRoute;
    }

    /**
     * @param Route $CalculatedRoute
     * @return FlightLog
     */
    public function setCalculatedRoute($CalculatedRoute)
    {
      $this->CalculatedRoute = $CalculatedRoute;
      return $this;
    }

    /**
     * @return Route
     */
    public function getCalculatedSecondDestinationAlternateRoute()
    {
      return $this->CalculatedSecondDestinationAlternateRoute;
    }

    /**
     * @param Route $CalculatedSecondDestinationAlternateRoute
     * @return FlightLog
     */
    public function setCalculatedSecondDestinationAlternateRoute($CalculatedSecondDestinationAlternateRoute)
    {
      $this->CalculatedSecondDestinationAlternateRoute = $CalculatedSecondDestinationAlternateRoute;
      return $this;
    }

    /**
     * @return Route
     */
    public function getCalculatedSecondDestinationRoute()
    {
      return $this->CalculatedSecondDestinationRoute;
    }

    /**
     * @param Route $CalculatedSecondDestinationRoute
     * @return FlightLog
     */
    public function setCalculatedSecondDestinationRoute($CalculatedSecondDestinationRoute)
    {
      $this->CalculatedSecondDestinationRoute = $CalculatedSecondDestinationRoute;
      return $this;
    }

    /**
     * @return Route
     */
    public function getCalculatedTakeOffAlternateRoute()
    {
      return $this->CalculatedTakeOffAlternateRoute;
    }

    /**
     * @param Route $CalculatedTakeOffAlternateRoute
     * @return FlightLog
     */
    public function setCalculatedTakeOffAlternateRoute($CalculatedTakeOffAlternateRoute)
    {
      $this->CalculatedTakeOffAlternateRoute = $CalculatedTakeOffAlternateRoute;
      return $this;
    }

    /**
     * @return CalculationInformation
     */
    public function getCalculationReport()
    {
      return $this->CalculationReport;
    }

    /**
     * @param CalculationInformation $CalculationReport
     * @return FlightLog
     */
    public function setCalculationReport($CalculationReport)
    {
      $this->CalculationReport = $CalculationReport;
      return $this;
    }

    /**
     * @return string
     */
    public function getClimbProfile()
    {
      return $this->ClimbProfile;
    }

    /**
     * @param string $ClimbProfile
     * @return FlightLog
     */
    public function setClimbProfile($ClimbProfile)
    {
      $this->ClimbProfile = $ClimbProfile;
      return $this;
    }

    /**
     * @return string
     */
    public function getCruiseProfile()
    {
      return $this->CruiseProfile;
    }

    /**
     * @param string $CruiseProfile
     * @return FlightLog
     */
    public function setCruiseProfile($CruiseProfile)
    {
      $this->CruiseProfile = $CruiseProfile;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescentProfile()
    {
      return $this->DescentProfile;
    }

    /**
     * @param string $DescentProfile
     * @return FlightLog
     */
    public function setDescentProfile($DescentProfile)
    {
      $this->DescentProfile = $DescentProfile;
      return $this;
    }

    /**
     * @return CalculatedETPInformation
     */
    public function getETPInformation()
    {
      return $this->ETPInformation;
    }

    /**
     * @param CalculatedETPInformation $ETPInformation
     * @return FlightLog
     */
    public function setETPInformation($ETPInformation)
    {
      $this->ETPInformation = $ETPInformation;
      return $this;
    }

    /**
     * @return OverflightCalculationInformation
     */
    public function getEnrouteCharges()
    {
      return $this->EnrouteCharges;
    }

    /**
     * @param OverflightCalculationInformation $EnrouteCharges
     * @return FlightLog
     */
    public function setEnrouteCharges($EnrouteCharges)
    {
      $this->EnrouteCharges = $EnrouteCharges;
      return $this;
    }

    /**
     * @return ArrayOfAFTNAddress
     */
    public function getFileToAddresses()
    {
      return $this->FileToAddresses;
    }

    /**
     * @param ArrayOfAFTNAddress $FileToAddresses
     * @return FlightLog
     */
    public function setFileToAddresses($FileToAddresses)
    {
      $this->FileToAddresses = $FileToAddresses;
      return $this;
    }

    /**
     * @return FlightAnalysis
     */
    public function getFlightAnalysis()
    {
      return $this->FlightAnalysis;
    }

    /**
     * @param FlightAnalysis $FlightAnalysis
     * @return FlightLog
     */
    public function setFlightAnalysis($FlightAnalysis)
    {
      $this->FlightAnalysis = $FlightAnalysis;
      return $this;
    }

    /**
     * @return FlightDistance
     */
    public function getFlightDistances()
    {
      return $this->FlightDistances;
    }

    /**
     * @param FlightDistance $FlightDistances
     * @return FlightLog
     */
    public function setFlightDistances($FlightDistances)
    {
      $this->FlightDistances = $FlightDistances;
      return $this;
    }

    /**
     * @return FLightATCEET
     */
    public function getFlightEETData()
    {
      return $this->FlightEETData;
    }

    /**
     * @param FLightATCEET $FlightEETData
     * @return FlightLog
     */
    public function setFlightEETData($FlightEETData)
    {
      $this->FlightEETData = $FlightEETData;
      return $this;
    }

    /**
     * @return FlightFuelValues
     */
    public function getFlightFuel()
    {
      return $this->FlightFuel;
    }

    /**
     * @param FlightFuelValues $FlightFuel
     * @return FlightLog
     */
    public function setFlightFuel($FlightFuel)
    {
      $this->FlightFuel = $FlightFuel;
      return $this;
    }

    /**
     * @return FlightLevelOptimizationData
     */
    public function getFlightLevelOptimzationData()
    {
      return $this->FlightLevelOptimzationData;
    }

    /**
     * @param FlightLevelOptimizationData $FlightLevelOptimzationData
     * @return FlightLog
     */
    public function setFlightLevelOptimzationData($FlightLevelOptimzationData)
    {
      $this->FlightLevelOptimzationData = $FlightLevelOptimzationData;
      return $this;
    }

    /**
     * @return FlightMetaData
     */
    public function getFlightMetaData()
    {
      return $this->FlightMetaData;
    }

    /**
     * @param FlightMetaData $FlightMetaData
     * @return FlightLog
     */
    public function setFlightMetaData($FlightMetaData)
    {
      $this->FlightMetaData = $FlightMetaData;
      return $this;
    }

    /**
     * @return FlightSpecification
     */
    public function getFlightSpecification()
    {
      return $this->FlightSpecification;
    }

    /**
     * @param FlightSpecification $FlightSpecification
     * @return FlightLog
     */
    public function setFlightSpecification($FlightSpecification)
    {
      $this->FlightSpecification = $FlightSpecification;
      return $this;
    }

    /**
     * @return FlightTimes
     */
    public function getFlightTimes()
    {
      return $this->FlightTimes;
    }

    /**
     * @param FlightTimes $FlightTimes
     * @return FlightLog
     */
    public function setFlightTimes($FlightTimes)
    {
      $this->FlightTimes = $FlightTimes;
      return $this;
    }

    /**
     * @return Currency
     */
    public function getFuelCost()
    {
      return $this->FuelCost;
    }

    /**
     * @param Currency $FuelCost
     * @return FlightLog
     */
    public function setFuelCost($FuelCost)
    {
      $this->FuelCost = $FuelCost;
      return $this;
    }

    /**
     * @return string
     */
    public function getICAOFlightPlanString()
    {
      return $this->ICAOFlightPlanString;
    }

    /**
     * @param string $ICAOFlightPlanString
     * @return FlightLog
     */
    public function setICAOFlightPlanString($ICAOFlightPlanString)
    {
      $this->ICAOFlightPlanString = $ICAOFlightPlanString;
      return $this;
    }

    /**
     * @return FlightMetrologicalData
     */
    public function getMetrologicalData()
    {
      return $this->MetrologicalData;
    }

    /**
     * @param FlightMetrologicalData $MetrologicalData
     * @return FlightLog
     */
    public function setMetrologicalData($MetrologicalData)
    {
      $this->MetrologicalData = $MetrologicalData;
      return $this;
    }

    /**
     * @return ArrayOfOverflightCosts
     */
    public function getOverflightCosts()
    {
      return $this->OverflightCosts;
    }

    /**
     * @param ArrayOfOverflightCosts $OverflightCosts
     * @return FlightLog
     */
    public function setOverflightCosts($OverflightCosts)
    {
      $this->OverflightCosts = $OverflightCosts;
      return $this;
    }

    /**
     * @return ArrayOfFIR
     */
    public function getOverflownFIRs()
    {
      return $this->OverflownFIRs;
    }

    /**
     * @param ArrayOfFIR $OverflownFIRs
     * @return FlightLog
     */
    public function setOverflownFIRs($OverflownFIRs)
    {
      $this->OverflownFIRs = $OverflownFIRs;
      return $this;
    }

    /**
     * @return PathwayData
     */
    public function getPathwayData()
    {
      return $this->PathwayData;
    }

    /**
     * @param PathwayData $PathwayData
     * @return FlightLog
     */
    public function setPathwayData($PathwayData)
    {
      $this->PathwayData = $PathwayData;
      return $this;
    }

    /**
     * @return WaypointData
     */
    public function getReducedContingencyDecisionPoint()
    {
      return $this->ReducedContingencyDecisionPoint;
    }

    /**
     * @param WaypointData $ReducedContingencyDecisionPoint
     * @return FlightLog
     */
    public function setReducedContingencyDecisionPoint($ReducedContingencyDecisionPoint)
    {
      $this->ReducedContingencyDecisionPoint = $ReducedContingencyDecisionPoint;
      return $this;
    }

    /**
     * @return string
     */
    public function getTakeoffAlternateCruiseProfile()
    {
      return $this->TakeoffAlternateCruiseProfile;
    }

    /**
     * @param string $TakeoffAlternateCruiseProfile
     * @return FlightLog
     */
    public function setTakeoffAlternateCruiseProfile($TakeoffAlternateCruiseProfile)
    {
      $this->TakeoffAlternateCruiseProfile = $TakeoffAlternateCruiseProfile;
      return $this;
    }

    /**
     * @return Currency
     */
    public function getTotalCost()
    {
      return $this->TotalCost;
    }

    /**
     * @param Currency $TotalCost
     * @return FlightLog
     */
    public function setTotalCost($TotalCost)
    {
      $this->TotalCost = $TotalCost;
      return $this;
    }

}
