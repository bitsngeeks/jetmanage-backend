<?php

class MeteorologicalData
{

    /**
     * @var ArrayOfWeatherDataRecord $AltitudePoints
     */
    protected $AltitudePoints = null;

    /**
     * @var float $ClearAirTurbulence
     */
    protected $ClearAirTurbulence = null;

    /**
     * @var float $ISA
     */
    protected $ISA = null;

    /**
     * @var float $ISAAtTropopause
     */
    protected $ISAAtTropopause = null;

    /**
     * @var ArrayOfIcingPoint $Icing
     */
    protected $Icing = null;

    /**
     * @var float $Temperature
     */
    protected $Temperature = null;

    /**
     * @var float $TemperatureAtTropopause
     */
    protected $TemperatureAtTropopause = null;

    /**
     * @var int $TropopauseAltitude
     */
    protected $TropopauseAltitude = null;

    /**
     * @var ArrayOfTurbulencePoint $Turbulence
     */
    protected $Turbulence = null;

    /**
     * @var float $WindCorrection
     */
    protected $WindCorrection = null;

    /**
     * @var float $WindDirection
     */
    protected $WindDirection = null;

    /**
     * @var float $WindDirectionAtTropopause
     */
    protected $WindDirectionAtTropopause = null;

    /**
     * @var float $WindShear
     */
    protected $WindShear = null;

    /**
     * @var float $WindVelocity
     */
    protected $WindVelocity = null;

    /**
     * @var float $WindVelocityAtTropopause
     */
    protected $WindVelocityAtTropopause = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfWeatherDataRecord
     */
    public function getAltitudePoints()
    {
      return $this->AltitudePoints;
    }

    /**
     * @param ArrayOfWeatherDataRecord $AltitudePoints
     * @return MeteorologicalData
     */
    public function setAltitudePoints($AltitudePoints)
    {
      $this->AltitudePoints = $AltitudePoints;
      return $this;
    }

    /**
     * @return float
     */
    public function getClearAirTurbulence()
    {
      return $this->ClearAirTurbulence;
    }

    /**
     * @param float $ClearAirTurbulence
     * @return MeteorologicalData
     */
    public function setClearAirTurbulence($ClearAirTurbulence)
    {
      $this->ClearAirTurbulence = $ClearAirTurbulence;
      return $this;
    }

    /**
     * @return float
     */
    public function getISA()
    {
      return $this->ISA;
    }

    /**
     * @param float $ISA
     * @return MeteorologicalData
     */
    public function setISA($ISA)
    {
      $this->ISA = $ISA;
      return $this;
    }

    /**
     * @return float
     */
    public function getISAAtTropopause()
    {
      return $this->ISAAtTropopause;
    }

    /**
     * @param float $ISAAtTropopause
     * @return MeteorologicalData
     */
    public function setISAAtTropopause($ISAAtTropopause)
    {
      $this->ISAAtTropopause = $ISAAtTropopause;
      return $this;
    }

    /**
     * @return ArrayOfIcingPoint
     */
    public function getIcing()
    {
      return $this->Icing;
    }

    /**
     * @param ArrayOfIcingPoint $Icing
     * @return MeteorologicalData
     */
    public function setIcing($Icing)
    {
      $this->Icing = $Icing;
      return $this;
    }

    /**
     * @return float
     */
    public function getTemperature()
    {
      return $this->Temperature;
    }

    /**
     * @param float $Temperature
     * @return MeteorologicalData
     */
    public function setTemperature($Temperature)
    {
      $this->Temperature = $Temperature;
      return $this;
    }

    /**
     * @return float
     */
    public function getTemperatureAtTropopause()
    {
      return $this->TemperatureAtTropopause;
    }

    /**
     * @param float $TemperatureAtTropopause
     * @return MeteorologicalData
     */
    public function setTemperatureAtTropopause($TemperatureAtTropopause)
    {
      $this->TemperatureAtTropopause = $TemperatureAtTropopause;
      return $this;
    }

    /**
     * @return int
     */
    public function getTropopauseAltitude()
    {
      return $this->TropopauseAltitude;
    }

    /**
     * @param int $TropopauseAltitude
     * @return MeteorologicalData
     */
    public function setTropopauseAltitude($TropopauseAltitude)
    {
      $this->TropopauseAltitude = $TropopauseAltitude;
      return $this;
    }

    /**
     * @return ArrayOfTurbulencePoint
     */
    public function getTurbulence()
    {
      return $this->Turbulence;
    }

    /**
     * @param ArrayOfTurbulencePoint $Turbulence
     * @return MeteorologicalData
     */
    public function setTurbulence($Turbulence)
    {
      $this->Turbulence = $Turbulence;
      return $this;
    }

    /**
     * @return float
     */
    public function getWindCorrection()
    {
      return $this->WindCorrection;
    }

    /**
     * @param float $WindCorrection
     * @return MeteorologicalData
     */
    public function setWindCorrection($WindCorrection)
    {
      $this->WindCorrection = $WindCorrection;
      return $this;
    }

    /**
     * @return float
     */
    public function getWindDirection()
    {
      return $this->WindDirection;
    }

    /**
     * @param float $WindDirection
     * @return MeteorologicalData
     */
    public function setWindDirection($WindDirection)
    {
      $this->WindDirection = $WindDirection;
      return $this;
    }

    /**
     * @return float
     */
    public function getWindDirectionAtTropopause()
    {
      return $this->WindDirectionAtTropopause;
    }

    /**
     * @param float $WindDirectionAtTropopause
     * @return MeteorologicalData
     */
    public function setWindDirectionAtTropopause($WindDirectionAtTropopause)
    {
      $this->WindDirectionAtTropopause = $WindDirectionAtTropopause;
      return $this;
    }

    /**
     * @return float
     */
    public function getWindShear()
    {
      return $this->WindShear;
    }

    /**
     * @param float $WindShear
     * @return MeteorologicalData
     */
    public function setWindShear($WindShear)
    {
      $this->WindShear = $WindShear;
      return $this;
    }

    /**
     * @return float
     */
    public function getWindVelocity()
    {
      return $this->WindVelocity;
    }

    /**
     * @param float $WindVelocity
     * @return MeteorologicalData
     */
    public function setWindVelocity($WindVelocity)
    {
      $this->WindVelocity = $WindVelocity;
      return $this;
    }

    /**
     * @return float
     */
    public function getWindVelocityAtTropopause()
    {
      return $this->WindVelocityAtTropopause;
    }

    /**
     * @param float $WindVelocityAtTropopause
     * @return MeteorologicalData
     */
    public function setWindVelocityAtTropopause($WindVelocityAtTropopause)
    {
      $this->WindVelocityAtTropopause = $WindVelocityAtTropopause;
      return $this;
    }

}
