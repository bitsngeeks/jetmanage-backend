<?php

class AFTNAddress
{

    /**
     * @var string $Address
     */
    protected $Address = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getAddress()
    {
      return $this->Address;
    }

    /**
     * @param string $Address
     * @return AFTNAddress
     */
    public function setAddress($Address)
    {
      $this->Address = $Address;
      return $this;
    }

}
