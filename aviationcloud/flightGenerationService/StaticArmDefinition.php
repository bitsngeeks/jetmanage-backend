<?php

class StaticArmDefinition extends ArmDefinition
{

    /**
     * @var float $Arm
     */
    protected $Arm = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getArm()
    {
      return $this->Arm;
    }

    /**
     * @param float $Arm
     * @return StaticArmDefinition
     */
    public function setArm($Arm)
    {
      $this->Arm = $Arm;
      return $this;
    }

}
