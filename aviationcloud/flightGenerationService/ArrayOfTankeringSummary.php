<?php

class ArrayOfTankeringSummary implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var TankeringSummary[] $TankeringSummary
     */
    protected $TankeringSummary = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return TankeringSummary[]
     */
    public function getTankeringSummary()
    {
      return $this->TankeringSummary;
    }

    /**
     * @param TankeringSummary[] $TankeringSummary
     * @return ArrayOfTankeringSummary
     */
    public function setTankeringSummary(array $TankeringSummary = null)
    {
      $this->TankeringSummary = $TankeringSummary;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->TankeringSummary[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return TankeringSummary
     */
    public function offsetGet($offset)
    {
      return $this->TankeringSummary[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param TankeringSummary $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->TankeringSummary[] = $value;
      } else {
        $this->TankeringSummary[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->TankeringSummary[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return TankeringSummary Return the current element
     */
    public function current()
    {
      return current($this->TankeringSummary);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->TankeringSummary);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->TankeringSummary);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->TankeringSummary);
    }

    /**
     * Countable implementation
     *
     * @return TankeringSummary Return count of elements
     */
    public function count()
    {
      return count($this->TankeringSummary);
    }

}
