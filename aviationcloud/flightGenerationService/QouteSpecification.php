<?php

class QouteSpecification
{

    /**
     * @var AircraftSpecification $AircraftDefinition
     */
    protected $AircraftDefinition = null;

    /**
     * @var CostFunctionSpecification $CostFunction
     */
    protected $CostFunction = null;

    /**
     * @var Airport $DepartureAirport
     */
    protected $DepartureAirport = null;

    /**
     * @var Airport $DestinationAirport
     */
    protected $DestinationAirport = null;

    /**
     * @var PathOptions $PathOptions
     */
    protected $PathOptions = null;

    /**
     * @var \DateTime $ScheduledTimeOfDeparture
     */
    protected $ScheduledTimeOfDeparture = null;

    /**
     * @var AircraftTraficLoadDefinition $TrafficLoad
     */
    protected $TrafficLoad = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return AircraftSpecification
     */
    public function getAircraftDefinition()
    {
      return $this->AircraftDefinition;
    }

    /**
     * @param AircraftSpecification $AircraftDefinition
     * @return QouteSpecification
     */
    public function setAircraftDefinition($AircraftDefinition)
    {
      $this->AircraftDefinition = $AircraftDefinition;
      return $this;
    }

    /**
     * @return CostFunctionSpecification
     */
    public function getCostFunction()
    {
      return $this->CostFunction;
    }

    /**
     * @param CostFunctionSpecification $CostFunction
     * @return QouteSpecification
     */
    public function setCostFunction($CostFunction)
    {
      $this->CostFunction = $CostFunction;
      return $this;
    }

    /**
     * @return Airport
     */
    public function getDepartureAirport()
    {
      return $this->DepartureAirport;
    }

    /**
     * @param Airport $DepartureAirport
     * @return QouteSpecification
     */
    public function setDepartureAirport($DepartureAirport)
    {
      $this->DepartureAirport = $DepartureAirport;
      return $this;
    }

    /**
     * @return Airport
     */
    public function getDestinationAirport()
    {
      return $this->DestinationAirport;
    }

    /**
     * @param Airport $DestinationAirport
     * @return QouteSpecification
     */
    public function setDestinationAirport($DestinationAirport)
    {
      $this->DestinationAirport = $DestinationAirport;
      return $this;
    }

    /**
     * @return PathOptions
     */
    public function getPathOptions()
    {
      return $this->PathOptions;
    }

    /**
     * @param PathOptions $PathOptions
     * @return QouteSpecification
     */
    public function setPathOptions($PathOptions)
    {
      $this->PathOptions = $PathOptions;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getScheduledTimeOfDeparture()
    {
      if ($this->ScheduledTimeOfDeparture == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->ScheduledTimeOfDeparture);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $ScheduledTimeOfDeparture
     * @return QouteSpecification
     */
    public function setScheduledTimeOfDeparture(\DateTime $ScheduledTimeOfDeparture = null)
    {
      if ($ScheduledTimeOfDeparture == null) {
       $this->ScheduledTimeOfDeparture = null;
      } else {
        $this->ScheduledTimeOfDeparture = $ScheduledTimeOfDeparture->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return AircraftTraficLoadDefinition
     */
    public function getTrafficLoad()
    {
      return $this->TrafficLoad;
    }

    /**
     * @param AircraftTraficLoadDefinition $TrafficLoad
     * @return QouteSpecification
     */
    public function setTrafficLoad($TrafficLoad)
    {
      $this->TrafficLoad = $TrafficLoad;
      return $this;
    }

}
