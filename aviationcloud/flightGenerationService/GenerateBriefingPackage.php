<?php

class GenerateBriefingPackage
{

    /**
     * @var BriefingPackageRequest $request
     */
    protected $request = null;

    /**
     * @param BriefingPackageRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return BriefingPackageRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param BriefingPackageRequest $request
     * @return GenerateBriefingPackage
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
