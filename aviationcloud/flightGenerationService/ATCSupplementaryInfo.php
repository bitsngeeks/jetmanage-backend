<?php

class ATCSupplementaryInfo
{

    /**
     * @var int $PersonsOnBoard
     */
    protected $PersonsOnBoard = null;

    /**
     * @var string $PilotInCommand
     */
    protected $PilotInCommand = null;

    /**
     * @var string $Remarks
     */
    protected $Remarks = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getPersonsOnBoard()
    {
      return $this->PersonsOnBoard;
    }

    /**
     * @param int $PersonsOnBoard
     * @return ATCSupplementaryInfo
     */
    public function setPersonsOnBoard($PersonsOnBoard)
    {
      $this->PersonsOnBoard = $PersonsOnBoard;
      return $this;
    }

    /**
     * @return string
     */
    public function getPilotInCommand()
    {
      return $this->PilotInCommand;
    }

    /**
     * @param string $PilotInCommand
     * @return ATCSupplementaryInfo
     */
    public function setPilotInCommand($PilotInCommand)
    {
      $this->PilotInCommand = $PilotInCommand;
      return $this;
    }

    /**
     * @return string
     */
    public function getRemarks()
    {
      return $this->Remarks;
    }

    /**
     * @param string $Remarks
     * @return ATCSupplementaryInfo
     */
    public function setRemarks($Remarks)
    {
      $this->Remarks = $Remarks;
      return $this;
    }

}
