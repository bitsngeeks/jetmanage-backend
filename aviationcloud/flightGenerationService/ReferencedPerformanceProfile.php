<?php

class ReferencedPerformanceProfile extends AircraftPerformanceProfile
{

    /**
     * @var int $ReferencedProfileId
     */
    protected $ReferencedProfileId = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return int
     */
    public function getReferencedProfileId()
    {
      return $this->ReferencedProfileId;
    }

    /**
     * @param int $ReferencedProfileId
     * @return ReferencedPerformanceProfile
     */
    public function setReferencedProfileId($ReferencedProfileId)
    {
      $this->ReferencedProfileId = $ReferencedProfileId;
      return $this;
    }

}
