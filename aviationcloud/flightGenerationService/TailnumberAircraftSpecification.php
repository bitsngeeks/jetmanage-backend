<?php

class TailnumberAircraftSpecification extends AircraftSpecification
{

    /**
     * @var string $Tailnumber
     */
    protected $Tailnumber = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getTailnumber()
    {
      return $this->Tailnumber;
    }

    /**
     * @param string $Tailnumber
     * @return TailnumberAircraftSpecification
     */
    public function setTailnumber($Tailnumber)
    {
      $this->Tailnumber = $Tailnumber;
      return $this;
    }

}
