<?php

class RiskAnalysis
{

    /**
     * @var ArrayOfEnrouteDiversionAirport $EnrouteDiversion
     */
    protected $EnrouteDiversion = null;

    /**
     * @var ArrayOfFlightSegment $FlightSegments
     */
    protected $FlightSegments = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfEnrouteDiversionAirport
     */
    public function getEnrouteDiversion()
    {
      return $this->EnrouteDiversion;
    }

    /**
     * @param ArrayOfEnrouteDiversionAirport $EnrouteDiversion
     * @return RiskAnalysis
     */
    public function setEnrouteDiversion($EnrouteDiversion)
    {
      $this->EnrouteDiversion = $EnrouteDiversion;
      return $this;
    }

    /**
     * @return ArrayOfFlightSegment
     */
    public function getFlightSegments()
    {
      return $this->FlightSegments;
    }

    /**
     * @param ArrayOfFlightSegment $FlightSegments
     * @return RiskAnalysis
     */
    public function setFlightSegments($FlightSegments)
    {
      $this->FlightSegments = $FlightSegments;
      return $this;
    }

}
