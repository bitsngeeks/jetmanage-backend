<?php

class DiscreteCruiseProfile extends CruiseProfile
{

    /**
     * @var ArrayOfCruisePerformanceDataSeries $CruisePerformanceDataSet
     */
    protected $CruisePerformanceDataSet = null;

    /**
     * @var ArrayOfWeightLimitation $WeightLimitations
     */
    protected $WeightLimitations = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return ArrayOfCruisePerformanceDataSeries
     */
    public function getCruisePerformanceDataSet()
    {
      return $this->CruisePerformanceDataSet;
    }

    /**
     * @param ArrayOfCruisePerformanceDataSeries $CruisePerformanceDataSet
     * @return DiscreteCruiseProfile
     */
    public function setCruisePerformanceDataSet($CruisePerformanceDataSet)
    {
      $this->CruisePerformanceDataSet = $CruisePerformanceDataSet;
      return $this;
    }

    /**
     * @return ArrayOfWeightLimitation
     */
    public function getWeightLimitations()
    {
      return $this->WeightLimitations;
    }

    /**
     * @param ArrayOfWeightLimitation $WeightLimitations
     * @return DiscreteCruiseProfile
     */
    public function setWeightLimitations($WeightLimitations)
    {
      $this->WeightLimitations = $WeightLimitations;
      return $this;
    }

}
