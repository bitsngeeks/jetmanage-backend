<?php

class WxNotamDocumentSpecification extends DocumentSpecification
{

    /**
     * @var boolean $ExcludeForAdequeateAirpotrs
     */
    protected $ExcludeForAdequeateAirpotrs = null;

    /**
     * @var boolean $ExcludeForAlternates
     */
    protected $ExcludeForAlternates = null;

    /**
     * @var boolean $ExcludeForETPAirpotrs
     */
    protected $ExcludeForETPAirpotrs = null;

    /**
     * @var boolean $HalfPageSize
     */
    protected $HalfPageSize = null;

    /**
     * @var int $MaximumFlightlevel
     */
    protected $MaximumFlightlevel = null;

    /**
     * @var int $MinimumFlightlevel
     */
    protected $MinimumFlightlevel = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return boolean
     */
    public function getExcludeForAdequeateAirpotrs()
    {
      return $this->ExcludeForAdequeateAirpotrs;
    }

    /**
     * @param boolean $ExcludeForAdequeateAirpotrs
     * @return WxNotamDocumentSpecification
     */
    public function setExcludeForAdequeateAirpotrs($ExcludeForAdequeateAirpotrs)
    {
      $this->ExcludeForAdequeateAirpotrs = $ExcludeForAdequeateAirpotrs;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getExcludeForAlternates()
    {
      return $this->ExcludeForAlternates;
    }

    /**
     * @param boolean $ExcludeForAlternates
     * @return WxNotamDocumentSpecification
     */
    public function setExcludeForAlternates($ExcludeForAlternates)
    {
      $this->ExcludeForAlternates = $ExcludeForAlternates;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getExcludeForETPAirpotrs()
    {
      return $this->ExcludeForETPAirpotrs;
    }

    /**
     * @param boolean $ExcludeForETPAirpotrs
     * @return WxNotamDocumentSpecification
     */
    public function setExcludeForETPAirpotrs($ExcludeForETPAirpotrs)
    {
      $this->ExcludeForETPAirpotrs = $ExcludeForETPAirpotrs;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getHalfPageSize()
    {
      return $this->HalfPageSize;
    }

    /**
     * @param boolean $HalfPageSize
     * @return WxNotamDocumentSpecification
     */
    public function setHalfPageSize($HalfPageSize)
    {
      $this->HalfPageSize = $HalfPageSize;
      return $this;
    }

    /**
     * @return int
     */
    public function getMaximumFlightlevel()
    {
      return $this->MaximumFlightlevel;
    }

    /**
     * @param int $MaximumFlightlevel
     * @return WxNotamDocumentSpecification
     */
    public function setMaximumFlightlevel($MaximumFlightlevel)
    {
      $this->MaximumFlightlevel = $MaximumFlightlevel;
      return $this;
    }

    /**
     * @return int
     */
    public function getMinimumFlightlevel()
    {
      return $this->MinimumFlightlevel;
    }

    /**
     * @param int $MinimumFlightlevel
     * @return WxNotamDocumentSpecification
     */
    public function setMinimumFlightlevel($MinimumFlightlevel)
    {
      $this->MinimumFlightlevel = $MinimumFlightlevel;
      return $this;
    }

}
