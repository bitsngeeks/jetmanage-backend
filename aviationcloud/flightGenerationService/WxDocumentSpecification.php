<?php

class WxDocumentSpecification extends WxNotamDocumentSpecification
{

    /**
     * @var boolean $UseV2Document
     */
    protected $UseV2Document = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return boolean
     */
    public function getUseV2Document()
    {
      return $this->UseV2Document;
    }

    /**
     * @param boolean $UseV2Document
     * @return WxDocumentSpecification
     */
    public function setUseV2Document($UseV2Document)
    {
      $this->UseV2Document = $UseV2Document;
      return $this;
    }

}
