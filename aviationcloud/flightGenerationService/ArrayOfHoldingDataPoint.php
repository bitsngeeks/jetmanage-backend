<?php

class ArrayOfHoldingDataPoint implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var HoldingDataPoint[] $HoldingDataPoint
     */
    protected $HoldingDataPoint = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return HoldingDataPoint[]
     */
    public function getHoldingDataPoint()
    {
      return $this->HoldingDataPoint;
    }

    /**
     * @param HoldingDataPoint[] $HoldingDataPoint
     * @return ArrayOfHoldingDataPoint
     */
    public function setHoldingDataPoint(array $HoldingDataPoint = null)
    {
      $this->HoldingDataPoint = $HoldingDataPoint;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->HoldingDataPoint[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return HoldingDataPoint
     */
    public function offsetGet($offset)
    {
      return $this->HoldingDataPoint[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param HoldingDataPoint $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->HoldingDataPoint[] = $value;
      } else {
        $this->HoldingDataPoint[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->HoldingDataPoint[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return HoldingDataPoint Return the current element
     */
    public function current()
    {
      return current($this->HoldingDataPoint);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->HoldingDataPoint);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->HoldingDataPoint);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->HoldingDataPoint);
    }

    /**
     * Countable implementation
     *
     * @return HoldingDataPoint Return count of elements
     */
    public function count()
    {
      return count($this->HoldingDataPoint);
    }

}
