<?php

class IntersectionPoint
{

    /**
     * @var SphereicPoint $Coordinate
     */
    protected $Coordinate = null;

    /**
     * @var float $DistanceFromStart
     */
    protected $DistanceFromStart = null;

    /**
     * @var float $Fraction
     */
    protected $Fraction = null;

    /**
     * @var IntersectionNodeType $IntersectionNodeType
     */
    protected $IntersectionNodeType = null;

    /**
     * @var \DateTime $Time
     */
    protected $Time = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return SphereicPoint
     */
    public function getCoordinate()
    {
      return $this->Coordinate;
    }

    /**
     * @param SphereicPoint $Coordinate
     * @return IntersectionPoint
     */
    public function setCoordinate($Coordinate)
    {
      $this->Coordinate = $Coordinate;
      return $this;
    }

    /**
     * @return float
     */
    public function getDistanceFromStart()
    {
      return $this->DistanceFromStart;
    }

    /**
     * @param float $DistanceFromStart
     * @return IntersectionPoint
     */
    public function setDistanceFromStart($DistanceFromStart)
    {
      $this->DistanceFromStart = $DistanceFromStart;
      return $this;
    }

    /**
     * @return float
     */
    public function getFraction()
    {
      return $this->Fraction;
    }

    /**
     * @param float $Fraction
     * @return IntersectionPoint
     */
    public function setFraction($Fraction)
    {
      $this->Fraction = $Fraction;
      return $this;
    }

    /**
     * @return IntersectionNodeType
     */
    public function getIntersectionNodeType()
    {
      return $this->IntersectionNodeType;
    }

    /**
     * @param IntersectionNodeType $IntersectionNodeType
     * @return IntersectionPoint
     */
    public function setIntersectionNodeType($IntersectionNodeType)
    {
      $this->IntersectionNodeType = $IntersectionNodeType;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getTime()
    {
      if ($this->Time == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->Time);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $Time
     * @return IntersectionPoint
     */
    public function setTime(\DateTime $Time = null)
    {
      if ($Time == null) {
       $this->Time = null;
      } else {
        $this->Time = $Time->format(\DateTime::ATOM);
      }
      return $this;
    }

}
