<?php

class FlightTimes
{

    /**
     * @var Time $ATCEndurance
     */
    protected $ATCEndurance = null;

    /**
     * @var Time $ActualTime
     */
    protected $ActualTime = null;

    /**
     * @var Time $AdditionalTime
     */
    protected $AdditionalTime = null;

    /**
     * @var float $Alternate1AverageCAS
     */
    protected $Alternate1AverageCAS = null;

    /**
     * @var float $Alternate1AverageEAS
     */
    protected $Alternate1AverageEAS = null;

    /**
     * @var float $Alternate1AverageGS
     */
    protected $Alternate1AverageGS = null;

    /**
     * @var int $Alternate1AverageISA
     */
    protected $Alternate1AverageISA = null;

    /**
     * @var float $Alternate1AverageMach
     */
    protected $Alternate1AverageMach = null;

    /**
     * @var int $Alternate1AverageTas
     */
    protected $Alternate1AverageTas = null;

    /**
     * @var int $Alternate1AverageWindComponent
     */
    protected $Alternate1AverageWindComponent = null;

    /**
     * @var Time $Alternate1Time
     */
    protected $Alternate1Time = null;

    /**
     * @var float $Alternate2AverageCAS
     */
    protected $Alternate2AverageCAS = null;

    /**
     * @var float $Alternate2AverageEAS
     */
    protected $Alternate2AverageEAS = null;

    /**
     * @var float $Alternate2AverageGS
     */
    protected $Alternate2AverageGS = null;

    /**
     * @var int $Alternate2AverageISA
     */
    protected $Alternate2AverageISA = null;

    /**
     * @var float $Alternate2AverageMach
     */
    protected $Alternate2AverageMach = null;

    /**
     * @var int $Alternate2AverageTas
     */
    protected $Alternate2AverageTas = null;

    /**
     * @var int $Alternate2AverageWindComponent
     */
    protected $Alternate2AverageWindComponent = null;

    /**
     * @var Time $Alternate2Time
     */
    protected $Alternate2Time = null;

    /**
     * @var Time $AlternateTime
     */
    protected $AlternateTime = null;

    /**
     * @var string $ArrivalTimeZoneAbbreviation
     */
    protected $ArrivalTimeZoneAbbreviation = null;

    /**
     * @var float $AverageCAS
     */
    protected $AverageCAS = null;

    /**
     * @var float $AverageCruiseMach
     */
    protected $AverageCruiseMach = null;

    /**
     * @var float $AverageEAS
     */
    protected $AverageEAS = null;

    /**
     * @var float $AverageGS
     */
    protected $AverageGS = null;

    /**
     * @var int $AverageISA
     */
    protected $AverageISA = null;

    /**
     * @var float $AverageMach
     */
    protected $AverageMach = null;

    /**
     * @var int $AverageTas
     */
    protected $AverageTas = null;

    /**
     * @var int $AverageWindComponent
     */
    protected $AverageWindComponent = null;

    /**
     * @var Time $ClimbTime
     */
    protected $ClimbTime = null;

    /**
     * @var Time $ContingencyTime
     */
    protected $ContingencyTime = null;

    /**
     * @var Time $CruiseTime
     */
    protected $CruiseTime = null;

    /**
     * @var boolean $DepartureTimeIsDST
     */
    protected $DepartureTimeIsDST = null;

    /**
     * @var \DateTime $DepartureTimeLocal
     */
    protected $DepartureTimeLocal = null;

    /**
     * @var \DateTime $DepartureTimeUTC
     */
    protected $DepartureTimeUTC = null;

    /**
     * @var float $DepartureTimeUTCOffset
     */
    protected $DepartureTimeUTCOffset = null;

    /**
     * @var string $DepartureTimeZoneAbbreviation
     */
    protected $DepartureTimeZoneAbbreviation = null;

    /**
     * @var Time $DescentTime
     */
    protected $DescentTime = null;

    /**
     * @var \DateTime $EstimatedAlternate1Arrival
     */
    protected $EstimatedAlternate1Arrival = null;

    /**
     * @var \DateTime $EstimatedAlternate2Arrival
     */
    protected $EstimatedAlternate2Arrival = null;

    /**
     * @var \DateTime $EstimatedArrivalTime
     */
    protected $EstimatedArrivalTime = null;

    /**
     * @var boolean $EstimatedArrivalTimeIsDST
     */
    protected $EstimatedArrivalTimeIsDST = null;

    /**
     * @var \DateTime $EstimatedArrivalTimeLocal
     */
    protected $EstimatedArrivalTimeLocal = null;

    /**
     * @var float $EstimatedArrivalUTCOffset
     */
    protected $EstimatedArrivalUTCOffset = null;

    /**
     * @var \DateTime $EstimatedTakeoffAlternateArrival
     */
    protected $EstimatedTakeoffAlternateArrival = null;

    /**
     * @var Time $ExtraTime
     */
    protected $ExtraTime = null;

    /**
     * @var Time $FinalReserveTime
     */
    protected $FinalReserveTime = null;

    /**
     * @var Time $HoldingTime
     */
    protected $HoldingTime = null;

    /**
     * @var Time $MinimumRequiredTime
     */
    protected $MinimumRequiredTime = null;

    /**
     * @var Time $TakeOffAlternateTime
     */
    protected $TakeOffAlternateTime = null;

    /**
     * @var float $TakeoffAlternateAverageCAS
     */
    protected $TakeoffAlternateAverageCAS = null;

    /**
     * @var float $TakeoffAlternateAverageEAS
     */
    protected $TakeoffAlternateAverageEAS = null;

    /**
     * @var float $TakeoffAlternateAverageGS
     */
    protected $TakeoffAlternateAverageGS = null;

    /**
     * @var int $TakeoffAlternateAverageISA
     */
    protected $TakeoffAlternateAverageISA = null;

    /**
     * @var float $TakeoffAlternateAverageMach
     */
    protected $TakeoffAlternateAverageMach = null;

    /**
     * @var int $TakeoffAlternateAverageTas
     */
    protected $TakeoffAlternateAverageTas = null;

    /**
     * @var int $TakeoffAlternateAverageWindComponent
     */
    protected $TakeoffAlternateAverageWindComponent = null;

    /**
     * @var Time $TaxiInTime
     */
    protected $TaxiInTime = null;

    /**
     * @var Time $TaxiTime
     */
    protected $TaxiTime = null;

    /**
     * @var Time $TimeToDestination
     */
    protected $TimeToDestination = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Time
     */
    public function getATCEndurance()
    {
      return $this->ATCEndurance;
    }

    /**
     * @param Time $ATCEndurance
     * @return FlightTimes
     */
    public function setATCEndurance($ATCEndurance)
    {
      $this->ATCEndurance = $ATCEndurance;
      return $this;
    }

    /**
     * @return Time
     */
    public function getActualTime()
    {
      return $this->ActualTime;
    }

    /**
     * @param Time $ActualTime
     * @return FlightTimes
     */
    public function setActualTime($ActualTime)
    {
      $this->ActualTime = $ActualTime;
      return $this;
    }

    /**
     * @return Time
     */
    public function getAdditionalTime()
    {
      return $this->AdditionalTime;
    }

    /**
     * @param Time $AdditionalTime
     * @return FlightTimes
     */
    public function setAdditionalTime($AdditionalTime)
    {
      $this->AdditionalTime = $AdditionalTime;
      return $this;
    }

    /**
     * @return float
     */
    public function getAlternate1AverageCAS()
    {
      return $this->Alternate1AverageCAS;
    }

    /**
     * @param float $Alternate1AverageCAS
     * @return FlightTimes
     */
    public function setAlternate1AverageCAS($Alternate1AverageCAS)
    {
      $this->Alternate1AverageCAS = $Alternate1AverageCAS;
      return $this;
    }

    /**
     * @return float
     */
    public function getAlternate1AverageEAS()
    {
      return $this->Alternate1AverageEAS;
    }

    /**
     * @param float $Alternate1AverageEAS
     * @return FlightTimes
     */
    public function setAlternate1AverageEAS($Alternate1AverageEAS)
    {
      $this->Alternate1AverageEAS = $Alternate1AverageEAS;
      return $this;
    }

    /**
     * @return float
     */
    public function getAlternate1AverageGS()
    {
      return $this->Alternate1AverageGS;
    }

    /**
     * @param float $Alternate1AverageGS
     * @return FlightTimes
     */
    public function setAlternate1AverageGS($Alternate1AverageGS)
    {
      $this->Alternate1AverageGS = $Alternate1AverageGS;
      return $this;
    }

    /**
     * @return int
     */
    public function getAlternate1AverageISA()
    {
      return $this->Alternate1AverageISA;
    }

    /**
     * @param int $Alternate1AverageISA
     * @return FlightTimes
     */
    public function setAlternate1AverageISA($Alternate1AverageISA)
    {
      $this->Alternate1AverageISA = $Alternate1AverageISA;
      return $this;
    }

    /**
     * @return float
     */
    public function getAlternate1AverageMach()
    {
      return $this->Alternate1AverageMach;
    }

    /**
     * @param float $Alternate1AverageMach
     * @return FlightTimes
     */
    public function setAlternate1AverageMach($Alternate1AverageMach)
    {
      $this->Alternate1AverageMach = $Alternate1AverageMach;
      return $this;
    }

    /**
     * @return int
     */
    public function getAlternate1AverageTas()
    {
      return $this->Alternate1AverageTas;
    }

    /**
     * @param int $Alternate1AverageTas
     * @return FlightTimes
     */
    public function setAlternate1AverageTas($Alternate1AverageTas)
    {
      $this->Alternate1AverageTas = $Alternate1AverageTas;
      return $this;
    }

    /**
     * @return int
     */
    public function getAlternate1AverageWindComponent()
    {
      return $this->Alternate1AverageWindComponent;
    }

    /**
     * @param int $Alternate1AverageWindComponent
     * @return FlightTimes
     */
    public function setAlternate1AverageWindComponent($Alternate1AverageWindComponent)
    {
      $this->Alternate1AverageWindComponent = $Alternate1AverageWindComponent;
      return $this;
    }

    /**
     * @return Time
     */
    public function getAlternate1Time()
    {
      return $this->Alternate1Time;
    }

    /**
     * @param Time $Alternate1Time
     * @return FlightTimes
     */
    public function setAlternate1Time($Alternate1Time)
    {
      $this->Alternate1Time = $Alternate1Time;
      return $this;
    }

    /**
     * @return float
     */
    public function getAlternate2AverageCAS()
    {
      return $this->Alternate2AverageCAS;
    }

    /**
     * @param float $Alternate2AverageCAS
     * @return FlightTimes
     */
    public function setAlternate2AverageCAS($Alternate2AverageCAS)
    {
      $this->Alternate2AverageCAS = $Alternate2AverageCAS;
      return $this;
    }

    /**
     * @return float
     */
    public function getAlternate2AverageEAS()
    {
      return $this->Alternate2AverageEAS;
    }

    /**
     * @param float $Alternate2AverageEAS
     * @return FlightTimes
     */
    public function setAlternate2AverageEAS($Alternate2AverageEAS)
    {
      $this->Alternate2AverageEAS = $Alternate2AverageEAS;
      return $this;
    }

    /**
     * @return float
     */
    public function getAlternate2AverageGS()
    {
      return $this->Alternate2AverageGS;
    }

    /**
     * @param float $Alternate2AverageGS
     * @return FlightTimes
     */
    public function setAlternate2AverageGS($Alternate2AverageGS)
    {
      $this->Alternate2AverageGS = $Alternate2AverageGS;
      return $this;
    }

    /**
     * @return int
     */
    public function getAlternate2AverageISA()
    {
      return $this->Alternate2AverageISA;
    }

    /**
     * @param int $Alternate2AverageISA
     * @return FlightTimes
     */
    public function setAlternate2AverageISA($Alternate2AverageISA)
    {
      $this->Alternate2AverageISA = $Alternate2AverageISA;
      return $this;
    }

    /**
     * @return float
     */
    public function getAlternate2AverageMach()
    {
      return $this->Alternate2AverageMach;
    }

    /**
     * @param float $Alternate2AverageMach
     * @return FlightTimes
     */
    public function setAlternate2AverageMach($Alternate2AverageMach)
    {
      $this->Alternate2AverageMach = $Alternate2AverageMach;
      return $this;
    }

    /**
     * @return int
     */
    public function getAlternate2AverageTas()
    {
      return $this->Alternate2AverageTas;
    }

    /**
     * @param int $Alternate2AverageTas
     * @return FlightTimes
     */
    public function setAlternate2AverageTas($Alternate2AverageTas)
    {
      $this->Alternate2AverageTas = $Alternate2AverageTas;
      return $this;
    }

    /**
     * @return int
     */
    public function getAlternate2AverageWindComponent()
    {
      return $this->Alternate2AverageWindComponent;
    }

    /**
     * @param int $Alternate2AverageWindComponent
     * @return FlightTimes
     */
    public function setAlternate2AverageWindComponent($Alternate2AverageWindComponent)
    {
      $this->Alternate2AverageWindComponent = $Alternate2AverageWindComponent;
      return $this;
    }

    /**
     * @return Time
     */
    public function getAlternate2Time()
    {
      return $this->Alternate2Time;
    }

    /**
     * @param Time $Alternate2Time
     * @return FlightTimes
     */
    public function setAlternate2Time($Alternate2Time)
    {
      $this->Alternate2Time = $Alternate2Time;
      return $this;
    }

    /**
     * @return Time
     */
    public function getAlternateTime()
    {
      return $this->AlternateTime;
    }

    /**
     * @param Time $AlternateTime
     * @return FlightTimes
     */
    public function setAlternateTime($AlternateTime)
    {
      $this->AlternateTime = $AlternateTime;
      return $this;
    }

    /**
     * @return string
     */
    public function getArrivalTimeZoneAbbreviation()
    {
      return $this->ArrivalTimeZoneAbbreviation;
    }

    /**
     * @param string $ArrivalTimeZoneAbbreviation
     * @return FlightTimes
     */
    public function setArrivalTimeZoneAbbreviation($ArrivalTimeZoneAbbreviation)
    {
      $this->ArrivalTimeZoneAbbreviation = $ArrivalTimeZoneAbbreviation;
      return $this;
    }

    /**
     * @return float
     */
    public function getAverageCAS()
    {
      return $this->AverageCAS;
    }

    /**
     * @param float $AverageCAS
     * @return FlightTimes
     */
    public function setAverageCAS($AverageCAS)
    {
      $this->AverageCAS = $AverageCAS;
      return $this;
    }

    /**
     * @return float
     */
    public function getAverageCruiseMach()
    {
      return $this->AverageCruiseMach;
    }

    /**
     * @param float $AverageCruiseMach
     * @return FlightTimes
     */
    public function setAverageCruiseMach($AverageCruiseMach)
    {
      $this->AverageCruiseMach = $AverageCruiseMach;
      return $this;
    }

    /**
     * @return float
     */
    public function getAverageEAS()
    {
      return $this->AverageEAS;
    }

    /**
     * @param float $AverageEAS
     * @return FlightTimes
     */
    public function setAverageEAS($AverageEAS)
    {
      $this->AverageEAS = $AverageEAS;
      return $this;
    }

    /**
     * @return float
     */
    public function getAverageGS()
    {
      return $this->AverageGS;
    }

    /**
     * @param float $AverageGS
     * @return FlightTimes
     */
    public function setAverageGS($AverageGS)
    {
      $this->AverageGS = $AverageGS;
      return $this;
    }

    /**
     * @return int
     */
    public function getAverageISA()
    {
      return $this->AverageISA;
    }

    /**
     * @param int $AverageISA
     * @return FlightTimes
     */
    public function setAverageISA($AverageISA)
    {
      $this->AverageISA = $AverageISA;
      return $this;
    }

    /**
     * @return float
     */
    public function getAverageMach()
    {
      return $this->AverageMach;
    }

    /**
     * @param float $AverageMach
     * @return FlightTimes
     */
    public function setAverageMach($AverageMach)
    {
      $this->AverageMach = $AverageMach;
      return $this;
    }

    /**
     * @return int
     */
    public function getAverageTas()
    {
      return $this->AverageTas;
    }

    /**
     * @param int $AverageTas
     * @return FlightTimes
     */
    public function setAverageTas($AverageTas)
    {
      $this->AverageTas = $AverageTas;
      return $this;
    }

    /**
     * @return int
     */
    public function getAverageWindComponent()
    {
      return $this->AverageWindComponent;
    }

    /**
     * @param int $AverageWindComponent
     * @return FlightTimes
     */
    public function setAverageWindComponent($AverageWindComponent)
    {
      $this->AverageWindComponent = $AverageWindComponent;
      return $this;
    }

    /**
     * @return Time
     */
    public function getClimbTime()
    {
      return $this->ClimbTime;
    }

    /**
     * @param Time $ClimbTime
     * @return FlightTimes
     */
    public function setClimbTime($ClimbTime)
    {
      $this->ClimbTime = $ClimbTime;
      return $this;
    }

    /**
     * @return Time
     */
    public function getContingencyTime()
    {
      return $this->ContingencyTime;
    }

    /**
     * @param Time $ContingencyTime
     * @return FlightTimes
     */
    public function setContingencyTime($ContingencyTime)
    {
      $this->ContingencyTime = $ContingencyTime;
      return $this;
    }

    /**
     * @return Time
     */
    public function getCruiseTime()
    {
      return $this->CruiseTime;
    }

    /**
     * @param Time $CruiseTime
     * @return FlightTimes
     */
    public function setCruiseTime($CruiseTime)
    {
      $this->CruiseTime = $CruiseTime;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getDepartureTimeIsDST()
    {
      return $this->DepartureTimeIsDST;
    }

    /**
     * @param boolean $DepartureTimeIsDST
     * @return FlightTimes
     */
    public function setDepartureTimeIsDST($DepartureTimeIsDST)
    {
      $this->DepartureTimeIsDST = $DepartureTimeIsDST;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDepartureTimeLocal()
    {
      if ($this->DepartureTimeLocal == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DepartureTimeLocal);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DepartureTimeLocal
     * @return FlightTimes
     */
    public function setDepartureTimeLocal(\DateTime $DepartureTimeLocal = null)
    {
      if ($DepartureTimeLocal == null) {
       $this->DepartureTimeLocal = null;
      } else {
        $this->DepartureTimeLocal = $DepartureTimeLocal->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDepartureTimeUTC()
    {
      if ($this->DepartureTimeUTC == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DepartureTimeUTC);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DepartureTimeUTC
     * @return FlightTimes
     */
    public function setDepartureTimeUTC(\DateTime $DepartureTimeUTC = null)
    {
      if ($DepartureTimeUTC == null) {
       $this->DepartureTimeUTC = null;
      } else {
        $this->DepartureTimeUTC = $DepartureTimeUTC->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return float
     */
    public function getDepartureTimeUTCOffset()
    {
      return $this->DepartureTimeUTCOffset;
    }

    /**
     * @param float $DepartureTimeUTCOffset
     * @return FlightTimes
     */
    public function setDepartureTimeUTCOffset($DepartureTimeUTCOffset)
    {
      $this->DepartureTimeUTCOffset = $DepartureTimeUTCOffset;
      return $this;
    }

    /**
     * @return string
     */
    public function getDepartureTimeZoneAbbreviation()
    {
      return $this->DepartureTimeZoneAbbreviation;
    }

    /**
     * @param string $DepartureTimeZoneAbbreviation
     * @return FlightTimes
     */
    public function setDepartureTimeZoneAbbreviation($DepartureTimeZoneAbbreviation)
    {
      $this->DepartureTimeZoneAbbreviation = $DepartureTimeZoneAbbreviation;
      return $this;
    }

    /**
     * @return Time
     */
    public function getDescentTime()
    {
      return $this->DescentTime;
    }

    /**
     * @param Time $DescentTime
     * @return FlightTimes
     */
    public function setDescentTime($DescentTime)
    {
      $this->DescentTime = $DescentTime;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEstimatedAlternate1Arrival()
    {
      if ($this->EstimatedAlternate1Arrival == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->EstimatedAlternate1Arrival);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $EstimatedAlternate1Arrival
     * @return FlightTimes
     */
    public function setEstimatedAlternate1Arrival(\DateTime $EstimatedAlternate1Arrival = null)
    {
      if ($EstimatedAlternate1Arrival == null) {
       $this->EstimatedAlternate1Arrival = null;
      } else {
        $this->EstimatedAlternate1Arrival = $EstimatedAlternate1Arrival->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEstimatedAlternate2Arrival()
    {
      if ($this->EstimatedAlternate2Arrival == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->EstimatedAlternate2Arrival);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $EstimatedAlternate2Arrival
     * @return FlightTimes
     */
    public function setEstimatedAlternate2Arrival(\DateTime $EstimatedAlternate2Arrival = null)
    {
      if ($EstimatedAlternate2Arrival == null) {
       $this->EstimatedAlternate2Arrival = null;
      } else {
        $this->EstimatedAlternate2Arrival = $EstimatedAlternate2Arrival->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEstimatedArrivalTime()
    {
      if ($this->EstimatedArrivalTime == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->EstimatedArrivalTime);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $EstimatedArrivalTime
     * @return FlightTimes
     */
    public function setEstimatedArrivalTime(\DateTime $EstimatedArrivalTime = null)
    {
      if ($EstimatedArrivalTime == null) {
       $this->EstimatedArrivalTime = null;
      } else {
        $this->EstimatedArrivalTime = $EstimatedArrivalTime->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return boolean
     */
    public function getEstimatedArrivalTimeIsDST()
    {
      return $this->EstimatedArrivalTimeIsDST;
    }

    /**
     * @param boolean $EstimatedArrivalTimeIsDST
     * @return FlightTimes
     */
    public function setEstimatedArrivalTimeIsDST($EstimatedArrivalTimeIsDST)
    {
      $this->EstimatedArrivalTimeIsDST = $EstimatedArrivalTimeIsDST;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEstimatedArrivalTimeLocal()
    {
      if ($this->EstimatedArrivalTimeLocal == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->EstimatedArrivalTimeLocal);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $EstimatedArrivalTimeLocal
     * @return FlightTimes
     */
    public function setEstimatedArrivalTimeLocal(\DateTime $EstimatedArrivalTimeLocal = null)
    {
      if ($EstimatedArrivalTimeLocal == null) {
       $this->EstimatedArrivalTimeLocal = null;
      } else {
        $this->EstimatedArrivalTimeLocal = $EstimatedArrivalTimeLocal->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return float
     */
    public function getEstimatedArrivalUTCOffset()
    {
      return $this->EstimatedArrivalUTCOffset;
    }

    /**
     * @param float $EstimatedArrivalUTCOffset
     * @return FlightTimes
     */
    public function setEstimatedArrivalUTCOffset($EstimatedArrivalUTCOffset)
    {
      $this->EstimatedArrivalUTCOffset = $EstimatedArrivalUTCOffset;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEstimatedTakeoffAlternateArrival()
    {
      if ($this->EstimatedTakeoffAlternateArrival == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->EstimatedTakeoffAlternateArrival);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $EstimatedTakeoffAlternateArrival
     * @return FlightTimes
     */
    public function setEstimatedTakeoffAlternateArrival(\DateTime $EstimatedTakeoffAlternateArrival = null)
    {
      if ($EstimatedTakeoffAlternateArrival == null) {
       $this->EstimatedTakeoffAlternateArrival = null;
      } else {
        $this->EstimatedTakeoffAlternateArrival = $EstimatedTakeoffAlternateArrival->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return Time
     */
    public function getExtraTime()
    {
      return $this->ExtraTime;
    }

    /**
     * @param Time $ExtraTime
     * @return FlightTimes
     */
    public function setExtraTime($ExtraTime)
    {
      $this->ExtraTime = $ExtraTime;
      return $this;
    }

    /**
     * @return Time
     */
    public function getFinalReserveTime()
    {
      return $this->FinalReserveTime;
    }

    /**
     * @param Time $FinalReserveTime
     * @return FlightTimes
     */
    public function setFinalReserveTime($FinalReserveTime)
    {
      $this->FinalReserveTime = $FinalReserveTime;
      return $this;
    }

    /**
     * @return Time
     */
    public function getHoldingTime()
    {
      return $this->HoldingTime;
    }

    /**
     * @param Time $HoldingTime
     * @return FlightTimes
     */
    public function setHoldingTime($HoldingTime)
    {
      $this->HoldingTime = $HoldingTime;
      return $this;
    }

    /**
     * @return Time
     */
    public function getMinimumRequiredTime()
    {
      return $this->MinimumRequiredTime;
    }

    /**
     * @param Time $MinimumRequiredTime
     * @return FlightTimes
     */
    public function setMinimumRequiredTime($MinimumRequiredTime)
    {
      $this->MinimumRequiredTime = $MinimumRequiredTime;
      return $this;
    }

    /**
     * @return Time
     */
    public function getTakeOffAlternateTime()
    {
      return $this->TakeOffAlternateTime;
    }

    /**
     * @param Time $TakeOffAlternateTime
     * @return FlightTimes
     */
    public function setTakeOffAlternateTime($TakeOffAlternateTime)
    {
      $this->TakeOffAlternateTime = $TakeOffAlternateTime;
      return $this;
    }

    /**
     * @return float
     */
    public function getTakeoffAlternateAverageCAS()
    {
      return $this->TakeoffAlternateAverageCAS;
    }

    /**
     * @param float $TakeoffAlternateAverageCAS
     * @return FlightTimes
     */
    public function setTakeoffAlternateAverageCAS($TakeoffAlternateAverageCAS)
    {
      $this->TakeoffAlternateAverageCAS = $TakeoffAlternateAverageCAS;
      return $this;
    }

    /**
     * @return float
     */
    public function getTakeoffAlternateAverageEAS()
    {
      return $this->TakeoffAlternateAverageEAS;
    }

    /**
     * @param float $TakeoffAlternateAverageEAS
     * @return FlightTimes
     */
    public function setTakeoffAlternateAverageEAS($TakeoffAlternateAverageEAS)
    {
      $this->TakeoffAlternateAverageEAS = $TakeoffAlternateAverageEAS;
      return $this;
    }

    /**
     * @return float
     */
    public function getTakeoffAlternateAverageGS()
    {
      return $this->TakeoffAlternateAverageGS;
    }

    /**
     * @param float $TakeoffAlternateAverageGS
     * @return FlightTimes
     */
    public function setTakeoffAlternateAverageGS($TakeoffAlternateAverageGS)
    {
      $this->TakeoffAlternateAverageGS = $TakeoffAlternateAverageGS;
      return $this;
    }

    /**
     * @return int
     */
    public function getTakeoffAlternateAverageISA()
    {
      return $this->TakeoffAlternateAverageISA;
    }

    /**
     * @param int $TakeoffAlternateAverageISA
     * @return FlightTimes
     */
    public function setTakeoffAlternateAverageISA($TakeoffAlternateAverageISA)
    {
      $this->TakeoffAlternateAverageISA = $TakeoffAlternateAverageISA;
      return $this;
    }

    /**
     * @return float
     */
    public function getTakeoffAlternateAverageMach()
    {
      return $this->TakeoffAlternateAverageMach;
    }

    /**
     * @param float $TakeoffAlternateAverageMach
     * @return FlightTimes
     */
    public function setTakeoffAlternateAverageMach($TakeoffAlternateAverageMach)
    {
      $this->TakeoffAlternateAverageMach = $TakeoffAlternateAverageMach;
      return $this;
    }

    /**
     * @return int
     */
    public function getTakeoffAlternateAverageTas()
    {
      return $this->TakeoffAlternateAverageTas;
    }

    /**
     * @param int $TakeoffAlternateAverageTas
     * @return FlightTimes
     */
    public function setTakeoffAlternateAverageTas($TakeoffAlternateAverageTas)
    {
      $this->TakeoffAlternateAverageTas = $TakeoffAlternateAverageTas;
      return $this;
    }

    /**
     * @return int
     */
    public function getTakeoffAlternateAverageWindComponent()
    {
      return $this->TakeoffAlternateAverageWindComponent;
    }

    /**
     * @param int $TakeoffAlternateAverageWindComponent
     * @return FlightTimes
     */
    public function setTakeoffAlternateAverageWindComponent($TakeoffAlternateAverageWindComponent)
    {
      $this->TakeoffAlternateAverageWindComponent = $TakeoffAlternateAverageWindComponent;
      return $this;
    }

    /**
     * @return Time
     */
    public function getTaxiInTime()
    {
      return $this->TaxiInTime;
    }

    /**
     * @param Time $TaxiInTime
     * @return FlightTimes
     */
    public function setTaxiInTime($TaxiInTime)
    {
      $this->TaxiInTime = $TaxiInTime;
      return $this;
    }

    /**
     * @return Time
     */
    public function getTaxiTime()
    {
      return $this->TaxiTime;
    }

    /**
     * @param Time $TaxiTime
     * @return FlightTimes
     */
    public function setTaxiTime($TaxiTime)
    {
      $this->TaxiTime = $TaxiTime;
      return $this;
    }

    /**
     * @return Time
     */
    public function getTimeToDestination()
    {
      return $this->TimeToDestination;
    }

    /**
     * @param Time $TimeToDestination
     * @return FlightTimes
     */
    public function setTimeToDestination($TimeToDestination)
    {
      $this->TimeToDestination = $TimeToDestination;
      return $this;
    }

}
