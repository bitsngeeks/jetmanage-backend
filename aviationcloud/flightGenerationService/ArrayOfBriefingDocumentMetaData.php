<?php

class ArrayOfBriefingDocumentMetaData implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var BriefingDocumentMetaData[] $BriefingDocumentMetaData
     */
    protected $BriefingDocumentMetaData = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return BriefingDocumentMetaData[]
     */
    public function getBriefingDocumentMetaData()
    {
      return $this->BriefingDocumentMetaData;
    }

    /**
     * @param BriefingDocumentMetaData[] $BriefingDocumentMetaData
     * @return ArrayOfBriefingDocumentMetaData
     */
    public function setBriefingDocumentMetaData(array $BriefingDocumentMetaData = null)
    {
      $this->BriefingDocumentMetaData = $BriefingDocumentMetaData;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->BriefingDocumentMetaData[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return BriefingDocumentMetaData
     */
    public function offsetGet($offset)
    {
      return $this->BriefingDocumentMetaData[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param BriefingDocumentMetaData $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->BriefingDocumentMetaData[] = $value;
      } else {
        $this->BriefingDocumentMetaData[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->BriefingDocumentMetaData[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return BriefingDocumentMetaData Return the current element
     */
    public function current()
    {
      return current($this->BriefingDocumentMetaData);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->BriefingDocumentMetaData);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->BriefingDocumentMetaData);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->BriefingDocumentMetaData);
    }

    /**
     * Countable implementation
     *
     * @return BriefingDocumentMetaData Return count of elements
     */
    public function count()
    {
      return count($this->BriefingDocumentMetaData);
    }

}
