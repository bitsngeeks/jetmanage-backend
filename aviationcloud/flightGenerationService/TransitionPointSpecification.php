<?php

class TransitionPointSpecification
{

    /**
     * @var RouteNodeIdentifier $TransitionPoint
     */
    protected $TransitionPoint = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return RouteNodeIdentifier
     */
    public function getTransitionPoint()
    {
      return $this->TransitionPoint;
    }

    /**
     * @param RouteNodeIdentifier $TransitionPoint
     * @return TransitionPointSpecification
     */
    public function setTransitionPoint($TransitionPoint)
    {
      $this->TransitionPoint = $TransitionPoint;
      return $this;
    }

}
