<?php

class TripSpecification
{

    /**
     * @var ArrayOfFlightSpecification $FlightSpecifications
     */
    protected $FlightSpecifications = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfFlightSpecification
     */
    public function getFlightSpecifications()
    {
      return $this->FlightSpecifications;
    }

    /**
     * @param ArrayOfFlightSpecification $FlightSpecifications
     * @return TripSpecification
     */
    public function setFlightSpecifications($FlightSpecifications)
    {
      $this->FlightSpecifications = $FlightSpecifications;
      return $this;
    }

}
