<?php

class CrossSectionDocumentSpecification extends DocumentSpecification
{

    /**
     * @var DocumentFormat $Format
     */
    protected $Format = null;

    /**
     * @var boolean $UseV2Chart
     */
    protected $UseV2Chart = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return DocumentFormat
     */
    public function getFormat()
    {
      return $this->Format;
    }

    /**
     * @param DocumentFormat $Format
     * @return CrossSectionDocumentSpecification
     */
    public function setFormat($Format)
    {
      $this->Format = $Format;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUseV2Chart()
    {
      return $this->UseV2Chart;
    }

    /**
     * @param boolean $UseV2Chart
     * @return CrossSectionDocumentSpecification
     */
    public function setUseV2Chart($UseV2Chart)
    {
      $this->UseV2Chart = $UseV2Chart;
      return $this;
    }

}
