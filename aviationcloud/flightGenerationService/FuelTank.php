<?php

class FuelTank extends AircraftStructureUnit
{

    /**
     * @var Weight $MaxFuel
     */
    protected $MaxFuel = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return Weight
     */
    public function getMaxFuel()
    {
      return $this->MaxFuel;
    }

    /**
     * @param Weight $MaxFuel
     * @return FuelTank
     */
    public function setMaxFuel($MaxFuel)
    {
      $this->MaxFuel = $MaxFuel;
      return $this;
    }

}
