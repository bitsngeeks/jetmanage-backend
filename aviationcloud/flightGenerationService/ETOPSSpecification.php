<?php

class ETOPSSpecification
{

    /**
     * @var ArrayOfAirport $AdequateAirports
     */
    protected $AdequateAirports = null;

    /**
     * @var boolean $AutoAdequateAirportSearch
     */
    protected $AutoAdequateAirportSearch = null;

    /**
     * @var Length $Distance
     */
    protected $Distance = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfAirport
     */
    public function getAdequateAirports()
    {
      return $this->AdequateAirports;
    }

    /**
     * @param ArrayOfAirport $AdequateAirports
     * @return ETOPSSpecification
     */
    public function setAdequateAirports($AdequateAirports)
    {
      $this->AdequateAirports = $AdequateAirports;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getAutoAdequateAirportSearch()
    {
      return $this->AutoAdequateAirportSearch;
    }

    /**
     * @param boolean $AutoAdequateAirportSearch
     * @return ETOPSSpecification
     */
    public function setAutoAdequateAirportSearch($AutoAdequateAirportSearch)
    {
      $this->AutoAdequateAirportSearch = $AutoAdequateAirportSearch;
      return $this;
    }

    /**
     * @return Length
     */
    public function getDistance()
    {
      return $this->Distance;
    }

    /**
     * @param Length $Distance
     * @return ETOPSSpecification
     */
    public function setDistance($Distance)
    {
      $this->Distance = $Distance;
      return $this;
    }

}
