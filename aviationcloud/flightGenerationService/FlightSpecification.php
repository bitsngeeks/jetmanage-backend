<?php

class FlightSpecification
{

    /**
     * @var ATCData $ATCData
     */
    protected $ATCData = null;

    /**
     * @var Currency $AircraftCostPerHour
     */
    protected $AircraftCostPerHour = null;

    /**
     * @var AircraftSpecification $AircraftDefinition
     */
    protected $AircraftDefinition = null;

    /**
     * @var string $AircraftTailNumber
     */
    protected $AircraftTailNumber = null;

    /**
     * @var Airport $AlternateAirport1
     */
    protected $AlternateAirport1 = null;

    /**
     * @var Airport $AlternateAirport2
     */
    protected $AlternateAirport2 = null;

    /**
     * @var FlightCalculationOptions $CalculationOptions
     */
    protected $CalculationOptions = null;

    /**
     * @var CargoLoadSpecification $CargoLoadSpecification
     */
    protected $CargoLoadSpecification = null;

    /**
     * @var PerformanceProfileSpecification $ClimbProfile
     */
    protected $ClimbProfile = null;

    /**
     * @var PerformanceProfileSpecification $CruiseProfile
     */
    protected $CruiseProfile = null;

    /**
     * @var CrewLoadSpecification $CrweLoadSpecification
     */
    protected $CrweLoadSpecification = null;

    /**
     * @var Airport $DepartureAirport
     */
    protected $DepartureAirport = null;

    /**
     * @var PerformanceProfileSpecification $DescentProfile
     */
    protected $DescentProfile = null;

    /**
     * @var \DateTime $DesiredTimeOfArrival
     */
    protected $DesiredTimeOfArrival = null;

    /**
     * @var Airport $DestinationAirport
     */
    protected $DestinationAirport = null;

    /**
     * @var ETPSettings $ETPSpecification
     */
    protected $ETPSpecification = null;

    /**
     * @var int $FlightLevel
     */
    protected $FlightLevel = null;

    /**
     * @var FlightLevelSpecification $FlightLevelDefinition
     */
    protected $FlightLevelDefinition = null;

    /**
     * @var int $FlightLevelToAlternate1
     */
    protected $FlightLevelToAlternate1 = null;

    /**
     * @var int $FlightLevelToAlternate2
     */
    protected $FlightLevelToAlternate2 = null;

    /**
     * @var string $FlightNumber
     */
    protected $FlightNumber = null;

    /**
     * @var int $FlightlevelTakeOffAlternate
     */
    protected $FlightlevelTakeOffAlternate = null;

    /**
     * @var Currency $FuelCostPerGallon
     */
    protected $FuelCostPerGallon = null;

    /**
     * @var FlightFuelRules $FuelRules
     */
    protected $FuelRules = null;

    /**
     * @var Currency $FuelUpliftCost
     */
    protected $FuelUpliftCost = null;

    /**
     * @var float $GallonsToWaive
     */
    protected $GallonsToWaive = null;

    /**
     * @var boolean $IncludeAllTBCDPoints
     */
    protected $IncludeAllTBCDPoints = null;

    /**
     * @var boolean $IncludeTCD
     */
    protected $IncludeTCD = null;

    /**
     * @var int $MassAndBalanceConfigurationId
     */
    protected $MassAndBalanceConfigurationId = null;

    /**
     * @var PaxLoadSpecification $PaxLoadSpecification
     */
    protected $PaxLoadSpecification = null;

    /**
     * @var FlightLevelAdjustmentSpecification $ProfileAdjustments
     */
    protected $ProfileAdjustments = null;

    /**
     * @var Currency $RampFee
     */
    protected $RampFee = null;

    /**
     * @var Route $RouteToAlternate1
     */
    protected $RouteToAlternate1 = null;

    /**
     * @var Route $RouteToAlternate2
     */
    protected $RouteToAlternate2 = null;

    /**
     * @var Route $RouteToDestination
     */
    protected $RouteToDestination = null;

    /**
     * @var Route $RouteToTakeOffAlternate
     */
    protected $RouteToTakeOffAlternate = null;

    /**
     * @var \DateTime $ScheduledTimeOfDeparture
     */
    protected $ScheduledTimeOfDeparture = null;

    /**
     * @var Airport $TakeOffAlternate
     */
    protected $TakeOffAlternate = null;

    /**
     * @var Winds $Winds
     */
    protected $Winds = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ATCData
     */
    public function getATCData()
    {
      return $this->ATCData;
    }

    /**
     * @param ATCData $ATCData
     * @return FlightSpecification
     */
    public function setATCData($ATCData)
    {
      $this->ATCData = $ATCData;
      return $this;
    }

    /**
     * @return Currency
     */
    public function getAircraftCostPerHour()
    {
      return $this->AircraftCostPerHour;
    }

    /**
     * @param Currency $AircraftCostPerHour
     * @return FlightSpecification
     */
    public function setAircraftCostPerHour($AircraftCostPerHour)
    {
      $this->AircraftCostPerHour = $AircraftCostPerHour;
      return $this;
    }

    /**
     * @return AircraftSpecification
     */
    public function getAircraftDefinition()
    {
      return $this->AircraftDefinition;
    }

    /**
     * @param AircraftSpecification $AircraftDefinition
     * @return FlightSpecification
     */
    public function setAircraftDefinition($AircraftDefinition)
    {
      $this->AircraftDefinition = $AircraftDefinition;
      return $this;
    }

    /**
     * @return string
     */
    public function getAircraftTailNumber()
    {
      return $this->AircraftTailNumber;
    }

    /**
     * @param string $AircraftTailNumber
     * @return FlightSpecification
     */
    public function setAircraftTailNumber($AircraftTailNumber)
    {
      $this->AircraftTailNumber = $AircraftTailNumber;
      return $this;
    }

    /**
     * @return Airport
     */
    public function getAlternateAirport1()
    {
      return $this->AlternateAirport1;
    }

    /**
     * @param Airport $AlternateAirport1
     * @return FlightSpecification
     */
    public function setAlternateAirport1($AlternateAirport1)
    {
      $this->AlternateAirport1 = $AlternateAirport1;
      return $this;
    }

    /**
     * @return Airport
     */
    public function getAlternateAirport2()
    {
      return $this->AlternateAirport2;
    }

    /**
     * @param Airport $AlternateAirport2
     * @return FlightSpecification
     */
    public function setAlternateAirport2($AlternateAirport2)
    {
      $this->AlternateAirport2 = $AlternateAirport2;
      return $this;
    }

    /**
     * @return FlightCalculationOptions
     */
    public function getCalculationOptions()
    {
      return $this->CalculationOptions;
    }

    /**
     * @param FlightCalculationOptions $CalculationOptions
     * @return FlightSpecification
     */
    public function setCalculationOptions($CalculationOptions)
    {
      $this->CalculationOptions = $CalculationOptions;
      return $this;
    }

    /**
     * @return CargoLoadSpecification
     */
    public function getCargoLoadSpecification()
    {
      return $this->CargoLoadSpecification;
    }

    /**
     * @param CargoLoadSpecification $CargoLoadSpecification
     * @return FlightSpecification
     */
    public function setCargoLoadSpecification($CargoLoadSpecification)
    {
      $this->CargoLoadSpecification = $CargoLoadSpecification;
      return $this;
    }

    /**
     * @return PerformanceProfileSpecification
     */
    public function getClimbProfile()
    {
      return $this->ClimbProfile;
    }

    /**
     * @param PerformanceProfileSpecification $ClimbProfile
     * @return FlightSpecification
     */
    public function setClimbProfile($ClimbProfile)
    {
      $this->ClimbProfile = $ClimbProfile;
      return $this;
    }

    /**
     * @return PerformanceProfileSpecification
     */
    public function getCruiseProfile()
    {
      return $this->CruiseProfile;
    }

    /**
     * @param PerformanceProfileSpecification $CruiseProfile
     * @return FlightSpecification
     */
    public function setCruiseProfile($CruiseProfile)
    {
      $this->CruiseProfile = $CruiseProfile;
      return $this;
    }

    /**
     * @return CrewLoadSpecification
     */
    public function getCrweLoadSpecification()
    {
      return $this->CrweLoadSpecification;
    }

    /**
     * @param CrewLoadSpecification $CrweLoadSpecification
     * @return FlightSpecification
     */
    public function setCrweLoadSpecification($CrweLoadSpecification)
    {
      $this->CrweLoadSpecification = $CrweLoadSpecification;
      return $this;
    }

    /**
     * @return Airport
     */
    public function getDepartureAirport()
    {
      return $this->DepartureAirport;
    }

    /**
     * @param Airport $DepartureAirport
     * @return FlightSpecification
     */
    public function setDepartureAirport($DepartureAirport)
    {
      $this->DepartureAirport = $DepartureAirport;
      return $this;
    }

    /**
     * @return PerformanceProfileSpecification
     */
    public function getDescentProfile()
    {
      return $this->DescentProfile;
    }

    /**
     * @param PerformanceProfileSpecification $DescentProfile
     * @return FlightSpecification
     */
    public function setDescentProfile($DescentProfile)
    {
      $this->DescentProfile = $DescentProfile;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDesiredTimeOfArrival()
    {
      if ($this->DesiredTimeOfArrival == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->DesiredTimeOfArrival);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $DesiredTimeOfArrival
     * @return FlightSpecification
     */
    public function setDesiredTimeOfArrival(\DateTime $DesiredTimeOfArrival = null)
    {
      if ($DesiredTimeOfArrival == null) {
       $this->DesiredTimeOfArrival = null;
      } else {
        $this->DesiredTimeOfArrival = $DesiredTimeOfArrival->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return Airport
     */
    public function getDestinationAirport()
    {
      return $this->DestinationAirport;
    }

    /**
     * @param Airport $DestinationAirport
     * @return FlightSpecification
     */
    public function setDestinationAirport($DestinationAirport)
    {
      $this->DestinationAirport = $DestinationAirport;
      return $this;
    }

    /**
     * @return ETPSettings
     */
    public function getETPSpecification()
    {
      return $this->ETPSpecification;
    }

    /**
     * @param ETPSettings $ETPSpecification
     * @return FlightSpecification
     */
    public function setETPSpecification($ETPSpecification)
    {
      $this->ETPSpecification = $ETPSpecification;
      return $this;
    }

    /**
     * @return int
     */
    public function getFlightLevel()
    {
      return $this->FlightLevel;
    }

    /**
     * @param int $FlightLevel
     * @return FlightSpecification
     */
    public function setFlightLevel($FlightLevel)
    {
      $this->FlightLevel = $FlightLevel;
      return $this;
    }

    /**
     * @return FlightLevelSpecification
     */
    public function getFlightLevelDefinition()
    {
      return $this->FlightLevelDefinition;
    }

    /**
     * @param FlightLevelSpecification $FlightLevelDefinition
     * @return FlightSpecification
     */
    public function setFlightLevelDefinition($FlightLevelDefinition)
    {
      $this->FlightLevelDefinition = $FlightLevelDefinition;
      return $this;
    }

    /**
     * @return int
     */
    public function getFlightLevelToAlternate1()
    {
      return $this->FlightLevelToAlternate1;
    }

    /**
     * @param int $FlightLevelToAlternate1
     * @return FlightSpecification
     */
    public function setFlightLevelToAlternate1($FlightLevelToAlternate1)
    {
      $this->FlightLevelToAlternate1 = $FlightLevelToAlternate1;
      return $this;
    }

    /**
     * @return int
     */
    public function getFlightLevelToAlternate2()
    {
      return $this->FlightLevelToAlternate2;
    }

    /**
     * @param int $FlightLevelToAlternate2
     * @return FlightSpecification
     */
    public function setFlightLevelToAlternate2($FlightLevelToAlternate2)
    {
      $this->FlightLevelToAlternate2 = $FlightLevelToAlternate2;
      return $this;
    }

    /**
     * @return string
     */
    public function getFlightNumber()
    {
      return $this->FlightNumber;
    }

    /**
     * @param string $FlightNumber
     * @return FlightSpecification
     */
    public function setFlightNumber($FlightNumber)
    {
      $this->FlightNumber = $FlightNumber;
      return $this;
    }

    /**
     * @return int
     */
    public function getFlightlevelTakeOffAlternate()
    {
      return $this->FlightlevelTakeOffAlternate;
    }

    /**
     * @param int $FlightlevelTakeOffAlternate
     * @return FlightSpecification
     */
    public function setFlightlevelTakeOffAlternate($FlightlevelTakeOffAlternate)
    {
      $this->FlightlevelTakeOffAlternate = $FlightlevelTakeOffAlternate;
      return $this;
    }

    /**
     * @return Currency
     */
    public function getFuelCostPerGallon()
    {
      return $this->FuelCostPerGallon;
    }

    /**
     * @param Currency $FuelCostPerGallon
     * @return FlightSpecification
     */
    public function setFuelCostPerGallon($FuelCostPerGallon)
    {
      $this->FuelCostPerGallon = $FuelCostPerGallon;
      return $this;
    }

    /**
     * @return FlightFuelRules
     */
    public function getFuelRules()
    {
      return $this->FuelRules;
    }

    /**
     * @param FlightFuelRules $FuelRules
     * @return FlightSpecification
     */
    public function setFuelRules($FuelRules)
    {
      $this->FuelRules = $FuelRules;
      return $this;
    }

    /**
     * @return Currency
     */
    public function getFuelUpliftCost()
    {
      return $this->FuelUpliftCost;
    }

    /**
     * @param Currency $FuelUpliftCost
     * @return FlightSpecification
     */
    public function setFuelUpliftCost($FuelUpliftCost)
    {
      $this->FuelUpliftCost = $FuelUpliftCost;
      return $this;
    }

    /**
     * @return float
     */
    public function getGallonsToWaive()
    {
      return $this->GallonsToWaive;
    }

    /**
     * @param float $GallonsToWaive
     * @return FlightSpecification
     */
    public function setGallonsToWaive($GallonsToWaive)
    {
      $this->GallonsToWaive = $GallonsToWaive;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIncludeAllTBCDPoints()
    {
      return $this->IncludeAllTBCDPoints;
    }

    /**
     * @param boolean $IncludeAllTBCDPoints
     * @return FlightSpecification
     */
    public function setIncludeAllTBCDPoints($IncludeAllTBCDPoints)
    {
      $this->IncludeAllTBCDPoints = $IncludeAllTBCDPoints;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIncludeTCD()
    {
      return $this->IncludeTCD;
    }

    /**
     * @param boolean $IncludeTCD
     * @return FlightSpecification
     */
    public function setIncludeTCD($IncludeTCD)
    {
      $this->IncludeTCD = $IncludeTCD;
      return $this;
    }

    /**
     * @return int
     */
    public function getMassAndBalanceConfigurationId()
    {
      return $this->MassAndBalanceConfigurationId;
    }

    /**
     * @param int $MassAndBalanceConfigurationId
     * @return FlightSpecification
     */
    public function setMassAndBalanceConfigurationId($MassAndBalanceConfigurationId)
    {
      $this->MassAndBalanceConfigurationId = $MassAndBalanceConfigurationId;
      return $this;
    }

    /**
     * @return PaxLoadSpecification
     */
    public function getPaxLoadSpecification()
    {
      return $this->PaxLoadSpecification;
    }

    /**
     * @param PaxLoadSpecification $PaxLoadSpecification
     * @return FlightSpecification
     */
    public function setPaxLoadSpecification($PaxLoadSpecification)
    {
      $this->PaxLoadSpecification = $PaxLoadSpecification;
      return $this;
    }

    /**
     * @return FlightLevelAdjustmentSpecification
     */
    public function getProfileAdjustments()
    {
      return $this->ProfileAdjustments;
    }

    /**
     * @param FlightLevelAdjustmentSpecification $ProfileAdjustments
     * @return FlightSpecification
     */
    public function setProfileAdjustments($ProfileAdjustments)
    {
      $this->ProfileAdjustments = $ProfileAdjustments;
      return $this;
    }

    /**
     * @return Currency
     */
    public function getRampFee()
    {
      return $this->RampFee;
    }

    /**
     * @param Currency $RampFee
     * @return FlightSpecification
     */
    public function setRampFee($RampFee)
    {
      $this->RampFee = $RampFee;
      return $this;
    }

    /**
     * @return Route
     */
    public function getRouteToAlternate1()
    {
      return $this->RouteToAlternate1;
    }

    /**
     * @param Route $RouteToAlternate1
     * @return FlightSpecification
     */
    public function setRouteToAlternate1($RouteToAlternate1)
    {
      $this->RouteToAlternate1 = $RouteToAlternate1;
      return $this;
    }

    /**
     * @return Route
     */
    public function getRouteToAlternate2()
    {
      return $this->RouteToAlternate2;
    }

    /**
     * @param Route $RouteToAlternate2
     * @return FlightSpecification
     */
    public function setRouteToAlternate2($RouteToAlternate2)
    {
      $this->RouteToAlternate2 = $RouteToAlternate2;
      return $this;
    }

    /**
     * @return Route
     */
    public function getRouteToDestination()
    {
      return $this->RouteToDestination;
    }

    /**
     * @param Route $RouteToDestination
     * @return FlightSpecification
     */
    public function setRouteToDestination($RouteToDestination)
    {
      $this->RouteToDestination = $RouteToDestination;
      return $this;
    }

    /**
     * @return Route
     */
    public function getRouteToTakeOffAlternate()
    {
      return $this->RouteToTakeOffAlternate;
    }

    /**
     * @param Route $RouteToTakeOffAlternate
     * @return FlightSpecification
     */
    public function setRouteToTakeOffAlternate($RouteToTakeOffAlternate)
    {
      $this->RouteToTakeOffAlternate = $RouteToTakeOffAlternate;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getScheduledTimeOfDeparture()
    {
      if ($this->ScheduledTimeOfDeparture == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->ScheduledTimeOfDeparture);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $ScheduledTimeOfDeparture
     * @return FlightSpecification
     */
    public function setScheduledTimeOfDeparture(\DateTime $ScheduledTimeOfDeparture = null)
    {
      if ($ScheduledTimeOfDeparture == null) {
       $this->ScheduledTimeOfDeparture = null;
      } else {
        $this->ScheduledTimeOfDeparture = $ScheduledTimeOfDeparture->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return Airport
     */
    public function getTakeOffAlternate()
    {
      return $this->TakeOffAlternate;
    }

    /**
     * @param Airport $TakeOffAlternate
     * @return FlightSpecification
     */
    public function setTakeOffAlternate($TakeOffAlternate)
    {
      $this->TakeOffAlternate = $TakeOffAlternate;
      return $this;
    }

    /**
     * @return Winds
     */
    public function getWinds()
    {
      return $this->Winds;
    }

    /**
     * @param Winds $Winds
     * @return FlightSpecification
     */
    public function setWinds($Winds)
    {
      $this->Winds = $Winds;
      return $this;
    }

}
