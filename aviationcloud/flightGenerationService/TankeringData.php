<?php

class TankeringData
{

    /**
     * @var string $AircraftType
     */
    protected $AircraftType = null;

    /**
     * @var float $BestSolution
     */
    protected $BestSolution = null;

    /**
     * @var Weight $FuelCapacity
     */
    protected $FuelCapacity = null;

    /**
     * @var ArrayOfArrayOfTankeringSummary $OrderedSolutions
     */
    protected $OrderedSolutions = null;

    /**
     * @var float $PotentialSave
     */
    protected $PotentialSave = null;

    /**
     * @var string $PriceCurrency
     */
    protected $PriceCurrency = null;

    /**
     * @var float $WorstSolution
     */
    protected $WorstSolution = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getAircraftType()
    {
      return $this->AircraftType;
    }

    /**
     * @param string $AircraftType
     * @return TankeringData
     */
    public function setAircraftType($AircraftType)
    {
      $this->AircraftType = $AircraftType;
      return $this;
    }

    /**
     * @return float
     */
    public function getBestSolution()
    {
      return $this->BestSolution;
    }

    /**
     * @param float $BestSolution
     * @return TankeringData
     */
    public function setBestSolution($BestSolution)
    {
      $this->BestSolution = $BestSolution;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getFuelCapacity()
    {
      return $this->FuelCapacity;
    }

    /**
     * @param Weight $FuelCapacity
     * @return TankeringData
     */
    public function setFuelCapacity($FuelCapacity)
    {
      $this->FuelCapacity = $FuelCapacity;
      return $this;
    }

    /**
     * @return ArrayOfArrayOfTankeringSummary
     */
    public function getOrderedSolutions()
    {
      return $this->OrderedSolutions;
    }

    /**
     * @param ArrayOfArrayOfTankeringSummary $OrderedSolutions
     * @return TankeringData
     */
    public function setOrderedSolutions($OrderedSolutions)
    {
      $this->OrderedSolutions = $OrderedSolutions;
      return $this;
    }

    /**
     * @return float
     */
    public function getPotentialSave()
    {
      return $this->PotentialSave;
    }

    /**
     * @param float $PotentialSave
     * @return TankeringData
     */
    public function setPotentialSave($PotentialSave)
    {
      $this->PotentialSave = $PotentialSave;
      return $this;
    }

    /**
     * @return string
     */
    public function getPriceCurrency()
    {
      return $this->PriceCurrency;
    }

    /**
     * @param string $PriceCurrency
     * @return TankeringData
     */
    public function setPriceCurrency($PriceCurrency)
    {
      $this->PriceCurrency = $PriceCurrency;
      return $this;
    }

    /**
     * @return float
     */
    public function getWorstSolution()
    {
      return $this->WorstSolution;
    }

    /**
     * @param float $WorstSolution
     * @return TankeringData
     */
    public function setWorstSolution($WorstSolution)
    {
      $this->WorstSolution = $WorstSolution;
      return $this;
    }

}
