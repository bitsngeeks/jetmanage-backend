<?php

class FlightCalculationFault
{

    /**
     * @var string $ErrorMessage
     */
    protected $ErrorMessage = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
      return $this->ErrorMessage;
    }

    /**
     * @param string $ErrorMessage
     * @return FlightCalculationFault
     */
    public function setErrorMessage($ErrorMessage)
    {
      $this->ErrorMessage = $ErrorMessage;
      return $this;
    }

}
