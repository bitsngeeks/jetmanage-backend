<?php

class FlightLevelOptimizationOptions
{

    /**
     * @var PerformanceProfileSpecification $AlternativeCruiseProfile
     */
    protected $AlternativeCruiseProfile = null;

    /**
     * @var int $AlternativeCruiseProfileId
     */
    protected $AlternativeCruiseProfileId = null;

    /**
     * @var ArrayOfAltitude $Altitudes
     */
    protected $Altitudes = null;

    /**
     * @var Weight $LowerWeightCalculation
     */
    protected $LowerWeightCalculation = null;

    /**
     * @var FlightlevelOptimizationCriteria $OptimizationCriteria
     */
    protected $OptimizationCriteria = null;

    /**
     * @var OptimumFlightlevelOptionSpecification $OptimumFlightlevelOption
     */
    protected $OptimumFlightlevelOption = null;

    /**
     * @var PerformanceProfileSpecification $SecondAlternativeCruiseProfile
     */
    protected $SecondAlternativeCruiseProfile = null;

    /**
     * @var int $SecondAlternativeCruiseProfileId
     */
    protected $SecondAlternativeCruiseProfileId = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return PerformanceProfileSpecification
     */
    public function getAlternativeCruiseProfile()
    {
      return $this->AlternativeCruiseProfile;
    }

    /**
     * @param PerformanceProfileSpecification $AlternativeCruiseProfile
     * @return FlightLevelOptimizationOptions
     */
    public function setAlternativeCruiseProfile($AlternativeCruiseProfile)
    {
      $this->AlternativeCruiseProfile = $AlternativeCruiseProfile;
      return $this;
    }

    /**
     * @return int
     */
    public function getAlternativeCruiseProfileId()
    {
      return $this->AlternativeCruiseProfileId;
    }

    /**
     * @param int $AlternativeCruiseProfileId
     * @return FlightLevelOptimizationOptions
     */
    public function setAlternativeCruiseProfileId($AlternativeCruiseProfileId)
    {
      $this->AlternativeCruiseProfileId = $AlternativeCruiseProfileId;
      return $this;
    }

    /**
     * @return ArrayOfAltitude
     */
    public function getAltitudes()
    {
      return $this->Altitudes;
    }

    /**
     * @param ArrayOfAltitude $Altitudes
     * @return FlightLevelOptimizationOptions
     */
    public function setAltitudes($Altitudes)
    {
      $this->Altitudes = $Altitudes;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getLowerWeightCalculation()
    {
      return $this->LowerWeightCalculation;
    }

    /**
     * @param Weight $LowerWeightCalculation
     * @return FlightLevelOptimizationOptions
     */
    public function setLowerWeightCalculation($LowerWeightCalculation)
    {
      $this->LowerWeightCalculation = $LowerWeightCalculation;
      return $this;
    }

    /**
     * @return FlightlevelOptimizationCriteria
     */
    public function getOptimizationCriteria()
    {
      return $this->OptimizationCriteria;
    }

    /**
     * @param FlightlevelOptimizationCriteria $OptimizationCriteria
     * @return FlightLevelOptimizationOptions
     */
    public function setOptimizationCriteria($OptimizationCriteria)
    {
      $this->OptimizationCriteria = $OptimizationCriteria;
      return $this;
    }

    /**
     * @return OptimumFlightlevelOptionSpecification
     */
    public function getOptimumFlightlevelOption()
    {
      return $this->OptimumFlightlevelOption;
    }

    /**
     * @param OptimumFlightlevelOptionSpecification $OptimumFlightlevelOption
     * @return FlightLevelOptimizationOptions
     */
    public function setOptimumFlightlevelOption($OptimumFlightlevelOption)
    {
      $this->OptimumFlightlevelOption = $OptimumFlightlevelOption;
      return $this;
    }

    /**
     * @return PerformanceProfileSpecification
     */
    public function getSecondAlternativeCruiseProfile()
    {
      return $this->SecondAlternativeCruiseProfile;
    }

    /**
     * @param PerformanceProfileSpecification $SecondAlternativeCruiseProfile
     * @return FlightLevelOptimizationOptions
     */
    public function setSecondAlternativeCruiseProfile($SecondAlternativeCruiseProfile)
    {
      $this->SecondAlternativeCruiseProfile = $SecondAlternativeCruiseProfile;
      return $this;
    }

    /**
     * @return int
     */
    public function getSecondAlternativeCruiseProfileId()
    {
      return $this->SecondAlternativeCruiseProfileId;
    }

    /**
     * @param int $SecondAlternativeCruiseProfileId
     * @return FlightLevelOptimizationOptions
     */
    public function setSecondAlternativeCruiseProfileId($SecondAlternativeCruiseProfileId)
    {
      $this->SecondAlternativeCruiseProfileId = $SecondAlternativeCruiseProfileId;
      return $this;
    }

}
