<?php

class TripCalculationResponse
{

    /**
     * @var BriefingPackageResponse $BriefingPackage
     */
    protected $BriefingPackage = null;

    /**
     * @var ArrayOfFlightLog $FlightData
     */
    protected $FlightData = null;

    /**
     * @var TankeringData $TankeringInformation
     */
    protected $TankeringInformation = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return BriefingPackageResponse
     */
    public function getBriefingPackage()
    {
      return $this->BriefingPackage;
    }

    /**
     * @param BriefingPackageResponse $BriefingPackage
     * @return TripCalculationResponse
     */
    public function setBriefingPackage($BriefingPackage)
    {
      $this->BriefingPackage = $BriefingPackage;
      return $this;
    }

    /**
     * @return ArrayOfFlightLog
     */
    public function getFlightData()
    {
      return $this->FlightData;
    }

    /**
     * @param ArrayOfFlightLog $FlightData
     * @return TripCalculationResponse
     */
    public function setFlightData($FlightData)
    {
      $this->FlightData = $FlightData;
      return $this;
    }

    /**
     * @return TankeringData
     */
    public function getTankeringInformation()
    {
      return $this->TankeringInformation;
    }

    /**
     * @param TankeringData $TankeringInformation
     * @return TripCalculationResponse
     */
    public function setTankeringInformation($TankeringInformation)
    {
      $this->TankeringInformation = $TankeringInformation;
      return $this;
    }

}
