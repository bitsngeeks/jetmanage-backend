<?php

class FlightCalculationOptions
{

    /**
     * @var Weight $AlternateApproachFuel
     */
    protected $AlternateApproachFuel = null;

    /**
     * @var boolean $AutomaticApproachFuelEstimation
     */
    protected $AutomaticApproachFuelEstimation = null;

    /**
     * @var boolean $BeginEndFlightAtWaypointAltitude
     */
    protected $BeginEndFlightAtWaypointAltitude = null;

    /**
     * @var ContingencyStrategy $ContingencyStrategy
     */
    protected $ContingencyStrategy = null;

    /**
     * @var Weight $DestinationApproachFuel
     */
    protected $DestinationApproachFuel = null;

    /**
     * @var boolean $DisableAirspaceFlightLevelStructure
     */
    protected $DisableAirspaceFlightLevelStructure = null;

    /**
     * @var boolean $DisableDispatchMode
     */
    protected $DisableDispatchMode = null;

    /**
     * @var boolean $DisableFlightLevelOptimizationTable
     */
    protected $DisableFlightLevelOptimizationTable = null;

    /**
     * @var boolean $DontModifySpeedOrHeight
     */
    protected $DontModifySpeedOrHeight = null;

    /**
     * @var boolean $EnableAdequateAirportSearch
     */
    protected $EnableAdequateAirportSearch = null;

    /**
     * @var boolean $EnableOverflightCostCalculation
     */
    protected $EnableOverflightCostCalculation = null;

    /**
     * @var boolean $EnableProcedureAltitudeRestrictions
     */
    protected $EnableProcedureAltitudeRestrictions = null;

    /**
     * @var boolean $EnableVFREdgeOverwrite
     */
    protected $EnableVFREdgeOverwrite = null;

    /**
     * @var FlightLevelOptimizationOptions $FlightLevelOptimizationOptions
     */
    protected $FlightLevelOptimizationOptions = null;

    /**
     * @var FuelCheckOptionsSpecification $FuelCheckOption
     */
    protected $FuelCheckOption = null;

    /**
     * @var FuelPolicy $FuelPolicy
     */
    protected $FuelPolicy = null;

    /**
     * @var GainLossCalculationInfo $GainLossCalculation
     */
    protected $GainLossCalculation = null;

    /**
     * @var boolean $GenerateAltRoute
     */
    protected $GenerateAltRoute = null;

    /**
     * @var InlineOptionSpecification $InlineOption
     */
    protected $InlineOption = null;

    /**
     * @var Weight $MinimumAlternateFuel
     */
    protected $MinimumAlternateFuel = null;

    /**
     * @var OutputUnitSpecification $OutputUnit
     */
    protected $OutputUnit = null;

    /**
     * @var boolean $OverrideInitialFlightlevel
     */
    protected $OverrideInitialFlightlevel = null;

    /**
     * @var RAIMPredictionOptions $RAIMOptions
     */
    protected $RAIMOptions = null;

    /**
     * @var FlightCalculationSIDSTAROptions $SIDSTAROptions
     */
    protected $SIDSTAROptions = null;

    /**
     * @var TaxiFuelSpecification $TaxiFuelSpecification
     */
    protected $TaxiFuelSpecification = null;

    /**
     * @var TaxiTimeSpecification $TaxiTimeSpecification
     */
    protected $TaxiTimeSpecification = null;

    /**
     * @var boolean $UseAutoSpeedCalculation
     */
    protected $UseAutoSpeedCalculation = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Weight
     */
    public function getAlternateApproachFuel()
    {
      return $this->AlternateApproachFuel;
    }

    /**
     * @param Weight $AlternateApproachFuel
     * @return FlightCalculationOptions
     */
    public function setAlternateApproachFuel($AlternateApproachFuel)
    {
      $this->AlternateApproachFuel = $AlternateApproachFuel;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getAutomaticApproachFuelEstimation()
    {
      return $this->AutomaticApproachFuelEstimation;
    }

    /**
     * @param boolean $AutomaticApproachFuelEstimation
     * @return FlightCalculationOptions
     */
    public function setAutomaticApproachFuelEstimation($AutomaticApproachFuelEstimation)
    {
      $this->AutomaticApproachFuelEstimation = $AutomaticApproachFuelEstimation;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBeginEndFlightAtWaypointAltitude()
    {
      return $this->BeginEndFlightAtWaypointAltitude;
    }

    /**
     * @param boolean $BeginEndFlightAtWaypointAltitude
     * @return FlightCalculationOptions
     */
    public function setBeginEndFlightAtWaypointAltitude($BeginEndFlightAtWaypointAltitude)
    {
      $this->BeginEndFlightAtWaypointAltitude = $BeginEndFlightAtWaypointAltitude;
      return $this;
    }

    /**
     * @return ContingencyStrategy
     */
    public function getContingencyStrategy()
    {
      return $this->ContingencyStrategy;
    }

    /**
     * @param ContingencyStrategy $ContingencyStrategy
     * @return FlightCalculationOptions
     */
    public function setContingencyStrategy($ContingencyStrategy)
    {
      $this->ContingencyStrategy = $ContingencyStrategy;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getDestinationApproachFuel()
    {
      return $this->DestinationApproachFuel;
    }

    /**
     * @param Weight $DestinationApproachFuel
     * @return FlightCalculationOptions
     */
    public function setDestinationApproachFuel($DestinationApproachFuel)
    {
      $this->DestinationApproachFuel = $DestinationApproachFuel;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getDisableAirspaceFlightLevelStructure()
    {
      return $this->DisableAirspaceFlightLevelStructure;
    }

    /**
     * @param boolean $DisableAirspaceFlightLevelStructure
     * @return FlightCalculationOptions
     */
    public function setDisableAirspaceFlightLevelStructure($DisableAirspaceFlightLevelStructure)
    {
      $this->DisableAirspaceFlightLevelStructure = $DisableAirspaceFlightLevelStructure;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getDisableDispatchMode()
    {
      return $this->DisableDispatchMode;
    }

    /**
     * @param boolean $DisableDispatchMode
     * @return FlightCalculationOptions
     */
    public function setDisableDispatchMode($DisableDispatchMode)
    {
      $this->DisableDispatchMode = $DisableDispatchMode;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getDisableFlightLevelOptimizationTable()
    {
      return $this->DisableFlightLevelOptimizationTable;
    }

    /**
     * @param boolean $DisableFlightLevelOptimizationTable
     * @return FlightCalculationOptions
     */
    public function setDisableFlightLevelOptimizationTable($DisableFlightLevelOptimizationTable)
    {
      $this->DisableFlightLevelOptimizationTable = $DisableFlightLevelOptimizationTable;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getDontModifySpeedOrHeight()
    {
      return $this->DontModifySpeedOrHeight;
    }

    /**
     * @param boolean $DontModifySpeedOrHeight
     * @return FlightCalculationOptions
     */
    public function setDontModifySpeedOrHeight($DontModifySpeedOrHeight)
    {
      $this->DontModifySpeedOrHeight = $DontModifySpeedOrHeight;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getEnableAdequateAirportSearch()
    {
      return $this->EnableAdequateAirportSearch;
    }

    /**
     * @param boolean $EnableAdequateAirportSearch
     * @return FlightCalculationOptions
     */
    public function setEnableAdequateAirportSearch($EnableAdequateAirportSearch)
    {
      $this->EnableAdequateAirportSearch = $EnableAdequateAirportSearch;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getEnableOverflightCostCalculation()
    {
      return $this->EnableOverflightCostCalculation;
    }

    /**
     * @param boolean $EnableOverflightCostCalculation
     * @return FlightCalculationOptions
     */
    public function setEnableOverflightCostCalculation($EnableOverflightCostCalculation)
    {
      $this->EnableOverflightCostCalculation = $EnableOverflightCostCalculation;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getEnableProcedureAltitudeRestrictions()
    {
      return $this->EnableProcedureAltitudeRestrictions;
    }

    /**
     * @param boolean $EnableProcedureAltitudeRestrictions
     * @return FlightCalculationOptions
     */
    public function setEnableProcedureAltitudeRestrictions($EnableProcedureAltitudeRestrictions)
    {
      $this->EnableProcedureAltitudeRestrictions = $EnableProcedureAltitudeRestrictions;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getEnableVFREdgeOverwrite()
    {
      return $this->EnableVFREdgeOverwrite;
    }

    /**
     * @param boolean $EnableVFREdgeOverwrite
     * @return FlightCalculationOptions
     */
    public function setEnableVFREdgeOverwrite($EnableVFREdgeOverwrite)
    {
      $this->EnableVFREdgeOverwrite = $EnableVFREdgeOverwrite;
      return $this;
    }

    /**
     * @return FlightLevelOptimizationOptions
     */
    public function getFlightLevelOptimizationOptions()
    {
      return $this->FlightLevelOptimizationOptions;
    }

    /**
     * @param FlightLevelOptimizationOptions $FlightLevelOptimizationOptions
     * @return FlightCalculationOptions
     */
    public function setFlightLevelOptimizationOptions($FlightLevelOptimizationOptions)
    {
      $this->FlightLevelOptimizationOptions = $FlightLevelOptimizationOptions;
      return $this;
    }

    /**
     * @return FuelCheckOptionsSpecification
     */
    public function getFuelCheckOption()
    {
      return $this->FuelCheckOption;
    }

    /**
     * @param FuelCheckOptionsSpecification $FuelCheckOption
     * @return FlightCalculationOptions
     */
    public function setFuelCheckOption($FuelCheckOption)
    {
      $this->FuelCheckOption = $FuelCheckOption;
      return $this;
    }

    /**
     * @return FuelPolicy
     */
    public function getFuelPolicy()
    {
      return $this->FuelPolicy;
    }

    /**
     * @param FuelPolicy $FuelPolicy
     * @return FlightCalculationOptions
     */
    public function setFuelPolicy($FuelPolicy)
    {
      $this->FuelPolicy = $FuelPolicy;
      return $this;
    }

    /**
     * @return GainLossCalculationInfo
     */
    public function getGainLossCalculation()
    {
      return $this->GainLossCalculation;
    }

    /**
     * @param GainLossCalculationInfo $GainLossCalculation
     * @return FlightCalculationOptions
     */
    public function setGainLossCalculation($GainLossCalculation)
    {
      $this->GainLossCalculation = $GainLossCalculation;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getGenerateAltRoute()
    {
      return $this->GenerateAltRoute;
    }

    /**
     * @param boolean $GenerateAltRoute
     * @return FlightCalculationOptions
     */
    public function setGenerateAltRoute($GenerateAltRoute)
    {
      $this->GenerateAltRoute = $GenerateAltRoute;
      return $this;
    }

    /**
     * @return InlineOptionSpecification
     */
    public function getInlineOption()
    {
      return $this->InlineOption;
    }

    /**
     * @param InlineOptionSpecification $InlineOption
     * @return FlightCalculationOptions
     */
    public function setInlineOption($InlineOption)
    {
      $this->InlineOption = $InlineOption;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getMinimumAlternateFuel()
    {
      return $this->MinimumAlternateFuel;
    }

    /**
     * @param Weight $MinimumAlternateFuel
     * @return FlightCalculationOptions
     */
    public function setMinimumAlternateFuel($MinimumAlternateFuel)
    {
      $this->MinimumAlternateFuel = $MinimumAlternateFuel;
      return $this;
    }

    /**
     * @return OutputUnitSpecification
     */
    public function getOutputUnit()
    {
      return $this->OutputUnit;
    }

    /**
     * @param OutputUnitSpecification $OutputUnit
     * @return FlightCalculationOptions
     */
    public function setOutputUnit($OutputUnit)
    {
      $this->OutputUnit = $OutputUnit;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getOverrideInitialFlightlevel()
    {
      return $this->OverrideInitialFlightlevel;
    }

    /**
     * @param boolean $OverrideInitialFlightlevel
     * @return FlightCalculationOptions
     */
    public function setOverrideInitialFlightlevel($OverrideInitialFlightlevel)
    {
      $this->OverrideInitialFlightlevel = $OverrideInitialFlightlevel;
      return $this;
    }

    /**
     * @return RAIMPredictionOptions
     */
    public function getRAIMOptions()
    {
      return $this->RAIMOptions;
    }

    /**
     * @param RAIMPredictionOptions $RAIMOptions
     * @return FlightCalculationOptions
     */
    public function setRAIMOptions($RAIMOptions)
    {
      $this->RAIMOptions = $RAIMOptions;
      return $this;
    }

    /**
     * @return FlightCalculationSIDSTAROptions
     */
    public function getSIDSTAROptions()
    {
      return $this->SIDSTAROptions;
    }

    /**
     * @param FlightCalculationSIDSTAROptions $SIDSTAROptions
     * @return FlightCalculationOptions
     */
    public function setSIDSTAROptions($SIDSTAROptions)
    {
      $this->SIDSTAROptions = $SIDSTAROptions;
      return $this;
    }

    /**
     * @return TaxiFuelSpecification
     */
    public function getTaxiFuelSpecification()
    {
      return $this->TaxiFuelSpecification;
    }

    /**
     * @param TaxiFuelSpecification $TaxiFuelSpecification
     * @return FlightCalculationOptions
     */
    public function setTaxiFuelSpecification($TaxiFuelSpecification)
    {
      $this->TaxiFuelSpecification = $TaxiFuelSpecification;
      return $this;
    }

    /**
     * @return TaxiTimeSpecification
     */
    public function getTaxiTimeSpecification()
    {
      return $this->TaxiTimeSpecification;
    }

    /**
     * @param TaxiTimeSpecification $TaxiTimeSpecification
     * @return FlightCalculationOptions
     */
    public function setTaxiTimeSpecification($TaxiTimeSpecification)
    {
      $this->TaxiTimeSpecification = $TaxiTimeSpecification;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUseAutoSpeedCalculation()
    {
      return $this->UseAutoSpeedCalculation;
    }

    /**
     * @param boolean $UseAutoSpeedCalculation
     * @return FlightCalculationOptions
     */
    public function setUseAutoSpeedCalculation($UseAutoSpeedCalculation)
    {
      $this->UseAutoSpeedCalculation = $UseAutoSpeedCalculation;
      return $this;
    }

}
