<?php

class NAVValidationError
{

    /**
     * @var NAVValidationErrorCode $ErrorCode
     */
    protected $ErrorCode = null;

    /**
     * @var string $ErrorDescription
     */
    protected $ErrorDescription = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return NAVValidationErrorCode
     */
    public function getErrorCode()
    {
      return $this->ErrorCode;
    }

    /**
     * @param NAVValidationErrorCode $ErrorCode
     * @return NAVValidationError
     */
    public function setErrorCode($ErrorCode)
    {
      $this->ErrorCode = $ErrorCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getErrorDescription()
    {
      return $this->ErrorDescription;
    }

    /**
     * @param string $ErrorDescription
     * @return NAVValidationError
     */
    public function setErrorDescription($ErrorDescription)
    {
      $this->ErrorDescription = $ErrorDescription;
      return $this;
    }

}
