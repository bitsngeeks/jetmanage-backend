<?php

class SpecialHandlingReason
{
    const __default = 'None';
    const None = 'None';
    const ALTRV = 'ALTRV';
    const ATFMX = 'ATFMX';
    const FFR = 'FFR';
    const FLTCK = 'FLTCK';
    const HAZMAT = 'HAZMAT';
    const HEAD = 'HEAD';
    const HOSP = 'HOSP';
    const HUM = 'HUM';
    const MARSA = 'MARSA';
    const MEDEVAC = 'MEDEVAC';
    const NONRVSM = 'NONRVSM';
    const SAR = 'SAR';
    const STATE = 'STATE';


}
