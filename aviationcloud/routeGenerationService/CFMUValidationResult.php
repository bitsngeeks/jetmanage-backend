<?php

class CFMUValidationResult
{

    /**
     * @var ArrayOfCFMUValidationError $Errors
     */
    protected $Errors = null;

    /**
     * @var ArrayOfCFMUValidationWarning $Warnings
     */
    protected $Warnings = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfCFMUValidationError
     */
    public function getErrors()
    {
      return $this->Errors;
    }

    /**
     * @param ArrayOfCFMUValidationError $Errors
     * @return CFMUValidationResult
     */
    public function setErrors($Errors)
    {
      $this->Errors = $Errors;
      return $this;
    }

    /**
     * @return ArrayOfCFMUValidationWarning
     */
    public function getWarnings()
    {
      return $this->Warnings;
    }

    /**
     * @param ArrayOfCFMUValidationWarning $Warnings
     * @return CFMUValidationResult
     */
    public function setWarnings($Warnings)
    {
      $this->Warnings = $Warnings;
      return $this;
    }

}
