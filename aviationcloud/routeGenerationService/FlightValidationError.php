<?php

class FlightValidationError
{

    /**
     * @var int $ErrorCode
     */
    protected $ErrorCode = null;

    /**
     * @var string $ErrorDescription
     */
    protected $ErrorDescription = null;

    /**
     * @var ArrayOfFlightValidationError $ErrorParameters
     */
    protected $ErrorParameters = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getErrorCode()
    {
      return $this->ErrorCode;
    }

    /**
     * @param int $ErrorCode
     * @return FlightValidationError
     */
    public function setErrorCode($ErrorCode)
    {
      $this->ErrorCode = $ErrorCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getErrorDescription()
    {
      return $this->ErrorDescription;
    }

    /**
     * @param string $ErrorDescription
     * @return FlightValidationError
     */
    public function setErrorDescription($ErrorDescription)
    {
      $this->ErrorDescription = $ErrorDescription;
      return $this;
    }

    /**
     * @return ArrayOfFlightValidationError
     */
    public function getErrorParameters()
    {
      return $this->ErrorParameters;
    }

    /**
     * @param ArrayOfFlightValidationError $ErrorParameters
     * @return FlightValidationError
     */
    public function setErrorParameters($ErrorParameters)
    {
      $this->ErrorParameters = $ErrorParameters;
      return $this;
    }

}
