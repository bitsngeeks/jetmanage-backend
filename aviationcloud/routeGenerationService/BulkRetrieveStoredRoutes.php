<?php

class BulkRetrieveStoredRoutes
{

    /**
     * @var BulkStoredRouteRequest $request
     */
    protected $request = null;

    /**
     * @param BulkStoredRouteRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return BulkStoredRouteRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param BulkStoredRouteRequest $request
     * @return BulkRetrieveStoredRoutes
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
