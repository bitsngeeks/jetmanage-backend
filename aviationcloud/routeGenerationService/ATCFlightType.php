<?php

class ATCFlightType
{
    const __default = 'Scheduled';
    const Scheduled = 'Scheduled';
    const NotScheduled = 'NotScheduled';
    const Military = 'Military';
    const Other = 'Other';
    const General = 'General';
    const DVFR = 'DVFR';


}
