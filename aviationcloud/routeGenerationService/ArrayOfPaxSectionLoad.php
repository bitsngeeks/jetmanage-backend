<?php

class ArrayOfPaxSectionLoad implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var PaxSectionLoad[] $PaxSectionLoad
     */
    protected $PaxSectionLoad = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return PaxSectionLoad[]
     */
    public function getPaxSectionLoad()
    {
      return $this->PaxSectionLoad;
    }

    /**
     * @param PaxSectionLoad[] $PaxSectionLoad
     * @return ArrayOfPaxSectionLoad
     */
    public function setPaxSectionLoad(array $PaxSectionLoad = null)
    {
      $this->PaxSectionLoad = $PaxSectionLoad;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->PaxSectionLoad[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return PaxSectionLoad
     */
    public function offsetGet($offset)
    {
      return $this->PaxSectionLoad[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param PaxSectionLoad $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->PaxSectionLoad[] = $value;
      } else {
        $this->PaxSectionLoad[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->PaxSectionLoad[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return PaxSectionLoad Return the current element
     */
    public function current()
    {
      return current($this->PaxSectionLoad);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->PaxSectionLoad);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->PaxSectionLoad);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->PaxSectionLoad);
    }

    /**
     * Countable implementation
     *
     * @return PaxSectionLoad Return count of elements
     */
    public function count()
    {
      return count($this->PaxSectionLoad);
    }

}
