<?php

class ATCClearedRouteOptions
{

    /**
     * @var boolean $Include
     */
    protected $Include = null;

    /**
     * @var ATCRecentlyClearedSortOption $SortOption
     */
    protected $SortOption = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return boolean
     */
    public function getInclude()
    {
      return $this->Include;
    }

    /**
     * @param boolean $Include
     * @return ATCClearedRouteOptions
     */
    public function setInclude($Include)
    {
      $this->Include = $Include;
      return $this;
    }

    /**
     * @return ATCRecentlyClearedSortOption
     */
    public function getSortOption()
    {
      return $this->SortOption;
    }

    /**
     * @param ATCRecentlyClearedSortOption $SortOption
     * @return ATCClearedRouteOptions
     */
    public function setSortOption($SortOption)
    {
      $this->SortOption = $SortOption;
      return $this;
    }

}
