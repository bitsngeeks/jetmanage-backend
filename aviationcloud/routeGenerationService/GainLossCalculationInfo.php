<?php

class GainLossCalculationInfo
{

    /**
     * @var Currency $FuelPriceAtDeparture
     */
    protected $FuelPriceAtDeparture = null;

    /**
     * @var Currency $FuelPriceAtDestination
     */
    protected $FuelPriceAtDestination = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Currency
     */
    public function getFuelPriceAtDeparture()
    {
      return $this->FuelPriceAtDeparture;
    }

    /**
     * @param Currency $FuelPriceAtDeparture
     * @return GainLossCalculationInfo
     */
    public function setFuelPriceAtDeparture($FuelPriceAtDeparture)
    {
      $this->FuelPriceAtDeparture = $FuelPriceAtDeparture;
      return $this;
    }

    /**
     * @return Currency
     */
    public function getFuelPriceAtDestination()
    {
      return $this->FuelPriceAtDestination;
    }

    /**
     * @param Currency $FuelPriceAtDestination
     * @return GainLossCalculationInfo
     */
    public function setFuelPriceAtDestination($FuelPriceAtDestination)
    {
      $this->FuelPriceAtDestination = $FuelPriceAtDestination;
      return $this;
    }

}
