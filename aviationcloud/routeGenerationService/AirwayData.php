<?php

class AirwayData
{

    /**
     * @var string $AirWayId
     */
    protected $AirWayId = null;

    /**
     * @var float $AirwayCourse
     */
    protected $AirwayCourse = null;

    /**
     * @var int $AirwayMaxFlightLevel
     */
    protected $AirwayMaxFlightLevel = null;

    /**
     * @var int $AirwayMinFlightLevel
     */
    protected $AirwayMinFlightLevel = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getAirWayId()
    {
      return $this->AirWayId;
    }

    /**
     * @param string $AirWayId
     * @return AirwayData
     */
    public function setAirWayId($AirWayId)
    {
      $this->AirWayId = $AirWayId;
      return $this;
    }

    /**
     * @return float
     */
    public function getAirwayCourse()
    {
      return $this->AirwayCourse;
    }

    /**
     * @param float $AirwayCourse
     * @return AirwayData
     */
    public function setAirwayCourse($AirwayCourse)
    {
      $this->AirwayCourse = $AirwayCourse;
      return $this;
    }

    /**
     * @return int
     */
    public function getAirwayMaxFlightLevel()
    {
      return $this->AirwayMaxFlightLevel;
    }

    /**
     * @param int $AirwayMaxFlightLevel
     * @return AirwayData
     */
    public function setAirwayMaxFlightLevel($AirwayMaxFlightLevel)
    {
      $this->AirwayMaxFlightLevel = $AirwayMaxFlightLevel;
      return $this;
    }

    /**
     * @return int
     */
    public function getAirwayMinFlightLevel()
    {
      return $this->AirwayMinFlightLevel;
    }

    /**
     * @param int $AirwayMinFlightLevel
     * @return AirwayData
     */
    public function setAirwayMinFlightLevel($AirwayMinFlightLevel)
    {
      $this->AirwayMinFlightLevel = $AirwayMinFlightLevel;
      return $this;
    }

}
