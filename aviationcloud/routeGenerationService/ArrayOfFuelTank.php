<?php

class ArrayOfFuelTank implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var FuelTank[] $FuelTank
     */
    protected $FuelTank = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return FuelTank[]
     */
    public function getFuelTank()
    {
      return $this->FuelTank;
    }

    /**
     * @param FuelTank[] $FuelTank
     * @return ArrayOfFuelTank
     */
    public function setFuelTank(array $FuelTank = null)
    {
      $this->FuelTank = $FuelTank;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->FuelTank[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return FuelTank
     */
    public function offsetGet($offset)
    {
      return $this->FuelTank[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param FuelTank $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->FuelTank[] = $value;
      } else {
        $this->FuelTank[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->FuelTank[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return FuelTank Return the current element
     */
    public function current()
    {
      return current($this->FuelTank);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->FuelTank);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->FuelTank);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->FuelTank);
    }

    /**
     * Countable implementation
     *
     * @return FuelTank Return count of elements
     */
    public function count()
    {
      return count($this->FuelTank);
    }

}
