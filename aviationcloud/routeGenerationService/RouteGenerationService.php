<?php

class RouteGenerationService extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
      'GenerateRoutes' => '\\GenerateRoutes',
      'RouteRequest' => '\\RouteRequest',
      'RequestBase' => '\\RequestBase',
      'RouteSpecification' => '\\RouteSpecification',
      'AircraftSpecification' => '\\AircraftSpecification',
      'TailnumberAircraftSpecification' => '\\TailnumberAircraftSpecification',
      'ICAOIdentAircraftSpecification' => '\\ICAOIdentAircraftSpecification',
      'AircraftATCInfo' => '\\AircraftATCInfo',
      'AircraftEquipment' => '\\AircraftEquipment',
      'PerformanceBasedNavigationCapabilities' => '\\PerformanceBasedNavigationCapabilities',
      'RadioAndNavigationEquipment' => '\\RadioAndNavigationEquipment',
      'ATCACARSEquipment' => '\\ATCACARSEquipment',
      'CPDLCEquipment' => '\\CPDLCEquipment',
      'RTFEquipment' => '\\RTFEquipment',
      'SSREquipment' => '\\SSREquipment',
      'ADSCapabilities' => '\\ADSCapabilities',
      'ModeSCapabilities' => '\\ModeSCapabilities',
      'AircraftFrequencyAvailability' => '\\AircraftFrequencyAvailability',
      'AircraftLifeJacketEquipment' => '\\AircraftLifeJacketEquipment',
      'RNAVPreferentialRoutes' => '\\RNAVPreferentialRoutes',
      'AircraftSurvivalEquipment' => '\\AircraftSurvivalEquipment',
      'Weight' => '\\Weight',
      'Time' => '\\Time',
      'FuelTankDefinition' => '\\FuelTankDefinition',
      'SimpleFuelTankDefinition' => '\\SimpleFuelTankDefinition',
      'FuelTankListDefinition' => '\\FuelTankListDefinition',
      'ArrayOfFuelTank' => '\\ArrayOfFuelTank',
      'FuelTank' => '\\FuelTank',
      'AircraftStructureUnit' => '\\AircraftStructureUnit',
      'ArmDefinition' => '\\ArmDefinition',
      'StaticArmDefinition' => '\\StaticArmDefinition',
      'DynamicArmDefinition' => '\\DynamicArmDefinition',
      'ArrayOfWeightDependentArm' => '\\ArrayOfWeightDependentArm',
      'WeightDependentArm' => '\\WeightDependentArm',
      'PassengerCompartment' => '\\PassengerCompartment',
      'CargoCompartment' => '\\CargoCompartment',
      'CrewCompartment' => '\\CrewCompartment',
      'MassAndBalanceConfigurationBase' => '\\MassAndBalanceConfigurationBase',
      'MassAndBalanceConfiguration' => '\\MassAndBalanceConfiguration',
      'SimpleMassAndBalanceConfiguration' => '\\SimpleMassAndBalanceConfiguration',
      'CompartmentBasedMassAndBalanceConfiguration' => '\\CompartmentBasedMassAndBalanceConfiguration',
      'ArrayOfPassengerCompartment' => '\\ArrayOfPassengerCompartment',
      'ArrayOfCargoCompartment' => '\\ArrayOfCargoCompartment',
      'ArrayOfCrewCompartment' => '\\ArrayOfCrewCompartment',
      'DefaultWeightParameters' => '\\DefaultWeightParameters',
      'ReferencedMassAndBalanceConfiguration' => '\\ReferencedMassAndBalanceConfiguration',
      'UnspecifiedMassAndBalance' => '\\UnspecifiedMassAndBalance',
      'EmbeddedAircraftSpecification' => '\\EmbeddedAircraftSpecification',
      'Aircraft' => '\\Aircraft',
      'AircraftPerformanceData' => '\\AircraftPerformanceData',
      'ArrayOfAircraftPerformanceProfile' => '\\ArrayOfAircraftPerformanceProfile',
      'AircraftPerformanceProfile' => '\\AircraftPerformanceProfile',
      'BiasedClimbPerformanceProfile' => '\\BiasedClimbPerformanceProfile',
      'ClimbProfile' => '\\ClimbProfile',
      'DiscreteClimbProfile' => '\\DiscreteClimbProfile',
      'ArrayOfClimbDescendDataSeries' => '\\ArrayOfClimbDescendDataSeries',
      'ClimbDescendDataSeries' => '\\ClimbDescendDataSeries',
      'PerformanceDataSeries' => '\\PerformanceDataSeries',
      'HoldingDataSeries' => '\\HoldingDataSeries',
      'ArrayOfHoldingDataPoint' => '\\ArrayOfHoldingDataPoint',
      'HoldingDataPoint' => '\\HoldingDataPoint',
      'PerformanceDataPoint' => '\\PerformanceDataPoint',
      'ISAPoint' => '\\ISAPoint',
      'ClimbDescendDataPoint' => '\\ClimbDescendDataPoint',
      'ClimbDescendData' => '\\ClimbDescendData',
      'Length' => '\\Length',
      'CruisePerformanceDataPoint' => '\\CruisePerformanceDataPoint',
      'AircraftCruisePerformanceData' => '\\AircraftCruisePerformanceData',
      'HoldingData' => '\\HoldingData',
      'CruisePerformanceDataSeries' => '\\CruisePerformanceDataSeries',
      'ArrayOfCruisePerformanceDataPoint' => '\\ArrayOfCruisePerformanceDataPoint',
      'ArrayOfClimbDescendDataPoint' => '\\ArrayOfClimbDescendDataPoint',
      'SimpleClimbProfile' => '\\SimpleClimbProfile',
      'Speed' => '\\Speed',
      'ByAltitudeClimbProfile' => '\\ByAltitudeClimbProfile',
      'ArrayOfByAltitudeFuelFlow' => '\\ArrayOfByAltitudeFuelFlow',
      'ByAltitudeFuelFlow' => '\\ByAltitudeFuelFlow',
      'ArrayOfByAltitudeClimbPerformance' => '\\ArrayOfByAltitudeClimbPerformance',
      'ByAltitudeClimbPerformance' => '\\ByAltitudeClimbPerformance',
      'BiasedCruisePerformanceProfile' => '\\BiasedCruisePerformanceProfile',
      'CruiseProfile' => '\\CruiseProfile',
      'DiscreteCruiseProfile' => '\\DiscreteCruiseProfile',
      'ArrayOfCruisePerformanceDataSeries' => '\\ArrayOfCruisePerformanceDataSeries',
      'ArrayOfWeightLimitation' => '\\ArrayOfWeightLimitation',
      'WeightLimitation' => '\\WeightLimitation',
      'ETOPSCruiseProfile' => '\\ETOPSCruiseProfile',
      'SimpleCruiseProfile' => '\\SimpleCruiseProfile',
      'Altitude' => '\\Altitude',
      'ByAltitudeCruiseProfile' => '\\ByAltitudeCruiseProfile',
      'ArrayOfByAltitudeCruisePerformance' => '\\ArrayOfByAltitudeCruisePerformance',
      'ByAltitudeCruisePerformance' => '\\ByAltitudeCruisePerformance',
      'BiasedDescentProfile' => '\\BiasedDescentProfile',
      'DescentProfile' => '\\DescentProfile',
      'DiscreteDescentProfile' => '\\DiscreteDescentProfile',
      'SimpleDescentProfile' => '\\SimpleDescentProfile',
      'ByAltitudeDescentProfile' => '\\ByAltitudeDescentProfile',
      'ArrayOfByAltitudeDescentPerformance' => '\\ArrayOfByAltitudeDescentPerformance',
      'ByAltitudeDescentPerformance' => '\\ByAltitudeDescentPerformance',
      'BiasedHoldingPerformanceProfile' => '\\BiasedHoldingPerformanceProfile',
      'HoldingProfile' => '\\HoldingProfile',
      'DiscreteHoldingProfile' => '\\DiscreteHoldingProfile',
      'ArrayOfHoldingDataSeries' => '\\ArrayOfHoldingDataSeries',
      'ReferencedPerformanceProfile' => '\\ReferencedPerformanceProfile',
      'AircraftStructure' => '\\AircraftStructure',
      'ArrayOfMassAndBalanceConfiguration' => '\\ArrayOfMassAndBalanceConfiguration',
      'AircraftEnvelope' => '\\AircraftEnvelope',
      'ArrayOfAircraftEnvelopePoint' => '\\ArrayOfAircraftEnvelopePoint',
      'AircraftEnvelopePoint' => '\\AircraftEnvelopePoint',
      'AircraftEngineSpecification' => '\\AircraftEngineSpecification',
      'AircraftType' => '\\AircraftType',
      'EngineDetails' => '\\EngineDetails',
      'ArrayOfAircraftCertification' => '\\ArrayOfAircraftCertification',
      'AircraftCertification' => '\\AircraftCertification',
      'ETOPSCertification' => '\\ETOPSCertification',
      'AircraftTypeConfigurationSpecification' => '\\AircraftTypeConfigurationSpecification',
      'SimplifiedAircraftSpecification' => '\\SimplifiedAircraftSpecification',
      'UUIDAircraftSpecification' => '\\UUIDAircraftSpecification',
      'ByAltitudeAircraftSpecification' => '\\ByAltitudeAircraftSpecification',
      'ArrayOfByAltitudeClimbProfile' => '\\ArrayOfByAltitudeClimbProfile',
      'ArrayOfByAltitudeCruiseProfile' => '\\ArrayOfByAltitudeCruiseProfile',
      'ArrayOfByAltitudeDescentProfile' => '\\ArrayOfByAltitudeDescentProfile',
      'ArrayOfAirspaceIdentifier' => '\\ArrayOfAirspaceIdentifier',
      'AirspaceIdentifier' => '\\AirspaceIdentifier',
      'CostFunctionSpecification' => '\\CostFunctionSpecification',
      'CostFunction' => '\\CostFunction',
      'DistanceCostFunction' => '\\DistanceCostFunction',
      'ESADCostFunction' => '\\ESADCostFunction',
      'CostMinimizingCostFunction' => '\\CostMinimizingCostFunction',
      'Currency' => '\\Currency',
      'TimeCostFunction' => '\\TimeCostFunction',
      'FuelCostFunction' => '\\FuelCostFunction',
      'ProcedureSpecification' => '\\ProcedureSpecification',
      'PerformanceProfileSpecification' => '\\PerformanceProfileSpecification',
      'SimpleftPerformanceProfileSpecification' => '\\SimpleftPerformanceProfileSpecification',
      'InterpolatedPerformanceProfileSpecification' => '\\InterpolatedPerformanceProfileSpecification',
      'DirectOptimizationSettings' => '\\DirectOptimizationSettings',
      'FlightLevelSpecification' => '\\FlightLevelSpecification',
      'MaximumFlightLevelSpecification' => '\\MaximumFlightLevelSpecification',
      'ExactFlightLevelSpecification' => '\\ExactFlightLevelSpecification',
      'OptimumFlightLevelDefinition' => '\\OptimumFlightLevelDefinition',
      'Airport' => '\\Airport',
      'ArrayOfAirportFrequencyARINC424' => '\\ArrayOfAirportFrequencyARINC424',
      'AirportFrequencyARINC424' => '\\AirportFrequencyARINC424',
      'FIR' => '\\FIR',
      'Airspace' => '\\Airspace',
      'NationalAirspace' => '\\NationalAirspace',
      'SpecialActivityAirspace' => '\\SpecialActivityAirspace',
      'TemporaryFlightRestrictedAirspace' => '\\TemporaryFlightRestrictedAirspace',
      'SphereicPoint' => '\\SphereicPoint',
      'PathPoint' => '\\PathPoint',
      'ArrayOfRunway' => '\\ArrayOfRunway',
      'Runway' => '\\Runway',
      'RouteNodeIdentifier' => '\\RouteNodeIdentifier',
      'FuelPolicy' => '\\FuelPolicy',
      'RelativeFuelPolicy' => '\\RelativeFuelPolicy',
      'ValueFuelPolicy' => '\\ValueFuelPolicy',
      'ActualFuelPolicy' => '\\ActualFuelPolicy',
      'ExtraFuelPolicy' => '\\ExtraFuelPolicy',
      'LandingFuelPolicy' => '\\LandingFuelPolicy',
      'TakeOffMassPolicy' => '\\TakeOffMassPolicy',
      'LandingMassPolicy' => '\\LandingMassPolicy',
      'MinimumRequiredFuelPolicy' => '\\MinimumRequiredFuelPolicy',
      'MaximumFuelPolicy' => '\\MaximumFuelPolicy',
      'ArrayOfRouteNodeIdentifier' => '\\ArrayOfRouteNodeIdentifier',
      'PathOptions' => '\\PathOptions',
      'ArrayOfArea' => '\\ArrayOfArea',
      'Area' => '\\Area',
      'ETOPSSpecification' => '\\ETOPSSpecification',
      'ArrayOfAirport' => '\\ArrayOfAirport',
      'ProcedureOptions' => '\\ProcedureOptions',
      'ArrayOfProcedureIdentifier' => '\\ArrayOfProcedureIdentifier',
      'ProcedureIdentifier' => '\\ProcedureIdentifier',
      'SIDIdentifier' => '\\SIDIdentifier',
      'STARIdentifier' => '\\STARIdentifier',
      'TransitionPointSpecification' => '\\TransitionPointSpecification',
      'TrackIdentifier' => '\\TrackIdentifier',
      'NATIdentifier' => '\\NATIdentifier',
      'PACIdentifier' => '\\PACIdentifier',
      'AUSIdentifier' => '\\AUSIdentifier',
      'AircraftTraficLoadDefinition' => '\\AircraftTraficLoadDefinition',
      'AbsoluteTraficLoadDefinition' => '\\AbsoluteTraficLoadDefinition',
      'RelativeTraficLoadDefinition' => '\\RelativeTraficLoadDefinition',
      'GenerateRoutesResponse' => '\\GenerateRoutesResponse',
      'ArrayOfRoute' => '\\ArrayOfRoute',
      'Route' => '\\Route',
      'WaypointData' => '\\WaypointData',
      'ArrayOfRouteNode' => '\\ArrayOfRouteNode',
      'RouteNode' => '\\RouteNode',
      'FlightData' => '\\FlightData',
      'ParserInformation' => '\\ParserInformation',
      'RouteProfile' => '\\RouteProfile',
      'RouteReport' => '\\RouteReport',
      'ArrayOfAirspace' => '\\ArrayOfAirspace',
      'ArrayOfInformationItem' => '\\ArrayOfInformationItem',
      'InformationItem' => '\\InformationItem',
      'ArrayOfInformationData' => '\\ArrayOfInformationData',
      'InformationData' => '\\InformationData',
      'ArrayOfRouteInfo' => '\\ArrayOfRouteInfo',
      'RouteInfo' => '\\RouteInfo',
      'ArrayOfWarning' => '\\ArrayOfWarning',
      'Warning' => '\\Warning',
      'AirportProcedure' => '\\AirportProcedure',
      'ArrayOfAirportEnrouteTransitionProcedure' => '\\ArrayOfAirportEnrouteTransitionProcedure',
      'AirportEnrouteTransitionProcedure' => '\\AirportEnrouteTransitionProcedure',
      'ArrayOfWaypointData' => '\\ArrayOfWaypointData',
      'AirportProcedureIdentifier' => '\\AirportProcedureIdentifier',
      'Account' => '\\Account',
      'ArrayOfFlightValidationError' => '\\ArrayOfFlightValidationError',
      'FlightValidationError' => '\\FlightValidationError',
      'GetCFMURoutes' => '\\GetCFMURoutes',
      'GetCFMURoutesResponse' => '\\GetCFMURoutesResponse',
      'RetrieveStoredRoutes' => '\\RetrieveStoredRoutes',
      'StoredRouteRequest' => '\\StoredRouteRequest',
      'ATCClearedRouteOptions' => '\\ATCClearedRouteOptions',
      'RetrieveStoredRoutesResponse' => '\\RetrieveStoredRoutesResponse',
      'StoredRouteResponse' => '\\StoredRouteResponse',
      'ArrayOfRouteInformation' => '\\ArrayOfRouteInformation',
      'RouteInformation' => '\\RouteInformation',
      'EstimatedFlighRoutetInformation' => '\\EstimatedFlighRoutetInformation',
      'CalculationInformation' => '\\CalculationInformation',
      'FlightMetrologicalData' => '\\FlightMetrologicalData',
      'IcingPoint' => '\\IcingPoint',
      'TurbulencePoint' => '\\TurbulencePoint',
      'FlightRAIMData' => '\\FlightRAIMData',
      'ArrayOfSphereicPoint' => '\\ArrayOfSphereicPoint',
      'ArrayOfOverflightCosts' => '\\ArrayOfOverflightCosts',
      'OverflightCosts' => '\\OverflightCosts',
      'ArrayOfDataPoint' => '\\ArrayOfDataPoint',
      'DataPoint' => '\\DataPoint',
      'AirwayData' => '\\AirwayData',
      'FuelData' => '\\FuelData',
      'MeteorologicalData' => '\\MeteorologicalData',
      'ArrayOfWeatherDataRecord' => '\\ArrayOfWeatherDataRecord',
      'WeatherDataRecord' => '\\WeatherDataRecord',
      'ArrayOfIcingPoint' => '\\ArrayOfIcingPoint',
      'ArrayOfTurbulencePoint' => '\\ArrayOfTurbulencePoint',
      'RouteIntersectionInformation' => '\\RouteIntersectionInformation',
      'ArrayOfIntersectionPoint' => '\\ArrayOfIntersectionPoint',
      'IntersectionPoint' => '\\IntersectionPoint',
      'StartEndPoint' => '\\StartEndPoint',
      'BorderIntersectionPoint' => '\\BorderIntersectionPoint',
      'InputPoint' => '\\InputPoint',
      'RouteSourceInformation' => '\\RouteSourceInformation',
      'FAAPreferedRoute' => '\\FAAPreferedRoute',
      'RecentlyFiledRoute' => '\\RecentlyFiledRoute',
      'RecentlyFiledRouteExtended' => '\\RecentlyFiledRouteExtended',
      'ArrayOfFiledAircraftTypeCount' => '\\ArrayOfFiledAircraftTypeCount',
      'FiledAircraftTypeCount' => '\\FiledAircraftTypeCount',
      'RouteValidationInformation' => '\\RouteValidationInformation',
      'CFMUValidationResult' => '\\CFMUValidationResult',
      'ArrayOfCFMUValidationError' => '\\ArrayOfCFMUValidationError',
      'CFMUValidationError' => '\\CFMUValidationError',
      'ArrayOfCFMUValidationWarning' => '\\ArrayOfCFMUValidationWarning',
      'CFMUValidationWarning' => '\\CFMUValidationWarning',
      'NAVValidationResult' => '\\NAVValidationResult',
      'ArrayOfNAVValidationError' => '\\ArrayOfNAVValidationError',
      'NAVValidationError' => '\\NAVValidationError',
      'BulkRetrieveStoredRoutes' => '\\BulkRetrieveStoredRoutes',
      'BulkStoredRouteRequest' => '\\BulkStoredRouteRequest',
      'BulkRetrieveStoredRoutesResponse' => '\\BulkRetrieveStoredRoutesResponse',
      'BulkStoredRouteResponse' => '\\BulkStoredRouteResponse',
      'ArrayOfBulkRouteInformation' => '\\ArrayOfBulkRouteInformation',
      'BulkRouteInformation' => '\\BulkRouteInformation',
      'CompactRoute' => '\\CompactRoute',
      'GetRADFlightLevelLimit' => '\\GetRADFlightLevelLimit',
      'RADFlightLevelLimitRequest' => '\\RADFlightLevelLimitRequest',
      'GetRADFlightLevelLimitResponse' => '\\GetRADFlightLevelLimitResponse',
      'RADCityPairLevelCapping' => '\\RADCityPairLevelCapping',
      'CalculateRouteMetrics' => '\\CalculateRouteMetrics',
      'RouteMetricsRequest' => '\\RouteMetricsRequest',
      'RouteEstimationMetricsSpecification' => '\\RouteEstimationMetricsSpecification',
      'FlightSpecification' => '\\FlightSpecification',
      'ATCData' => '\\ATCData',
      'ArrayOfATCOtherItemField' => '\\ArrayOfATCOtherItemField',
      'ATCOtherItemField' => '\\ATCOtherItemField',
      'ATCSupplementaryInfo' => '\\ATCSupplementaryInfo',
      'FlightCalculationOptions' => '\\FlightCalculationOptions',
      'ContingencyStrategy' => '\\ContingencyStrategy',
      'DefaultContingenyStrategy' => '\\DefaultContingenyStrategy',
      'ERAContingencyStrategy' => '\\ERAContingencyStrategy',
      'ReducedContingencyFuelStrategy' => '\\ReducedContingencyFuelStrategy',
      'FlightLevelOptimizationOptions' => '\\FlightLevelOptimizationOptions',
      'ArrayOfAltitude' => '\\ArrayOfAltitude',
      'OptimumFlightlevelOptionSpecification' => '\\OptimumFlightlevelOptionSpecification',
      'GainLossCalculationInfo' => '\\GainLossCalculationInfo',
      'InlineOptionSpecification' => '\\InlineOptionSpecification',
      'RAIMPredictionOptions' => '\\RAIMPredictionOptions',
      'FlightCalculationSIDSTAROptions' => '\\FlightCalculationSIDSTAROptions',
      'TaxiFuelSpecification' => '\\TaxiFuelSpecification',
      'MassBasedTaxiFuelSpecification' => '\\MassBasedTaxiFuelSpecification',
      'TimeBasedTaxiFuelSpecification' => '\\TimeBasedTaxiFuelSpecification',
      'TaxiTimeSpecification' => '\\TaxiTimeSpecification',
      'ManuelTaxiTime' => '\\ManuelTaxiTime',
      'DefaultTaxiTime' => '\\DefaultTaxiTime',
      'CargoLoadSpecification' => '\\CargoLoadSpecification',
      'SimpleCargoLoadSpecification' => '\\SimpleCargoLoadSpecification',
      'SpecificCargoLoadSpecification' => '\\SpecificCargoLoadSpecification',
      'ArrayOfCargoSectionLoad' => '\\ArrayOfCargoSectionLoad',
      'CargoSectionLoad' => '\\CargoSectionLoad',
      'RelativeCargoLoadSpecification' => '\\RelativeCargoLoadSpecification',
      'MaxCargoLoadSpecification' => '\\MaxCargoLoadSpecification',
      'CrewLoadSpecification' => '\\CrewLoadSpecification',
      'SimpleCrewSpecification' => '\\SimpleCrewSpecification',
      'SpecificCrewLoadSpecification' => '\\SpecificCrewLoadSpecification',
      'ETPSettings' => '\\ETPSettings',
      'ArrayOfETOPSSettingSpecification' => '\\ArrayOfETOPSSettingSpecification',
      'ETOPSSettingSpecification' => '\\ETOPSSettingSpecification',
      'FlightFuelRules' => '\\FlightFuelRules',
      'FAAOpsRules' => '\\FAAOpsRules',
      'EUOpsRules' => '\\EUOpsRules',
      'CustomFuelRule' => '\\CustomFuelRule',
      'PaxLoadSpecification' => '\\PaxLoadSpecification',
      'SimplePaxLoadSpecification' => '\\SimplePaxLoadSpecification',
      'PaxLoad' => '\\PaxLoad',
      'UnspecifiedPaxTypeLoad' => '\\UnspecifiedPaxTypeLoad',
      'SpecifiedPaxTypeLoad' => '\\SpecifiedPaxTypeLoad',
      'SectionBasedPaxLoadSpecification' => '\\SectionBasedPaxLoadSpecification',
      'ArrayOfPaxSectionLoad' => '\\ArrayOfPaxSectionLoad',
      'PaxSectionLoad' => '\\PaxSectionLoad',
      'FlightLevelAdjustmentSpecification' => '\\FlightLevelAdjustmentSpecification',
      'ArrayOfAltitudeNode' => '\\ArrayOfAltitudeNode',
      'AltitudeNode' => '\\AltitudeNode',
      'ArrayOfCruiseChangeNode' => '\\ArrayOfCruiseChangeNode',
      'CruiseChangeNode' => '\\CruiseChangeNode',
      'Winds' => '\\Winds',
      'ForecastWinds' => '\\ForecastWinds',
      'DenseWindModel' => '\\DenseWindModel',
      'HistoricalWinds' => '\\HistoricalWinds',
      'FixedWinds' => '\\FixedWinds',
      'RouteIntersectionMetricsSpecification' => '\\RouteIntersectionMetricsSpecification',
      'RouteValidationMetricsSpecification' => '\\RouteValidationMetricsSpecification',
      'CalculateRouteMetricsResponse' => '\\CalculateRouteMetricsResponse',
      'RouteMetricsResponse' => '\\RouteMetricsResponse',
      'ResolveRoute' => '\\ResolveRoute',
      'ResolveRouteRequest' => '\\ResolveRouteRequest',
      'RouteResolveParameters' => '\\RouteResolveParameters',
      'ResolveRouteResponse' => '\\ResolveRouteResponse',
      'RouteResolveResponse' => '\\RouteResolveResponse',
      'VerticalOptimizeRoute' => '\\VerticalOptimizeRoute',
      'VerticalOptimizationRequest' => '\\VerticalOptimizationRequest',
      'VerticalOptimizeRouteResponse' => '\\VerticalOptimizeRouteResponse',
      'VerticalOptimizationResult' => '\\VerticalOptimizationResult',
      'ChallengeRoute' => '\\ChallengeRoute',
      'RouteChallengeRequest' => '\\RouteChallengeRequest',
      'ChallengeRouteResponse' => '\\ChallengeRouteResponse',
      'RouteChallengeResponse' => '\\RouteChallengeResponse',
      'TypeReferencedMassAndBalanceConfiguration' => '\\TypeReferencedMassAndBalanceConfiguration',
      'ArrayOfAltitudeChangeInformation' => '\\ArrayOfAltitudeChangeInformation',
      'AltitudeChangeInformation' => '\\AltitudeChangeInformation',
      'ArrayOfstring' => '\\ArrayOfstring',
      'UnidentifiableAirportFault' => '\\UnidentifiableAirportFault',
      'UnidentifiableAircraftFault' => '\\UnidentifiableAircraftFault',
      'InvalidFlightSpecificationFault' => '\\InvalidFlightSpecificationFault',
      'ImpossibleRouteFault' => '\\ImpossibleRouteFault',
      'InvalidRouteSpecificationFault' => '\\InvalidRouteSpecificationFault',
      'AircraftGenericDataDeviationFault' => '\\AircraftGenericDataDeviationFault',
      'AircraftMaxWeightFault' => '\\AircraftMaxWeightFault',
      'ValidationFault' => '\\ValidationFault',
      'ArrayOfValidationDetail' => '\\ArrayOfValidationDetail',
      'ValidationDetail' => '\\ValidationDetail',
      'ArrayOfAircraftGenericDataMaxDeviatioNExceedError' => '\\ArrayOfAircraftGenericDataMaxDeviatioNExceedError',
      'AircraftGenericDataMaxDeviatioNExceedError' => '\\AircraftGenericDataMaxDeviatioNExceedError',
      'ArrayOfAircraftMaxWeightExceededError' => '\\ArrayOfAircraftMaxWeightExceededError',
      'AircraftMaxWeightExceededError' => '\\AircraftMaxWeightExceededError',
      'FuelCheckOptionsSpecification' => '\\FuelCheckOptionsSpecification',
      'OutputUnitSpecification' => '\\OutputUnitSpecification',
    );

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      $options = array_merge(array (
      'features' => 1,
    ), $options);
      if (!$wsdl) {
        $wsdl = 'https://testservices.aviationcloud.aero/RouteGenerationService.svc?singleWsdl';
      }
      parent::__construct($wsdl, $options);
    }

    /**
     *
     * Generate routes using AFS routing algorithms. The route generation options can be controlled using the route specification parameter.
     * Please notice that the cost of invoking this operation varies depending on the parameters set
     *
     * <param name="routeRequest">The route request object containing the route specification. Account ID must be provided
     * A list of routes, where the number of routes matches the amount specified
     *
     * @param GenerateRoutes $parameters
     * @return GenerateRoutesResponse
     */
    public function GenerateRoutes(GenerateRoutes $parameters)
    {
      return $this->__soapCall('GenerateRoutes', array($parameters));
    }

    /**
     *
     * Get routes provided by CFMU. CFMU routes can only be generated for departure and destination airports within the EUROCONTROL region
     *
     * <param name="routeRequest">The route request object containing the route specification. Account ID must be provided
     * A list of routes, where the number of routes matches the amount specified
     *
     * @param GetCFMURoutes $parameters
     * @return GetCFMURoutesResponse
     */
    public function GetCFMURoutes(GetCFMURoutes $parameters)
    {
      return $this->__soapCall('GetCFMURoutes', array($parameters));
    }

    /**
     *
     * Retrieve stored (static) or semi-static routes from various sources such as Recently Filed, CFMU, FAA etc. See the stored route request for additional information
     *
     * @param RetrieveStoredRoutes $parameters
     * @return RetrieveStoredRoutesResponse
     */
    public function RetrieveStoredRoutes(RetrieveStoredRoutes $parameters)
    {
      return $this->__soapCall('RetrieveStoredRoutes', array($parameters));
    }

    /**
     *
     * Retrieve stored (static) or semi-static routes from various sources such as Recently Filed, CFMU, FAA etc. See the stored route request for additional information
     *
     * @param BulkRetrieveStoredRoutes $parameters
     * @return BulkRetrieveStoredRoutesResponse
     */
    public function BulkRetrieveStoredRoutes(BulkRetrieveStoredRoutes $parameters)
    {
      return $this->__soapCall('BulkRetrieveStoredRoutes', array($parameters));
    }

    /**
     *
     * Get the RAD flight level limit between a city pair (also known as city-pair level capping, RAD appendix 3)
     *
     * A request specifying for which aerodrome to retrieve RAD FL limits
     * The maximum/minimum flight level between the two city pairs, or 0/999 if non-restricted
     *
     * @param GetRADFlightLevelLimit $parameters
     * @return GetRADFlightLevelLimitResponse
     */
    public function GetRADFlightLevelLimit(GetRADFlightLevelLimit $parameters)
    {
      return $this->__soapCall('GetRADFlightLevelLimit', array($parameters));
    }

    /**
     *
     * Calculate route metrics.  Route metrics is an abstract term used to describe any calculations that can be done on the basis of a route. For some of these calculations a flight specification must be provided, while for other the route in itself is enough.
     *
     * @param CalculateRouteMetrics $parameters
     * @return CalculateRouteMetricsResponse
     */
    public function CalculateRouteMetrics(CalculateRouteMetrics $parameters)
    {
      return $this->__soapCall('CalculateRouteMetrics', array($parameters));
    }

    /**
     *
     * ResolveRoute. ResolveRoute is a service which resolves routes, and includes parsed tokens.
     *
     * @param ResolveRoute $parameters
     * @return ResolveRouteResponse
     */
    public function ResolveRoute(ResolveRoute $parameters)
    {
      return $this->__soapCall('ResolveRoute', array($parameters));
    }

    /**
     *
     * Perform a vertical optimization of the given route
     *
     * @param VerticalOptimizeRoute $parameters
     * @return VerticalOptimizeRouteResponse
     */
    public function VerticalOptimizeRoute(VerticalOptimizeRoute $parameters)
    {
      return $this->__soapCall('VerticalOptimizeRoute', array($parameters));
    }

    /**
     * @param ChallengeRoute $parameters
     * @return ChallengeRouteResponse
     */
    public function ChallengeRoute(ChallengeRoute $parameters)
    {
      return $this->__soapCall('ChallengeRoute', array($parameters));
    }

}
