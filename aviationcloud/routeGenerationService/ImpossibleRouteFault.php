<?php

class ImpossibleRouteFault
{

    /**
     * @var int $ErrorCode
     */
    protected $ErrorCode = null;

    /**
     * @var RouteSpecification $OriginalSpecification
     */
    protected $OriginalSpecification = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getErrorCode()
    {
      return $this->ErrorCode;
    }

    /**
     * @param int $ErrorCode
     * @return ImpossibleRouteFault
     */
    public function setErrorCode($ErrorCode)
    {
      $this->ErrorCode = $ErrorCode;
      return $this;
    }

    /**
     * @return RouteSpecification
     */
    public function getOriginalSpecification()
    {
      return $this->OriginalSpecification;
    }

    /**
     * @param RouteSpecification $OriginalSpecification
     * @return ImpossibleRouteFault
     */
    public function setOriginalSpecification($OriginalSpecification)
    {
      $this->OriginalSpecification = $OriginalSpecification;
      return $this;
    }

}
