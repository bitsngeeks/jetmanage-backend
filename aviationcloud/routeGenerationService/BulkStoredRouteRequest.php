<?php

class BulkStoredRouteRequest extends RequestBase
{

    /**
     * @var boolean $ExcludeFAARoutes
     */
    protected $ExcludeFAARoutes = null;

    /**
     * @var boolean $ExcludeStandardRoutes
     */
    protected $ExcludeStandardRoutes = null;

    /**
     * @var int $Skip
     */
    protected $Skip = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return boolean
     */
    public function getExcludeFAARoutes()
    {
      return $this->ExcludeFAARoutes;
    }

    /**
     * @param boolean $ExcludeFAARoutes
     * @return BulkStoredRouteRequest
     */
    public function setExcludeFAARoutes($ExcludeFAARoutes)
    {
      $this->ExcludeFAARoutes = $ExcludeFAARoutes;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getExcludeStandardRoutes()
    {
      return $this->ExcludeStandardRoutes;
    }

    /**
     * @param boolean $ExcludeStandardRoutes
     * @return BulkStoredRouteRequest
     */
    public function setExcludeStandardRoutes($ExcludeStandardRoutes)
    {
      $this->ExcludeStandardRoutes = $ExcludeStandardRoutes;
      return $this;
    }

    /**
     * @return int
     */
    public function getSkip()
    {
      return $this->Skip;
    }

    /**
     * @param int $Skip
     * @return BulkStoredRouteRequest
     */
    public function setSkip($Skip)
    {
      $this->Skip = $Skip;
      return $this;
    }

}
