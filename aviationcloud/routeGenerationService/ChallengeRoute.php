<?php

class ChallengeRoute
{

    /**
     * @var RouteChallengeRequest $request
     */
    protected $request = null;

    /**
     * @param RouteChallengeRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return RouteChallengeRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param RouteChallengeRequest $request
     * @return ChallengeRoute
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
