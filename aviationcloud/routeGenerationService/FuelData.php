<?php

class FuelData
{

    /**
     * @var Weight $AccumulatedFuel
     */
    protected $AccumulatedFuel = null;

    /**
     * @var Weight $FuelFlow
     */
    protected $FuelFlow = null;

    /**
     * @var Weight $MinimumRequiredFuel
     */
    protected $MinimumRequiredFuel = null;

    /**
     * @var Weight $MinimumRequiredFuelIncludingContingency
     */
    protected $MinimumRequiredFuelIncludingContingency = null;

    /**
     * @var Weight $RemainingFuel
     */
    protected $RemainingFuel = null;

    /**
     * @var Weight $SegmentFuel
     */
    protected $SegmentFuel = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Weight
     */
    public function getAccumulatedFuel()
    {
      return $this->AccumulatedFuel;
    }

    /**
     * @param Weight $AccumulatedFuel
     * @return FuelData
     */
    public function setAccumulatedFuel($AccumulatedFuel)
    {
      $this->AccumulatedFuel = $AccumulatedFuel;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getFuelFlow()
    {
      return $this->FuelFlow;
    }

    /**
     * @param Weight $FuelFlow
     * @return FuelData
     */
    public function setFuelFlow($FuelFlow)
    {
      $this->FuelFlow = $FuelFlow;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getMinimumRequiredFuel()
    {
      return $this->MinimumRequiredFuel;
    }

    /**
     * @param Weight $MinimumRequiredFuel
     * @return FuelData
     */
    public function setMinimumRequiredFuel($MinimumRequiredFuel)
    {
      $this->MinimumRequiredFuel = $MinimumRequiredFuel;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getMinimumRequiredFuelIncludingContingency()
    {
      return $this->MinimumRequiredFuelIncludingContingency;
    }

    /**
     * @param Weight $MinimumRequiredFuelIncludingContingency
     * @return FuelData
     */
    public function setMinimumRequiredFuelIncludingContingency($MinimumRequiredFuelIncludingContingency)
    {
      $this->MinimumRequiredFuelIncludingContingency = $MinimumRequiredFuelIncludingContingency;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getRemainingFuel()
    {
      return $this->RemainingFuel;
    }

    /**
     * @param Weight $RemainingFuel
     * @return FuelData
     */
    public function setRemainingFuel($RemainingFuel)
    {
      $this->RemainingFuel = $RemainingFuel;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getSegmentFuel()
    {
      return $this->SegmentFuel;
    }

    /**
     * @param Weight $SegmentFuel
     * @return FuelData
     */
    public function setSegmentFuel($SegmentFuel)
    {
      $this->SegmentFuel = $SegmentFuel;
      return $this;
    }

}
