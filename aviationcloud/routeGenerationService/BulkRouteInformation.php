<?php

class BulkRouteInformation
{

    /**
     * @var CompactRoute $Route
     */
    protected $Route = null;

    /**
     * @var RouteSourceInformation $Source
     */
    protected $Source = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return CompactRoute
     */
    public function getRoute()
    {
      return $this->Route;
    }

    /**
     * @param CompactRoute $Route
     * @return BulkRouteInformation
     */
    public function setRoute($Route)
    {
      $this->Route = $Route;
      return $this;
    }

    /**
     * @return RouteSourceInformation
     */
    public function getSource()
    {
      return $this->Source;
    }

    /**
     * @param RouteSourceInformation $Source
     * @return BulkRouteInformation
     */
    public function setSource($Source)
    {
      $this->Source = $Source;
      return $this;
    }

}
