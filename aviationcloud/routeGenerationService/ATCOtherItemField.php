<?php

class ATCOtherItemField
{

    /**
     * @var string $Key
     */
    protected $Key = null;

    /**
     * @var string $Text
     */
    protected $Text = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getKey()
    {
      return $this->Key;
    }

    /**
     * @param string $Key
     * @return ATCOtherItemField
     */
    public function setKey($Key)
    {
      $this->Key = $Key;
      return $this;
    }

    /**
     * @return string
     */
    public function getText()
    {
      return $this->Text;
    }

    /**
     * @param string $Text
     * @return ATCOtherItemField
     */
    public function setText($Text)
    {
      $this->Text = $Text;
      return $this;
    }

}
