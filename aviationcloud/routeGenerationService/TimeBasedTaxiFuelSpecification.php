<?php

class TimeBasedTaxiFuelSpecification extends TaxiFuelSpecification
{

    /**
     * @var Time $TaxiTime
     */
    protected $TaxiTime = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Time
     */
    public function getTaxiTime()
    {
      return $this->TaxiTime;
    }

    /**
     * @param Time $TaxiTime
     * @return TimeBasedTaxiFuelSpecification
     */
    public function setTaxiTime($TaxiTime)
    {
      $this->TaxiTime = $TaxiTime;
      return $this;
    }

}
