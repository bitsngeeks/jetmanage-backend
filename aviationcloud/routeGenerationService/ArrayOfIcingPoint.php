<?php

class ArrayOfIcingPoint implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var IcingPoint[] $IcingPoint
     */
    protected $IcingPoint = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return IcingPoint[]
     */
    public function getIcingPoint()
    {
      return $this->IcingPoint;
    }

    /**
     * @param IcingPoint[] $IcingPoint
     * @return ArrayOfIcingPoint
     */
    public function setIcingPoint(array $IcingPoint = null)
    {
      $this->IcingPoint = $IcingPoint;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->IcingPoint[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return IcingPoint
     */
    public function offsetGet($offset)
    {
      return $this->IcingPoint[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param IcingPoint $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->IcingPoint[] = $value;
      } else {
        $this->IcingPoint[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->IcingPoint[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return IcingPoint Return the current element
     */
    public function current()
    {
      return current($this->IcingPoint);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->IcingPoint);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->IcingPoint);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->IcingPoint);
    }

    /**
     * Countable implementation
     *
     * @return IcingPoint Return count of elements
     */
    public function count()
    {
      return count($this->IcingPoint);
    }

}
