<?php

class FIR extends Airspace
{

    /**
     * @var string $Address
     */
    protected $Address = null;

    /**
     * @var string $FullName
     */
    protected $FullName = null;

    /**
     * @var string $ICAOCode
     */
    protected $ICAOCode = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return string
     */
    public function getAddress()
    {
      return $this->Address;
    }

    /**
     * @param string $Address
     * @return FIR
     */
    public function setAddress($Address)
    {
      $this->Address = $Address;
      return $this;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
      return $this->FullName;
    }

    /**
     * @param string $FullName
     * @return FIR
     */
    public function setFullName($FullName)
    {
      $this->FullName = $FullName;
      return $this;
    }

    /**
     * @return string
     */
    public function getICAOCode()
    {
      return $this->ICAOCode;
    }

    /**
     * @param string $ICAOCode
     * @return FIR
     */
    public function setICAOCode($ICAOCode)
    {
      $this->ICAOCode = $ICAOCode;
      return $this;
    }

}
