<?php

class RequestBase
{

    /**
     * @var int $AccountId
     */
    protected $AccountId = null;

    /**
     * @var string $InvoiceFlightId
     */
    protected $InvoiceFlightId = null;

    /**
     * @var string $Password
     */
    protected $Password = null;

    /**
     * @var string $UserName
     */
    protected $UserName = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getAccountId()
    {
      return $this->AccountId;
    }

    /**
     * @param int $AccountId
     * @return RequestBase
     */
    public function setAccountId($AccountId)
    {
      $this->AccountId = $AccountId;
      return $this;
    }

    /**
     * @return string
     */
    public function getInvoiceFlightId()
    {
      return $this->InvoiceFlightId;
    }

    /**
     * @param string $InvoiceFlightId
     * @return RequestBase
     */
    public function setInvoiceFlightId($InvoiceFlightId)
    {
      $this->InvoiceFlightId = $InvoiceFlightId;
      return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
      return $this->Password;
    }

    /**
     * @param string $Password
     * @return RequestBase
     */
    public function setPassword($Password)
    {
      $this->Password = $Password;
      return $this;
    }

    /**
     * @return string
     */
    public function getUserName()
    {
      return $this->UserName;
    }

    /**
     * @param string $UserName
     * @return RequestBase
     */
    public function setUserName($UserName)
    {
      $this->UserName = $UserName;
      return $this;
    }

}
