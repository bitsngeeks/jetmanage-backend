<?php

class RetrieveStoredRoutesResponse
{

    /**
     * @var StoredRouteResponse $RetrieveStoredRoutesResult
     */
    protected $RetrieveStoredRoutesResult = null;

    /**
     * @param StoredRouteResponse $RetrieveStoredRoutesResult
     */
    public function __construct($RetrieveStoredRoutesResult)
    {
      $this->RetrieveStoredRoutesResult = $RetrieveStoredRoutesResult;
    }

    /**
     * @return StoredRouteResponse
     */
    public function getRetrieveStoredRoutesResult()
    {
      return $this->RetrieveStoredRoutesResult;
    }

    /**
     * @param StoredRouteResponse $RetrieveStoredRoutesResult
     * @return RetrieveStoredRoutesResponse
     */
    public function setRetrieveStoredRoutesResult($RetrieveStoredRoutesResult)
    {
      $this->RetrieveStoredRoutesResult = $RetrieveStoredRoutesResult;
      return $this;
    }

}
