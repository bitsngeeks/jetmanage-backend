<?php

class PaxLoadSpecification
{

    /**
     * @var boolean $EnableAutoLuggage
     */
    protected $EnableAutoLuggage = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return boolean
     */
    public function getEnableAutoLuggage()
    {
      return $this->EnableAutoLuggage;
    }

    /**
     * @param boolean $EnableAutoLuggage
     * @return PaxLoadSpecification
     */
    public function setEnableAutoLuggage($EnableAutoLuggage)
    {
      $this->EnableAutoLuggage = $EnableAutoLuggage;
      return $this;
    }

}
