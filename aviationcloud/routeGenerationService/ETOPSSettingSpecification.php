<?php

class ETOPSSettingSpecification
{

    /**
     * @var Length $Distance
     */
    protected $Distance = null;

    /**
     * @var Time $Time
     */
    protected $Time = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Length
     */
    public function getDistance()
    {
      return $this->Distance;
    }

    /**
     * @param Length $Distance
     * @return ETOPSSettingSpecification
     */
    public function setDistance($Distance)
    {
      $this->Distance = $Distance;
      return $this;
    }

    /**
     * @return Time
     */
    public function getTime()
    {
      return $this->Time;
    }

    /**
     * @param Time $Time
     * @return ETOPSSettingSpecification
     */
    public function setTime($Time)
    {
      $this->Time = $Time;
      return $this;
    }

}
