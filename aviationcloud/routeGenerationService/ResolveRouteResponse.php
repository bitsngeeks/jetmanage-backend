<?php

class ResolveRouteResponse
{

    /**
     * @var RouteResolveResponse $ResolveRouteResult
     */
    protected $ResolveRouteResult = null;

    /**
     * @param RouteResolveResponse $ResolveRouteResult
     */
    public function __construct($ResolveRouteResult)
    {
      $this->ResolveRouteResult = $ResolveRouteResult;
    }

    /**
     * @return RouteResolveResponse
     */
    public function getResolveRouteResult()
    {
      return $this->ResolveRouteResult;
    }

    /**
     * @param RouteResolveResponse $ResolveRouteResult
     * @return ResolveRouteResponse
     */
    public function setResolveRouteResult($ResolveRouteResult)
    {
      $this->ResolveRouteResult = $ResolveRouteResult;
      return $this;
    }

}
