<?php

class RouteChallengeRequest extends RequestBase
{

    /**
     * @var FlightSpecification $CurrentFlight
     */
    protected $CurrentFlight = null;

    /**
     * @var boolean $IgnoreVerticalProfileOnOriginalFlight
     */
    protected $IgnoreVerticalProfileOnOriginalFlight = null;

    /**
     * @var float $ImprovementValue
     */
    protected $ImprovementValue = null;

    /**
     * @var boolean $IncludeFullDataSet
     */
    protected $IncludeFullDataSet = null;

    /**
     * @var RouteSpecification $RouteSpecification
     */
    protected $RouteSpecification = null;

    /**
     * @var boolean $UseAbsoluteImprovement
     */
    protected $UseAbsoluteImprovement = null;

    /**
     * @var boolean $UseRelativeImprovement
     */
    protected $UseRelativeImprovement = null;

    /**
     * @var boolean $UseV2Engine
     */
    protected $UseV2Engine = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return FlightSpecification
     */
    public function getCurrentFlight()
    {
      return $this->CurrentFlight;
    }

    /**
     * @param FlightSpecification $CurrentFlight
     * @return RouteChallengeRequest
     */
    public function setCurrentFlight($CurrentFlight)
    {
      $this->CurrentFlight = $CurrentFlight;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIgnoreVerticalProfileOnOriginalFlight()
    {
      return $this->IgnoreVerticalProfileOnOriginalFlight;
    }

    /**
     * @param boolean $IgnoreVerticalProfileOnOriginalFlight
     * @return RouteChallengeRequest
     */
    public function setIgnoreVerticalProfileOnOriginalFlight($IgnoreVerticalProfileOnOriginalFlight)
    {
      $this->IgnoreVerticalProfileOnOriginalFlight = $IgnoreVerticalProfileOnOriginalFlight;
      return $this;
    }

    /**
     * @return float
     */
    public function getImprovementValue()
    {
      return $this->ImprovementValue;
    }

    /**
     * @param float $ImprovementValue
     * @return RouteChallengeRequest
     */
    public function setImprovementValue($ImprovementValue)
    {
      $this->ImprovementValue = $ImprovementValue;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIncludeFullDataSet()
    {
      return $this->IncludeFullDataSet;
    }

    /**
     * @param boolean $IncludeFullDataSet
     * @return RouteChallengeRequest
     */
    public function setIncludeFullDataSet($IncludeFullDataSet)
    {
      $this->IncludeFullDataSet = $IncludeFullDataSet;
      return $this;
    }

    /**
     * @return RouteSpecification
     */
    public function getRouteSpecification()
    {
      return $this->RouteSpecification;
    }

    /**
     * @param RouteSpecification $RouteSpecification
     * @return RouteChallengeRequest
     */
    public function setRouteSpecification($RouteSpecification)
    {
      $this->RouteSpecification = $RouteSpecification;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUseAbsoluteImprovement()
    {
      return $this->UseAbsoluteImprovement;
    }

    /**
     * @param boolean $UseAbsoluteImprovement
     * @return RouteChallengeRequest
     */
    public function setUseAbsoluteImprovement($UseAbsoluteImprovement)
    {
      $this->UseAbsoluteImprovement = $UseAbsoluteImprovement;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUseRelativeImprovement()
    {
      return $this->UseRelativeImprovement;
    }

    /**
     * @param boolean $UseRelativeImprovement
     * @return RouteChallengeRequest
     */
    public function setUseRelativeImprovement($UseRelativeImprovement)
    {
      $this->UseRelativeImprovement = $UseRelativeImprovement;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUseV2Engine()
    {
      return $this->UseV2Engine;
    }

    /**
     * @param boolean $UseV2Engine
     * @return RouteChallengeRequest
     */
    public function setUseV2Engine($UseV2Engine)
    {
      $this->UseV2Engine = $UseV2Engine;
      return $this;
    }

}
