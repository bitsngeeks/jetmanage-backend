<?php

class ERAContingencyStrategy extends ContingencyStrategy
{

    /**
     * @var Airport $ERAAirport
     */
    protected $ERAAirport = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return Airport
     */
    public function getERAAirport()
    {
      return $this->ERAAirport;
    }

    /**
     * @param Airport $ERAAirport
     * @return ERAContingencyStrategy
     */
    public function setERAAirport($ERAAirport)
    {
      $this->ERAAirport = $ERAAirport;
      return $this;
    }

}
