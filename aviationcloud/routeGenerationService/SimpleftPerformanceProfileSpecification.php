<?php

class SimpleftPerformanceProfileSpecification extends PerformanceProfileSpecification
{

    /**
     * @var int $Id
     */
    protected $Id = null;

    /**
     * @var string $UUID
     */
    protected $UUID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getId()
    {
      return $this->Id;
    }

    /**
     * @param int $Id
     * @return SimpleftPerformanceProfileSpecification
     */
    public function setId($Id)
    {
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return string
     */
    public function getUUID()
    {
      return $this->UUID;
    }

    /**
     * @param string $UUID
     * @return SimpleftPerformanceProfileSpecification
     */
    public function setUUID($UUID)
    {
      $this->UUID = $UUID;
      return $this;
    }

}
