<?php

class ArrayOfCrewCompartment implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var CrewCompartment[] $CrewCompartment
     */
    protected $CrewCompartment = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return CrewCompartment[]
     */
    public function getCrewCompartment()
    {
      return $this->CrewCompartment;
    }

    /**
     * @param CrewCompartment[] $CrewCompartment
     * @return ArrayOfCrewCompartment
     */
    public function setCrewCompartment(array $CrewCompartment = null)
    {
      $this->CrewCompartment = $CrewCompartment;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->CrewCompartment[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return CrewCompartment
     */
    public function offsetGet($offset)
    {
      return $this->CrewCompartment[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param CrewCompartment $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->CrewCompartment[] = $value;
      } else {
        $this->CrewCompartment[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->CrewCompartment[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return CrewCompartment Return the current element
     */
    public function current()
    {
      return current($this->CrewCompartment);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->CrewCompartment);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->CrewCompartment);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->CrewCompartment);
    }

    /**
     * Countable implementation
     *
     * @return CrewCompartment Return count of elements
     */
    public function count()
    {
      return count($this->CrewCompartment);
    }

}
