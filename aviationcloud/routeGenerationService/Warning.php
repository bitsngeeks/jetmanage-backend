<?php

class Warning
{

    /**
     * @var string $ConstraintDescription
     */
    protected $ConstraintDescription = null;

    /**
     * @var string $ConstraintID
     */
    protected $ConstraintID = null;

    /**
     * @var string $ConstraintType
     */
    protected $ConstraintType = null;

    /**
     * @var string $WarningInstruction
     */
    protected $WarningInstruction = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getConstraintDescription()
    {
      return $this->ConstraintDescription;
    }

    /**
     * @param string $ConstraintDescription
     * @return Warning
     */
    public function setConstraintDescription($ConstraintDescription)
    {
      $this->ConstraintDescription = $ConstraintDescription;
      return $this;
    }

    /**
     * @return string
     */
    public function getConstraintID()
    {
      return $this->ConstraintID;
    }

    /**
     * @param string $ConstraintID
     * @return Warning
     */
    public function setConstraintID($ConstraintID)
    {
      $this->ConstraintID = $ConstraintID;
      return $this;
    }

    /**
     * @return string
     */
    public function getConstraintType()
    {
      return $this->ConstraintType;
    }

    /**
     * @param string $ConstraintType
     * @return Warning
     */
    public function setConstraintType($ConstraintType)
    {
      $this->ConstraintType = $ConstraintType;
      return $this;
    }

    /**
     * @return string
     */
    public function getWarningInstruction()
    {
      return $this->WarningInstruction;
    }

    /**
     * @param string $WarningInstruction
     * @return Warning
     */
    public function setWarningInstruction($WarningInstruction)
    {
      $this->WarningInstruction = $WarningInstruction;
      return $this;
    }

}
