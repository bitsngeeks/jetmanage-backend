<?php

class ArrayOfWaypointData implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var WaypointData[] $WaypointData
     */
    protected $WaypointData = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return WaypointData[]
     */
    public function getWaypointData()
    {
      return $this->WaypointData;
    }

    /**
     * @param WaypointData[] $WaypointData
     * @return ArrayOfWaypointData
     */
    public function setWaypointData(array $WaypointData = null)
    {
      $this->WaypointData = $WaypointData;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->WaypointData[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return WaypointData
     */
    public function offsetGet($offset)
    {
      return $this->WaypointData[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param WaypointData $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->WaypointData[] = $value;
      } else {
        $this->WaypointData[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->WaypointData[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return WaypointData Return the current element
     */
    public function current()
    {
      return current($this->WaypointData);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->WaypointData);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->WaypointData);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->WaypointData);
    }

    /**
     * Countable implementation
     *
     * @return WaypointData Return count of elements
     */
    public function count()
    {
      return count($this->WaypointData);
    }

}
