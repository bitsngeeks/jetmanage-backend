<?php

class OutputUnitSpecification
{

    /**
     * @var FuelUnit $FuelErrorUnit
     */
    protected $FuelErrorUnit = null;

    /**
     * @var WeightUnit $WeightErrorUnit
     */
    protected $WeightErrorUnit = null;

    /**
     * @var WeightUnit $WeightOutputUnit
     */
    protected $WeightOutputUnit = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return FuelUnit
     */
    public function getFuelErrorUnit()
    {
      return $this->FuelErrorUnit;
    }

    /**
     * @param FuelUnit $FuelErrorUnit
     * @return OutputUnitSpecification
     */
    public function setFuelErrorUnit($FuelErrorUnit)
    {
      $this->FuelErrorUnit = $FuelErrorUnit;
      return $this;
    }

    /**
     * @return WeightUnit
     */
    public function getWeightErrorUnit()
    {
      return $this->WeightErrorUnit;
    }

    /**
     * @param WeightUnit $WeightErrorUnit
     * @return OutputUnitSpecification
     */
    public function setWeightErrorUnit($WeightErrorUnit)
    {
      $this->WeightErrorUnit = $WeightErrorUnit;
      return $this;
    }

    /**
     * @return WeightUnit
     */
    public function getWeightOutputUnit()
    {
      return $this->WeightOutputUnit;
    }

    /**
     * @param WeightUnit $WeightOutputUnit
     * @return OutputUnitSpecification
     */
    public function setWeightOutputUnit($WeightOutputUnit)
    {
      $this->WeightOutputUnit = $WeightOutputUnit;
      return $this;
    }

}
