<?php

class StoredRouteRequest extends RequestBase
{

    /**
     * @var ATCClearedRouteOptions $ATCClearedRouteSettings
     */
    protected $ATCClearedRouteSettings = null;

    /**
     * @var boolean $AutoValidate
     */
    protected $AutoValidate = null;

    /**
     * @var boolean $ExcludeEUROCONTROLRoutes
     */
    protected $ExcludeEUROCONTROLRoutes = null;

    /**
     * @var boolean $ExcludeFAARoutes
     */
    protected $ExcludeFAARoutes = null;

    /**
     * @var boolean $ExcludeRecentlyFiled
     */
    protected $ExcludeRecentlyFiled = null;

    /**
     * @var boolean $ExcludeStandardRoutes
     */
    protected $ExcludeStandardRoutes = null;

    /**
     * @var boolean $IncludeFlightLevelInRoutes
     */
    protected $IncludeFlightLevelInRoutes = null;

    /**
     * @var boolean $LimitNumberOfRoutes
     */
    protected $LimitNumberOfRoutes = null;

    /**
     * @var int $MaximumNumberOfRoutes
     */
    protected $MaximumNumberOfRoutes = null;

    /**
     * @var RouteSpecification $RouteSpecification
     */
    protected $RouteSpecification = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return ATCClearedRouteOptions
     */
    public function getATCClearedRouteSettings()
    {
      return $this->ATCClearedRouteSettings;
    }

    /**
     * @param ATCClearedRouteOptions $ATCClearedRouteSettings
     * @return StoredRouteRequest
     */
    public function setATCClearedRouteSettings($ATCClearedRouteSettings)
    {
      $this->ATCClearedRouteSettings = $ATCClearedRouteSettings;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getAutoValidate()
    {
      return $this->AutoValidate;
    }

    /**
     * @param boolean $AutoValidate
     * @return StoredRouteRequest
     */
    public function setAutoValidate($AutoValidate)
    {
      $this->AutoValidate = $AutoValidate;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getExcludeEUROCONTROLRoutes()
    {
      return $this->ExcludeEUROCONTROLRoutes;
    }

    /**
     * @param boolean $ExcludeEUROCONTROLRoutes
     * @return StoredRouteRequest
     */
    public function setExcludeEUROCONTROLRoutes($ExcludeEUROCONTROLRoutes)
    {
      $this->ExcludeEUROCONTROLRoutes = $ExcludeEUROCONTROLRoutes;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getExcludeFAARoutes()
    {
      return $this->ExcludeFAARoutes;
    }

    /**
     * @param boolean $ExcludeFAARoutes
     * @return StoredRouteRequest
     */
    public function setExcludeFAARoutes($ExcludeFAARoutes)
    {
      $this->ExcludeFAARoutes = $ExcludeFAARoutes;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getExcludeRecentlyFiled()
    {
      return $this->ExcludeRecentlyFiled;
    }

    /**
     * @param boolean $ExcludeRecentlyFiled
     * @return StoredRouteRequest
     */
    public function setExcludeRecentlyFiled($ExcludeRecentlyFiled)
    {
      $this->ExcludeRecentlyFiled = $ExcludeRecentlyFiled;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getExcludeStandardRoutes()
    {
      return $this->ExcludeStandardRoutes;
    }

    /**
     * @param boolean $ExcludeStandardRoutes
     * @return StoredRouteRequest
     */
    public function setExcludeStandardRoutes($ExcludeStandardRoutes)
    {
      $this->ExcludeStandardRoutes = $ExcludeStandardRoutes;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIncludeFlightLevelInRoutes()
    {
      return $this->IncludeFlightLevelInRoutes;
    }

    /**
     * @param boolean $IncludeFlightLevelInRoutes
     * @return StoredRouteRequest
     */
    public function setIncludeFlightLevelInRoutes($IncludeFlightLevelInRoutes)
    {
      $this->IncludeFlightLevelInRoutes = $IncludeFlightLevelInRoutes;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getLimitNumberOfRoutes()
    {
      return $this->LimitNumberOfRoutes;
    }

    /**
     * @param boolean $LimitNumberOfRoutes
     * @return StoredRouteRequest
     */
    public function setLimitNumberOfRoutes($LimitNumberOfRoutes)
    {
      $this->LimitNumberOfRoutes = $LimitNumberOfRoutes;
      return $this;
    }

    /**
     * @return int
     */
    public function getMaximumNumberOfRoutes()
    {
      return $this->MaximumNumberOfRoutes;
    }

    /**
     * @param int $MaximumNumberOfRoutes
     * @return StoredRouteRequest
     */
    public function setMaximumNumberOfRoutes($MaximumNumberOfRoutes)
    {
      $this->MaximumNumberOfRoutes = $MaximumNumberOfRoutes;
      return $this;
    }

    /**
     * @return RouteSpecification
     */
    public function getRouteSpecification()
    {
      return $this->RouteSpecification;
    }

    /**
     * @param RouteSpecification $RouteSpecification
     * @return StoredRouteRequest
     */
    public function setRouteSpecification($RouteSpecification)
    {
      $this->RouteSpecification = $RouteSpecification;
      return $this;
    }

}
