<?php

class ProcedureIdentifier
{

    /**
     * @var string $Identifier
     */
    protected $Identifier = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
      return $this->Identifier;
    }

    /**
     * @param string $Identifier
     * @return ProcedureIdentifier
     */
    public function setIdentifier($Identifier)
    {
      $this->Identifier = $Identifier;
      return $this;
    }

}
