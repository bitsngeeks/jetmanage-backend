<?php

class VerticalOptimizationRequest extends RequestBase
{

    /**
     * @var AircraftSpecification $AircraftDefinition
     */
    protected $AircraftDefinition = null;

    /**
     * @var Airport $DepartureAirport
     */
    protected $DepartureAirport = null;

    /**
     * @var Airport $DestinationAirport
     */
    protected $DestinationAirport = null;

    /**
     * @var AircraftTraficLoadDefinition $LoadDefinition
     */
    protected $LoadDefinition = null;

    /**
     * @var Route $RouteToOptimize
     */
    protected $RouteToOptimize = null;

    /**
     * @var \DateTime $ScheduledTimeOfDeparture
     */
    protected $ScheduledTimeOfDeparture = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return AircraftSpecification
     */
    public function getAircraftDefinition()
    {
      return $this->AircraftDefinition;
    }

    /**
     * @param AircraftSpecification $AircraftDefinition
     * @return VerticalOptimizationRequest
     */
    public function setAircraftDefinition($AircraftDefinition)
    {
      $this->AircraftDefinition = $AircraftDefinition;
      return $this;
    }

    /**
     * @return Airport
     */
    public function getDepartureAirport()
    {
      return $this->DepartureAirport;
    }

    /**
     * @param Airport $DepartureAirport
     * @return VerticalOptimizationRequest
     */
    public function setDepartureAirport($DepartureAirport)
    {
      $this->DepartureAirport = $DepartureAirport;
      return $this;
    }

    /**
     * @return Airport
     */
    public function getDestinationAirport()
    {
      return $this->DestinationAirport;
    }

    /**
     * @param Airport $DestinationAirport
     * @return VerticalOptimizationRequest
     */
    public function setDestinationAirport($DestinationAirport)
    {
      $this->DestinationAirport = $DestinationAirport;
      return $this;
    }

    /**
     * @return AircraftTraficLoadDefinition
     */
    public function getLoadDefinition()
    {
      return $this->LoadDefinition;
    }

    /**
     * @param AircraftTraficLoadDefinition $LoadDefinition
     * @return VerticalOptimizationRequest
     */
    public function setLoadDefinition($LoadDefinition)
    {
      $this->LoadDefinition = $LoadDefinition;
      return $this;
    }

    /**
     * @return Route
     */
    public function getRouteToOptimize()
    {
      return $this->RouteToOptimize;
    }

    /**
     * @param Route $RouteToOptimize
     * @return VerticalOptimizationRequest
     */
    public function setRouteToOptimize($RouteToOptimize)
    {
      $this->RouteToOptimize = $RouteToOptimize;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getScheduledTimeOfDeparture()
    {
      if ($this->ScheduledTimeOfDeparture == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->ScheduledTimeOfDeparture);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $ScheduledTimeOfDeparture
     * @return VerticalOptimizationRequest
     */
    public function setScheduledTimeOfDeparture(\DateTime $ScheduledTimeOfDeparture = null)
    {
      if ($ScheduledTimeOfDeparture == null) {
       $this->ScheduledTimeOfDeparture = null;
      } else {
        $this->ScheduledTimeOfDeparture = $ScheduledTimeOfDeparture->format(\DateTime::ATOM);
      }
      return $this;
    }

}
