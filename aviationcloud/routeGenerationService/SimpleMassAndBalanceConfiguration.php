<?php

class SimpleMassAndBalanceConfiguration extends MassAndBalanceConfigurationBase
{

    /**
     * @var Weight $MaxCargoWeight
     */
    protected $MaxCargoWeight = null;

    /**
     * @var int $MaxCrew
     */
    protected $MaxCrew = null;

    /**
     * @var int $MaxPax
     */
    protected $MaxPax = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return Weight
     */
    public function getMaxCargoWeight()
    {
      return $this->MaxCargoWeight;
    }

    /**
     * @param Weight $MaxCargoWeight
     * @return SimpleMassAndBalanceConfiguration
     */
    public function setMaxCargoWeight($MaxCargoWeight)
    {
      $this->MaxCargoWeight = $MaxCargoWeight;
      return $this;
    }

    /**
     * @return int
     */
    public function getMaxCrew()
    {
      return $this->MaxCrew;
    }

    /**
     * @param int $MaxCrew
     * @return SimpleMassAndBalanceConfiguration
     */
    public function setMaxCrew($MaxCrew)
    {
      $this->MaxCrew = $MaxCrew;
      return $this;
    }

    /**
     * @return int
     */
    public function getMaxPax()
    {
      return $this->MaxPax;
    }

    /**
     * @param int $MaxPax
     * @return SimpleMassAndBalanceConfiguration
     */
    public function setMaxPax($MaxPax)
    {
      $this->MaxPax = $MaxPax;
      return $this;
    }

}
