<?php

class FiledAircraftTypeCount
{

    /**
     * @var string $AircraftTypeICAO
     */
    protected $AircraftTypeICAO = null;

    /**
     * @var AircraftEngineType $EngineType
     */
    protected $EngineType = null;

    /**
     * @var int $NumberOfFilings
     */
    protected $NumberOfFilings = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getAircraftTypeICAO()
    {
      return $this->AircraftTypeICAO;
    }

    /**
     * @param string $AircraftTypeICAO
     * @return FiledAircraftTypeCount
     */
    public function setAircraftTypeICAO($AircraftTypeICAO)
    {
      $this->AircraftTypeICAO = $AircraftTypeICAO;
      return $this;
    }

    /**
     * @return AircraftEngineType
     */
    public function getEngineType()
    {
      return $this->EngineType;
    }

    /**
     * @param AircraftEngineType $EngineType
     * @return FiledAircraftTypeCount
     */
    public function setEngineType($EngineType)
    {
      $this->EngineType = $EngineType;
      return $this;
    }

    /**
     * @return int
     */
    public function getNumberOfFilings()
    {
      return $this->NumberOfFilings;
    }

    /**
     * @param int $NumberOfFilings
     * @return FiledAircraftTypeCount
     */
    public function setNumberOfFilings($NumberOfFilings)
    {
      $this->NumberOfFilings = $NumberOfFilings;
      return $this;
    }

}
