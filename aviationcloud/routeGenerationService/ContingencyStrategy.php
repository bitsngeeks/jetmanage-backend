<?php

class ContingencyStrategy
{

    /**
     * @var boolean $ContingencyForAlternates
     */
    protected $ContingencyForAlternates = null;

    /**
     * @var boolean $IncludeContingencyInEndurance
     */
    protected $IncludeContingencyInEndurance = null;

    /**
     * @var Weight $MinimumContingencyFuel
     */
    protected $MinimumContingencyFuel = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return boolean
     */
    public function getContingencyForAlternates()
    {
      return $this->ContingencyForAlternates;
    }

    /**
     * @param boolean $ContingencyForAlternates
     * @return ContingencyStrategy
     */
    public function setContingencyForAlternates($ContingencyForAlternates)
    {
      $this->ContingencyForAlternates = $ContingencyForAlternates;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIncludeContingencyInEndurance()
    {
      return $this->IncludeContingencyInEndurance;
    }

    /**
     * @param boolean $IncludeContingencyInEndurance
     * @return ContingencyStrategy
     */
    public function setIncludeContingencyInEndurance($IncludeContingencyInEndurance)
    {
      $this->IncludeContingencyInEndurance = $IncludeContingencyInEndurance;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getMinimumContingencyFuel()
    {
      return $this->MinimumContingencyFuel;
    }

    /**
     * @param Weight $MinimumContingencyFuel
     * @return ContingencyStrategy
     */
    public function setMinimumContingencyFuel($MinimumContingencyFuel)
    {
      $this->MinimumContingencyFuel = $MinimumContingencyFuel;
      return $this;
    }

}
