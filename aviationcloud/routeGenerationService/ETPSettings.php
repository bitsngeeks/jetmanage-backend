<?php

class ETPSettings
{

    /**
     * @var ArrayOfAirport $AdequateAirports
     */
    protected $AdequateAirports = null;

    /**
     * @var DepressurisationScenario $DepressurisationScenarioSetting
     */
    protected $DepressurisationScenarioSetting = null;

    /**
     * @var ArrayOfETOPSSettingSpecification $ETOPSCertification
     */
    protected $ETOPSCertification = null;

    /**
     * @var ArrayOfAirport $ETPAirports
     */
    protected $ETPAirports = null;

    /**
     * @var boolean $EnableETOPSCalculation
     */
    protected $EnableETOPSCalculation = null;

    /**
     * @var boolean $EnableETPCalculation
     */
    protected $EnableETPCalculation = null;

    /**
     * @var boolean $EnablePSRCalculation
     */
    protected $EnablePSRCalculation = null;

    /**
     * @var Altitude $FinalAltitude
     */
    protected $FinalAltitude = null;

    /**
     * @var Time $HoldingTime
     */
    protected $HoldingTime = null;

    /**
     * @var PerformanceProfileSpecification $MedicalCruiseProfile
     */
    protected $MedicalCruiseProfile = null;

    /**
     * @var Altitude $OneEngineFinalAltitude
     */
    protected $OneEngineFinalAltitude = null;

    /**
     * @var Altitude $OxygenAltitude
     */
    protected $OxygenAltitude = null;

    /**
     * @var Time $OxygenTime
     */
    protected $OxygenTime = null;

    /**
     * @var boolean $UseVerticalDescent
     */
    protected $UseVerticalDescent = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfAirport
     */
    public function getAdequateAirports()
    {
      return $this->AdequateAirports;
    }

    /**
     * @param ArrayOfAirport $AdequateAirports
     * @return ETPSettings
     */
    public function setAdequateAirports($AdequateAirports)
    {
      $this->AdequateAirports = $AdequateAirports;
      return $this;
    }

    /**
     * @return DepressurisationScenario
     */
    public function getDepressurisationScenarioSetting()
    {
      return $this->DepressurisationScenarioSetting;
    }

    /**
     * @param DepressurisationScenario $DepressurisationScenarioSetting
     * @return ETPSettings
     */
    public function setDepressurisationScenarioSetting($DepressurisationScenarioSetting)
    {
      $this->DepressurisationScenarioSetting = $DepressurisationScenarioSetting;
      return $this;
    }

    /**
     * @return ArrayOfETOPSSettingSpecification
     */
    public function getETOPSCertification()
    {
      return $this->ETOPSCertification;
    }

    /**
     * @param ArrayOfETOPSSettingSpecification $ETOPSCertification
     * @return ETPSettings
     */
    public function setETOPSCertification($ETOPSCertification)
    {
      $this->ETOPSCertification = $ETOPSCertification;
      return $this;
    }

    /**
     * @return ArrayOfAirport
     */
    public function getETPAirports()
    {
      return $this->ETPAirports;
    }

    /**
     * @param ArrayOfAirport $ETPAirports
     * @return ETPSettings
     */
    public function setETPAirports($ETPAirports)
    {
      $this->ETPAirports = $ETPAirports;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getEnableETOPSCalculation()
    {
      return $this->EnableETOPSCalculation;
    }

    /**
     * @param boolean $EnableETOPSCalculation
     * @return ETPSettings
     */
    public function setEnableETOPSCalculation($EnableETOPSCalculation)
    {
      $this->EnableETOPSCalculation = $EnableETOPSCalculation;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getEnableETPCalculation()
    {
      return $this->EnableETPCalculation;
    }

    /**
     * @param boolean $EnableETPCalculation
     * @return ETPSettings
     */
    public function setEnableETPCalculation($EnableETPCalculation)
    {
      $this->EnableETPCalculation = $EnableETPCalculation;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getEnablePSRCalculation()
    {
      return $this->EnablePSRCalculation;
    }

    /**
     * @param boolean $EnablePSRCalculation
     * @return ETPSettings
     */
    public function setEnablePSRCalculation($EnablePSRCalculation)
    {
      $this->EnablePSRCalculation = $EnablePSRCalculation;
      return $this;
    }

    /**
     * @return Altitude
     */
    public function getFinalAltitude()
    {
      return $this->FinalAltitude;
    }

    /**
     * @param Altitude $FinalAltitude
     * @return ETPSettings
     */
    public function setFinalAltitude($FinalAltitude)
    {
      $this->FinalAltitude = $FinalAltitude;
      return $this;
    }

    /**
     * @return Time
     */
    public function getHoldingTime()
    {
      return $this->HoldingTime;
    }

    /**
     * @param Time $HoldingTime
     * @return ETPSettings
     */
    public function setHoldingTime($HoldingTime)
    {
      $this->HoldingTime = $HoldingTime;
      return $this;
    }

    /**
     * @return PerformanceProfileSpecification
     */
    public function getMedicalCruiseProfile()
    {
      return $this->MedicalCruiseProfile;
    }

    /**
     * @param PerformanceProfileSpecification $MedicalCruiseProfile
     * @return ETPSettings
     */
    public function setMedicalCruiseProfile($MedicalCruiseProfile)
    {
      $this->MedicalCruiseProfile = $MedicalCruiseProfile;
      return $this;
    }

    /**
     * @return Altitude
     */
    public function getOneEngineFinalAltitude()
    {
      return $this->OneEngineFinalAltitude;
    }

    /**
     * @param Altitude $OneEngineFinalAltitude
     * @return ETPSettings
     */
    public function setOneEngineFinalAltitude($OneEngineFinalAltitude)
    {
      $this->OneEngineFinalAltitude = $OneEngineFinalAltitude;
      return $this;
    }

    /**
     * @return Altitude
     */
    public function getOxygenAltitude()
    {
      return $this->OxygenAltitude;
    }

    /**
     * @param Altitude $OxygenAltitude
     * @return ETPSettings
     */
    public function setOxygenAltitude($OxygenAltitude)
    {
      $this->OxygenAltitude = $OxygenAltitude;
      return $this;
    }

    /**
     * @return Time
     */
    public function getOxygenTime()
    {
      return $this->OxygenTime;
    }

    /**
     * @param Time $OxygenTime
     * @return ETPSettings
     */
    public function setOxygenTime($OxygenTime)
    {
      $this->OxygenTime = $OxygenTime;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUseVerticalDescent()
    {
      return $this->UseVerticalDescent;
    }

    /**
     * @param boolean $UseVerticalDescent
     * @return ETPSettings
     */
    public function setUseVerticalDescent($UseVerticalDescent)
    {
      $this->UseVerticalDescent = $UseVerticalDescent;
      return $this;
    }

}
