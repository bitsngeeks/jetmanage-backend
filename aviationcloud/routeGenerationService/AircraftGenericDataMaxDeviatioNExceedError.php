<?php

class AircraftGenericDataMaxDeviatioNExceedError
{

    /**
     * @var float $AllowedDeviation
     */
    protected $AllowedDeviation = null;

    /**
     * @var float $FoundDeviation
     */
    protected $FoundDeviation = null;

    /**
     * @var float $GenericDataValue
     */
    protected $GenericDataValue = null;

    /**
     * @var float $SpecfieidValue
     */
    protected $SpecfieidValue = null;

    /**
     * @var string $ValueName
     */
    protected $ValueName = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getAllowedDeviation()
    {
      return $this->AllowedDeviation;
    }

    /**
     * @param float $AllowedDeviation
     * @return AircraftGenericDataMaxDeviatioNExceedError
     */
    public function setAllowedDeviation($AllowedDeviation)
    {
      $this->AllowedDeviation = $AllowedDeviation;
      return $this;
    }

    /**
     * @return float
     */
    public function getFoundDeviation()
    {
      return $this->FoundDeviation;
    }

    /**
     * @param float $FoundDeviation
     * @return AircraftGenericDataMaxDeviatioNExceedError
     */
    public function setFoundDeviation($FoundDeviation)
    {
      $this->FoundDeviation = $FoundDeviation;
      return $this;
    }

    /**
     * @return float
     */
    public function getGenericDataValue()
    {
      return $this->GenericDataValue;
    }

    /**
     * @param float $GenericDataValue
     * @return AircraftGenericDataMaxDeviatioNExceedError
     */
    public function setGenericDataValue($GenericDataValue)
    {
      $this->GenericDataValue = $GenericDataValue;
      return $this;
    }

    /**
     * @return float
     */
    public function getSpecfieidValue()
    {
      return $this->SpecfieidValue;
    }

    /**
     * @param float $SpecfieidValue
     * @return AircraftGenericDataMaxDeviatioNExceedError
     */
    public function setSpecfieidValue($SpecfieidValue)
    {
      $this->SpecfieidValue = $SpecfieidValue;
      return $this;
    }

    /**
     * @return string
     */
    public function getValueName()
    {
      return $this->ValueName;
    }

    /**
     * @param string $ValueName
     * @return AircraftGenericDataMaxDeviatioNExceedError
     */
    public function setValueName($ValueName)
    {
      $this->ValueName = $ValueName;
      return $this;
    }

}
