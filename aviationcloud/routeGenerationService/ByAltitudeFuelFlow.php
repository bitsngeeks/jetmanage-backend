<?php

class ByAltitudeFuelFlow
{

    /**
     * @var float $Feet
     */
    protected $Feet = null;

    /**
     * @var float $PoundPerHour
     */
    protected $PoundPerHour = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getFeet()
    {
      return $this->Feet;
    }

    /**
     * @param float $Feet
     * @return ByAltitudeFuelFlow
     */
    public function setFeet($Feet)
    {
      $this->Feet = $Feet;
      return $this;
    }

    /**
     * @return float
     */
    public function getPoundPerHour()
    {
      return $this->PoundPerHour;
    }

    /**
     * @param float $PoundPerHour
     * @return ByAltitudeFuelFlow
     */
    public function setPoundPerHour($PoundPerHour)
    {
      $this->PoundPerHour = $PoundPerHour;
      return $this;
    }

}
