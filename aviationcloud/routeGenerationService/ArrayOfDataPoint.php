<?php

class ArrayOfDataPoint implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var DataPoint[] $DataPoint
     */
    protected $DataPoint = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return DataPoint[]
     */
    public function getDataPoint()
    {
      return $this->DataPoint;
    }

    /**
     * @param DataPoint[] $DataPoint
     * @return ArrayOfDataPoint
     */
    public function setDataPoint(array $DataPoint = null)
    {
      $this->DataPoint = $DataPoint;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->DataPoint[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return DataPoint
     */
    public function offsetGet($offset)
    {
      return $this->DataPoint[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param DataPoint $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->DataPoint[] = $value;
      } else {
        $this->DataPoint[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->DataPoint[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return DataPoint Return the current element
     */
    public function current()
    {
      return current($this->DataPoint);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->DataPoint);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->DataPoint);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->DataPoint);
    }

    /**
     * Countable implementation
     *
     * @return DataPoint Return count of elements
     */
    public function count()
    {
      return count($this->DataPoint);
    }

}
