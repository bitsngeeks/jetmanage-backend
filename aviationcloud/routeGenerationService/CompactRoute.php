<?php

class CompactRoute
{

    /**
     * @var Airport $DepartureAirport
     */
    protected $DepartureAirport = null;

    /**
     * @var Airport $DestinationAirport
     */
    protected $DestinationAirport = null;

    /**
     * @var ArrayOfRouteNode $Path
     */
    protected $Path = null;

    /**
     * @var string $ATCroute
     */
    protected $ATCroute = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Airport
     */
    public function getDepartureAirport()
    {
      return $this->DepartureAirport;
    }

    /**
     * @param Airport $DepartureAirport
     * @return CompactRoute
     */
    public function setDepartureAirport($DepartureAirport)
    {
      $this->DepartureAirport = $DepartureAirport;
      return $this;
    }

    /**
     * @return Airport
     */
    public function getDestinationAirport()
    {
      return $this->DestinationAirport;
    }

    /**
     * @param Airport $DestinationAirport
     * @return CompactRoute
     */
    public function setDestinationAirport($DestinationAirport)
    {
      $this->DestinationAirport = $DestinationAirport;
      return $this;
    }

    /**
     * @return ArrayOfRouteNode
     */
    public function getPath()
    {
      return $this->Path;
    }

    /**
     * @param ArrayOfRouteNode $Path
     * @return CompactRoute
     */
    public function setPath($Path)
    {
      $this->Path = $Path;
      return $this;
    }

    /**
     * @return string
     */
    public function getATCroute()
    {
      return $this->ATCroute;
    }

    /**
     * @param string $ATCroute
     * @return CompactRoute
     */
    public function setATCroute($ATCroute)
    {
      $this->ATCroute = $ATCroute;
      return $this;
    }

}
