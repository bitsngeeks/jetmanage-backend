<?php

class FlightData
{

    /**
     * @var Length $AccumulatedDistane
     */
    protected $AccumulatedDistane = null;

    /**
     * @var Length $AccumulatedESAD
     */
    protected $AccumulatedESAD = null;

    /**
     * @var Time $AccumulatedTime
     */
    protected $AccumulatedTime = null;

    /**
     * @var string $Altitude
     */
    protected $Altitude = null;

    /**
     * @var Speed $CalibratedAirSpeed
     */
    protected $CalibratedAirSpeed = null;

    /**
     * @var float $DesiredFlightLevel
     */
    protected $DesiredFlightLevel = null;

    /**
     * @var Length $DistanceRemaining
     */
    protected $DistanceRemaining = null;

    /**
     * @var Speed $EquivalentAirSpeed
     */
    protected $EquivalentAirSpeed = null;

    /**
     * @var float $FlightLevel
     */
    protected $FlightLevel = null;

    /**
     * @var float $GroundSpeed
     */
    protected $GroundSpeed = null;

    /**
     * @var float $Mach
     */
    protected $Mach = null;

    /**
     * @var float $MagneticHeading
     */
    protected $MagneticHeading = null;

    /**
     * @var float $MagneticTrack
     */
    protected $MagneticTrack = null;

    /**
     * @var Length $SegmentDistance
     */
    protected $SegmentDistance = null;

    /**
     * @var Length $SegmentESAD
     */
    protected $SegmentESAD = null;

    /**
     * @var Time $SegmentTime
     */
    protected $SegmentTime = null;

    /**
     * @var string $SpecialPointType
     */
    protected $SpecialPointType = null;

    /**
     * @var FlightState $State
     */
    protected $State = null;

    /**
     * @var \DateTime $TimeOverWaypoint
     */
    protected $TimeOverWaypoint = null;

    /**
     * @var Time $TimeRemaining
     */
    protected $TimeRemaining = null;

    /**
     * @var float $TrueAirSpeed
     */
    protected $TrueAirSpeed = null;

    /**
     * @var float $TrueHeading
     */
    protected $TrueHeading = null;

    /**
     * @var float $TrueTrack
     */
    protected $TrueTrack = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Length
     */
    public function getAccumulatedDistane()
    {
      return $this->AccumulatedDistane;
    }

    /**
     * @param Length $AccumulatedDistane
     * @return FlightData
     */
    public function setAccumulatedDistane($AccumulatedDistane)
    {
      $this->AccumulatedDistane = $AccumulatedDistane;
      return $this;
    }

    /**
     * @return Length
     */
    public function getAccumulatedESAD()
    {
      return $this->AccumulatedESAD;
    }

    /**
     * @param Length $AccumulatedESAD
     * @return FlightData
     */
    public function setAccumulatedESAD($AccumulatedESAD)
    {
      $this->AccumulatedESAD = $AccumulatedESAD;
      return $this;
    }

    /**
     * @return Time
     */
    public function getAccumulatedTime()
    {
      return $this->AccumulatedTime;
    }

    /**
     * @param Time $AccumulatedTime
     * @return FlightData
     */
    public function setAccumulatedTime($AccumulatedTime)
    {
      $this->AccumulatedTime = $AccumulatedTime;
      return $this;
    }

    /**
     * @return string
     */
    public function getAltitude()
    {
      return $this->Altitude;
    }

    /**
     * @param string $Altitude
     * @return FlightData
     */
    public function setAltitude($Altitude)
    {
      $this->Altitude = $Altitude;
      return $this;
    }

    /**
     * @return Speed
     */
    public function getCalibratedAirSpeed()
    {
      return $this->CalibratedAirSpeed;
    }

    /**
     * @param Speed $CalibratedAirSpeed
     * @return FlightData
     */
    public function setCalibratedAirSpeed($CalibratedAirSpeed)
    {
      $this->CalibratedAirSpeed = $CalibratedAirSpeed;
      return $this;
    }

    /**
     * @return float
     */
    public function getDesiredFlightLevel()
    {
      return $this->DesiredFlightLevel;
    }

    /**
     * @param float $DesiredFlightLevel
     * @return FlightData
     */
    public function setDesiredFlightLevel($DesiredFlightLevel)
    {
      $this->DesiredFlightLevel = $DesiredFlightLevel;
      return $this;
    }

    /**
     * @return Length
     */
    public function getDistanceRemaining()
    {
      return $this->DistanceRemaining;
    }

    /**
     * @param Length $DistanceRemaining
     * @return FlightData
     */
    public function setDistanceRemaining($DistanceRemaining)
    {
      $this->DistanceRemaining = $DistanceRemaining;
      return $this;
    }

    /**
     * @return Speed
     */
    public function getEquivalentAirSpeed()
    {
      return $this->EquivalentAirSpeed;
    }

    /**
     * @param Speed $EquivalentAirSpeed
     * @return FlightData
     */
    public function setEquivalentAirSpeed($EquivalentAirSpeed)
    {
      $this->EquivalentAirSpeed = $EquivalentAirSpeed;
      return $this;
    }

    /**
     * @return float
     */
    public function getFlightLevel()
    {
      return $this->FlightLevel;
    }

    /**
     * @param float $FlightLevel
     * @return FlightData
     */
    public function setFlightLevel($FlightLevel)
    {
      $this->FlightLevel = $FlightLevel;
      return $this;
    }

    /**
     * @return float
     */
    public function getGroundSpeed()
    {
      return $this->GroundSpeed;
    }

    /**
     * @param float $GroundSpeed
     * @return FlightData
     */
    public function setGroundSpeed($GroundSpeed)
    {
      $this->GroundSpeed = $GroundSpeed;
      return $this;
    }

    /**
     * @return float
     */
    public function getMach()
    {
      return $this->Mach;
    }

    /**
     * @param float $Mach
     * @return FlightData
     */
    public function setMach($Mach)
    {
      $this->Mach = $Mach;
      return $this;
    }

    /**
     * @return float
     */
    public function getMagneticHeading()
    {
      return $this->MagneticHeading;
    }

    /**
     * @param float $MagneticHeading
     * @return FlightData
     */
    public function setMagneticHeading($MagneticHeading)
    {
      $this->MagneticHeading = $MagneticHeading;
      return $this;
    }

    /**
     * @return float
     */
    public function getMagneticTrack()
    {
      return $this->MagneticTrack;
    }

    /**
     * @param float $MagneticTrack
     * @return FlightData
     */
    public function setMagneticTrack($MagneticTrack)
    {
      $this->MagneticTrack = $MagneticTrack;
      return $this;
    }

    /**
     * @return Length
     */
    public function getSegmentDistance()
    {
      return $this->SegmentDistance;
    }

    /**
     * @param Length $SegmentDistance
     * @return FlightData
     */
    public function setSegmentDistance($SegmentDistance)
    {
      $this->SegmentDistance = $SegmentDistance;
      return $this;
    }

    /**
     * @return Length
     */
    public function getSegmentESAD()
    {
      return $this->SegmentESAD;
    }

    /**
     * @param Length $SegmentESAD
     * @return FlightData
     */
    public function setSegmentESAD($SegmentESAD)
    {
      $this->SegmentESAD = $SegmentESAD;
      return $this;
    }

    /**
     * @return Time
     */
    public function getSegmentTime()
    {
      return $this->SegmentTime;
    }

    /**
     * @param Time $SegmentTime
     * @return FlightData
     */
    public function setSegmentTime($SegmentTime)
    {
      $this->SegmentTime = $SegmentTime;
      return $this;
    }

    /**
     * @return string
     */
    public function getSpecialPointType()
    {
      return $this->SpecialPointType;
    }

    /**
     * @param string $SpecialPointType
     * @return FlightData
     */
    public function setSpecialPointType($SpecialPointType)
    {
      $this->SpecialPointType = $SpecialPointType;
      return $this;
    }

    /**
     * @return FlightState
     */
    public function getState()
    {
      return $this->State;
    }

    /**
     * @param FlightState $State
     * @return FlightData
     */
    public function setState($State)
    {
      $this->State = $State;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getTimeOverWaypoint()
    {
      if ($this->TimeOverWaypoint == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->TimeOverWaypoint);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $TimeOverWaypoint
     * @return FlightData
     */
    public function setTimeOverWaypoint(\DateTime $TimeOverWaypoint = null)
    {
      if ($TimeOverWaypoint == null) {
       $this->TimeOverWaypoint = null;
      } else {
        $this->TimeOverWaypoint = $TimeOverWaypoint->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return Time
     */
    public function getTimeRemaining()
    {
      return $this->TimeRemaining;
    }

    /**
     * @param Time $TimeRemaining
     * @return FlightData
     */
    public function setTimeRemaining($TimeRemaining)
    {
      $this->TimeRemaining = $TimeRemaining;
      return $this;
    }

    /**
     * @return float
     */
    public function getTrueAirSpeed()
    {
      return $this->TrueAirSpeed;
    }

    /**
     * @param float $TrueAirSpeed
     * @return FlightData
     */
    public function setTrueAirSpeed($TrueAirSpeed)
    {
      $this->TrueAirSpeed = $TrueAirSpeed;
      return $this;
    }

    /**
     * @return float
     */
    public function getTrueHeading()
    {
      return $this->TrueHeading;
    }

    /**
     * @param float $TrueHeading
     * @return FlightData
     */
    public function setTrueHeading($TrueHeading)
    {
      $this->TrueHeading = $TrueHeading;
      return $this;
    }

    /**
     * @return float
     */
    public function getTrueTrack()
    {
      return $this->TrueTrack;
    }

    /**
     * @param float $TrueTrack
     * @return FlightData
     */
    public function setTrueTrack($TrueTrack)
    {
      $this->TrueTrack = $TrueTrack;
      return $this;
    }

}
