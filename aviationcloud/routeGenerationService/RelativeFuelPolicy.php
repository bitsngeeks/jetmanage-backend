<?php

class RelativeFuelPolicy extends FuelPolicy
{

    /**
     * @var Weight $Deviation
     */
    protected $Deviation = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return Weight
     */
    public function getDeviation()
    {
      return $this->Deviation;
    }

    /**
     * @param Weight $Deviation
     * @return RelativeFuelPolicy
     */
    public function setDeviation($Deviation)
    {
      $this->Deviation = $Deviation;
      return $this;
    }

}
