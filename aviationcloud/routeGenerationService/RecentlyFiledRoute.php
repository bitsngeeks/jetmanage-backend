<?php

class RecentlyFiledRoute extends RouteSourceInformation
{

    /**
     * @var int $NumberOfFilings
     */
    protected $NumberOfFilings = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return int
     */
    public function getNumberOfFilings()
    {
      return $this->NumberOfFilings;
    }

    /**
     * @param int $NumberOfFilings
     * @return RecentlyFiledRoute
     */
    public function setNumberOfFilings($NumberOfFilings)
    {
      $this->NumberOfFilings = $NumberOfFilings;
      return $this;
    }

}
