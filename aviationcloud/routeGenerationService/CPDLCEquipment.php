<?php

class CPDLCEquipment
{

    /**
     * @var boolean $ATNVDLMode2
     */
    protected $ATNVDLMode2 = null;

    /**
     * @var boolean $FANS1AHFDL
     */
    protected $FANS1AHFDL = null;

    /**
     * @var boolean $FANS1ASATCOMINMARSAT
     */
    protected $FANS1ASATCOMINMARSAT = null;

    /**
     * @var boolean $FANS1ASATCOMIRDIUM
     */
    protected $FANS1ASATCOMIRDIUM = null;

    /**
     * @var boolean $FANS1ASATCOMMTSAT
     */
    protected $FANS1ASATCOMMTSAT = null;

    /**
     * @var boolean $FANS1AVDLMode2
     */
    protected $FANS1AVDLMode2 = null;

    /**
     * @var boolean $FANS1AVDLModeA
     */
    protected $FANS1AVDLModeA = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return boolean
     */
    public function getATNVDLMode2()
    {
      return $this->ATNVDLMode2;
    }

    /**
     * @param boolean $ATNVDLMode2
     * @return CPDLCEquipment
     */
    public function setATNVDLMode2($ATNVDLMode2)
    {
      $this->ATNVDLMode2 = $ATNVDLMode2;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getFANS1AHFDL()
    {
      return $this->FANS1AHFDL;
    }

    /**
     * @param boolean $FANS1AHFDL
     * @return CPDLCEquipment
     */
    public function setFANS1AHFDL($FANS1AHFDL)
    {
      $this->FANS1AHFDL = $FANS1AHFDL;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getFANS1ASATCOMINMARSAT()
    {
      return $this->FANS1ASATCOMINMARSAT;
    }

    /**
     * @param boolean $FANS1ASATCOMINMARSAT
     * @return CPDLCEquipment
     */
    public function setFANS1ASATCOMINMARSAT($FANS1ASATCOMINMARSAT)
    {
      $this->FANS1ASATCOMINMARSAT = $FANS1ASATCOMINMARSAT;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getFANS1ASATCOMIRDIUM()
    {
      return $this->FANS1ASATCOMIRDIUM;
    }

    /**
     * @param boolean $FANS1ASATCOMIRDIUM
     * @return CPDLCEquipment
     */
    public function setFANS1ASATCOMIRDIUM($FANS1ASATCOMIRDIUM)
    {
      $this->FANS1ASATCOMIRDIUM = $FANS1ASATCOMIRDIUM;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getFANS1ASATCOMMTSAT()
    {
      return $this->FANS1ASATCOMMTSAT;
    }

    /**
     * @param boolean $FANS1ASATCOMMTSAT
     * @return CPDLCEquipment
     */
    public function setFANS1ASATCOMMTSAT($FANS1ASATCOMMTSAT)
    {
      $this->FANS1ASATCOMMTSAT = $FANS1ASATCOMMTSAT;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getFANS1AVDLMode2()
    {
      return $this->FANS1AVDLMode2;
    }

    /**
     * @param boolean $FANS1AVDLMode2
     * @return CPDLCEquipment
     */
    public function setFANS1AVDLMode2($FANS1AVDLMode2)
    {
      $this->FANS1AVDLMode2 = $FANS1AVDLMode2;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getFANS1AVDLModeA()
    {
      return $this->FANS1AVDLModeA;
    }

    /**
     * @param boolean $FANS1AVDLModeA
     * @return CPDLCEquipment
     */
    public function setFANS1AVDLModeA($FANS1AVDLModeA)
    {
      $this->FANS1AVDLModeA = $FANS1AVDLModeA;
      return $this;
    }

}
