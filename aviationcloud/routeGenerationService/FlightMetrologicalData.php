<?php

class FlightMetrologicalData
{

    /**
     * @var float $AverageISADeviation
     */
    protected $AverageISADeviation = null;

    /**
     * @var float $AverageWindDirection
     */
    protected $AverageWindDirection = null;

    /**
     * @var float $AverageWindVelocity
     */
    protected $AverageWindVelocity = null;

    /**
     * @var IcingPoint $MaxIcing
     */
    protected $MaxIcing = null;

    /**
     * @var TurbulencePoint $MaxTurbulence
     */
    protected $MaxTurbulence = null;

    /**
     * @var float $MaxWindShear
     */
    protected $MaxWindShear = null;

    /**
     * @var RouteNodeIdentifier $PointOfMaxWindShear
     */
    protected $PointOfMaxWindShear = null;

    /**
     * @var FlightRAIMData $RAIMResult
     */
    protected $RAIMResult = null;

    /**
     * @var float $TopOfClimbISA
     */
    protected $TopOfClimbISA = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getAverageISADeviation()
    {
      return $this->AverageISADeviation;
    }

    /**
     * @param float $AverageISADeviation
     * @return FlightMetrologicalData
     */
    public function setAverageISADeviation($AverageISADeviation)
    {
      $this->AverageISADeviation = $AverageISADeviation;
      return $this;
    }

    /**
     * @return float
     */
    public function getAverageWindDirection()
    {
      return $this->AverageWindDirection;
    }

    /**
     * @param float $AverageWindDirection
     * @return FlightMetrologicalData
     */
    public function setAverageWindDirection($AverageWindDirection)
    {
      $this->AverageWindDirection = $AverageWindDirection;
      return $this;
    }

    /**
     * @return float
     */
    public function getAverageWindVelocity()
    {
      return $this->AverageWindVelocity;
    }

    /**
     * @param float $AverageWindVelocity
     * @return FlightMetrologicalData
     */
    public function setAverageWindVelocity($AverageWindVelocity)
    {
      $this->AverageWindVelocity = $AverageWindVelocity;
      return $this;
    }

    /**
     * @return IcingPoint
     */
    public function getMaxIcing()
    {
      return $this->MaxIcing;
    }

    /**
     * @param IcingPoint $MaxIcing
     * @return FlightMetrologicalData
     */
    public function setMaxIcing($MaxIcing)
    {
      $this->MaxIcing = $MaxIcing;
      return $this;
    }

    /**
     * @return TurbulencePoint
     */
    public function getMaxTurbulence()
    {
      return $this->MaxTurbulence;
    }

    /**
     * @param TurbulencePoint $MaxTurbulence
     * @return FlightMetrologicalData
     */
    public function setMaxTurbulence($MaxTurbulence)
    {
      $this->MaxTurbulence = $MaxTurbulence;
      return $this;
    }

    /**
     * @return float
     */
    public function getMaxWindShear()
    {
      return $this->MaxWindShear;
    }

    /**
     * @param float $MaxWindShear
     * @return FlightMetrologicalData
     */
    public function setMaxWindShear($MaxWindShear)
    {
      $this->MaxWindShear = $MaxWindShear;
      return $this;
    }

    /**
     * @return RouteNodeIdentifier
     */
    public function getPointOfMaxWindShear()
    {
      return $this->PointOfMaxWindShear;
    }

    /**
     * @param RouteNodeIdentifier $PointOfMaxWindShear
     * @return FlightMetrologicalData
     */
    public function setPointOfMaxWindShear($PointOfMaxWindShear)
    {
      $this->PointOfMaxWindShear = $PointOfMaxWindShear;
      return $this;
    }

    /**
     * @return FlightRAIMData
     */
    public function getRAIMResult()
    {
      return $this->RAIMResult;
    }

    /**
     * @param FlightRAIMData $RAIMResult
     * @return FlightMetrologicalData
     */
    public function setRAIMResult($RAIMResult)
    {
      $this->RAIMResult = $RAIMResult;
      return $this;
    }

    /**
     * @return float
     */
    public function getTopOfClimbISA()
    {
      return $this->TopOfClimbISA;
    }

    /**
     * @param float $TopOfClimbISA
     * @return FlightMetrologicalData
     */
    public function setTopOfClimbISA($TopOfClimbISA)
    {
      $this->TopOfClimbISA = $TopOfClimbISA;
      return $this;
    }

}
