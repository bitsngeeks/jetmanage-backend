<?php

class RouteEstimationMetricsSpecification
{

    /**
     * @var boolean $CalculateMetrics
     */
    protected $CalculateMetrics = null;

    /**
     * @var RouteEstimationLevel $EstimationLevel
     */
    protected $EstimationLevel = null;

    /**
     * @var float $FuelPrice
     */
    protected $FuelPrice = null;

    /**
     * @var float $OperationalCostPerHour
     */
    protected $OperationalCostPerHour = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return boolean
     */
    public function getCalculateMetrics()
    {
      return $this->CalculateMetrics;
    }

    /**
     * @param boolean $CalculateMetrics
     * @return RouteEstimationMetricsSpecification
     */
    public function setCalculateMetrics($CalculateMetrics)
    {
      $this->CalculateMetrics = $CalculateMetrics;
      return $this;
    }

    /**
     * @return RouteEstimationLevel
     */
    public function getEstimationLevel()
    {
      return $this->EstimationLevel;
    }

    /**
     * @param RouteEstimationLevel $EstimationLevel
     * @return RouteEstimationMetricsSpecification
     */
    public function setEstimationLevel($EstimationLevel)
    {
      $this->EstimationLevel = $EstimationLevel;
      return $this;
    }

    /**
     * @return float
     */
    public function getFuelPrice()
    {
      return $this->FuelPrice;
    }

    /**
     * @param float $FuelPrice
     * @return RouteEstimationMetricsSpecification
     */
    public function setFuelPrice($FuelPrice)
    {
      $this->FuelPrice = $FuelPrice;
      return $this;
    }

    /**
     * @return float
     */
    public function getOperationalCostPerHour()
    {
      return $this->OperationalCostPerHour;
    }

    /**
     * @param float $OperationalCostPerHour
     * @return RouteEstimationMetricsSpecification
     */
    public function setOperationalCostPerHour($OperationalCostPerHour)
    {
      $this->OperationalCostPerHour = $OperationalCostPerHour;
      return $this;
    }

}
