<?php

class CostMinimizingCostFunction extends CostFunction
{

    /**
     * @var Currency $AircraftOperationCostPerHour
     */
    protected $AircraftOperationCostPerHour = null;

    /**
     * @var Currency $FuelPricePerGallon
     */
    protected $FuelPricePerGallon = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Currency
     */
    public function getAircraftOperationCostPerHour()
    {
      return $this->AircraftOperationCostPerHour;
    }

    /**
     * @param Currency $AircraftOperationCostPerHour
     * @return CostMinimizingCostFunction
     */
    public function setAircraftOperationCostPerHour($AircraftOperationCostPerHour)
    {
      $this->AircraftOperationCostPerHour = $AircraftOperationCostPerHour;
      return $this;
    }

    /**
     * @return Currency
     */
    public function getFuelPricePerGallon()
    {
      return $this->FuelPricePerGallon;
    }

    /**
     * @param Currency $FuelPricePerGallon
     * @return CostMinimizingCostFunction
     */
    public function setFuelPricePerGallon($FuelPricePerGallon)
    {
      $this->FuelPricePerGallon = $FuelPricePerGallon;
      return $this;
    }

}
