<?php

class RouteNode
{

    /**
     * @var Length $AccumulatedDistance
     */
    protected $AccumulatedDistance = null;

    /**
     * @var string $Airway
     */
    protected $Airway = null;

    /**
     * @var string $CountryName
     */
    protected $CountryName = null;

    /**
     * @var FlightData $EstimatedFlightData
     */
    protected $EstimatedFlightData = null;

    /**
     * @var string $FIR
     */
    protected $FIR = null;

    /**
     * @var string $FixType
     */
    protected $FixType = null;

    /**
     * @var ATCFlightRule $FlightRuleState
     */
    protected $FlightRuleState = null;

    /**
     * @var float $Frequency
     */
    protected $Frequency = null;

    /**
     * @var float $MagneticVarians
     */
    protected $MagneticVarians = null;

    /**
     * @var int $MaxFlightLevel
     */
    protected $MaxFlightLevel = null;

    /**
     * @var int $MinFlightLevel
     */
    protected $MinFlightLevel = null;

    /**
     * @var RouteNodeIdentifier $NodeIdentifier
     */
    protected $NodeIdentifier = null;

    /**
     * @var ParserInformation $ParserData
     */
    protected $ParserData = null;

    /**
     * @var RouteNodeType $RouteNodeType
     */
    protected $RouteNodeType = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Length
     */
    public function getAccumulatedDistance()
    {
      return $this->AccumulatedDistance;
    }

    /**
     * @param Length $AccumulatedDistance
     * @return RouteNode
     */
    public function setAccumulatedDistance($AccumulatedDistance)
    {
      $this->AccumulatedDistance = $AccumulatedDistance;
      return $this;
    }

    /**
     * @return string
     */
    public function getAirway()
    {
      return $this->Airway;
    }

    /**
     * @param string $Airway
     * @return RouteNode
     */
    public function setAirway($Airway)
    {
      $this->Airway = $Airway;
      return $this;
    }

    /**
     * @return string
     */
    public function getCountryName()
    {
      return $this->CountryName;
    }

    /**
     * @param string $CountryName
     * @return RouteNode
     */
    public function setCountryName($CountryName)
    {
      $this->CountryName = $CountryName;
      return $this;
    }

    /**
     * @return FlightData
     */
    public function getEstimatedFlightData()
    {
      return $this->EstimatedFlightData;
    }

    /**
     * @param FlightData $EstimatedFlightData
     * @return RouteNode
     */
    public function setEstimatedFlightData($EstimatedFlightData)
    {
      $this->EstimatedFlightData = $EstimatedFlightData;
      return $this;
    }

    /**
     * @return string
     */
    public function getFIR()
    {
      return $this->FIR;
    }

    /**
     * @param string $FIR
     * @return RouteNode
     */
    public function setFIR($FIR)
    {
      $this->FIR = $FIR;
      return $this;
    }

    /**
     * @return string
     */
    public function getFixType()
    {
      return $this->FixType;
    }

    /**
     * @param string $FixType
     * @return RouteNode
     */
    public function setFixType($FixType)
    {
      $this->FixType = $FixType;
      return $this;
    }

    /**
     * @return ATCFlightRule
     */
    public function getFlightRuleState()
    {
      return $this->FlightRuleState;
    }

    /**
     * @param ATCFlightRule $FlightRuleState
     * @return RouteNode
     */
    public function setFlightRuleState($FlightRuleState)
    {
      $this->FlightRuleState = $FlightRuleState;
      return $this;
    }

    /**
     * @return float
     */
    public function getFrequency()
    {
      return $this->Frequency;
    }

    /**
     * @param float $Frequency
     * @return RouteNode
     */
    public function setFrequency($Frequency)
    {
      $this->Frequency = $Frequency;
      return $this;
    }

    /**
     * @return float
     */
    public function getMagneticVarians()
    {
      return $this->MagneticVarians;
    }

    /**
     * @param float $MagneticVarians
     * @return RouteNode
     */
    public function setMagneticVarians($MagneticVarians)
    {
      $this->MagneticVarians = $MagneticVarians;
      return $this;
    }

    /**
     * @return int
     */
    public function getMaxFlightLevel()
    {
      return $this->MaxFlightLevel;
    }

    /**
     * @param int $MaxFlightLevel
     * @return RouteNode
     */
    public function setMaxFlightLevel($MaxFlightLevel)
    {
      $this->MaxFlightLevel = $MaxFlightLevel;
      return $this;
    }

    /**
     * @return int
     */
    public function getMinFlightLevel()
    {
      return $this->MinFlightLevel;
    }

    /**
     * @param int $MinFlightLevel
     * @return RouteNode
     */
    public function setMinFlightLevel($MinFlightLevel)
    {
      $this->MinFlightLevel = $MinFlightLevel;
      return $this;
    }

    /**
     * @return RouteNodeIdentifier
     */
    public function getNodeIdentifier()
    {
      return $this->NodeIdentifier;
    }

    /**
     * @param RouteNodeIdentifier $NodeIdentifier
     * @return RouteNode
     */
    public function setNodeIdentifier($NodeIdentifier)
    {
      $this->NodeIdentifier = $NodeIdentifier;
      return $this;
    }

    /**
     * @return ParserInformation
     */
    public function getParserData()
    {
      return $this->ParserData;
    }

    /**
     * @param ParserInformation $ParserData
     * @return RouteNode
     */
    public function setParserData($ParserData)
    {
      $this->ParserData = $ParserData;
      return $this;
    }

    /**
     * @return RouteNodeType
     */
    public function getRouteNodeType()
    {
      return $this->RouteNodeType;
    }

    /**
     * @param RouteNodeType $RouteNodeType
     * @return RouteNode
     */
    public function setRouteNodeType($RouteNodeType)
    {
      $this->RouteNodeType = $RouteNodeType;
      return $this;
    }

}
