<?php

class BulkStoredRouteResponse
{

    /**
     * @var ArrayOfBulkRouteInformation $FoundRoutes
     */
    protected $FoundRoutes = null;

    /**
     * @var int $NumberOfRoutes
     */
    protected $NumberOfRoutes = null;

    /**
     * @var int $TotalNumberOfAvailableRoutes
     */
    protected $TotalNumberOfAvailableRoutes = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfBulkRouteInformation
     */
    public function getFoundRoutes()
    {
      return $this->FoundRoutes;
    }

    /**
     * @param ArrayOfBulkRouteInformation $FoundRoutes
     * @return BulkStoredRouteResponse
     */
    public function setFoundRoutes($FoundRoutes)
    {
      $this->FoundRoutes = $FoundRoutes;
      return $this;
    }

    /**
     * @return int
     */
    public function getNumberOfRoutes()
    {
      return $this->NumberOfRoutes;
    }

    /**
     * @param int $NumberOfRoutes
     * @return BulkStoredRouteResponse
     */
    public function setNumberOfRoutes($NumberOfRoutes)
    {
      $this->NumberOfRoutes = $NumberOfRoutes;
      return $this;
    }

    /**
     * @return int
     */
    public function getTotalNumberOfAvailableRoutes()
    {
      return $this->TotalNumberOfAvailableRoutes;
    }

    /**
     * @param int $TotalNumberOfAvailableRoutes
     * @return BulkStoredRouteResponse
     */
    public function setTotalNumberOfAvailableRoutes($TotalNumberOfAvailableRoutes)
    {
      $this->TotalNumberOfAvailableRoutes = $TotalNumberOfAvailableRoutes;
      return $this;
    }

}
