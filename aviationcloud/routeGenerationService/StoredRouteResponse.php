<?php

class StoredRouteResponse
{

    /**
     * @var ArrayOfRouteInformation $FoundRoutes
     */
    protected $FoundRoutes = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfRouteInformation
     */
    public function getFoundRoutes()
    {
      return $this->FoundRoutes;
    }

    /**
     * @param ArrayOfRouteInformation $FoundRoutes
     * @return StoredRouteResponse
     */
    public function setFoundRoutes($FoundRoutes)
    {
      $this->FoundRoutes = $FoundRoutes;
      return $this;
    }

}
