<?php

class BulkRetrieveStoredRoutesResponse
{

    /**
     * @var BulkStoredRouteResponse $BulkRetrieveStoredRoutesResult
     */
    protected $BulkRetrieveStoredRoutesResult = null;

    /**
     * @param BulkStoredRouteResponse $BulkRetrieveStoredRoutesResult
     */
    public function __construct($BulkRetrieveStoredRoutesResult)
    {
      $this->BulkRetrieveStoredRoutesResult = $BulkRetrieveStoredRoutesResult;
    }

    /**
     * @return BulkStoredRouteResponse
     */
    public function getBulkRetrieveStoredRoutesResult()
    {
      return $this->BulkRetrieveStoredRoutesResult;
    }

    /**
     * @param BulkStoredRouteResponse $BulkRetrieveStoredRoutesResult
     * @return BulkRetrieveStoredRoutesResponse
     */
    public function setBulkRetrieveStoredRoutesResult($BulkRetrieveStoredRoutesResult)
    {
      $this->BulkRetrieveStoredRoutesResult = $BulkRetrieveStoredRoutesResult;
      return $this;
    }

}
