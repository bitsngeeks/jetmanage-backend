<?php

class TurbulencePoint
{

    /**
     * @var Altitude $Altitude
     */
    protected $Altitude = null;

    /**
     * @var float $EDR
     */
    protected $EDR = null;

    /**
     * @var SphereicPoint $Position
     */
    protected $Position = null;

    /**
     * @var \DateTime $Time
     */
    protected $Time = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Altitude
     */
    public function getAltitude()
    {
      return $this->Altitude;
    }

    /**
     * @param Altitude $Altitude
     * @return TurbulencePoint
     */
    public function setAltitude($Altitude)
    {
      $this->Altitude = $Altitude;
      return $this;
    }

    /**
     * @return float
     */
    public function getEDR()
    {
      return $this->EDR;
    }

    /**
     * @param float $EDR
     * @return TurbulencePoint
     */
    public function setEDR($EDR)
    {
      $this->EDR = $EDR;
      return $this;
    }

    /**
     * @return SphereicPoint
     */
    public function getPosition()
    {
      return $this->Position;
    }

    /**
     * @param SphereicPoint $Position
     * @return TurbulencePoint
     */
    public function setPosition($Position)
    {
      $this->Position = $Position;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getTime()
    {
      if ($this->Time == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->Time);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $Time
     * @return TurbulencePoint
     */
    public function setTime(\DateTime $Time = null)
    {
      if ($Time == null) {
       $this->Time = null;
      } else {
        $this->Time = $Time->format(\DateTime::ATOM);
      }
      return $this;
    }

}
