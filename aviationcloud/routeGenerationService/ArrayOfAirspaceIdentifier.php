<?php

class ArrayOfAirspaceIdentifier implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var AirspaceIdentifier[] $AirspaceIdentifier
     */
    protected $AirspaceIdentifier = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return AirspaceIdentifier[]
     */
    public function getAirspaceIdentifier()
    {
      return $this->AirspaceIdentifier;
    }

    /**
     * @param AirspaceIdentifier[] $AirspaceIdentifier
     * @return ArrayOfAirspaceIdentifier
     */
    public function setAirspaceIdentifier(array $AirspaceIdentifier = null)
    {
      $this->AirspaceIdentifier = $AirspaceIdentifier;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->AirspaceIdentifier[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return AirspaceIdentifier
     */
    public function offsetGet($offset)
    {
      return $this->AirspaceIdentifier[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param AirspaceIdentifier $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->AirspaceIdentifier[] = $value;
      } else {
        $this->AirspaceIdentifier[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->AirspaceIdentifier[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return AirspaceIdentifier Return the current element
     */
    public function current()
    {
      return current($this->AirspaceIdentifier);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->AirspaceIdentifier);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->AirspaceIdentifier);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->AirspaceIdentifier);
    }

    /**
     * Countable implementation
     *
     * @return AirspaceIdentifier Return count of elements
     */
    public function count()
    {
      return count($this->AirspaceIdentifier);
    }

}
