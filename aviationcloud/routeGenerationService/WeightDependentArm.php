<?php

class WeightDependentArm
{

    /**
     * @var float $Arm
     */
    protected $Arm = null;

    /**
     * @var Weight $Weight
     */
    protected $Weight = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getArm()
    {
      return $this->Arm;
    }

    /**
     * @param float $Arm
     * @return WeightDependentArm
     */
    public function setArm($Arm)
    {
      $this->Arm = $Arm;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getWeight()
    {
      return $this->Weight;
    }

    /**
     * @param Weight $Weight
     * @return WeightDependentArm
     */
    public function setWeight($Weight)
    {
      $this->Weight = $Weight;
      return $this;
    }

}
