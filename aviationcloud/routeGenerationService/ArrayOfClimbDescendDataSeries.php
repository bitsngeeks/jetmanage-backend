<?php

class ArrayOfClimbDescendDataSeries implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var ClimbDescendDataSeries[] $ClimbDescendDataSeries
     */
    protected $ClimbDescendDataSeries = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ClimbDescendDataSeries[]
     */
    public function getClimbDescendDataSeries()
    {
      return $this->ClimbDescendDataSeries;
    }

    /**
     * @param ClimbDescendDataSeries[] $ClimbDescendDataSeries
     * @return ArrayOfClimbDescendDataSeries
     */
    public function setClimbDescendDataSeries(array $ClimbDescendDataSeries = null)
    {
      $this->ClimbDescendDataSeries = $ClimbDescendDataSeries;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->ClimbDescendDataSeries[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return ClimbDescendDataSeries
     */
    public function offsetGet($offset)
    {
      return $this->ClimbDescendDataSeries[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param ClimbDescendDataSeries $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->ClimbDescendDataSeries[] = $value;
      } else {
        $this->ClimbDescendDataSeries[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->ClimbDescendDataSeries[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return ClimbDescendDataSeries Return the current element
     */
    public function current()
    {
      return current($this->ClimbDescendDataSeries);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->ClimbDescendDataSeries);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->ClimbDescendDataSeries);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->ClimbDescendDataSeries);
    }

    /**
     * Countable implementation
     *
     * @return ClimbDescendDataSeries Return count of elements
     */
    public function count()
    {
      return count($this->ClimbDescendDataSeries);
    }

}
