<?php

class RADCityPairLevelCapping
{

    /**
     * @var int $MaximumFlightLevel
     */
    protected $MaximumFlightLevel = null;

    /**
     * @var int $MinimumFlightlevel
     */
    protected $MinimumFlightlevel = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getMaximumFlightLevel()
    {
      return $this->MaximumFlightLevel;
    }

    /**
     * @param int $MaximumFlightLevel
     * @return RADCityPairLevelCapping
     */
    public function setMaximumFlightLevel($MaximumFlightLevel)
    {
      $this->MaximumFlightLevel = $MaximumFlightLevel;
      return $this;
    }

    /**
     * @return int
     */
    public function getMinimumFlightlevel()
    {
      return $this->MinimumFlightlevel;
    }

    /**
     * @param int $MinimumFlightlevel
     * @return RADCityPairLevelCapping
     */
    public function setMinimumFlightlevel($MinimumFlightlevel)
    {
      $this->MinimumFlightlevel = $MinimumFlightlevel;
      return $this;
    }

}
