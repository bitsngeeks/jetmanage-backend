<?php

class PathOptions
{

    /**
     * @var ArrayOfArea $AvoidAreas
     */
    protected $AvoidAreas = null;

    /**
     * @var ArrayOfAirspaceIdentifier $AvoidFIRS
     */
    protected $AvoidFIRS = null;

    /**
     * @var ETOPSSpecification $ETOPSDefinition
     */
    protected $ETOPSDefinition = null;

    /**
     * @var ArrayOfRouteNodeIdentifier $NotViaPoints
     */
    protected $NotViaPoints = null;

    /**
     * @var ProcedureOptions $NotViaProcedures
     */
    protected $NotViaProcedures = null;

    /**
     * @var boolean $RelaxedRouteTemplateParsing
     */
    protected $RelaxedRouteTemplateParsing = null;

    /**
     * @var string $RouteTemplate
     */
    protected $RouteTemplate = null;

    /**
     * @var TransitionPointSpecification $TransitionPointDefinition
     */
    protected $TransitionPointDefinition = null;

    /**
     * @var ArrayOfRouteNodeIdentifier $ViaPoints
     */
    protected $ViaPoints = null;

    /**
     * @var ProcedureOptions $ViaProcedures
     */
    protected $ViaProcedures = null;

    /**
     * @var TrackIdentifier $ViaTrack
     */
    protected $ViaTrack = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfArea
     */
    public function getAvoidAreas()
    {
      return $this->AvoidAreas;
    }

    /**
     * @param ArrayOfArea $AvoidAreas
     * @return PathOptions
     */
    public function setAvoidAreas($AvoidAreas)
    {
      $this->AvoidAreas = $AvoidAreas;
      return $this;
    }

    /**
     * @return ArrayOfAirspaceIdentifier
     */
    public function getAvoidFIRS()
    {
      return $this->AvoidFIRS;
    }

    /**
     * @param ArrayOfAirspaceIdentifier $AvoidFIRS
     * @return PathOptions
     */
    public function setAvoidFIRS($AvoidFIRS)
    {
      $this->AvoidFIRS = $AvoidFIRS;
      return $this;
    }

    /**
     * @return ETOPSSpecification
     */
    public function getETOPSDefinition()
    {
      return $this->ETOPSDefinition;
    }

    /**
     * @param ETOPSSpecification $ETOPSDefinition
     * @return PathOptions
     */
    public function setETOPSDefinition($ETOPSDefinition)
    {
      $this->ETOPSDefinition = $ETOPSDefinition;
      return $this;
    }

    /**
     * @return ArrayOfRouteNodeIdentifier
     */
    public function getNotViaPoints()
    {
      return $this->NotViaPoints;
    }

    /**
     * @param ArrayOfRouteNodeIdentifier $NotViaPoints
     * @return PathOptions
     */
    public function setNotViaPoints($NotViaPoints)
    {
      $this->NotViaPoints = $NotViaPoints;
      return $this;
    }

    /**
     * @return ProcedureOptions
     */
    public function getNotViaProcedures()
    {
      return $this->NotViaProcedures;
    }

    /**
     * @param ProcedureOptions $NotViaProcedures
     * @return PathOptions
     */
    public function setNotViaProcedures($NotViaProcedures)
    {
      $this->NotViaProcedures = $NotViaProcedures;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRelaxedRouteTemplateParsing()
    {
      return $this->RelaxedRouteTemplateParsing;
    }

    /**
     * @param boolean $RelaxedRouteTemplateParsing
     * @return PathOptions
     */
    public function setRelaxedRouteTemplateParsing($RelaxedRouteTemplateParsing)
    {
      $this->RelaxedRouteTemplateParsing = $RelaxedRouteTemplateParsing;
      return $this;
    }

    /**
     * @return string
     */
    public function getRouteTemplate()
    {
      return $this->RouteTemplate;
    }

    /**
     * @param string $RouteTemplate
     * @return PathOptions
     */
    public function setRouteTemplate($RouteTemplate)
    {
      $this->RouteTemplate = $RouteTemplate;
      return $this;
    }

    /**
     * @return TransitionPointSpecification
     */
    public function getTransitionPointDefinition()
    {
      return $this->TransitionPointDefinition;
    }

    /**
     * @param TransitionPointSpecification $TransitionPointDefinition
     * @return PathOptions
     */
    public function setTransitionPointDefinition($TransitionPointDefinition)
    {
      $this->TransitionPointDefinition = $TransitionPointDefinition;
      return $this;
    }

    /**
     * @return ArrayOfRouteNodeIdentifier
     */
    public function getViaPoints()
    {
      return $this->ViaPoints;
    }

    /**
     * @param ArrayOfRouteNodeIdentifier $ViaPoints
     * @return PathOptions
     */
    public function setViaPoints($ViaPoints)
    {
      $this->ViaPoints = $ViaPoints;
      return $this;
    }

    /**
     * @return ProcedureOptions
     */
    public function getViaProcedures()
    {
      return $this->ViaProcedures;
    }

    /**
     * @param ProcedureOptions $ViaProcedures
     * @return PathOptions
     */
    public function setViaProcedures($ViaProcedures)
    {
      $this->ViaProcedures = $ViaProcedures;
      return $this;
    }

    /**
     * @return TrackIdentifier
     */
    public function getViaTrack()
    {
      return $this->ViaTrack;
    }

    /**
     * @param TrackIdentifier $ViaTrack
     * @return PathOptions
     */
    public function setViaTrack($ViaTrack)
    {
      $this->ViaTrack = $ViaTrack;
      return $this;
    }

}
