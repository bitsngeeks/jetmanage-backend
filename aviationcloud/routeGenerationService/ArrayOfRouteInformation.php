<?php

class ArrayOfRouteInformation implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var RouteInformation[] $RouteInformation
     */
    protected $RouteInformation = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return RouteInformation[]
     */
    public function getRouteInformation()
    {
      return $this->RouteInformation;
    }

    /**
     * @param RouteInformation[] $RouteInformation
     * @return ArrayOfRouteInformation
     */
    public function setRouteInformation(array $RouteInformation = null)
    {
      $this->RouteInformation = $RouteInformation;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->RouteInformation[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return RouteInformation
     */
    public function offsetGet($offset)
    {
      return $this->RouteInformation[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param RouteInformation $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->RouteInformation[] = $value;
      } else {
        $this->RouteInformation[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->RouteInformation[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return RouteInformation Return the current element
     */
    public function current()
    {
      return current($this->RouteInformation);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->RouteInformation);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->RouteInformation);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->RouteInformation);
    }

    /**
     * Countable implementation
     *
     * @return RouteInformation Return count of elements
     */
    public function count()
    {
      return count($this->RouteInformation);
    }

}
