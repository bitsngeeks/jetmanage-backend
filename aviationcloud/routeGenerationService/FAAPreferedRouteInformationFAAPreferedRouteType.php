<?php

class FAAPreferedRouteInformationFAAPreferedRouteType
{
    const __default = 'LowAltitude';
    const LowAltitude = 'LowAltitude';
    const HighAltitude = 'HighAltitude';
    const LowerAltitudeSingleDirection = 'LowerAltitudeSingleDirection';
    const HighALtitudeSingleDirection = 'HighALtitudeSingleDirection';
    const SpeicalLowAltitudeDirectional = 'SpeicalLowAltitudeDirectional';
    const SpecialHighAltitudeDirectional = 'SpecialHighAltitudeDirectional';
    const TowerEnrouteControl = 'TowerEnrouteControl';


}
