<?php

class VerticalOptimizeRouteResponse
{

    /**
     * @var VerticalOptimizationResult $VerticalOptimizeRouteResult
     */
    protected $VerticalOptimizeRouteResult = null;

    /**
     * @param VerticalOptimizationResult $VerticalOptimizeRouteResult
     */
    public function __construct($VerticalOptimizeRouteResult)
    {
      $this->VerticalOptimizeRouteResult = $VerticalOptimizeRouteResult;
    }

    /**
     * @return VerticalOptimizationResult
     */
    public function getVerticalOptimizeRouteResult()
    {
      return $this->VerticalOptimizeRouteResult;
    }

    /**
     * @param VerticalOptimizationResult $VerticalOptimizeRouteResult
     * @return VerticalOptimizeRouteResponse
     */
    public function setVerticalOptimizeRouteResult($VerticalOptimizeRouteResult)
    {
      $this->VerticalOptimizeRouteResult = $VerticalOptimizeRouteResult;
      return $this;
    }

}
