<?php

class ArrayOfCountryInformation implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var CountryInformation[] $CountryInformation
     */
    protected $CountryInformation = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return CountryInformation[]
     */
    public function getCountryInformation()
    {
      return $this->CountryInformation;
    }

    /**
     * @param CountryInformation[] $CountryInformation
     * @return ArrayOfCountryInformation
     */
    public function setCountryInformation(array $CountryInformation = null)
    {
      $this->CountryInformation = $CountryInformation;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->CountryInformation[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return CountryInformation
     */
    public function offsetGet($offset)
    {
      return $this->CountryInformation[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param CountryInformation $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->CountryInformation[] = $value;
      } else {
        $this->CountryInformation[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->CountryInformation[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return CountryInformation Return the current element
     */
    public function current()
    {
      return current($this->CountryInformation);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->CountryInformation);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->CountryInformation);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->CountryInformation);
    }

    /**
     * Countable implementation
     *
     * @return CountryInformation Return count of elements
     */
    public function count()
    {
      return count($this->CountryInformation);
    }

}
