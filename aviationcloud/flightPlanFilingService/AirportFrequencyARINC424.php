<?php

class AirportFrequencyARINC424
{

    /**
     * @var string $Callsign
     */
    protected $Callsign = null;

    /**
     * @var float $Frequency
     */
    protected $Frequency = null;

    /**
     * @var string $RemoteFacility
     */
    protected $RemoteFacility = null;

    /**
     * @var string $ServiceIndicator
     */
    protected $ServiceIndicator = null;

    /**
     * @var FrequencyType $Type
     */
    protected $Type = null;

    /**
     * @var FrequencyUnit $Unit
     */
    protected $Unit = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getCallsign()
    {
      return $this->Callsign;
    }

    /**
     * @param string $Callsign
     * @return AirportFrequencyARINC424
     */
    public function setCallsign($Callsign)
    {
      $this->Callsign = $Callsign;
      return $this;
    }

    /**
     * @return float
     */
    public function getFrequency()
    {
      return $this->Frequency;
    }

    /**
     * @param float $Frequency
     * @return AirportFrequencyARINC424
     */
    public function setFrequency($Frequency)
    {
      $this->Frequency = $Frequency;
      return $this;
    }

    /**
     * @return string
     */
    public function getRemoteFacility()
    {
      return $this->RemoteFacility;
    }

    /**
     * @param string $RemoteFacility
     * @return AirportFrequencyARINC424
     */
    public function setRemoteFacility($RemoteFacility)
    {
      $this->RemoteFacility = $RemoteFacility;
      return $this;
    }

    /**
     * @return string
     */
    public function getServiceIndicator()
    {
      return $this->ServiceIndicator;
    }

    /**
     * @param string $ServiceIndicator
     * @return AirportFrequencyARINC424
     */
    public function setServiceIndicator($ServiceIndicator)
    {
      $this->ServiceIndicator = $ServiceIndicator;
      return $this;
    }

    /**
     * @return FrequencyType
     */
    public function getType()
    {
      return $this->Type;
    }

    /**
     * @param FrequencyType $Type
     * @return AirportFrequencyARINC424
     */
    public function setType($Type)
    {
      $this->Type = $Type;
      return $this;
    }

    /**
     * @return FrequencyUnit
     */
    public function getUnit()
    {
      return $this->Unit;
    }

    /**
     * @param FrequencyUnit $Unit
     * @return AirportFrequencyARINC424
     */
    public function setUnit($Unit)
    {
      $this->Unit = $Unit;
      return $this;
    }

}
