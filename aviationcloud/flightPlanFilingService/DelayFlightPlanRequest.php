<?php

class DelayFlightPlanRequest extends RequestBase
{

    /**
     * @var EmulatorResponseOptions $EmulatorResponseOptions
     */
    protected $EmulatorResponseOptions = null;

    /**
     * @var boolean $EnableProductionDelay
     */
    protected $EnableProductionDelay = null;

    /**
     * @var int $FlightId
     */
    protected $FlightId = null;

    /**
     * @var string $PreferredTransportProvider
     */
    protected $PreferredTransportProvider = null;

    /**
     * @var \DateTime $UpdatedTimeOfDeparture
     */
    protected $UpdatedTimeOfDeparture = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return EmulatorResponseOptions
     */
    public function getEmulatorResponseOptions()
    {
      return $this->EmulatorResponseOptions;
    }

    /**
     * @param EmulatorResponseOptions $EmulatorResponseOptions
     * @return DelayFlightPlanRequest
     */
    public function setEmulatorResponseOptions($EmulatorResponseOptions)
    {
      $this->EmulatorResponseOptions = $EmulatorResponseOptions;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getEnableProductionDelay()
    {
      return $this->EnableProductionDelay;
    }

    /**
     * @param boolean $EnableProductionDelay
     * @return DelayFlightPlanRequest
     */
    public function setEnableProductionDelay($EnableProductionDelay)
    {
      $this->EnableProductionDelay = $EnableProductionDelay;
      return $this;
    }

    /**
     * @return int
     */
    public function getFlightId()
    {
      return $this->FlightId;
    }

    /**
     * @param int $FlightId
     * @return DelayFlightPlanRequest
     */
    public function setFlightId($FlightId)
    {
      $this->FlightId = $FlightId;
      return $this;
    }

    /**
     * @return string
     */
    public function getPreferredTransportProvider()
    {
      return $this->PreferredTransportProvider;
    }

    /**
     * @param string $PreferredTransportProvider
     * @return DelayFlightPlanRequest
     */
    public function setPreferredTransportProvider($PreferredTransportProvider)
    {
      $this->PreferredTransportProvider = $PreferredTransportProvider;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedTimeOfDeparture()
    {
      if ($this->UpdatedTimeOfDeparture == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->UpdatedTimeOfDeparture);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $UpdatedTimeOfDeparture
     * @return DelayFlightPlanRequest
     */
    public function setUpdatedTimeOfDeparture(\DateTime $UpdatedTimeOfDeparture = null)
    {
      if ($UpdatedTimeOfDeparture == null) {
       $this->UpdatedTimeOfDeparture = null;
      } else {
        $this->UpdatedTimeOfDeparture = $UpdatedTimeOfDeparture->format(\DateTime::ATOM);
      }
      return $this;
    }

}
