<?php

class SubscribeAFTNMessages
{

    /**
     * @var AFTNMessageSubscriptionRequest $request
     */
    protected $request = null;

    /**
     * @param AFTNMessageSubscriptionRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return AFTNMessageSubscriptionRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param AFTNMessageSubscriptionRequest $request
     * @return SubscribeAFTNMessages
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
