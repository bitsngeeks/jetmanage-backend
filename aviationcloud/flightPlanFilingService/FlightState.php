<?php

class FlightState
{
    const __default = 'Ramp';
    const Ramp = 'Ramp';
    const Climb = 'Climb';
    const Cruise = 'Cruise';
    const Descend = 'Descend';
    const OnBlock = 'OnBlock';


}
