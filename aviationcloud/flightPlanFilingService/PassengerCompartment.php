<?php

class PassengerCompartment extends AircraftStructureUnit
{

    /**
     * @var int $MaxNumberOfPassengers
     */
    protected $MaxNumberOfPassengers = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return int
     */
    public function getMaxNumberOfPassengers()
    {
      return $this->MaxNumberOfPassengers;
    }

    /**
     * @param int $MaxNumberOfPassengers
     * @return PassengerCompartment
     */
    public function setMaxNumberOfPassengers($MaxNumberOfPassengers)
    {
      $this->MaxNumberOfPassengers = $MaxNumberOfPassengers;
      return $this;
    }

}
