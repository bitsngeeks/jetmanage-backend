<?php

class EmulatorAFTNResponse
{

    /**
     * @var string $AFTNAddress
     */
    protected $AFTNAddress = null;

    /**
     * @var string $ResponseMessage
     */
    protected $ResponseMessage = null;

    /**
     * @var int $ResponseTimeMs
     */
    protected $ResponseTimeMs = null;

    /**
     * @var string $ResponseTypeTemplate
     */
    protected $ResponseTypeTemplate = null;

    /**
     * @var int $Sequence
     */
    protected $Sequence = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getAFTNAddress()
    {
      return $this->AFTNAddress;
    }

    /**
     * @param string $AFTNAddress
     * @return EmulatorAFTNResponse
     */
    public function setAFTNAddress($AFTNAddress)
    {
      $this->AFTNAddress = $AFTNAddress;
      return $this;
    }

    /**
     * @return string
     */
    public function getResponseMessage()
    {
      return $this->ResponseMessage;
    }

    /**
     * @param string $ResponseMessage
     * @return EmulatorAFTNResponse
     */
    public function setResponseMessage($ResponseMessage)
    {
      $this->ResponseMessage = $ResponseMessage;
      return $this;
    }

    /**
     * @return int
     */
    public function getResponseTimeMs()
    {
      return $this->ResponseTimeMs;
    }

    /**
     * @param int $ResponseTimeMs
     * @return EmulatorAFTNResponse
     */
    public function setResponseTimeMs($ResponseTimeMs)
    {
      $this->ResponseTimeMs = $ResponseTimeMs;
      return $this;
    }

    /**
     * @return string
     */
    public function getResponseTypeTemplate()
    {
      return $this->ResponseTypeTemplate;
    }

    /**
     * @param string $ResponseTypeTemplate
     * @return EmulatorAFTNResponse
     */
    public function setResponseTypeTemplate($ResponseTypeTemplate)
    {
      $this->ResponseTypeTemplate = $ResponseTypeTemplate;
      return $this;
    }

    /**
     * @return int
     */
    public function getSequence()
    {
      return $this->Sequence;
    }

    /**
     * @param int $Sequence
     * @return EmulatorAFTNResponse
     */
    public function setSequence($Sequence)
    {
      $this->Sequence = $Sequence;
      return $this;
    }

}
