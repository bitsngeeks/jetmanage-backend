<?php

class AircraftPerformanceProfile
{

    /**
     * @var int $Index
     */
    protected $Index = null;

    /**
     * @var AircrraftPerformanceProfileIndexType $IndexType
     */
    protected $IndexType = null;

    /**
     * @var int $ProfileId
     */
    protected $ProfileId = null;

    /**
     * @var string $ProfileName
     */
    protected $ProfileName = null;

    /**
     * @var string $ProfileType
     */
    protected $ProfileType = null;

    /**
     * @var string $ProfileUUID
     */
    protected $ProfileUUID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getIndex()
    {
      return $this->Index;
    }

    /**
     * @param int $Index
     * @return AircraftPerformanceProfile
     */
    public function setIndex($Index)
    {
      $this->Index = $Index;
      return $this;
    }

    /**
     * @return AircrraftPerformanceProfileIndexType
     */
    public function getIndexType()
    {
      return $this->IndexType;
    }

    /**
     * @param AircrraftPerformanceProfileIndexType $IndexType
     * @return AircraftPerformanceProfile
     */
    public function setIndexType($IndexType)
    {
      $this->IndexType = $IndexType;
      return $this;
    }

    /**
     * @return int
     */
    public function getProfileId()
    {
      return $this->ProfileId;
    }

    /**
     * @param int $ProfileId
     * @return AircraftPerformanceProfile
     */
    public function setProfileId($ProfileId)
    {
      $this->ProfileId = $ProfileId;
      return $this;
    }

    /**
     * @return string
     */
    public function getProfileName()
    {
      return $this->ProfileName;
    }

    /**
     * @param string $ProfileName
     * @return AircraftPerformanceProfile
     */
    public function setProfileName($ProfileName)
    {
      $this->ProfileName = $ProfileName;
      return $this;
    }

    /**
     * @return string
     */
    public function getProfileType()
    {
      return $this->ProfileType;
    }

    /**
     * @param string $ProfileType
     * @return AircraftPerformanceProfile
     */
    public function setProfileType($ProfileType)
    {
      $this->ProfileType = $ProfileType;
      return $this;
    }

    /**
     * @return string
     */
    public function getProfileUUID()
    {
      return $this->ProfileUUID;
    }

    /**
     * @param string $ProfileUUID
     * @return AircraftPerformanceProfile
     */
    public function setProfileUUID($ProfileUUID)
    {
      $this->ProfileUUID = $ProfileUUID;
      return $this;
    }

}
