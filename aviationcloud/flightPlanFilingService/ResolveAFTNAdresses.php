<?php

class ResolveAFTNAdresses
{

    /**
     * @var AFTNResolveRequest $request
     */
    protected $request = null;

    /**
     * @param AFTNResolveRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return AFTNResolveRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param AFTNResolveRequest $request
     * @return ResolveAFTNAdresses
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
