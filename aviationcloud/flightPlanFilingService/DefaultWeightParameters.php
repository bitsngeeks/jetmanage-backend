<?php

class DefaultWeightParameters
{

    /**
     * @var Weight $LuggagePerPax
     */
    protected $LuggagePerPax = null;

    /**
     * @var Weight $PaxAdultWeight
     */
    protected $PaxAdultWeight = null;

    /**
     * @var Weight $PaxChildWeight
     */
    protected $PaxChildWeight = null;

    /**
     * @var Weight $PaxFemaleWeight
     */
    protected $PaxFemaleWeight = null;

    /**
     * @var Weight $PaxInfantWeight
     */
    protected $PaxInfantWeight = null;

    /**
     * @var Weight $PaxMaleWeight
     */
    protected $PaxMaleWeight = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Weight
     */
    public function getLuggagePerPax()
    {
      return $this->LuggagePerPax;
    }

    /**
     * @param Weight $LuggagePerPax
     * @return DefaultWeightParameters
     */
    public function setLuggagePerPax($LuggagePerPax)
    {
      $this->LuggagePerPax = $LuggagePerPax;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getPaxAdultWeight()
    {
      return $this->PaxAdultWeight;
    }

    /**
     * @param Weight $PaxAdultWeight
     * @return DefaultWeightParameters
     */
    public function setPaxAdultWeight($PaxAdultWeight)
    {
      $this->PaxAdultWeight = $PaxAdultWeight;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getPaxChildWeight()
    {
      return $this->PaxChildWeight;
    }

    /**
     * @param Weight $PaxChildWeight
     * @return DefaultWeightParameters
     */
    public function setPaxChildWeight($PaxChildWeight)
    {
      $this->PaxChildWeight = $PaxChildWeight;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getPaxFemaleWeight()
    {
      return $this->PaxFemaleWeight;
    }

    /**
     * @param Weight $PaxFemaleWeight
     * @return DefaultWeightParameters
     */
    public function setPaxFemaleWeight($PaxFemaleWeight)
    {
      $this->PaxFemaleWeight = $PaxFemaleWeight;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getPaxInfantWeight()
    {
      return $this->PaxInfantWeight;
    }

    /**
     * @param Weight $PaxInfantWeight
     * @return DefaultWeightParameters
     */
    public function setPaxInfantWeight($PaxInfantWeight)
    {
      $this->PaxInfantWeight = $PaxInfantWeight;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getPaxMaleWeight()
    {
      return $this->PaxMaleWeight;
    }

    /**
     * @param Weight $PaxMaleWeight
     * @return DefaultWeightParameters
     */
    public function setPaxMaleWeight($PaxMaleWeight)
    {
      $this->PaxMaleWeight = $PaxMaleWeight;
      return $this;
    }

}
