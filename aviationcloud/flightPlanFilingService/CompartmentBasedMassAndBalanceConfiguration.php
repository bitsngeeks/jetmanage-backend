<?php

class CompartmentBasedMassAndBalanceConfiguration extends MassAndBalanceConfigurationBase
{

    /**
     * @var float $DryArm
     */
    protected $DryArm = null;

    /**
     * @var ArrayOfPassengerCompartment $PassengerCompartments
     */
    protected $PassengerCompartments = null;

    /**
     * @var ArrayOfCargoCompartment $CargoCompartments
     */
    protected $CargoCompartments = null;

    /**
     * @var ArrayOfCrewCompartment $CrewCompartments
     */
    protected $CrewCompartments = null;

    /**
     * @var DefaultWeightParameters $DefaultWeightParameters
     */
    protected $DefaultWeightParameters = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return float
     */
    public function getDryArm()
    {
      return $this->DryArm;
    }

    /**
     * @param float $DryArm
     * @return CompartmentBasedMassAndBalanceConfiguration
     */
    public function setDryArm($DryArm)
    {
      $this->DryArm = $DryArm;
      return $this;
    }

    /**
     * @return ArrayOfPassengerCompartment
     */
    public function getPassengerCompartments()
    {
      return $this->PassengerCompartments;
    }

    /**
     * @param ArrayOfPassengerCompartment $PassengerCompartments
     * @return CompartmentBasedMassAndBalanceConfiguration
     */
    public function setPassengerCompartments($PassengerCompartments)
    {
      $this->PassengerCompartments = $PassengerCompartments;
      return $this;
    }

    /**
     * @return ArrayOfCargoCompartment
     */
    public function getCargoCompartments()
    {
      return $this->CargoCompartments;
    }

    /**
     * @param ArrayOfCargoCompartment $CargoCompartments
     * @return CompartmentBasedMassAndBalanceConfiguration
     */
    public function setCargoCompartments($CargoCompartments)
    {
      $this->CargoCompartments = $CargoCompartments;
      return $this;
    }

    /**
     * @return ArrayOfCrewCompartment
     */
    public function getCrewCompartments()
    {
      return $this->CrewCompartments;
    }

    /**
     * @param ArrayOfCrewCompartment $CrewCompartments
     * @return CompartmentBasedMassAndBalanceConfiguration
     */
    public function setCrewCompartments($CrewCompartments)
    {
      $this->CrewCompartments = $CrewCompartments;
      return $this;
    }

    /**
     * @return DefaultWeightParameters
     */
    public function getDefaultWeightParameters()
    {
      return $this->DefaultWeightParameters;
    }

    /**
     * @param DefaultWeightParameters $DefaultWeightParameters
     * @return CompartmentBasedMassAndBalanceConfiguration
     */
    public function setDefaultWeightParameters($DefaultWeightParameters)
    {
      $this->DefaultWeightParameters = $DefaultWeightParameters;
      return $this;
    }

}
