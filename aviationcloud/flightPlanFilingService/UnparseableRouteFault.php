<?php

class UnparseableRouteFault
{

    /**
     * @var string $ErrorCode
     */
    protected $ErrorCode = null;

    /**
     * @var string $ErrorText
     */
    protected $ErrorText = null;

    /**
     * @var string $FailedToken
     */
    protected $FailedToken = null;

    /**
     * @var string $ProvidedRoute
     */
    protected $ProvidedRoute = null;

    /**
     * @var ArrayOfFlightValidationError $ValidationErrors
     */
    protected $ValidationErrors = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getErrorCode()
    {
      return $this->ErrorCode;
    }

    /**
     * @param string $ErrorCode
     * @return UnparseableRouteFault
     */
    public function setErrorCode($ErrorCode)
    {
      $this->ErrorCode = $ErrorCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getErrorText()
    {
      return $this->ErrorText;
    }

    /**
     * @param string $ErrorText
     * @return UnparseableRouteFault
     */
    public function setErrorText($ErrorText)
    {
      $this->ErrorText = $ErrorText;
      return $this;
    }

    /**
     * @return string
     */
    public function getFailedToken()
    {
      return $this->FailedToken;
    }

    /**
     * @param string $FailedToken
     * @return UnparseableRouteFault
     */
    public function setFailedToken($FailedToken)
    {
      $this->FailedToken = $FailedToken;
      return $this;
    }

    /**
     * @return string
     */
    public function getProvidedRoute()
    {
      return $this->ProvidedRoute;
    }

    /**
     * @param string $ProvidedRoute
     * @return UnparseableRouteFault
     */
    public function setProvidedRoute($ProvidedRoute)
    {
      $this->ProvidedRoute = $ProvidedRoute;
      return $this;
    }

    /**
     * @return ArrayOfFlightValidationError
     */
    public function getValidationErrors()
    {
      return $this->ValidationErrors;
    }

    /**
     * @param ArrayOfFlightValidationError $ValidationErrors
     * @return UnparseableRouteFault
     */
    public function setValidationErrors($ValidationErrors)
    {
      $this->ValidationErrors = $ValidationErrors;
      return $this;
    }

}
