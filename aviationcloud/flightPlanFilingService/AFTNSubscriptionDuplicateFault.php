<?php

class AFTNSubscriptionDuplicateFault
{

    /**
     * @var string $ErrorCode
     */
    protected $ErrorCode = null;

    /**
     * @var int $ExistingSubscriptionId
     */
    protected $ExistingSubscriptionId = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getErrorCode()
    {
      return $this->ErrorCode;
    }

    /**
     * @param string $ErrorCode
     * @return AFTNSubscriptionDuplicateFault
     */
    public function setErrorCode($ErrorCode)
    {
      $this->ErrorCode = $ErrorCode;
      return $this;
    }

    /**
     * @return int
     */
    public function getExistingSubscriptionId()
    {
      return $this->ExistingSubscriptionId;
    }

    /**
     * @param int $ExistingSubscriptionId
     * @return AFTNSubscriptionDuplicateFault
     */
    public function setExistingSubscriptionId($ExistingSubscriptionId)
    {
      $this->ExistingSubscriptionId = $ExistingSubscriptionId;
      return $this;
    }

}
