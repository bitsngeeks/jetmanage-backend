<?php

class ArrayOfEmulatorAFTNResponse implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var EmulatorAFTNResponse[] $EmulatorAFTNResponse
     */
    protected $EmulatorAFTNResponse = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return EmulatorAFTNResponse[]
     */
    public function getEmulatorAFTNResponse()
    {
      return $this->EmulatorAFTNResponse;
    }

    /**
     * @param EmulatorAFTNResponse[] $EmulatorAFTNResponse
     * @return ArrayOfEmulatorAFTNResponse
     */
    public function setEmulatorAFTNResponse(array $EmulatorAFTNResponse = null)
    {
      $this->EmulatorAFTNResponse = $EmulatorAFTNResponse;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->EmulatorAFTNResponse[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return EmulatorAFTNResponse
     */
    public function offsetGet($offset)
    {
      return $this->EmulatorAFTNResponse[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param EmulatorAFTNResponse $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->EmulatorAFTNResponse[] = $value;
      } else {
        $this->EmulatorAFTNResponse[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->EmulatorAFTNResponse[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return EmulatorAFTNResponse Return the current element
     */
    public function current()
    {
      return current($this->EmulatorAFTNResponse);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->EmulatorAFTNResponse);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->EmulatorAFTNResponse);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->EmulatorAFTNResponse);
    }

    /**
     * Countable implementation
     *
     * @return EmulatorAFTNResponse Return count of elements
     */
    public function count()
    {
      return count($this->EmulatorAFTNResponse);
    }

}
