<?php

class FlightPlanFilingFault
{

    /**
     * @var string $AircraftIdentification
     */
    protected $AircraftIdentification = null;

    /**
     * @var string $DepartureICAO
     */
    protected $DepartureICAO = null;

    /**
     * @var string $DestinationICAO
     */
    protected $DestinationICAO = null;

    /**
     * @var string $ErrorCode
     */
    protected $ErrorCode = null;

    /**
     * @var string $ErrorMessage
     */
    protected $ErrorMessage = null;

    /**
     * @var boolean $IsProduction
     */
    protected $IsProduction = null;

    /**
     * @var string $TimeOfDeparture
     */
    protected $TimeOfDeparture = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getAircraftIdentification()
    {
      return $this->AircraftIdentification;
    }

    /**
     * @param string $AircraftIdentification
     * @return FlightPlanFilingFault
     */
    public function setAircraftIdentification($AircraftIdentification)
    {
      $this->AircraftIdentification = $AircraftIdentification;
      return $this;
    }

    /**
     * @return string
     */
    public function getDepartureICAO()
    {
      return $this->DepartureICAO;
    }

    /**
     * @param string $DepartureICAO
     * @return FlightPlanFilingFault
     */
    public function setDepartureICAO($DepartureICAO)
    {
      $this->DepartureICAO = $DepartureICAO;
      return $this;
    }

    /**
     * @return string
     */
    public function getDestinationICAO()
    {
      return $this->DestinationICAO;
    }

    /**
     * @param string $DestinationICAO
     * @return FlightPlanFilingFault
     */
    public function setDestinationICAO($DestinationICAO)
    {
      $this->DestinationICAO = $DestinationICAO;
      return $this;
    }

    /**
     * @return string
     */
    public function getErrorCode()
    {
      return $this->ErrorCode;
    }

    /**
     * @param string $ErrorCode
     * @return FlightPlanFilingFault
     */
    public function setErrorCode($ErrorCode)
    {
      $this->ErrorCode = $ErrorCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
      return $this->ErrorMessage;
    }

    /**
     * @param string $ErrorMessage
     * @return FlightPlanFilingFault
     */
    public function setErrorMessage($ErrorMessage)
    {
      $this->ErrorMessage = $ErrorMessage;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsProduction()
    {
      return $this->IsProduction;
    }

    /**
     * @param boolean $IsProduction
     * @return FlightPlanFilingFault
     */
    public function setIsProduction($IsProduction)
    {
      $this->IsProduction = $IsProduction;
      return $this;
    }

    /**
     * @return string
     */
    public function getTimeOfDeparture()
    {
      return $this->TimeOfDeparture;
    }

    /**
     * @param string $TimeOfDeparture
     * @return FlightPlanFilingFault
     */
    public function setTimeOfDeparture($TimeOfDeparture)
    {
      $this->TimeOfDeparture = $TimeOfDeparture;
      return $this;
    }

}
