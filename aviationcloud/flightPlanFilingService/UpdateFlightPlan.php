<?php

class UpdateFlightPlan
{

    /**
     * @var FlightPlanUpdateRequest $request
     */
    protected $request = null;

    /**
     * @param FlightPlanUpdateRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return FlightPlanUpdateRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param FlightPlanUpdateRequest $request
     * @return UpdateFlightPlan
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
