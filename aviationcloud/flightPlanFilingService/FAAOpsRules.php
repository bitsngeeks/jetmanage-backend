<?php

class FAAOpsRules extends FlightFuelRules
{

    /**
     * @var boolean $IsPart121
     */
    protected $IsPart121 = null;

    /**
     * @var boolean $IsPart121FlagOperations
     */
    protected $IsPart121FlagOperations = null;

    /**
     * @var boolean $IsPart91
     */
    protected $IsPart91 = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return boolean
     */
    public function getIsPart121()
    {
      return $this->IsPart121;
    }

    /**
     * @param boolean $IsPart121
     * @return FAAOpsRules
     */
    public function setIsPart121($IsPart121)
    {
      $this->IsPart121 = $IsPart121;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsPart121FlagOperations()
    {
      return $this->IsPart121FlagOperations;
    }

    /**
     * @param boolean $IsPart121FlagOperations
     * @return FAAOpsRules
     */
    public function setIsPart121FlagOperations($IsPart121FlagOperations)
    {
      $this->IsPart121FlagOperations = $IsPart121FlagOperations;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsPart91()
    {
      return $this->IsPart91;
    }

    /**
     * @param boolean $IsPart91
     * @return FAAOpsRules
     */
    public function setIsPart91($IsPart91)
    {
      $this->IsPart91 = $IsPart91;
      return $this;
    }

}
