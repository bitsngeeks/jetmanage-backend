<?php

class ATCFlightMessage extends ATCMessageBase
{

    /**
     * @var string $AircraftIdentification
     */
    protected $AircraftIdentification = null;

    /**
     * @var Airport $DepartureAirport
     */
    protected $DepartureAirport = null;

    /**
     * @var Airport $DestinationAirport
     */
    protected $DestinationAirport = null;

    /**
     * @var \DateTime $TimeOfDeparture
     */
    protected $TimeOfDeparture = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return string
     */
    public function getAircraftIdentification()
    {
      return $this->AircraftIdentification;
    }

    /**
     * @param string $AircraftIdentification
     * @return ATCFlightMessage
     */
    public function setAircraftIdentification($AircraftIdentification)
    {
      $this->AircraftIdentification = $AircraftIdentification;
      return $this;
    }

    /**
     * @return Airport
     */
    public function getDepartureAirport()
    {
      return $this->DepartureAirport;
    }

    /**
     * @param Airport $DepartureAirport
     * @return ATCFlightMessage
     */
    public function setDepartureAirport($DepartureAirport)
    {
      $this->DepartureAirport = $DepartureAirport;
      return $this;
    }

    /**
     * @return Airport
     */
    public function getDestinationAirport()
    {
      return $this->DestinationAirport;
    }

    /**
     * @param Airport $DestinationAirport
     * @return ATCFlightMessage
     */
    public function setDestinationAirport($DestinationAirport)
    {
      $this->DestinationAirport = $DestinationAirport;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getTimeOfDeparture()
    {
      if ($this->TimeOfDeparture == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->TimeOfDeparture);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $TimeOfDeparture
     * @return ATCFlightMessage
     */
    public function setTimeOfDeparture(\DateTime $TimeOfDeparture = null)
    {
      if ($TimeOfDeparture == null) {
       $this->TimeOfDeparture = null;
      } else {
        $this->TimeOfDeparture = $TimeOfDeparture->format(\DateTime::ATOM);
      }
      return $this;
    }

}
