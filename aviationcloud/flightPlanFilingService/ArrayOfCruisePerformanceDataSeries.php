<?php

class ArrayOfCruisePerformanceDataSeries implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var CruisePerformanceDataSeries[] $CruisePerformanceDataSeries
     */
    protected $CruisePerformanceDataSeries = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return CruisePerformanceDataSeries[]
     */
    public function getCruisePerformanceDataSeries()
    {
      return $this->CruisePerformanceDataSeries;
    }

    /**
     * @param CruisePerformanceDataSeries[] $CruisePerformanceDataSeries
     * @return ArrayOfCruisePerformanceDataSeries
     */
    public function setCruisePerformanceDataSeries(array $CruisePerformanceDataSeries = null)
    {
      $this->CruisePerformanceDataSeries = $CruisePerformanceDataSeries;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->CruisePerformanceDataSeries[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return CruisePerformanceDataSeries
     */
    public function offsetGet($offset)
    {
      return $this->CruisePerformanceDataSeries[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param CruisePerformanceDataSeries $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->CruisePerformanceDataSeries[] = $value;
      } else {
        $this->CruisePerformanceDataSeries[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->CruisePerformanceDataSeries[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return CruisePerformanceDataSeries Return the current element
     */
    public function current()
    {
      return current($this->CruisePerformanceDataSeries);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->CruisePerformanceDataSeries);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->CruisePerformanceDataSeries);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->CruisePerformanceDataSeries);
    }

    /**
     * Countable implementation
     *
     * @return CruisePerformanceDataSeries Return count of elements
     */
    public function count()
    {
      return count($this->CruisePerformanceDataSeries);
    }

}
