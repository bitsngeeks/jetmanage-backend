<?php

class UnidentifiableAircraftFault
{

    /**
     * @var Account $SpecifiedAccount
     */
    protected $SpecifiedAccount = null;

    /**
     * @var string $SpecifiedTailnumber
     */
    protected $SpecifiedTailnumber = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Account
     */
    public function getSpecifiedAccount()
    {
      return $this->SpecifiedAccount;
    }

    /**
     * @param Account $SpecifiedAccount
     * @return UnidentifiableAircraftFault
     */
    public function setSpecifiedAccount($SpecifiedAccount)
    {
      $this->SpecifiedAccount = $SpecifiedAccount;
      return $this;
    }

    /**
     * @return string
     */
    public function getSpecifiedTailnumber()
    {
      return $this->SpecifiedTailnumber;
    }

    /**
     * @param string $SpecifiedTailnumber
     * @return UnidentifiableAircraftFault
     */
    public function setSpecifiedTailnumber($SpecifiedTailnumber)
    {
      $this->SpecifiedTailnumber = $SpecifiedTailnumber;
      return $this;
    }

}
