<?php

class ByAltitudeCruisePerformance
{

    /**
     * @var float $AltitudeFeet
     */
    protected $AltitudeFeet = null;

    /**
     * @var float $FuelPoundPerHour
     */
    protected $FuelPoundPerHour = null;

    /**
     * @var float $Isa
     */
    protected $Isa = null;

    /**
     * @var float $SpeedKnots
     */
    protected $SpeedKnots = null;

    /**
     * @var float $WeightPounds
     */
    protected $WeightPounds = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getAltitudeFeet()
    {
      return $this->AltitudeFeet;
    }

    /**
     * @param float $AltitudeFeet
     * @return ByAltitudeCruisePerformance
     */
    public function setAltitudeFeet($AltitudeFeet)
    {
      $this->AltitudeFeet = $AltitudeFeet;
      return $this;
    }

    /**
     * @return float
     */
    public function getFuelPoundPerHour()
    {
      return $this->FuelPoundPerHour;
    }

    /**
     * @param float $FuelPoundPerHour
     * @return ByAltitudeCruisePerformance
     */
    public function setFuelPoundPerHour($FuelPoundPerHour)
    {
      $this->FuelPoundPerHour = $FuelPoundPerHour;
      return $this;
    }

    /**
     * @return float
     */
    public function getIsa()
    {
      return $this->Isa;
    }

    /**
     * @param float $Isa
     * @return ByAltitudeCruisePerformance
     */
    public function setIsa($Isa)
    {
      $this->Isa = $Isa;
      return $this;
    }

    /**
     * @return float
     */
    public function getSpeedKnots()
    {
      return $this->SpeedKnots;
    }

    /**
     * @param float $SpeedKnots
     * @return ByAltitudeCruisePerformance
     */
    public function setSpeedKnots($SpeedKnots)
    {
      $this->SpeedKnots = $SpeedKnots;
      return $this;
    }

    /**
     * @return float
     */
    public function getWeightPounds()
    {
      return $this->WeightPounds;
    }

    /**
     * @param float $WeightPounds
     * @return ByAltitudeCruisePerformance
     */
    public function setWeightPounds($WeightPounds)
    {
      $this->WeightPounds = $WeightPounds;
      return $this;
    }

}
