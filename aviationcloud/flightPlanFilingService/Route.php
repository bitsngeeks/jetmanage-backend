<?php

class Route
{

    /**
     * @var ArrayOfAltitudeChangeInformation $AltitudeChanges
     */
    protected $AltitudeChanges = null;

    /**
     * @var AltitudeType $AltitudeUnit
     */
    protected $AltitudeUnit = null;

    /**
     * @var Airport $DepartureAirport
     */
    protected $DepartureAirport = null;

    /**
     * @var Airport $DestinationAirport
     */
    protected $DestinationAirport = null;

    /**
     * @var boolean $EnableNonStrictProcedureValidation
     */
    protected $EnableNonStrictProcedureValidation = null;

    /**
     * @var boolean $EnableNonStrictValidation
     */
    protected $EnableNonStrictValidation = null;

    /**
     * @var Time $EstimatedFlightTime
     */
    protected $EstimatedFlightTime = null;

    /**
     * @var int $FlightLevel
     */
    protected $FlightLevel = null;

    /**
     * @var ATCFlightRule $FlightRule
     */
    protected $FlightRule = null;

    /**
     * @var Speed $InitialSpeed
     */
    protected $InitialSpeed = null;

    /**
     * @var ArrayOfstring $InvalidTokens
     */
    protected $InvalidTokens = null;

    /**
     * @var Length $Length
     */
    protected $Length = null;

    /**
     * @var Altitude $MaximumAltitude
     */
    protected $MaximumAltitude = null;

    /**
     * @var Altitude $MinimumAltitude
     */
    protected $MinimumAltitude = null;

    /**
     * @var ArrayOfRouteNode $Path
     */
    protected $Path = null;

    /**
     * @var RouteProfile $Profile
     */
    protected $Profile = null;

    /**
     * @var RouteReport $Report
     */
    protected $Report = null;

    /**
     * @var string $RouteIdentifier
     */
    protected $RouteIdentifier = null;

    /**
     * @var AirportProcedure $Sid
     */
    protected $Sid = null;

    /**
     * @var AirportProcedure $Star
     */
    protected $Star = null;

    /**
     * @var string $ATCroute
     */
    protected $ATCroute = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfAltitudeChangeInformation
     */
    public function getAltitudeChanges()
    {
      return $this->AltitudeChanges;
    }

    /**
     * @param ArrayOfAltitudeChangeInformation $AltitudeChanges
     * @return Route
     */
    public function setAltitudeChanges($AltitudeChanges)
    {
      $this->AltitudeChanges = $AltitudeChanges;
      return $this;
    }

    /**
     * @return AltitudeType
     */
    public function getAltitudeUnit()
    {
      return $this->AltitudeUnit;
    }

    /**
     * @param AltitudeType $AltitudeUnit
     * @return Route
     */
    public function setAltitudeUnit($AltitudeUnit)
    {
      $this->AltitudeUnit = $AltitudeUnit;
      return $this;
    }

    /**
     * @return Airport
     */
    public function getDepartureAirport()
    {
      return $this->DepartureAirport;
    }

    /**
     * @param Airport $DepartureAirport
     * @return Route
     */
    public function setDepartureAirport($DepartureAirport)
    {
      $this->DepartureAirport = $DepartureAirport;
      return $this;
    }

    /**
     * @return Airport
     */
    public function getDestinationAirport()
    {
      return $this->DestinationAirport;
    }

    /**
     * @param Airport $DestinationAirport
     * @return Route
     */
    public function setDestinationAirport($DestinationAirport)
    {
      $this->DestinationAirport = $DestinationAirport;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getEnableNonStrictProcedureValidation()
    {
      return $this->EnableNonStrictProcedureValidation;
    }

    /**
     * @param boolean $EnableNonStrictProcedureValidation
     * @return Route
     */
    public function setEnableNonStrictProcedureValidation($EnableNonStrictProcedureValidation)
    {
      $this->EnableNonStrictProcedureValidation = $EnableNonStrictProcedureValidation;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getEnableNonStrictValidation()
    {
      return $this->EnableNonStrictValidation;
    }

    /**
     * @param boolean $EnableNonStrictValidation
     * @return Route
     */
    public function setEnableNonStrictValidation($EnableNonStrictValidation)
    {
      $this->EnableNonStrictValidation = $EnableNonStrictValidation;
      return $this;
    }

    /**
     * @return Time
     */
    public function getEstimatedFlightTime()
    {
      return $this->EstimatedFlightTime;
    }

    /**
     * @param Time $EstimatedFlightTime
     * @return Route
     */
    public function setEstimatedFlightTime($EstimatedFlightTime)
    {
      $this->EstimatedFlightTime = $EstimatedFlightTime;
      return $this;
    }

    /**
     * @return int
     */
    public function getFlightLevel()
    {
      return $this->FlightLevel;
    }

    /**
     * @param int $FlightLevel
     * @return Route
     */
    public function setFlightLevel($FlightLevel)
    {
      $this->FlightLevel = $FlightLevel;
      return $this;
    }

    /**
     * @return ATCFlightRule
     */
    public function getFlightRule()
    {
      return $this->FlightRule;
    }

    /**
     * @param ATCFlightRule $FlightRule
     * @return Route
     */
    public function setFlightRule($FlightRule)
    {
      $this->FlightRule = $FlightRule;
      return $this;
    }

    /**
     * @return Speed
     */
    public function getInitialSpeed()
    {
      return $this->InitialSpeed;
    }

    /**
     * @param Speed $InitialSpeed
     * @return Route
     */
    public function setInitialSpeed($InitialSpeed)
    {
      $this->InitialSpeed = $InitialSpeed;
      return $this;
    }

    /**
     * @return ArrayOfstring
     */
    public function getInvalidTokens()
    {
      return $this->InvalidTokens;
    }

    /**
     * @param ArrayOfstring $InvalidTokens
     * @return Route
     */
    public function setInvalidTokens($InvalidTokens)
    {
      $this->InvalidTokens = $InvalidTokens;
      return $this;
    }

    /**
     * @return Length
     */
    public function getLength()
    {
      return $this->Length;
    }

    /**
     * @param Length $Length
     * @return Route
     */
    public function setLength($Length)
    {
      $this->Length = $Length;
      return $this;
    }

    /**
     * @return Altitude
     */
    public function getMaximumAltitude()
    {
      return $this->MaximumAltitude;
    }

    /**
     * @param Altitude $MaximumAltitude
     * @return Route
     */
    public function setMaximumAltitude($MaximumAltitude)
    {
      $this->MaximumAltitude = $MaximumAltitude;
      return $this;
    }

    /**
     * @return Altitude
     */
    public function getMinimumAltitude()
    {
      return $this->MinimumAltitude;
    }

    /**
     * @param Altitude $MinimumAltitude
     * @return Route
     */
    public function setMinimumAltitude($MinimumAltitude)
    {
      $this->MinimumAltitude = $MinimumAltitude;
      return $this;
    }

    /**
     * @return ArrayOfRouteNode
     */
    public function getPath()
    {
      return $this->Path;
    }

    /**
     * @param ArrayOfRouteNode $Path
     * @return Route
     */
    public function setPath($Path)
    {
      $this->Path = $Path;
      return $this;
    }

    /**
     * @return RouteProfile
     */
    public function getProfile()
    {
      return $this->Profile;
    }

    /**
     * @param RouteProfile $Profile
     * @return Route
     */
    public function setProfile($Profile)
    {
      $this->Profile = $Profile;
      return $this;
    }

    /**
     * @return RouteReport
     */
    public function getReport()
    {
      return $this->Report;
    }

    /**
     * @param RouteReport $Report
     * @return Route
     */
    public function setReport($Report)
    {
      $this->Report = $Report;
      return $this;
    }

    /**
     * @return string
     */
    public function getRouteIdentifier()
    {
      return $this->RouteIdentifier;
    }

    /**
     * @param string $RouteIdentifier
     * @return Route
     */
    public function setRouteIdentifier($RouteIdentifier)
    {
      $this->RouteIdentifier = $RouteIdentifier;
      return $this;
    }

    /**
     * @return AirportProcedure
     */
    public function getSid()
    {
      return $this->Sid;
    }

    /**
     * @param AirportProcedure $Sid
     * @return Route
     */
    public function setSid($Sid)
    {
      $this->Sid = $Sid;
      return $this;
    }

    /**
     * @return AirportProcedure
     */
    public function getStar()
    {
      return $this->Star;
    }

    /**
     * @param AirportProcedure $Star
     * @return Route
     */
    public function setStar($Star)
    {
      $this->Star = $Star;
      return $this;
    }

    /**
     * @return string
     */
    public function getATCroute()
    {
      return $this->ATCroute;
    }

    /**
     * @param string $ATCroute
     * @return Route
     */
    public function setATCroute($ATCroute)
    {
      $this->ATCroute = $ATCroute;
      return $this;
    }

}
