<?php

class FlightLevelAdjustmentSpecification
{

    /**
     * @var ArrayOfAltitudeNode $AltitudeCorrectingNodes
     */
    protected $AltitudeCorrectingNodes = null;

    /**
     * @var ArrayOfCruiseChangeNode $CruiseSelectionNodes
     */
    protected $CruiseSelectionNodes = null;

    /**
     * @var boolean $EnsureAltitudeAfterWaypoint
     */
    protected $EnsureAltitudeAfterWaypoint = null;

    /**
     * @var boolean $EnsureAltitudeAtWaypoint
     */
    protected $EnsureAltitudeAtWaypoint = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfAltitudeNode
     */
    public function getAltitudeCorrectingNodes()
    {
      return $this->AltitudeCorrectingNodes;
    }

    /**
     * @param ArrayOfAltitudeNode $AltitudeCorrectingNodes
     * @return FlightLevelAdjustmentSpecification
     */
    public function setAltitudeCorrectingNodes($AltitudeCorrectingNodes)
    {
      $this->AltitudeCorrectingNodes = $AltitudeCorrectingNodes;
      return $this;
    }

    /**
     * @return ArrayOfCruiseChangeNode
     */
    public function getCruiseSelectionNodes()
    {
      return $this->CruiseSelectionNodes;
    }

    /**
     * @param ArrayOfCruiseChangeNode $CruiseSelectionNodes
     * @return FlightLevelAdjustmentSpecification
     */
    public function setCruiseSelectionNodes($CruiseSelectionNodes)
    {
      $this->CruiseSelectionNodes = $CruiseSelectionNodes;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getEnsureAltitudeAfterWaypoint()
    {
      return $this->EnsureAltitudeAfterWaypoint;
    }

    /**
     * @param boolean $EnsureAltitudeAfterWaypoint
     * @return FlightLevelAdjustmentSpecification
     */
    public function setEnsureAltitudeAfterWaypoint($EnsureAltitudeAfterWaypoint)
    {
      $this->EnsureAltitudeAfterWaypoint = $EnsureAltitudeAfterWaypoint;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getEnsureAltitudeAtWaypoint()
    {
      return $this->EnsureAltitudeAtWaypoint;
    }

    /**
     * @param boolean $EnsureAltitudeAtWaypoint
     * @return FlightLevelAdjustmentSpecification
     */
    public function setEnsureAltitudeAtWaypoint($EnsureAltitudeAtWaypoint)
    {
      $this->EnsureAltitudeAtWaypoint = $EnsureAltitudeAtWaypoint;
      return $this;
    }

}
