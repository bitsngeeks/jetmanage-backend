<?php

class AFTNSubscriptionUpdateRequest extends RequestBase
{

    /**
     * @var int $SubscriptionId
     */
    protected $SubscriptionId = null;

    /**
     * @var AFTNMessageSubscription $UpdatedSubscription
     */
    protected $UpdatedSubscription = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return int
     */
    public function getSubscriptionId()
    {
      return $this->SubscriptionId;
    }

    /**
     * @param int $SubscriptionId
     * @return AFTNSubscriptionUpdateRequest
     */
    public function setSubscriptionId($SubscriptionId)
    {
      $this->SubscriptionId = $SubscriptionId;
      return $this;
    }

    /**
     * @return AFTNMessageSubscription
     */
    public function getUpdatedSubscription()
    {
      return $this->UpdatedSubscription;
    }

    /**
     * @param AFTNMessageSubscription $UpdatedSubscription
     * @return AFTNSubscriptionUpdateRequest
     */
    public function setUpdatedSubscription($UpdatedSubscription)
    {
      $this->UpdatedSubscription = $UpdatedSubscription;
      return $this;
    }

}
