<?php

class SimpleCargoLoadSpecification extends CargoLoadSpecification
{

    /**
     * @var Weight $CargoLoad
     */
    protected $CargoLoad = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return Weight
     */
    public function getCargoLoad()
    {
      return $this->CargoLoad;
    }

    /**
     * @param Weight $CargoLoad
     * @return SimpleCargoLoadSpecification
     */
    public function setCargoLoad($CargoLoad)
    {
      $this->CargoLoad = $CargoLoad;
      return $this;
    }

}
