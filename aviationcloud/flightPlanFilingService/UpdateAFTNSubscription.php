<?php

class UpdateAFTNSubscription
{

    /**
     * @var AFTNSubscriptionUpdateRequest $request
     */
    protected $request = null;

    /**
     * @param AFTNSubscriptionUpdateRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return AFTNSubscriptionUpdateRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param AFTNSubscriptionUpdateRequest $request
     * @return UpdateAFTNSubscription
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
