<?php

class ATCDivertedArrivialMessage extends ATCFlightMessage
{

    /**
     * @var Airport $ActualDestinationAirport
     */
    protected $ActualDestinationAirport = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return Airport
     */
    public function getActualDestinationAirport()
    {
      return $this->ActualDestinationAirport;
    }

    /**
     * @param Airport $ActualDestinationAirport
     * @return ATCDivertedArrivialMessage
     */
    public function setActualDestinationAirport($ActualDestinationAirport)
    {
      $this->ActualDestinationAirport = $ActualDestinationAirport;
      return $this;
    }

}
