<?php

class RouteReport
{

    /**
     * @var ArrayOfAirspace $AvoidedAirspaces
     */
    protected $AvoidedAirspaces = null;

    /**
     * @var ArrayOfInformationItem $CalculationReport
     */
    protected $CalculationReport = null;

    /**
     * @var ArrayOfRouteInfo $RouteInformation
     */
    protected $RouteInformation = null;

    /**
     * @var ArrayOfWarning $RouteWarnings
     */
    protected $RouteWarnings = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfAirspace
     */
    public function getAvoidedAirspaces()
    {
      return $this->AvoidedAirspaces;
    }

    /**
     * @param ArrayOfAirspace $AvoidedAirspaces
     * @return RouteReport
     */
    public function setAvoidedAirspaces($AvoidedAirspaces)
    {
      $this->AvoidedAirspaces = $AvoidedAirspaces;
      return $this;
    }

    /**
     * @return ArrayOfInformationItem
     */
    public function getCalculationReport()
    {
      return $this->CalculationReport;
    }

    /**
     * @param ArrayOfInformationItem $CalculationReport
     * @return RouteReport
     */
    public function setCalculationReport($CalculationReport)
    {
      $this->CalculationReport = $CalculationReport;
      return $this;
    }

    /**
     * @return ArrayOfRouteInfo
     */
    public function getRouteInformation()
    {
      return $this->RouteInformation;
    }

    /**
     * @param ArrayOfRouteInfo $RouteInformation
     * @return RouteReport
     */
    public function setRouteInformation($RouteInformation)
    {
      $this->RouteInformation = $RouteInformation;
      return $this;
    }

    /**
     * @return ArrayOfWarning
     */
    public function getRouteWarnings()
    {
      return $this->RouteWarnings;
    }

    /**
     * @param ArrayOfWarning $RouteWarnings
     * @return RouteReport
     */
    public function setRouteWarnings($RouteWarnings)
    {
      $this->RouteWarnings = $RouteWarnings;
      return $this;
    }

}
