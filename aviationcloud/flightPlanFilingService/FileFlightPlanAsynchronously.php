<?php

class FileFlightPlanAsynchronously
{

    /**
     * @var FileFlightplanRequest $request
     */
    protected $request = null;

    /**
     * @param FileFlightplanRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return FileFlightplanRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param FileFlightplanRequest $request
     * @return FileFlightPlanAsynchronously
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
