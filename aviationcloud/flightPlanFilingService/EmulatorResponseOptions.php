<?php

class EmulatorResponseOptions
{

    /**
     * @var ArrayOfEmulatorAFTNResponse $CustomAFTNResponses
     */
    protected $CustomAFTNResponses = null;

    /**
     * @var string $DefaultAFTNResponseTemplate
     */
    protected $DefaultAFTNResponseTemplate = null;

    /**
     * @var int $DefaultResponseTimeMs
     */
    protected $DefaultResponseTimeMs = null;

    /**
     * @var boolean $EnableEULongACK
     */
    protected $EnableEULongACK = null;

    /**
     * @var boolean $RespondWithCanadianVfrALRQALQ
     */
    protected $RespondWithCanadianVfrALRQALQ = null;

    /**
     * @var boolean $RespondWithCanadianVfrMISQALQ
     */
    protected $RespondWithCanadianVfrMISQALQ = null;

    /**
     * @var boolean $RespondWithCanadianVfrQALQ
     */
    protected $RespondWithCanadianVfrQALQ = null;

    /**
     * @var boolean $RespondWithRQP
     */
    protected $RespondWithRQP = null;

    /**
     * @var boolean $RespondWithRQS
     */
    protected $RespondWithRQS = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfEmulatorAFTNResponse
     */
    public function getCustomAFTNResponses()
    {
      return $this->CustomAFTNResponses;
    }

    /**
     * @param ArrayOfEmulatorAFTNResponse $CustomAFTNResponses
     * @return EmulatorResponseOptions
     */
    public function setCustomAFTNResponses($CustomAFTNResponses)
    {
      $this->CustomAFTNResponses = $CustomAFTNResponses;
      return $this;
    }

    /**
     * @return string
     */
    public function getDefaultAFTNResponseTemplate()
    {
      return $this->DefaultAFTNResponseTemplate;
    }

    /**
     * @param string $DefaultAFTNResponseTemplate
     * @return EmulatorResponseOptions
     */
    public function setDefaultAFTNResponseTemplate($DefaultAFTNResponseTemplate)
    {
      $this->DefaultAFTNResponseTemplate = $DefaultAFTNResponseTemplate;
      return $this;
    }

    /**
     * @return int
     */
    public function getDefaultResponseTimeMs()
    {
      return $this->DefaultResponseTimeMs;
    }

    /**
     * @param int $DefaultResponseTimeMs
     * @return EmulatorResponseOptions
     */
    public function setDefaultResponseTimeMs($DefaultResponseTimeMs)
    {
      $this->DefaultResponseTimeMs = $DefaultResponseTimeMs;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getEnableEULongACK()
    {
      return $this->EnableEULongACK;
    }

    /**
     * @param boolean $EnableEULongACK
     * @return EmulatorResponseOptions
     */
    public function setEnableEULongACK($EnableEULongACK)
    {
      $this->EnableEULongACK = $EnableEULongACK;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRespondWithCanadianVfrALRQALQ()
    {
      return $this->RespondWithCanadianVfrALRQALQ;
    }

    /**
     * @param boolean $RespondWithCanadianVfrALRQALQ
     * @return EmulatorResponseOptions
     */
    public function setRespondWithCanadianVfrALRQALQ($RespondWithCanadianVfrALRQALQ)
    {
      $this->RespondWithCanadianVfrALRQALQ = $RespondWithCanadianVfrALRQALQ;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRespondWithCanadianVfrMISQALQ()
    {
      return $this->RespondWithCanadianVfrMISQALQ;
    }

    /**
     * @param boolean $RespondWithCanadianVfrMISQALQ
     * @return EmulatorResponseOptions
     */
    public function setRespondWithCanadianVfrMISQALQ($RespondWithCanadianVfrMISQALQ)
    {
      $this->RespondWithCanadianVfrMISQALQ = $RespondWithCanadianVfrMISQALQ;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRespondWithCanadianVfrQALQ()
    {
      return $this->RespondWithCanadianVfrQALQ;
    }

    /**
     * @param boolean $RespondWithCanadianVfrQALQ
     * @return EmulatorResponseOptions
     */
    public function setRespondWithCanadianVfrQALQ($RespondWithCanadianVfrQALQ)
    {
      $this->RespondWithCanadianVfrQALQ = $RespondWithCanadianVfrQALQ;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRespondWithRQP()
    {
      return $this->RespondWithRQP;
    }

    /**
     * @param boolean $RespondWithRQP
     * @return EmulatorResponseOptions
     */
    public function setRespondWithRQP($RespondWithRQP)
    {
      $this->RespondWithRQP = $RespondWithRQP;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRespondWithRQS()
    {
      return $this->RespondWithRQS;
    }

    /**
     * @param boolean $RespondWithRQS
     * @return EmulatorResponseOptions
     */
    public function setRespondWithRQS($RespondWithRQS)
    {
      $this->RespondWithRQS = $RespondWithRQS;
      return $this;
    }

}
