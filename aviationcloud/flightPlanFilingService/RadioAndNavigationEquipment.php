<?php

class RadioAndNavigationEquipment
{

    /**
     * @var ATCACARSEquipment $ACARSEquipment
     */
    protected $ACARSEquipment = null;

    /**
     * @var boolean $ADF
     */
    protected $ADF = null;

    /**
     * @var CPDLCEquipment $CPDLCEquipment
     */
    protected $CPDLCEquipment = null;

    /**
     * @var boolean $DME
     */
    protected $DME = null;

    /**
     * @var boolean $GBASLandingSystem
     */
    protected $GBASLandingSystem = null;

    /**
     * @var boolean $GNSS
     */
    protected $GNSS = null;

    /**
     * @var boolean $HFRTF
     */
    protected $HFRTF = null;

    /**
     * @var boolean $ILS
     */
    protected $ILS = null;

    /**
     * @var boolean $INS
     */
    protected $INS = null;

    /**
     * @var boolean $LORAN
     */
    protected $LORAN = null;

    /**
     * @var boolean $LPV
     */
    protected $LPV = null;

    /**
     * @var boolean $MLS
     */
    protected $MLS = null;

    /**
     * @var boolean $MNPS
     */
    protected $MNPS = null;

    /**
     * @var boolean $NoEquipment
     */
    protected $NoEquipment = null;

    /**
     * @var boolean $Other
     */
    protected $Other = null;

    /**
     * @var boolean $PBN
     */
    protected $PBN = null;

    /**
     * @var RTFEquipment $RTFEquipment
     */
    protected $RTFEquipment = null;

    /**
     * @var boolean $RVSM
     */
    protected $RVSM = null;

    /**
     * @var boolean $StandardEquipment
     */
    protected $StandardEquipment = null;

    /**
     * @var boolean $TACAN
     */
    protected $TACAN = null;

    /**
     * @var boolean $UHF
     */
    protected $UHF = null;

    /**
     * @var boolean $VHF
     */
    protected $VHF = null;

    /**
     * @var boolean $VHF833Spacing
     */
    protected $VHF833Spacing = null;

    /**
     * @var boolean $VOR
     */
    protected $VOR = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ATCACARSEquipment
     */
    public function getACARSEquipment()
    {
      return $this->ACARSEquipment;
    }

    /**
     * @param ATCACARSEquipment $ACARSEquipment
     * @return RadioAndNavigationEquipment
     */
    public function setACARSEquipment($ACARSEquipment)
    {
      $this->ACARSEquipment = $ACARSEquipment;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getADF()
    {
      return $this->ADF;
    }

    /**
     * @param boolean $ADF
     * @return RadioAndNavigationEquipment
     */
    public function setADF($ADF)
    {
      $this->ADF = $ADF;
      return $this;
    }

    /**
     * @return CPDLCEquipment
     */
    public function getCPDLCEquipment()
    {
      return $this->CPDLCEquipment;
    }

    /**
     * @param CPDLCEquipment $CPDLCEquipment
     * @return RadioAndNavigationEquipment
     */
    public function setCPDLCEquipment($CPDLCEquipment)
    {
      $this->CPDLCEquipment = $CPDLCEquipment;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getDME()
    {
      return $this->DME;
    }

    /**
     * @param boolean $DME
     * @return RadioAndNavigationEquipment
     */
    public function setDME($DME)
    {
      $this->DME = $DME;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getGBASLandingSystem()
    {
      return $this->GBASLandingSystem;
    }

    /**
     * @param boolean $GBASLandingSystem
     * @return RadioAndNavigationEquipment
     */
    public function setGBASLandingSystem($GBASLandingSystem)
    {
      $this->GBASLandingSystem = $GBASLandingSystem;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getGNSS()
    {
      return $this->GNSS;
    }

    /**
     * @param boolean $GNSS
     * @return RadioAndNavigationEquipment
     */
    public function setGNSS($GNSS)
    {
      $this->GNSS = $GNSS;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getHFRTF()
    {
      return $this->HFRTF;
    }

    /**
     * @param boolean $HFRTF
     * @return RadioAndNavigationEquipment
     */
    public function setHFRTF($HFRTF)
    {
      $this->HFRTF = $HFRTF;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getILS()
    {
      return $this->ILS;
    }

    /**
     * @param boolean $ILS
     * @return RadioAndNavigationEquipment
     */
    public function setILS($ILS)
    {
      $this->ILS = $ILS;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getINS()
    {
      return $this->INS;
    }

    /**
     * @param boolean $INS
     * @return RadioAndNavigationEquipment
     */
    public function setINS($INS)
    {
      $this->INS = $INS;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getLORAN()
    {
      return $this->LORAN;
    }

    /**
     * @param boolean $LORAN
     * @return RadioAndNavigationEquipment
     */
    public function setLORAN($LORAN)
    {
      $this->LORAN = $LORAN;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getLPV()
    {
      return $this->LPV;
    }

    /**
     * @param boolean $LPV
     * @return RadioAndNavigationEquipment
     */
    public function setLPV($LPV)
    {
      $this->LPV = $LPV;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getMLS()
    {
      return $this->MLS;
    }

    /**
     * @param boolean $MLS
     * @return RadioAndNavigationEquipment
     */
    public function setMLS($MLS)
    {
      $this->MLS = $MLS;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getMNPS()
    {
      return $this->MNPS;
    }

    /**
     * @param boolean $MNPS
     * @return RadioAndNavigationEquipment
     */
    public function setMNPS($MNPS)
    {
      $this->MNPS = $MNPS;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getNoEquipment()
    {
      return $this->NoEquipment;
    }

    /**
     * @param boolean $NoEquipment
     * @return RadioAndNavigationEquipment
     */
    public function setNoEquipment($NoEquipment)
    {
      $this->NoEquipment = $NoEquipment;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getOther()
    {
      return $this->Other;
    }

    /**
     * @param boolean $Other
     * @return RadioAndNavigationEquipment
     */
    public function setOther($Other)
    {
      $this->Other = $Other;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getPBN()
    {
      return $this->PBN;
    }

    /**
     * @param boolean $PBN
     * @return RadioAndNavigationEquipment
     */
    public function setPBN($PBN)
    {
      $this->PBN = $PBN;
      return $this;
    }

    /**
     * @return RTFEquipment
     */
    public function getRTFEquipment()
    {
      return $this->RTFEquipment;
    }

    /**
     * @param RTFEquipment $RTFEquipment
     * @return RadioAndNavigationEquipment
     */
    public function setRTFEquipment($RTFEquipment)
    {
      $this->RTFEquipment = $RTFEquipment;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRVSM()
    {
      return $this->RVSM;
    }

    /**
     * @param boolean $RVSM
     * @return RadioAndNavigationEquipment
     */
    public function setRVSM($RVSM)
    {
      $this->RVSM = $RVSM;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getStandardEquipment()
    {
      return $this->StandardEquipment;
    }

    /**
     * @param boolean $StandardEquipment
     * @return RadioAndNavigationEquipment
     */
    public function setStandardEquipment($StandardEquipment)
    {
      $this->StandardEquipment = $StandardEquipment;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getTACAN()
    {
      return $this->TACAN;
    }

    /**
     * @param boolean $TACAN
     * @return RadioAndNavigationEquipment
     */
    public function setTACAN($TACAN)
    {
      $this->TACAN = $TACAN;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUHF()
    {
      return $this->UHF;
    }

    /**
     * @param boolean $UHF
     * @return RadioAndNavigationEquipment
     */
    public function setUHF($UHF)
    {
      $this->UHF = $UHF;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getVHF()
    {
      return $this->VHF;
    }

    /**
     * @param boolean $VHF
     * @return RadioAndNavigationEquipment
     */
    public function setVHF($VHF)
    {
      $this->VHF = $VHF;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getVHF833Spacing()
    {
      return $this->VHF833Spacing;
    }

    /**
     * @param boolean $VHF833Spacing
     * @return RadioAndNavigationEquipment
     */
    public function setVHF833Spacing($VHF833Spacing)
    {
      $this->VHF833Spacing = $VHF833Spacing;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getVOR()
    {
      return $this->VOR;
    }

    /**
     * @param boolean $VOR
     * @return RadioAndNavigationEquipment
     */
    public function setVOR($VOR)
    {
      $this->VOR = $VOR;
      return $this;
    }

}
