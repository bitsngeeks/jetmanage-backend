<?php

class ValidationFault
{

    /**
     * @var ArrayOfValidationDetail $Details
     */
    protected $Details = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfValidationDetail
     */
    public function getDetails()
    {
      return $this->Details;
    }

    /**
     * @param ArrayOfValidationDetail $Details
     * @return ValidationFault
     */
    public function setDetails($Details)
    {
      $this->Details = $Details;
      return $this;
    }

}
