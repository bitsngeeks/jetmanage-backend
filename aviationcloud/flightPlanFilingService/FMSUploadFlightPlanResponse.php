<?php

class FMSUploadFlightPlanResponse
{

    /**
     * @var FMSUploadResponse $FMSUploadFlightPlanResult
     */
    protected $FMSUploadFlightPlanResult = null;

    /**
     * @param FMSUploadResponse $FMSUploadFlightPlanResult
     */
    public function __construct($FMSUploadFlightPlanResult)
    {
      $this->FMSUploadFlightPlanResult = $FMSUploadFlightPlanResult;
    }

    /**
     * @return FMSUploadResponse
     */
    public function getFMSUploadFlightPlanResult()
    {
      return $this->FMSUploadFlightPlanResult;
    }

    /**
     * @param FMSUploadResponse $FMSUploadFlightPlanResult
     * @return FMSUploadFlightPlanResponse
     */
    public function setFMSUploadFlightPlanResult($FMSUploadFlightPlanResult)
    {
      $this->FMSUploadFlightPlanResult = $FMSUploadFlightPlanResult;
      return $this;
    }

}
