<?php

class AFTNResolveRequest extends RequestBase
{

    /**
     * @var ATCFPLMessage $FlightPlan
     */
    protected $FlightPlan = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return ATCFPLMessage
     */
    public function getFlightPlan()
    {
      return $this->FlightPlan;
    }

    /**
     * @param ATCFPLMessage $FlightPlan
     * @return AFTNResolveRequest
     */
    public function setFlightPlan($FlightPlan)
    {
      $this->FlightPlan = $FlightPlan;
      return $this;
    }

}
