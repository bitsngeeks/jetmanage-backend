<?php

class ArrayOfAFTNMessageSubscription implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var AFTNMessageSubscription[] $AFTNMessageSubscription
     */
    protected $AFTNMessageSubscription = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return AFTNMessageSubscription[]
     */
    public function getAFTNMessageSubscription()
    {
      return $this->AFTNMessageSubscription;
    }

    /**
     * @param AFTNMessageSubscription[] $AFTNMessageSubscription
     * @return ArrayOfAFTNMessageSubscription
     */
    public function setAFTNMessageSubscription(array $AFTNMessageSubscription = null)
    {
      $this->AFTNMessageSubscription = $AFTNMessageSubscription;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->AFTNMessageSubscription[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return AFTNMessageSubscription
     */
    public function offsetGet($offset)
    {
      return $this->AFTNMessageSubscription[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param AFTNMessageSubscription $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->AFTNMessageSubscription[] = $value;
      } else {
        $this->AFTNMessageSubscription[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->AFTNMessageSubscription[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return AFTNMessageSubscription Return the current element
     */
    public function current()
    {
      return current($this->AFTNMessageSubscription);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->AFTNMessageSubscription);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->AFTNMessageSubscription);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->AFTNMessageSubscription);
    }

    /**
     * Countable implementation
     *
     * @return AFTNMessageSubscription Return count of elements
     */
    public function count()
    {
      return count($this->AFTNMessageSubscription);
    }

}
