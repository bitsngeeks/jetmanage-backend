<?php

class CancelFlightResposne
{

    /**
     * @var ATCCancelMessage $Message
     */
    protected $Message = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ATCCancelMessage
     */
    public function getMessage()
    {
      return $this->Message;
    }

    /**
     * @param ATCCancelMessage $Message
     * @return CancelFlightResposne
     */
    public function setMessage($Message)
    {
      $this->Message = $Message;
      return $this;
    }

}
