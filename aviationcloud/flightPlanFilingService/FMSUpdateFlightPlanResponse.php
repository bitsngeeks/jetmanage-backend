<?php

class FMSUpdateFlightPlanResponse
{

    /**
     * @var FMSUpdateResponse $FMSUpdateFlightPlanResult
     */
    protected $FMSUpdateFlightPlanResult = null;

    /**
     * @param FMSUpdateResponse $FMSUpdateFlightPlanResult
     */
    public function __construct($FMSUpdateFlightPlanResult)
    {
      $this->FMSUpdateFlightPlanResult = $FMSUpdateFlightPlanResult;
    }

    /**
     * @return FMSUpdateResponse
     */
    public function getFMSUpdateFlightPlanResult()
    {
      return $this->FMSUpdateFlightPlanResult;
    }

    /**
     * @param FMSUpdateResponse $FMSUpdateFlightPlanResult
     * @return FMSUpdateFlightPlanResponse
     */
    public function setFMSUpdateFlightPlanResult($FMSUpdateFlightPlanResult)
    {
      $this->FMSUpdateFlightPlanResult = $FMSUpdateFlightPlanResult;
      return $this;
    }

}
