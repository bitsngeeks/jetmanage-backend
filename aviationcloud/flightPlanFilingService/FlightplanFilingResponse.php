<?php

class FlightplanFilingResponse
{

    /**
     * @var ArrayOfAFTNAddress $AFTNAddresses
     */
    protected $AFTNAddresses = null;

    /**
     * @var ATCFPLMessage $FPLMessage
     */
    protected $FPLMessage = null;

    /**
     * @var string $FiledATCMessage
     */
    protected $FiledATCMessage = null;

    /**
     * @var int $FiledFlightplanId
     */
    protected $FiledFlightplanId = null;

    /**
     * @var boolean $IsDeferredFiling
     */
    protected $IsDeferredFiling = null;

    /**
     * @var string $MetaFPLString
     */
    protected $MetaFPLString = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfAFTNAddress
     */
    public function getAFTNAddresses()
    {
      return $this->AFTNAddresses;
    }

    /**
     * @param ArrayOfAFTNAddress $AFTNAddresses
     * @return FlightplanFilingResponse
     */
    public function setAFTNAddresses($AFTNAddresses)
    {
      $this->AFTNAddresses = $AFTNAddresses;
      return $this;
    }

    /**
     * @return ATCFPLMessage
     */
    public function getFPLMessage()
    {
      return $this->FPLMessage;
    }

    /**
     * @param ATCFPLMessage $FPLMessage
     * @return FlightplanFilingResponse
     */
    public function setFPLMessage($FPLMessage)
    {
      $this->FPLMessage = $FPLMessage;
      return $this;
    }

    /**
     * @return string
     */
    public function getFiledATCMessage()
    {
      return $this->FiledATCMessage;
    }

    /**
     * @param string $FiledATCMessage
     * @return FlightplanFilingResponse
     */
    public function setFiledATCMessage($FiledATCMessage)
    {
      $this->FiledATCMessage = $FiledATCMessage;
      return $this;
    }

    /**
     * @return int
     */
    public function getFiledFlightplanId()
    {
      return $this->FiledFlightplanId;
    }

    /**
     * @param int $FiledFlightplanId
     * @return FlightplanFilingResponse
     */
    public function setFiledFlightplanId($FiledFlightplanId)
    {
      $this->FiledFlightplanId = $FiledFlightplanId;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsDeferredFiling()
    {
      return $this->IsDeferredFiling;
    }

    /**
     * @param boolean $IsDeferredFiling
     * @return FlightplanFilingResponse
     */
    public function setIsDeferredFiling($IsDeferredFiling)
    {
      $this->IsDeferredFiling = $IsDeferredFiling;
      return $this;
    }

    /**
     * @return string
     */
    public function getMetaFPLString()
    {
      return $this->MetaFPLString;
    }

    /**
     * @param string $MetaFPLString
     * @return FlightplanFilingResponse
     */
    public function setMetaFPLString($MetaFPLString)
    {
      $this->MetaFPLString = $MetaFPLString;
      return $this;
    }

}
