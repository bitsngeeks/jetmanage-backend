<?php

class FlightMetaData
{

    /**
     * @var \DateTime $MetrologicalDataTimestamp
     */
    protected $MetrologicalDataTimestamp = null;

    /**
     * @var \DateTime $TimeComputed
     */
    protected $TimeComputed = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return \DateTime
     */
    public function getMetrologicalDataTimestamp()
    {
      if ($this->MetrologicalDataTimestamp == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->MetrologicalDataTimestamp);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $MetrologicalDataTimestamp
     * @return FlightMetaData
     */
    public function setMetrologicalDataTimestamp(\DateTime $MetrologicalDataTimestamp = null)
    {
      if ($MetrologicalDataTimestamp == null) {
       $this->MetrologicalDataTimestamp = null;
      } else {
        $this->MetrologicalDataTimestamp = $MetrologicalDataTimestamp->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getTimeComputed()
    {
      if ($this->TimeComputed == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->TimeComputed);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $TimeComputed
     * @return FlightMetaData
     */
    public function setTimeComputed(\DateTime $TimeComputed = null)
    {
      if ($TimeComputed == null) {
       $this->TimeComputed = null;
      } else {
        $this->TimeComputed = $TimeComputed->format(\DateTime::ATOM);
      }
      return $this;
    }

}
