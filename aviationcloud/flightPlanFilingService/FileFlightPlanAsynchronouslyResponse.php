<?php

class FileFlightPlanAsynchronouslyResponse
{

    /**
     * @var FlightplanFilingResponse $FileFlightPlanAsynchronouslyResult
     */
    protected $FileFlightPlanAsynchronouslyResult = null;

    /**
     * @param FlightplanFilingResponse $FileFlightPlanAsynchronouslyResult
     */
    public function __construct($FileFlightPlanAsynchronouslyResult)
    {
      $this->FileFlightPlanAsynchronouslyResult = $FileFlightPlanAsynchronouslyResult;
    }

    /**
     * @return FlightplanFilingResponse
     */
    public function getFileFlightPlanAsynchronouslyResult()
    {
      return $this->FileFlightPlanAsynchronouslyResult;
    }

    /**
     * @param FlightplanFilingResponse $FileFlightPlanAsynchronouslyResult
     * @return FileFlightPlanAsynchronouslyResponse
     */
    public function setFileFlightPlanAsynchronouslyResult($FileFlightPlanAsynchronouslyResult)
    {
      $this->FileFlightPlanAsynchronouslyResult = $FileFlightPlanAsynchronouslyResult;
      return $this;
    }

}
