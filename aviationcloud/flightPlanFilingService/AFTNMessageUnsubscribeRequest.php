<?php

class AFTNMessageUnsubscribeRequest extends RequestBase
{

    /**
     * @var boolean $IsProductionSubscription
     */
    protected $IsProductionSubscription = null;

    /**
     * @var int $SubscriptionId
     */
    protected $SubscriptionId = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return boolean
     */
    public function getIsProductionSubscription()
    {
      return $this->IsProductionSubscription;
    }

    /**
     * @param boolean $IsProductionSubscription
     * @return AFTNMessageUnsubscribeRequest
     */
    public function setIsProductionSubscription($IsProductionSubscription)
    {
      $this->IsProductionSubscription = $IsProductionSubscription;
      return $this;
    }

    /**
     * @return int
     */
    public function getSubscriptionId()
    {
      return $this->SubscriptionId;
    }

    /**
     * @param int $SubscriptionId
     * @return AFTNMessageUnsubscribeRequest
     */
    public function setSubscriptionId($SubscriptionId)
    {
      $this->SubscriptionId = $SubscriptionId;
      return $this;
    }

}
