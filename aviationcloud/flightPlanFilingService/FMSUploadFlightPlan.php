<?php

class FMSUploadFlightPlan
{

    /**
     * @var FMSUploadRequest $request
     */
    protected $request = null;

    /**
     * @param FMSUploadRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return FMSUploadRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param FMSUploadRequest $request
     * @return FMSUploadFlightPlan
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
