<?php

class FlightPlanUpdateRequest extends RequestBase
{

    /**
     * @var boolean $AllowNoUpdates
     */
    protected $AllowNoUpdates = null;

    /**
     * @var EmulatorResponseOptions $EmulatorResponseOptions
     */
    protected $EmulatorResponseOptions = null;

    /**
     * @var boolean $EnableProductionUpdate
     */
    protected $EnableProductionUpdate = null;

    /**
     * @var boolean $EnableRegionRestriction
     */
    protected $EnableRegionRestriction = null;

    /**
     * @var boolean $EnableRouteSyntaxChecking
     */
    protected $EnableRouteSyntaxChecking = null;

    /**
     * @var int $FlightplanId
     */
    protected $FlightplanId = null;

    /**
     * @var ATCFPLMessage $NewFPLMessage
     */
    protected $NewFPLMessage = null;

    /**
     * @var string $PreferredTransportProvider
     */
    protected $PreferredTransportProvider = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return boolean
     */
    public function getAllowNoUpdates()
    {
      return $this->AllowNoUpdates;
    }

    /**
     * @param boolean $AllowNoUpdates
     * @return FlightPlanUpdateRequest
     */
    public function setAllowNoUpdates($AllowNoUpdates)
    {
      $this->AllowNoUpdates = $AllowNoUpdates;
      return $this;
    }

    /**
     * @return EmulatorResponseOptions
     */
    public function getEmulatorResponseOptions()
    {
      return $this->EmulatorResponseOptions;
    }

    /**
     * @param EmulatorResponseOptions $EmulatorResponseOptions
     * @return FlightPlanUpdateRequest
     */
    public function setEmulatorResponseOptions($EmulatorResponseOptions)
    {
      $this->EmulatorResponseOptions = $EmulatorResponseOptions;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getEnableProductionUpdate()
    {
      return $this->EnableProductionUpdate;
    }

    /**
     * @param boolean $EnableProductionUpdate
     * @return FlightPlanUpdateRequest
     */
    public function setEnableProductionUpdate($EnableProductionUpdate)
    {
      $this->EnableProductionUpdate = $EnableProductionUpdate;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getEnableRegionRestriction()
    {
      return $this->EnableRegionRestriction;
    }

    /**
     * @param boolean $EnableRegionRestriction
     * @return FlightPlanUpdateRequest
     */
    public function setEnableRegionRestriction($EnableRegionRestriction)
    {
      $this->EnableRegionRestriction = $EnableRegionRestriction;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getEnableRouteSyntaxChecking()
    {
      return $this->EnableRouteSyntaxChecking;
    }

    /**
     * @param boolean $EnableRouteSyntaxChecking
     * @return FlightPlanUpdateRequest
     */
    public function setEnableRouteSyntaxChecking($EnableRouteSyntaxChecking)
    {
      $this->EnableRouteSyntaxChecking = $EnableRouteSyntaxChecking;
      return $this;
    }

    /**
     * @return int
     */
    public function getFlightplanId()
    {
      return $this->FlightplanId;
    }

    /**
     * @param int $FlightplanId
     * @return FlightPlanUpdateRequest
     */
    public function setFlightplanId($FlightplanId)
    {
      $this->FlightplanId = $FlightplanId;
      return $this;
    }

    /**
     * @return ATCFPLMessage
     */
    public function getNewFPLMessage()
    {
      return $this->NewFPLMessage;
    }

    /**
     * @param ATCFPLMessage $NewFPLMessage
     * @return FlightPlanUpdateRequest
     */
    public function setNewFPLMessage($NewFPLMessage)
    {
      $this->NewFPLMessage = $NewFPLMessage;
      return $this;
    }

    /**
     * @return string
     */
    public function getPreferredTransportProvider()
    {
      return $this->PreferredTransportProvider;
    }

    /**
     * @param string $PreferredTransportProvider
     * @return FlightPlanUpdateRequest
     */
    public function setPreferredTransportProvider($PreferredTransportProvider)
    {
      $this->PreferredTransportProvider = $PreferredTransportProvider;
      return $this;
    }

}
