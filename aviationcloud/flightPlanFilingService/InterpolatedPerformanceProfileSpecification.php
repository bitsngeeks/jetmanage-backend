<?php

class InterpolatedPerformanceProfileSpecification extends PerformanceProfileSpecification
{

    /**
     * @var float $Index
     */
    protected $Index = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getIndex()
    {
      return $this->Index;
    }

    /**
     * @param float $Index
     * @return InterpolatedPerformanceProfileSpecification
     */
    public function setIndex($Index)
    {
      $this->Index = $Index;
      return $this;
    }

}
