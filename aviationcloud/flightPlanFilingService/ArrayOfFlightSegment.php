<?php

class ArrayOfFlightSegment implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var FlightSegment[] $FlightSegment
     */
    protected $FlightSegment = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return FlightSegment[]
     */
    public function getFlightSegment()
    {
      return $this->FlightSegment;
    }

    /**
     * @param FlightSegment[] $FlightSegment
     * @return ArrayOfFlightSegment
     */
    public function setFlightSegment(array $FlightSegment = null)
    {
      $this->FlightSegment = $FlightSegment;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->FlightSegment[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return FlightSegment
     */
    public function offsetGet($offset)
    {
      return $this->FlightSegment[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param FlightSegment $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->FlightSegment[] = $value;
      } else {
        $this->FlightSegment[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->FlightSegment[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return FlightSegment Return the current element
     */
    public function current()
    {
      return current($this->FlightSegment);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->FlightSegment);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->FlightSegment);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->FlightSegment);
    }

    /**
     * Countable implementation
     *
     * @return FlightSegment Return count of elements
     */
    public function count()
    {
      return count($this->FlightSegment);
    }

}
