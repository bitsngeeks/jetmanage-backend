<?php

class FlightATCEETElement
{

    /**
     * @var SphereicPoint $Coordinate
     */
    protected $Coordinate = null;

    /**
     * @var Length $DistanceInFIR
     */
    protected $DistanceInFIR = null;

    /**
     * @var \DateTime $EntryTime
     */
    protected $EntryTime = null;

    /**
     * @var \DateTime $ExitTime
     */
    protected $ExitTime = null;

    /**
     * @var Time $MinutesToReach
     */
    protected $MinutesToReach = null;

    /**
     * @var FIR $OverflownFIR
     */
    protected $OverflownFIR = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return SphereicPoint
     */
    public function getCoordinate()
    {
      return $this->Coordinate;
    }

    /**
     * @param SphereicPoint $Coordinate
     * @return FlightATCEETElement
     */
    public function setCoordinate($Coordinate)
    {
      $this->Coordinate = $Coordinate;
      return $this;
    }

    /**
     * @return Length
     */
    public function getDistanceInFIR()
    {
      return $this->DistanceInFIR;
    }

    /**
     * @param Length $DistanceInFIR
     * @return FlightATCEETElement
     */
    public function setDistanceInFIR($DistanceInFIR)
    {
      $this->DistanceInFIR = $DistanceInFIR;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEntryTime()
    {
      if ($this->EntryTime == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->EntryTime);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $EntryTime
     * @return FlightATCEETElement
     */
    public function setEntryTime(\DateTime $EntryTime = null)
    {
      if ($EntryTime == null) {
       $this->EntryTime = null;
      } else {
        $this->EntryTime = $EntryTime->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getExitTime()
    {
      if ($this->ExitTime == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->ExitTime);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $ExitTime
     * @return FlightATCEETElement
     */
    public function setExitTime(\DateTime $ExitTime = null)
    {
      if ($ExitTime == null) {
       $this->ExitTime = null;
      } else {
        $this->ExitTime = $ExitTime->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return Time
     */
    public function getMinutesToReach()
    {
      return $this->MinutesToReach;
    }

    /**
     * @param Time $MinutesToReach
     * @return FlightATCEETElement
     */
    public function setMinutesToReach($MinutesToReach)
    {
      $this->MinutesToReach = $MinutesToReach;
      return $this;
    }

    /**
     * @return FIR
     */
    public function getOverflownFIR()
    {
      return $this->OverflownFIR;
    }

    /**
     * @param FIR $OverflownFIR
     * @return FlightATCEETElement
     */
    public function setOverflownFIR($OverflownFIR)
    {
      $this->OverflownFIR = $OverflownFIR;
      return $this;
    }

}
