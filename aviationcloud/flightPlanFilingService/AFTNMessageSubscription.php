<?php

class AFTNMessageSubscription
{

    /**
     * @var AwsSQSContext $AwsSQSContext
     */
    protected $AwsSQSContext = null;

    /**
     * @var BasicHttpAuthCredentials $BasicHttpAuthCredentials
     */
    protected $BasicHttpAuthCredentials = null;

    /**
     * @var DataSerializationFormat $DataFormat
     */
    protected $DataFormat = null;

    /**
     * @var string $DeliveryAddress
     */
    protected $DeliveryAddress = null;

    /**
     * @var DataDeliveryMethod $DeliveryMethod
     */
    protected $DeliveryMethod = null;

    /**
     * @var boolean $IsProductionSubscription
     */
    protected $IsProductionSubscription = null;

    /**
     * @var int $MessageSubscriptionId
     */
    protected $MessageSubscriptionId = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return AwsSQSContext
     */
    public function getAwsSQSContext()
    {
      return $this->AwsSQSContext;
    }

    /**
     * @param AwsSQSContext $AwsSQSContext
     * @return AFTNMessageSubscription
     */
    public function setAwsSQSContext($AwsSQSContext)
    {
      $this->AwsSQSContext = $AwsSQSContext;
      return $this;
    }

    /**
     * @return BasicHttpAuthCredentials
     */
    public function getBasicHttpAuthCredentials()
    {
      return $this->BasicHttpAuthCredentials;
    }

    /**
     * @param BasicHttpAuthCredentials $BasicHttpAuthCredentials
     * @return AFTNMessageSubscription
     */
    public function setBasicHttpAuthCredentials($BasicHttpAuthCredentials)
    {
      $this->BasicHttpAuthCredentials = $BasicHttpAuthCredentials;
      return $this;
    }

    /**
     * @return DataSerializationFormat
     */
    public function getDataFormat()
    {
      return $this->DataFormat;
    }

    /**
     * @param DataSerializationFormat $DataFormat
     * @return AFTNMessageSubscription
     */
    public function setDataFormat($DataFormat)
    {
      $this->DataFormat = $DataFormat;
      return $this;
    }

    /**
     * @return string
     */
    public function getDeliveryAddress()
    {
      return $this->DeliveryAddress;
    }

    /**
     * @param string $DeliveryAddress
     * @return AFTNMessageSubscription
     */
    public function setDeliveryAddress($DeliveryAddress)
    {
      $this->DeliveryAddress = $DeliveryAddress;
      return $this;
    }

    /**
     * @return DataDeliveryMethod
     */
    public function getDeliveryMethod()
    {
      return $this->DeliveryMethod;
    }

    /**
     * @param DataDeliveryMethod $DeliveryMethod
     * @return AFTNMessageSubscription
     */
    public function setDeliveryMethod($DeliveryMethod)
    {
      $this->DeliveryMethod = $DeliveryMethod;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsProductionSubscription()
    {
      return $this->IsProductionSubscription;
    }

    /**
     * @param boolean $IsProductionSubscription
     * @return AFTNMessageSubscription
     */
    public function setIsProductionSubscription($IsProductionSubscription)
    {
      $this->IsProductionSubscription = $IsProductionSubscription;
      return $this;
    }

    /**
     * @return int
     */
    public function getMessageSubscriptionId()
    {
      return $this->MessageSubscriptionId;
    }

    /**
     * @param int $MessageSubscriptionId
     * @return AFTNMessageSubscription
     */
    public function setMessageSubscriptionId($MessageSubscriptionId)
    {
      $this->MessageSubscriptionId = $MessageSubscriptionId;
      return $this;
    }

}
