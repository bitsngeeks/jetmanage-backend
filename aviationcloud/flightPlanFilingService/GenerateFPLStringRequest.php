<?php

class GenerateFPLStringRequest extends RequestBase
{

    /**
     * @var ATCFPLMessage $FPLMessage
     */
    protected $FPLMessage = null;

    /**
     * @var boolean $IncludeSupplementaryInfo
     */
    protected $IncludeSupplementaryInfo = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return ATCFPLMessage
     */
    public function getFPLMessage()
    {
      return $this->FPLMessage;
    }

    /**
     * @param ATCFPLMessage $FPLMessage
     * @return GenerateFPLStringRequest
     */
    public function setFPLMessage($FPLMessage)
    {
      $this->FPLMessage = $FPLMessage;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIncludeSupplementaryInfo()
    {
      return $this->IncludeSupplementaryInfo;
    }

    /**
     * @param boolean $IncludeSupplementaryInfo
     * @return GenerateFPLStringRequest
     */
    public function setIncludeSupplementaryInfo($IncludeSupplementaryInfo)
    {
      $this->IncludeSupplementaryInfo = $IncludeSupplementaryInfo;
      return $this;
    }

}
