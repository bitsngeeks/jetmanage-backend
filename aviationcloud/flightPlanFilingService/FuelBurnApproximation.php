<?php

class FuelBurnApproximation
{

    /**
     * @var float $Slope
     */
    protected $Slope = null;

    /**
     * @var float $YIntersection
     */
    protected $YIntersection = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return float
     */
    public function getSlope()
    {
      return $this->Slope;
    }

    /**
     * @param float $Slope
     * @return FuelBurnApproximation
     */
    public function setSlope($Slope)
    {
      $this->Slope = $Slope;
      return $this;
    }

    /**
     * @return float
     */
    public function getYIntersection()
    {
      return $this->YIntersection;
    }

    /**
     * @param float $YIntersection
     * @return FuelBurnApproximation
     */
    public function setYIntersection($YIntersection)
    {
      $this->YIntersection = $YIntersection;
      return $this;
    }

}
