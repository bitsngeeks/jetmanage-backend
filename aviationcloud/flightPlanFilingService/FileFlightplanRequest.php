<?php

class FileFlightplanRequest extends RequestBase
{

    /**
     * @var DeferredFilingOptions $DeferredFilingOptions
     */
    protected $DeferredFilingOptions = null;

    /**
     * @var EmulatorResponseOptions $EmulatorResponseOptions
     */
    protected $EmulatorResponseOptions = null;

    /**
     * @var boolean $EnableForceRefile
     */
    protected $EnableForceRefile = null;

    /**
     * @var boolean $EnableProductionFiling
     */
    protected $EnableProductionFiling = null;

    /**
     * @var boolean $EnableRegionRestriction
     */
    protected $EnableRegionRestriction = null;

    /**
     * @var boolean $EnableRouteSyntaxChecking
     */
    protected $EnableRouteSyntaxChecking = null;

    /**
     * @var ATCFPLMessage $FPLMessage
     */
    protected $FPLMessage = null;

    /**
     * @var FlightLog $Flightlog
     */
    protected $Flightlog = null;

    /**
     * @var string $PreferredTransportProvider
     */
    protected $PreferredTransportProvider = null;

    /**
     * @var boolean $SendToTestSystem
     */
    protected $SendToTestSystem = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return DeferredFilingOptions
     */
    public function getDeferredFilingOptions()
    {
      return $this->DeferredFilingOptions;
    }

    /**
     * @param DeferredFilingOptions $DeferredFilingOptions
     * @return FileFlightplanRequest
     */
    public function setDeferredFilingOptions($DeferredFilingOptions)
    {
      $this->DeferredFilingOptions = $DeferredFilingOptions;
      return $this;
    }

    /**
     * @return EmulatorResponseOptions
     */
    public function getEmulatorResponseOptions()
    {
      return $this->EmulatorResponseOptions;
    }

    /**
     * @param EmulatorResponseOptions $EmulatorResponseOptions
     * @return FileFlightplanRequest
     */
    public function setEmulatorResponseOptions($EmulatorResponseOptions)
    {
      $this->EmulatorResponseOptions = $EmulatorResponseOptions;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getEnableForceRefile()
    {
      return $this->EnableForceRefile;
    }

    /**
     * @param boolean $EnableForceRefile
     * @return FileFlightplanRequest
     */
    public function setEnableForceRefile($EnableForceRefile)
    {
      $this->EnableForceRefile = $EnableForceRefile;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getEnableProductionFiling()
    {
      return $this->EnableProductionFiling;
    }

    /**
     * @param boolean $EnableProductionFiling
     * @return FileFlightplanRequest
     */
    public function setEnableProductionFiling($EnableProductionFiling)
    {
      $this->EnableProductionFiling = $EnableProductionFiling;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getEnableRegionRestriction()
    {
      return $this->EnableRegionRestriction;
    }

    /**
     * @param boolean $EnableRegionRestriction
     * @return FileFlightplanRequest
     */
    public function setEnableRegionRestriction($EnableRegionRestriction)
    {
      $this->EnableRegionRestriction = $EnableRegionRestriction;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getEnableRouteSyntaxChecking()
    {
      return $this->EnableRouteSyntaxChecking;
    }

    /**
     * @param boolean $EnableRouteSyntaxChecking
     * @return FileFlightplanRequest
     */
    public function setEnableRouteSyntaxChecking($EnableRouteSyntaxChecking)
    {
      $this->EnableRouteSyntaxChecking = $EnableRouteSyntaxChecking;
      return $this;
    }

    /**
     * @return ATCFPLMessage
     */
    public function getFPLMessage()
    {
      return $this->FPLMessage;
    }

    /**
     * @param ATCFPLMessage $FPLMessage
     * @return FileFlightplanRequest
     */
    public function setFPLMessage($FPLMessage)
    {
      $this->FPLMessage = $FPLMessage;
      return $this;
    }

    /**
     * @return FlightLog
     */
    public function getFlightlog()
    {
      return $this->Flightlog;
    }

    /**
     * @param FlightLog $Flightlog
     * @return FileFlightplanRequest
     */
    public function setFlightlog($Flightlog)
    {
      $this->Flightlog = $Flightlog;
      return $this;
    }

    /**
     * @return string
     */
    public function getPreferredTransportProvider()
    {
      return $this->PreferredTransportProvider;
    }

    /**
     * @param string $PreferredTransportProvider
     * @return FileFlightplanRequest
     */
    public function setPreferredTransportProvider($PreferredTransportProvider)
    {
      $this->PreferredTransportProvider = $PreferredTransportProvider;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getSendToTestSystem()
    {
      return $this->SendToTestSystem;
    }

    /**
     * @param boolean $SendToTestSystem
     * @return FileFlightplanRequest
     */
    public function setSendToTestSystem($SendToTestSystem)
    {
      $this->SendToTestSystem = $SendToTestSystem;
      return $this;
    }

}
