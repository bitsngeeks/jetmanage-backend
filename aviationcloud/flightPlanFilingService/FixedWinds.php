<?php

class FixedWinds extends Winds
{

    /**
     * @var int $FixedISADeviation
     */
    protected $FixedISADeviation = null;

    /**
     * @var int $WindComponent
     */
    protected $WindComponent = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getFixedISADeviation()
    {
      return $this->FixedISADeviation;
    }

    /**
     * @param int $FixedISADeviation
     * @return FixedWinds
     */
    public function setFixedISADeviation($FixedISADeviation)
    {
      $this->FixedISADeviation = $FixedISADeviation;
      return $this;
    }

    /**
     * @return int
     */
    public function getWindComponent()
    {
      return $this->WindComponent;
    }

    /**
     * @param int $WindComponent
     * @return FixedWinds
     */
    public function setWindComponent($WindComponent)
    {
      $this->WindComponent = $WindComponent;
      return $this;
    }

}
