<?php

class SpecificCargoLoadSpecification extends CargoLoadSpecification
{

    /**
     * @var ArrayOfCargoSectionLoad $SectionLoads
     */
    protected $SectionLoads = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return ArrayOfCargoSectionLoad
     */
    public function getSectionLoads()
    {
      return $this->SectionLoads;
    }

    /**
     * @param ArrayOfCargoSectionLoad $SectionLoads
     * @return SpecificCargoLoadSpecification
     */
    public function setSectionLoads($SectionLoads)
    {
      $this->SectionLoads = $SectionLoads;
      return $this;
    }

}
