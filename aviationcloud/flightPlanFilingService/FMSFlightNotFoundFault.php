<?php

class FMSFlightNotFoundFault
{

    /**
     * @var string $AircraftIdent
     */
    protected $AircraftIdent = null;

    /**
     * @var string $ErrorCode
     */
    protected $ErrorCode = null;

    /**
     * @var string $RecallIdent
     */
    protected $RecallIdent = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getAircraftIdent()
    {
      return $this->AircraftIdent;
    }

    /**
     * @param string $AircraftIdent
     * @return FMSFlightNotFoundFault
     */
    public function setAircraftIdent($AircraftIdent)
    {
      $this->AircraftIdent = $AircraftIdent;
      return $this;
    }

    /**
     * @return string
     */
    public function getErrorCode()
    {
      return $this->ErrorCode;
    }

    /**
     * @param string $ErrorCode
     * @return FMSFlightNotFoundFault
     */
    public function setErrorCode($ErrorCode)
    {
      $this->ErrorCode = $ErrorCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getRecallIdent()
    {
      return $this->RecallIdent;
    }

    /**
     * @param string $RecallIdent
     * @return FMSFlightNotFoundFault
     */
    public function setRecallIdent($RecallIdent)
    {
      $this->RecallIdent = $RecallIdent;
      return $this;
    }

}
