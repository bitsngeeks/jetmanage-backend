<?php

class RetrieveFiledFlights
{

    /**
     * @var RetrieveFiledFlightsRequest $request
     */
    protected $request = null;

    /**
     * @param RetrieveFiledFlightsRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return RetrieveFiledFlightsRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param RetrieveFiledFlightsRequest $request
     * @return RetrieveFiledFlights
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
