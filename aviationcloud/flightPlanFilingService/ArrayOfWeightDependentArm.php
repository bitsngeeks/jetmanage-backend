<?php

class ArrayOfWeightDependentArm implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var WeightDependentArm[] $WeightDependentArm
     */
    protected $WeightDependentArm = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return WeightDependentArm[]
     */
    public function getWeightDependentArm()
    {
      return $this->WeightDependentArm;
    }

    /**
     * @param WeightDependentArm[] $WeightDependentArm
     * @return ArrayOfWeightDependentArm
     */
    public function setWeightDependentArm(array $WeightDependentArm = null)
    {
      $this->WeightDependentArm = $WeightDependentArm;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->WeightDependentArm[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return WeightDependentArm
     */
    public function offsetGet($offset)
    {
      return $this->WeightDependentArm[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param WeightDependentArm $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->WeightDependentArm[] = $value;
      } else {
        $this->WeightDependentArm[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->WeightDependentArm[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return WeightDependentArm Return the current element
     */
    public function current()
    {
      return current($this->WeightDependentArm);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->WeightDependentArm);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->WeightDependentArm);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->WeightDependentArm);
    }

    /**
     * Countable implementation
     *
     * @return WeightDependentArm Return count of elements
     */
    public function count()
    {
      return count($this->WeightDependentArm);
    }

}
