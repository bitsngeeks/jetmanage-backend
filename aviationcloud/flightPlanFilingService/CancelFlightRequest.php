<?php

class CancelFlightRequest extends RequestBase
{

    /**
     * @var FlightIdentifier $FlightIdentifier
     */
    protected $FlightIdentifier = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return FlightIdentifier
     */
    public function getFlightIdentifier()
    {
      return $this->FlightIdentifier;
    }

    /**
     * @param FlightIdentifier $FlightIdentifier
     * @return CancelFlightRequest
     */
    public function setFlightIdentifier($FlightIdentifier)
    {
      $this->FlightIdentifier = $FlightIdentifier;
      return $this;
    }

}
