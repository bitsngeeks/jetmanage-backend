<?php

class GenerateFPLString
{

    /**
     * @var GenerateFPLStringRequest $request
     */
    protected $request = null;

    /**
     * @param GenerateFPLStringRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return GenerateFPLStringRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param GenerateFPLStringRequest $request
     * @return GenerateFPLString
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
