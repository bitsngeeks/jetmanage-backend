<?php

class ETPPoint
{

    /**
     * @var ETPData $AfterAirportData
     */
    protected $AfterAirportData = null;

    /**
     * @var Airport $AfterPointAirport
     */
    protected $AfterPointAirport = null;

    /**
     * @var ETPData $BeforeAirportData
     */
    protected $BeforeAirportData = null;

    /**
     * @var Airport $BeforePointAirport
     */
    protected $BeforePointAirport = null;

    /**
     * @var string $CruiseProfile
     */
    protected $CruiseProfile = null;

    /**
     * @var Length $EnrouteDistance
     */
    protected $EnrouteDistance = null;

    /**
     * @var float $EnrouteTAS
     */
    protected $EnrouteTAS = null;

    /**
     * @var Time $EnrouteTime
     */
    protected $EnrouteTime = null;

    /**
     * @var int $FinalLevelOffAltitude
     */
    protected $FinalLevelOffAltitude = null;

    /**
     * @var Weight $FuelRemaining
     */
    protected $FuelRemaining = null;

    /**
     * @var Weight $FuelUsed
     */
    protected $FuelUsed = null;

    /**
     * @var Altitude $Height
     */
    protected $Height = null;

    /**
     * @var int $InitialFlightLevel
     */
    protected $InitialFlightLevel = null;

    /**
     * @var SphereicPoint $Location
     */
    protected $Location = null;

    /**
     * @var Weight $MinRequiredAtETP
     */
    protected $MinRequiredAtETP = null;

    /**
     * @var Time $OxygenTime
     */
    protected $OxygenTime = null;

    /**
     * @var string $ScenarioName
     */
    protected $ScenarioName = null;

    /**
     * @var string $ScenarioShortName
     */
    protected $ScenarioShortName = null;

    /**
     * @var \DateTime $Time
     */
    protected $Time = null;

    /**
     * @var string $Type
     */
    protected $Type = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ETPData
     */
    public function getAfterAirportData()
    {
      return $this->AfterAirportData;
    }

    /**
     * @param ETPData $AfterAirportData
     * @return ETPPoint
     */
    public function setAfterAirportData($AfterAirportData)
    {
      $this->AfterAirportData = $AfterAirportData;
      return $this;
    }

    /**
     * @return Airport
     */
    public function getAfterPointAirport()
    {
      return $this->AfterPointAirport;
    }

    /**
     * @param Airport $AfterPointAirport
     * @return ETPPoint
     */
    public function setAfterPointAirport($AfterPointAirport)
    {
      $this->AfterPointAirport = $AfterPointAirport;
      return $this;
    }

    /**
     * @return ETPData
     */
    public function getBeforeAirportData()
    {
      return $this->BeforeAirportData;
    }

    /**
     * @param ETPData $BeforeAirportData
     * @return ETPPoint
     */
    public function setBeforeAirportData($BeforeAirportData)
    {
      $this->BeforeAirportData = $BeforeAirportData;
      return $this;
    }

    /**
     * @return Airport
     */
    public function getBeforePointAirport()
    {
      return $this->BeforePointAirport;
    }

    /**
     * @param Airport $BeforePointAirport
     * @return ETPPoint
     */
    public function setBeforePointAirport($BeforePointAirport)
    {
      $this->BeforePointAirport = $BeforePointAirport;
      return $this;
    }

    /**
     * @return string
     */
    public function getCruiseProfile()
    {
      return $this->CruiseProfile;
    }

    /**
     * @param string $CruiseProfile
     * @return ETPPoint
     */
    public function setCruiseProfile($CruiseProfile)
    {
      $this->CruiseProfile = $CruiseProfile;
      return $this;
    }

    /**
     * @return Length
     */
    public function getEnrouteDistance()
    {
      return $this->EnrouteDistance;
    }

    /**
     * @param Length $EnrouteDistance
     * @return ETPPoint
     */
    public function setEnrouteDistance($EnrouteDistance)
    {
      $this->EnrouteDistance = $EnrouteDistance;
      return $this;
    }

    /**
     * @return float
     */
    public function getEnrouteTAS()
    {
      return $this->EnrouteTAS;
    }

    /**
     * @param float $EnrouteTAS
     * @return ETPPoint
     */
    public function setEnrouteTAS($EnrouteTAS)
    {
      $this->EnrouteTAS = $EnrouteTAS;
      return $this;
    }

    /**
     * @return Time
     */
    public function getEnrouteTime()
    {
      return $this->EnrouteTime;
    }

    /**
     * @param Time $EnrouteTime
     * @return ETPPoint
     */
    public function setEnrouteTime($EnrouteTime)
    {
      $this->EnrouteTime = $EnrouteTime;
      return $this;
    }

    /**
     * @return int
     */
    public function getFinalLevelOffAltitude()
    {
      return $this->FinalLevelOffAltitude;
    }

    /**
     * @param int $FinalLevelOffAltitude
     * @return ETPPoint
     */
    public function setFinalLevelOffAltitude($FinalLevelOffAltitude)
    {
      $this->FinalLevelOffAltitude = $FinalLevelOffAltitude;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getFuelRemaining()
    {
      return $this->FuelRemaining;
    }

    /**
     * @param Weight $FuelRemaining
     * @return ETPPoint
     */
    public function setFuelRemaining($FuelRemaining)
    {
      $this->FuelRemaining = $FuelRemaining;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getFuelUsed()
    {
      return $this->FuelUsed;
    }

    /**
     * @param Weight $FuelUsed
     * @return ETPPoint
     */
    public function setFuelUsed($FuelUsed)
    {
      $this->FuelUsed = $FuelUsed;
      return $this;
    }

    /**
     * @return Altitude
     */
    public function getHeight()
    {
      return $this->Height;
    }

    /**
     * @param Altitude $Height
     * @return ETPPoint
     */
    public function setHeight($Height)
    {
      $this->Height = $Height;
      return $this;
    }

    /**
     * @return int
     */
    public function getInitialFlightLevel()
    {
      return $this->InitialFlightLevel;
    }

    /**
     * @param int $InitialFlightLevel
     * @return ETPPoint
     */
    public function setInitialFlightLevel($InitialFlightLevel)
    {
      $this->InitialFlightLevel = $InitialFlightLevel;
      return $this;
    }

    /**
     * @return SphereicPoint
     */
    public function getLocation()
    {
      return $this->Location;
    }

    /**
     * @param SphereicPoint $Location
     * @return ETPPoint
     */
    public function setLocation($Location)
    {
      $this->Location = $Location;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getMinRequiredAtETP()
    {
      return $this->MinRequiredAtETP;
    }

    /**
     * @param Weight $MinRequiredAtETP
     * @return ETPPoint
     */
    public function setMinRequiredAtETP($MinRequiredAtETP)
    {
      $this->MinRequiredAtETP = $MinRequiredAtETP;
      return $this;
    }

    /**
     * @return Time
     */
    public function getOxygenTime()
    {
      return $this->OxygenTime;
    }

    /**
     * @param Time $OxygenTime
     * @return ETPPoint
     */
    public function setOxygenTime($OxygenTime)
    {
      $this->OxygenTime = $OxygenTime;
      return $this;
    }

    /**
     * @return string
     */
    public function getScenarioName()
    {
      return $this->ScenarioName;
    }

    /**
     * @param string $ScenarioName
     * @return ETPPoint
     */
    public function setScenarioName($ScenarioName)
    {
      $this->ScenarioName = $ScenarioName;
      return $this;
    }

    /**
     * @return string
     */
    public function getScenarioShortName()
    {
      return $this->ScenarioShortName;
    }

    /**
     * @param string $ScenarioShortName
     * @return ETPPoint
     */
    public function setScenarioShortName($ScenarioShortName)
    {
      $this->ScenarioShortName = $ScenarioShortName;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getTime()
    {
      if ($this->Time == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->Time);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $Time
     * @return ETPPoint
     */
    public function setTime(\DateTime $Time = null)
    {
      if ($Time == null) {
       $this->Time = null;
      } else {
        $this->Time = $Time->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
      return $this->Type;
    }

    /**
     * @param string $Type
     * @return ETPPoint
     */
    public function setType($Type)
    {
      $this->Type = $Type;
      return $this;
    }

}
