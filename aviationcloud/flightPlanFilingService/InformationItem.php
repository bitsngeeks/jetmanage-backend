<?php

class InformationItem
{

    /**
     * @var int $Code
     */
    protected $Code = null;

    /**
     * @var ArrayOfInformationData $Data
     */
    protected $Data = null;

    /**
     * @var string $Text
     */
    protected $Text = null;

    /**
     * @var string $Type
     */
    protected $Type = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getCode()
    {
      return $this->Code;
    }

    /**
     * @param int $Code
     * @return InformationItem
     */
    public function setCode($Code)
    {
      $this->Code = $Code;
      return $this;
    }

    /**
     * @return ArrayOfInformationData
     */
    public function getData()
    {
      return $this->Data;
    }

    /**
     * @param ArrayOfInformationData $Data
     * @return InformationItem
     */
    public function setData($Data)
    {
      $this->Data = $Data;
      return $this;
    }

    /**
     * @return string
     */
    public function getText()
    {
      return $this->Text;
    }

    /**
     * @param string $Text
     * @return InformationItem
     */
    public function setText($Text)
    {
      $this->Text = $Text;
      return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
      return $this->Type;
    }

    /**
     * @param string $Type
     * @return InformationItem
     */
    public function setType($Type)
    {
      $this->Type = $Type;
      return $this;
    }

}
