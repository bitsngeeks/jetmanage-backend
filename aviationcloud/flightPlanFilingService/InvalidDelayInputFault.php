<?php

class InvalidDelayInputFault
{

    /**
     * @var \DateTime $CurrentEOBT
     */
    protected $CurrentEOBT = null;

    /**
     * @var string $ErrorCode
     */
    protected $ErrorCode = null;

    /**
     * @var string $ErrorMessage
     */
    protected $ErrorMessage = null;

    /**
     * @var int $FlightPlanId
     */
    protected $FlightPlanId = null;

    /**
     * @var boolean $IsProduction
     */
    protected $IsProduction = null;

    /**
     * @var \DateTime $NewEOBT
     */
    protected $NewEOBT = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return \DateTime
     */
    public function getCurrentEOBT()
    {
      if ($this->CurrentEOBT == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->CurrentEOBT);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $CurrentEOBT
     * @return InvalidDelayInputFault
     */
    public function setCurrentEOBT(\DateTime $CurrentEOBT = null)
    {
      if ($CurrentEOBT == null) {
       $this->CurrentEOBT = null;
      } else {
        $this->CurrentEOBT = $CurrentEOBT->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return string
     */
    public function getErrorCode()
    {
      return $this->ErrorCode;
    }

    /**
     * @param string $ErrorCode
     * @return InvalidDelayInputFault
     */
    public function setErrorCode($ErrorCode)
    {
      $this->ErrorCode = $ErrorCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
      return $this->ErrorMessage;
    }

    /**
     * @param string $ErrorMessage
     * @return InvalidDelayInputFault
     */
    public function setErrorMessage($ErrorMessage)
    {
      $this->ErrorMessage = $ErrorMessage;
      return $this;
    }

    /**
     * @return int
     */
    public function getFlightPlanId()
    {
      return $this->FlightPlanId;
    }

    /**
     * @param int $FlightPlanId
     * @return InvalidDelayInputFault
     */
    public function setFlightPlanId($FlightPlanId)
    {
      $this->FlightPlanId = $FlightPlanId;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsProduction()
    {
      return $this->IsProduction;
    }

    /**
     * @param boolean $IsProduction
     * @return InvalidDelayInputFault
     */
    public function setIsProduction($IsProduction)
    {
      $this->IsProduction = $IsProduction;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getNewEOBT()
    {
      if ($this->NewEOBT == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->NewEOBT);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $NewEOBT
     * @return InvalidDelayInputFault
     */
    public function setNewEOBT(\DateTime $NewEOBT = null)
    {
      if ($NewEOBT == null) {
       $this->NewEOBT = null;
      } else {
        $this->NewEOBT = $NewEOBT->format(\DateTime::ATOM);
      }
      return $this;
    }

}
