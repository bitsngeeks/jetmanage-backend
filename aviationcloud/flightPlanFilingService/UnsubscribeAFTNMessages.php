<?php

class UnsubscribeAFTNMessages
{

    /**
     * @var AFTNMessageUnsubscribeRequest $request
     */
    protected $request = null;

    /**
     * @param AFTNMessageUnsubscribeRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return AFTNMessageUnsubscribeRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param AFTNMessageUnsubscribeRequest $request
     * @return UnsubscribeAFTNMessages
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
