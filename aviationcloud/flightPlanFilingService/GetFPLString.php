<?php

class GetFPLString
{

    /**
     * @var FPLStringRequest $request
     */
    protected $request = null;

    /**
     * @param FPLStringRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return FPLStringRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param FPLStringRequest $request
     * @return GetFPLString
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
