<?php

class RouteNodeIdentifier
{

    /**
     * @var string $Designator
     */
    protected $Designator = null;

    /**
     * @var string $Identifier
     */
    protected $Identifier = null;

    /**
     * @var SphereicPoint $Location
     */
    protected $Location = null;

    /**
     * @var string $Name
     */
    protected $Name = null;

    /**
     * @var FIR $NodeFIR
     */
    protected $NodeFIR = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getDesignator()
    {
      return $this->Designator;
    }

    /**
     * @param string $Designator
     * @return RouteNodeIdentifier
     */
    public function setDesignator($Designator)
    {
      $this->Designator = $Designator;
      return $this;
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
      return $this->Identifier;
    }

    /**
     * @param string $Identifier
     * @return RouteNodeIdentifier
     */
    public function setIdentifier($Identifier)
    {
      $this->Identifier = $Identifier;
      return $this;
    }

    /**
     * @return SphereicPoint
     */
    public function getLocation()
    {
      return $this->Location;
    }

    /**
     * @param SphereicPoint $Location
     * @return RouteNodeIdentifier
     */
    public function setLocation($Location)
    {
      $this->Location = $Location;
      return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->Name;
    }

    /**
     * @param string $Name
     * @return RouteNodeIdentifier
     */
    public function setName($Name)
    {
      $this->Name = $Name;
      return $this;
    }

    /**
     * @return FIR
     */
    public function getNodeFIR()
    {
      return $this->NodeFIR;
    }

    /**
     * @param FIR $NodeFIR
     * @return RouteNodeIdentifier
     */
    public function setNodeFIR($NodeFIR)
    {
      $this->NodeFIR = $NodeFIR;
      return $this;
    }

}
