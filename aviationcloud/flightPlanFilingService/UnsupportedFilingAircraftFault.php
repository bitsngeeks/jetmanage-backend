<?php

class UnsupportedFilingAircraftFault
{

    /**
     * @var string $AircraftICAO
     */
    protected $AircraftICAO = null;

    /**
     * @var string $ErrorCode
     */
    protected $ErrorCode = null;

    /**
     * @var string $ErrorMessage
     */
    protected $ErrorMessage = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getAircraftICAO()
    {
      return $this->AircraftICAO;
    }

    /**
     * @param string $AircraftICAO
     * @return UnsupportedFilingAircraftFault
     */
    public function setAircraftICAO($AircraftICAO)
    {
      $this->AircraftICAO = $AircraftICAO;
      return $this;
    }

    /**
     * @return string
     */
    public function getErrorCode()
    {
      return $this->ErrorCode;
    }

    /**
     * @param string $ErrorCode
     * @return UnsupportedFilingAircraftFault
     */
    public function setErrorCode($ErrorCode)
    {
      $this->ErrorCode = $ErrorCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
      return $this->ErrorMessage;
    }

    /**
     * @param string $ErrorMessage
     * @return UnsupportedFilingAircraftFault
     */
    public function setErrorMessage($ErrorMessage)
    {
      $this->ErrorMessage = $ErrorMessage;
      return $this;
    }

}
