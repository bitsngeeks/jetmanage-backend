<?php

class Airport
{

    /**
     * @var ArrayOfAirportFrequencyARINC424 $AirportFrequencies
     */
    protected $AirportFrequencies = null;

    /**
     * @var Length $Elevation
     */
    protected $Elevation = null;

    /**
     * @var FIR $FIR
     */
    protected $FIR = null;

    /**
     * @var string $IATACode
     */
    protected $IATACode = null;

    /**
     * @var string $ICAOCode
     */
    protected $ICAOCode = null;

    /**
     * @var boolean $IsAirportOfEntry
     */
    protected $IsAirportOfEntry = null;

    /**
     * @var boolean $IsArtificialAirport
     */
    protected $IsArtificialAirport = null;

    /**
     * @var boolean $IsArtificialPoint
     */
    protected $IsArtificialPoint = null;

    /**
     * @var Length $LengthOfLongestRunway
     */
    protected $LengthOfLongestRunway = null;

    /**
     * @var float $MagneticVariation
     */
    protected $MagneticVariation = null;

    /**
     * @var string $Name
     */
    protected $Name = null;

    /**
     * @var SphereicPoint $ReferencePosition
     */
    protected $ReferencePosition = null;

    /**
     * @var string $RegionalCode
     */
    protected $RegionalCode = null;

    /**
     * @var ArrayOfRunway $Runways
     */
    protected $Runways = null;

    /**
     * @var boolean $SupportsIFR
     */
    protected $SupportsIFR = null;

    /**
     * @var Altitude $TransitionAltitude
     */
    protected $TransitionAltitude = null;

    /**
     * @var AirportType $Type
     */
    protected $Type = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfAirportFrequencyARINC424
     */
    public function getAirportFrequencies()
    {
      return $this->AirportFrequencies;
    }

    /**
     * @param ArrayOfAirportFrequencyARINC424 $AirportFrequencies
     * @return Airport
     */
    public function setAirportFrequencies($AirportFrequencies)
    {
      $this->AirportFrequencies = $AirportFrequencies;
      return $this;
    }

    /**
     * @return Length
     */
    public function getElevation()
    {
      return $this->Elevation;
    }

    /**
     * @param Length $Elevation
     * @return Airport
     */
    public function setElevation($Elevation)
    {
      $this->Elevation = $Elevation;
      return $this;
    }

    /**
     * @return FIR
     */
    public function getFIR()
    {
      return $this->FIR;
    }

    /**
     * @param FIR $FIR
     * @return Airport
     */
    public function setFIR($FIR)
    {
      $this->FIR = $FIR;
      return $this;
    }

    /**
     * @return string
     */
    public function getIATACode()
    {
      return $this->IATACode;
    }

    /**
     * @param string $IATACode
     * @return Airport
     */
    public function setIATACode($IATACode)
    {
      $this->IATACode = $IATACode;
      return $this;
    }

    /**
     * @return string
     */
    public function getICAOCode()
    {
      return $this->ICAOCode;
    }

    /**
     * @param string $ICAOCode
     * @return Airport
     */
    public function setICAOCode($ICAOCode)
    {
      $this->ICAOCode = $ICAOCode;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsAirportOfEntry()
    {
      return $this->IsAirportOfEntry;
    }

    /**
     * @param boolean $IsAirportOfEntry
     * @return Airport
     */
    public function setIsAirportOfEntry($IsAirportOfEntry)
    {
      $this->IsAirportOfEntry = $IsAirportOfEntry;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsArtificialAirport()
    {
      return $this->IsArtificialAirport;
    }

    /**
     * @param boolean $IsArtificialAirport
     * @return Airport
     */
    public function setIsArtificialAirport($IsArtificialAirport)
    {
      $this->IsArtificialAirport = $IsArtificialAirport;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsArtificialPoint()
    {
      return $this->IsArtificialPoint;
    }

    /**
     * @param boolean $IsArtificialPoint
     * @return Airport
     */
    public function setIsArtificialPoint($IsArtificialPoint)
    {
      $this->IsArtificialPoint = $IsArtificialPoint;
      return $this;
    }

    /**
     * @return Length
     */
    public function getLengthOfLongestRunway()
    {
      return $this->LengthOfLongestRunway;
    }

    /**
     * @param Length $LengthOfLongestRunway
     * @return Airport
     */
    public function setLengthOfLongestRunway($LengthOfLongestRunway)
    {
      $this->LengthOfLongestRunway = $LengthOfLongestRunway;
      return $this;
    }

    /**
     * @return float
     */
    public function getMagneticVariation()
    {
      return $this->MagneticVariation;
    }

    /**
     * @param float $MagneticVariation
     * @return Airport
     */
    public function setMagneticVariation($MagneticVariation)
    {
      $this->MagneticVariation = $MagneticVariation;
      return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->Name;
    }

    /**
     * @param string $Name
     * @return Airport
     */
    public function setName($Name)
    {
      $this->Name = $Name;
      return $this;
    }

    /**
     * @return SphereicPoint
     */
    public function getReferencePosition()
    {
      return $this->ReferencePosition;
    }

    /**
     * @param SphereicPoint $ReferencePosition
     * @return Airport
     */
    public function setReferencePosition($ReferencePosition)
    {
      $this->ReferencePosition = $ReferencePosition;
      return $this;
    }

    /**
     * @return string
     */
    public function getRegionalCode()
    {
      return $this->RegionalCode;
    }

    /**
     * @param string $RegionalCode
     * @return Airport
     */
    public function setRegionalCode($RegionalCode)
    {
      $this->RegionalCode = $RegionalCode;
      return $this;
    }

    /**
     * @return ArrayOfRunway
     */
    public function getRunways()
    {
      return $this->Runways;
    }

    /**
     * @param ArrayOfRunway $Runways
     * @return Airport
     */
    public function setRunways($Runways)
    {
      $this->Runways = $Runways;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getSupportsIFR()
    {
      return $this->SupportsIFR;
    }

    /**
     * @param boolean $SupportsIFR
     * @return Airport
     */
    public function setSupportsIFR($SupportsIFR)
    {
      $this->SupportsIFR = $SupportsIFR;
      return $this;
    }

    /**
     * @return Altitude
     */
    public function getTransitionAltitude()
    {
      return $this->TransitionAltitude;
    }

    /**
     * @param Altitude $TransitionAltitude
     * @return Airport
     */
    public function setTransitionAltitude($TransitionAltitude)
    {
      $this->TransitionAltitude = $TransitionAltitude;
      return $this;
    }

    /**
     * @return AirportType
     */
    public function getType()
    {
      return $this->Type;
    }

    /**
     * @param AirportType $Type
     * @return Airport
     */
    public function setType($Type)
    {
      $this->Type = $Type;
      return $this;
    }

}
