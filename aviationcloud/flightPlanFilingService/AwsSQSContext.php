<?php

class AwsSQSContext
{

    /**
     * @var string $AccessKey
     */
    protected $AccessKey = null;

    /**
     * @var string $RegionEndpoint
     */
    protected $RegionEndpoint = null;

    /**
     * @var string $SecretKey
     */
    protected $SecretKey = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getAccessKey()
    {
      return $this->AccessKey;
    }

    /**
     * @param string $AccessKey
     * @return AwsSQSContext
     */
    public function setAccessKey($AccessKey)
    {
      $this->AccessKey = $AccessKey;
      return $this;
    }

    /**
     * @return string
     */
    public function getRegionEndpoint()
    {
      return $this->RegionEndpoint;
    }

    /**
     * @param string $RegionEndpoint
     * @return AwsSQSContext
     */
    public function setRegionEndpoint($RegionEndpoint)
    {
      $this->RegionEndpoint = $RegionEndpoint;
      return $this;
    }

    /**
     * @return string
     */
    public function getSecretKey()
    {
      return $this->SecretKey;
    }

    /**
     * @param string $SecretKey
     * @return AwsSQSContext
     */
    public function setSecretKey($SecretKey)
    {
      $this->SecretKey = $SecretKey;
      return $this;
    }

}
