<?php

class IntersectionNodeType
{
    const __default = 'Undefined';
    const Undefined = 'Undefined';
    const Airport = 'Airport';
    const Runway = 'Runway';
    const SID = 'SID';
    const STAR = 'STAR';
    const CountryIntersection = 'CountryIntersection';
    const FirIntersection = 'FirIntersection';
    const WayPoint = 'WayPoint';


}
