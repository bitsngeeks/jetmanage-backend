<?php

class InlineOptionSpecification
{

    /**
     * @var boolean $InlineETP
     */
    protected $InlineETP = null;

    /**
     * @var boolean $InlineFIR
     */
    protected $InlineFIR = null;

    /**
     * @var boolean $InlinePSR
     */
    protected $InlinePSR = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return boolean
     */
    public function getInlineETP()
    {
      return $this->InlineETP;
    }

    /**
     * @param boolean $InlineETP
     * @return InlineOptionSpecification
     */
    public function setInlineETP($InlineETP)
    {
      $this->InlineETP = $InlineETP;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getInlineFIR()
    {
      return $this->InlineFIR;
    }

    /**
     * @param boolean $InlineFIR
     * @return InlineOptionSpecification
     */
    public function setInlineFIR($InlineFIR)
    {
      $this->InlineFIR = $InlineFIR;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getInlinePSR()
    {
      return $this->InlinePSR;
    }

    /**
     * @param boolean $InlinePSR
     * @return InlineOptionSpecification
     */
    public function setInlinePSR($InlinePSR)
    {
      $this->InlinePSR = $InlinePSR;
      return $this;
    }

}
