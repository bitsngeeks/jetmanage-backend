<?php

class SimpleFuelTankDefinition extends FuelTankDefinition
{

    /**
     * @var Weight $FuelCapacity
     */
    protected $FuelCapacity = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Weight
     */
    public function getFuelCapacity()
    {
      return $this->FuelCapacity;
    }

    /**
     * @param Weight $FuelCapacity
     * @return SimpleFuelTankDefinition
     */
    public function setFuelCapacity($FuelCapacity)
    {
      $this->FuelCapacity = $FuelCapacity;
      return $this;
    }

}
