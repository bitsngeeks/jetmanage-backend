<?php

class AircraftRegistrationRequest extends RequestBase
{

    /**
     * @var Aircraft $Aircraft
     */
    protected $Aircraft = null;

    /**
     * @var DataLevel $AircraftTypeDataLevel
     */
    protected $AircraftTypeDataLevel = null;

    /**
     * @var string $AircraftTypeEngineName
     */
    protected $AircraftTypeEngineName = null;

    /**
     * @var string $AircraftTypeName
     */
    protected $AircraftTypeName = null;

    /**
     * @var boolean $AllowUpdateOfAircraft
     */
    protected $AllowUpdateOfAircraft = null;

    /**
     * @var boolean $AllowUpdateOfAircraftTypeConfigurationData
     */
    protected $AllowUpdateOfAircraftTypeConfigurationData = null;

    /**
     * @var boolean $AllowUpdateOfAircraftTypeData
     */
    protected $AllowUpdateOfAircraftTypeData = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return Aircraft
     */
    public function getAircraft()
    {
      return $this->Aircraft;
    }

    /**
     * @param Aircraft $Aircraft
     * @return AircraftRegistrationRequest
     */
    public function setAircraft($Aircraft)
    {
      $this->Aircraft = $Aircraft;
      return $this;
    }

    /**
     * @return DataLevel
     */
    public function getAircraftTypeDataLevel()
    {
      return $this->AircraftTypeDataLevel;
    }

    /**
     * @param DataLevel $AircraftTypeDataLevel
     * @return AircraftRegistrationRequest
     */
    public function setAircraftTypeDataLevel($AircraftTypeDataLevel)
    {
      $this->AircraftTypeDataLevel = $AircraftTypeDataLevel;
      return $this;
    }

    /**
     * @return string
     */
    public function getAircraftTypeEngineName()
    {
      return $this->AircraftTypeEngineName;
    }

    /**
     * @param string $AircraftTypeEngineName
     * @return AircraftRegistrationRequest
     */
    public function setAircraftTypeEngineName($AircraftTypeEngineName)
    {
      $this->AircraftTypeEngineName = $AircraftTypeEngineName;
      return $this;
    }

    /**
     * @return string
     */
    public function getAircraftTypeName()
    {
      return $this->AircraftTypeName;
    }

    /**
     * @param string $AircraftTypeName
     * @return AircraftRegistrationRequest
     */
    public function setAircraftTypeName($AircraftTypeName)
    {
      $this->AircraftTypeName = $AircraftTypeName;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getAllowUpdateOfAircraft()
    {
      return $this->AllowUpdateOfAircraft;
    }

    /**
     * @param boolean $AllowUpdateOfAircraft
     * @return AircraftRegistrationRequest
     */
    public function setAllowUpdateOfAircraft($AllowUpdateOfAircraft)
    {
      $this->AllowUpdateOfAircraft = $AllowUpdateOfAircraft;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getAllowUpdateOfAircraftTypeConfigurationData()
    {
      return $this->AllowUpdateOfAircraftTypeConfigurationData;
    }

    /**
     * @param boolean $AllowUpdateOfAircraftTypeConfigurationData
     * @return AircraftRegistrationRequest
     */
    public function setAllowUpdateOfAircraftTypeConfigurationData($AllowUpdateOfAircraftTypeConfigurationData)
    {
      $this->AllowUpdateOfAircraftTypeConfigurationData = $AllowUpdateOfAircraftTypeConfigurationData;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getAllowUpdateOfAircraftTypeData()
    {
      return $this->AllowUpdateOfAircraftTypeData;
    }

    /**
     * @param boolean $AllowUpdateOfAircraftTypeData
     * @return AircraftRegistrationRequest
     */
    public function setAllowUpdateOfAircraftTypeData($AllowUpdateOfAircraftTypeData)
    {
      $this->AllowUpdateOfAircraftTypeData = $AllowUpdateOfAircraftTypeData;
      return $this;
    }

}
