<?php

class OFPLayoutListRequest extends RequestBase
{

    /**
     * @var boolean $SkipContent
     */
    protected $SkipContent = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return boolean
     */
    public function getSkipContent()
    {
      return $this->SkipContent;
    }

    /**
     * @param boolean $SkipContent
     * @return OFPLayoutListRequest
     */
    public function setSkipContent($SkipContent)
    {
      $this->SkipContent = $SkipContent;
      return $this;
    }

}
