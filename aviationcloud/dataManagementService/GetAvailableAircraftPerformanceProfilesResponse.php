<?php

class GetAvailableAircraftPerformanceProfilesResponse
{

    /**
     * @var AircraftPerformanceProfileListResponse $GetAvailableAircraftPerformanceProfilesResult
     */
    protected $GetAvailableAircraftPerformanceProfilesResult = null;

    /**
     * @param AircraftPerformanceProfileListResponse $GetAvailableAircraftPerformanceProfilesResult
     */
    public function __construct($GetAvailableAircraftPerformanceProfilesResult)
    {
      $this->GetAvailableAircraftPerformanceProfilesResult = $GetAvailableAircraftPerformanceProfilesResult;
    }

    /**
     * @return AircraftPerformanceProfileListResponse
     */
    public function getGetAvailableAircraftPerformanceProfilesResult()
    {
      return $this->GetAvailableAircraftPerformanceProfilesResult;
    }

    /**
     * @param AircraftPerformanceProfileListResponse $GetAvailableAircraftPerformanceProfilesResult
     * @return GetAvailableAircraftPerformanceProfilesResponse
     */
    public function setGetAvailableAircraftPerformanceProfilesResult($GetAvailableAircraftPerformanceProfilesResult)
    {
      $this->GetAvailableAircraftPerformanceProfilesResult = $GetAvailableAircraftPerformanceProfilesResult;
      return $this;
    }

}
