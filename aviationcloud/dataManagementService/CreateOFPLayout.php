<?php

class CreateOFPLayout
{

    /**
     * @var OFPLayoutCreateRequest $aRequest
     */
    protected $aRequest = null;

    /**
     * @param OFPLayoutCreateRequest $aRequest
     */
    public function __construct($aRequest)
    {
      $this->aRequest = $aRequest;
    }

    /**
     * @return OFPLayoutCreateRequest
     */
    public function getARequest()
    {
      return $this->aRequest;
    }

    /**
     * @param OFPLayoutCreateRequest $aRequest
     * @return CreateOFPLayout
     */
    public function setARequest($aRequest)
    {
      $this->aRequest = $aRequest;
      return $this;
    }

}
