<?php

class DeleteOFPLayout
{

    /**
     * @var OFPLayoutDeleteRequest $aRequest
     */
    protected $aRequest = null;

    /**
     * @param OFPLayoutDeleteRequest $aRequest
     */
    public function __construct($aRequest)
    {
      $this->aRequest = $aRequest;
    }

    /**
     * @return OFPLayoutDeleteRequest
     */
    public function getARequest()
    {
      return $this->aRequest;
    }

    /**
     * @param OFPLayoutDeleteRequest $aRequest
     * @return DeleteOFPLayout
     */
    public function setARequest($aRequest)
    {
      $this->aRequest = $aRequest;
      return $this;
    }

}
