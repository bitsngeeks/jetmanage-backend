<?php

class UpdateOFPLayoutResponse
{

    /**
     * @var OFPLayoutUpdateResonse $UpdateOFPLayoutResult
     */
    protected $UpdateOFPLayoutResult = null;

    /**
     * @param OFPLayoutUpdateResonse $UpdateOFPLayoutResult
     */
    public function __construct($UpdateOFPLayoutResult)
    {
      $this->UpdateOFPLayoutResult = $UpdateOFPLayoutResult;
    }

    /**
     * @return OFPLayoutUpdateResonse
     */
    public function getUpdateOFPLayoutResult()
    {
      return $this->UpdateOFPLayoutResult;
    }

    /**
     * @param OFPLayoutUpdateResonse $UpdateOFPLayoutResult
     * @return UpdateOFPLayoutResponse
     */
    public function setUpdateOFPLayoutResult($UpdateOFPLayoutResult)
    {
      $this->UpdateOFPLayoutResult = $UpdateOFPLayoutResult;
      return $this;
    }

}
