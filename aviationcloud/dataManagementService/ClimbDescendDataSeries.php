<?php

class ClimbDescendDataSeries extends PerformanceDataSeries
{

    /**
     * @var ArrayOfClimbDescendDataPoint $PerformanceDataPoints
     */
    protected $PerformanceDataPoints = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return ArrayOfClimbDescendDataPoint
     */
    public function getPerformanceDataPoints()
    {
      return $this->PerformanceDataPoints;
    }

    /**
     * @param ArrayOfClimbDescendDataPoint $PerformanceDataPoints
     * @return ClimbDescendDataSeries
     */
    public function setPerformanceDataPoints($PerformanceDataPoints)
    {
      $this->PerformanceDataPoints = $PerformanceDataPoints;
      return $this;
    }

}
