<?php

class CreateAccount
{

    /**
     * @var AccountCreationRequest $req
     */
    protected $req = null;

    /**
     * @param AccountCreationRequest $req
     */
    public function __construct($req)
    {
      $this->req = $req;
    }

    /**
     * @return AccountCreationRequest
     */
    public function getReq()
    {
      return $this->req;
    }

    /**
     * @param AccountCreationRequest $req
     * @return CreateAccount
     */
    public function setReq($req)
    {
      $this->req = $req;
      return $this;
    }

}
