<?php

class ArrayOfOFPLayout implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var OFPLayout[] $OFPLayout
     */
    protected $OFPLayout = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return OFPLayout[]
     */
    public function getOFPLayout()
    {
      return $this->OFPLayout;
    }

    /**
     * @param OFPLayout[] $OFPLayout
     * @return ArrayOfOFPLayout
     */
    public function setOFPLayout(array $OFPLayout = null)
    {
      $this->OFPLayout = $OFPLayout;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->OFPLayout[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return OFPLayout
     */
    public function offsetGet($offset)
    {
      return $this->OFPLayout[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param OFPLayout $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->OFPLayout[] = $value;
      } else {
        $this->OFPLayout[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->OFPLayout[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return OFPLayout Return the current element
     */
    public function current()
    {
      return current($this->OFPLayout);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->OFPLayout);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->OFPLayout);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->OFPLayout);
    }

    /**
     * Countable implementation
     *
     * @return OFPLayout Return count of elements
     */
    public function count()
    {
      return count($this->OFPLayout);
    }

}
