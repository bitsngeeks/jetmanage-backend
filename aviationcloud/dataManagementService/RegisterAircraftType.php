<?php

class RegisterAircraftType
{

    /**
     * @var AircraftTypeRegistrationRequest $request
     */
    protected $request = null;

    /**
     * @param AircraftTypeRegistrationRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return AircraftTypeRegistrationRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param AircraftTypeRegistrationRequest $request
     * @return RegisterAircraftType
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
