<?php

class ArrayOfAircraft implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var Aircraft[] $Aircraft
     */
    protected $Aircraft = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Aircraft[]
     */
    public function getAircraft()
    {
      return $this->Aircraft;
    }

    /**
     * @param Aircraft[] $Aircraft
     * @return ArrayOfAircraft
     */
    public function setAircraft(array $Aircraft = null)
    {
      $this->Aircraft = $Aircraft;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->Aircraft[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return Aircraft
     */
    public function offsetGet($offset)
    {
      return $this->Aircraft[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param Aircraft $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->Aircraft[] = $value;
      } else {
        $this->Aircraft[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->Aircraft[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return Aircraft Return the current element
     */
    public function current()
    {
      return current($this->Aircraft);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->Aircraft);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->Aircraft);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->Aircraft);
    }

    /**
     * Countable implementation
     *
     * @return Aircraft Return count of elements
     */
    public function count()
    {
      return count($this->Aircraft);
    }

}
