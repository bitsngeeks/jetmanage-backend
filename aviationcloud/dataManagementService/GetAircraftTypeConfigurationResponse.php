<?php

class GetAircraftTypeConfigurationResponse
{

    /**
     * @var AircraftEngineSpecification $GetAircraftTypeConfigurationResult
     */
    protected $GetAircraftTypeConfigurationResult = null;

    /**
     * @param AircraftEngineSpecification $GetAircraftTypeConfigurationResult
     */
    public function __construct($GetAircraftTypeConfigurationResult)
    {
      $this->GetAircraftTypeConfigurationResult = $GetAircraftTypeConfigurationResult;
    }

    /**
     * @return AircraftEngineSpecification
     */
    public function getGetAircraftTypeConfigurationResult()
    {
      return $this->GetAircraftTypeConfigurationResult;
    }

    /**
     * @param AircraftEngineSpecification $GetAircraftTypeConfigurationResult
     * @return GetAircraftTypeConfigurationResponse
     */
    public function setGetAircraftTypeConfigurationResult($GetAircraftTypeConfigurationResult)
    {
      $this->GetAircraftTypeConfigurationResult = $GetAircraftTypeConfigurationResult;
      return $this;
    }

}
