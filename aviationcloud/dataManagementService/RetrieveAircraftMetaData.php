<?php

class RetrieveAircraftMetaData
{

    /**
     * @var AircraftMetaDataRetrivalRequest $request
     */
    protected $request = null;

    /**
     * @param AircraftMetaDataRetrivalRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return AircraftMetaDataRetrivalRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param AircraftMetaDataRetrivalRequest $request
     * @return RetrieveAircraftMetaData
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
