<?php

class AircraftTypeCopyRequest extends RequestBase
{

    /**
     * @var string $AircraftTypeName
     */
    protected $AircraftTypeName = null;

    /**
     * @var DataLevel $SourceLevel
     */
    protected $SourceLevel = null;

    /**
     * @var DataLevel $TargetLevel
     */
    protected $TargetLevel = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return string
     */
    public function getAircraftTypeName()
    {
      return $this->AircraftTypeName;
    }

    /**
     * @param string $AircraftTypeName
     * @return AircraftTypeCopyRequest
     */
    public function setAircraftTypeName($AircraftTypeName)
    {
      $this->AircraftTypeName = $AircraftTypeName;
      return $this;
    }

    /**
     * @return DataLevel
     */
    public function getSourceLevel()
    {
      return $this->SourceLevel;
    }

    /**
     * @param DataLevel $SourceLevel
     * @return AircraftTypeCopyRequest
     */
    public function setSourceLevel($SourceLevel)
    {
      $this->SourceLevel = $SourceLevel;
      return $this;
    }

    /**
     * @return DataLevel
     */
    public function getTargetLevel()
    {
      return $this->TargetLevel;
    }

    /**
     * @param DataLevel $TargetLevel
     * @return AircraftTypeCopyRequest
     */
    public function setTargetLevel($TargetLevel)
    {
      $this->TargetLevel = $TargetLevel;
      return $this;
    }

}
