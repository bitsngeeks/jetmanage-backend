<?php

class GetAircraftResponse
{

    /**
     * @var Aircraft $GetAircraftResult
     */
    protected $GetAircraftResult = null;

    /**
     * @param Aircraft $GetAircraftResult
     */
    public function __construct($GetAircraftResult)
    {
      $this->GetAircraftResult = $GetAircraftResult;
    }

    /**
     * @return Aircraft
     */
    public function getGetAircraftResult()
    {
      return $this->GetAircraftResult;
    }

    /**
     * @param Aircraft $GetAircraftResult
     * @return GetAircraftResponse
     */
    public function setGetAircraftResult($GetAircraftResult)
    {
      $this->GetAircraftResult = $GetAircraftResult;
      return $this;
    }

}
