<?php

class AircraftMetaDataRetrivalResponse
{

    /**
     * @var AircraftMetaData $AircraftData
     */
    protected $AircraftData = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return AircraftMetaData
     */
    public function getAircraftData()
    {
      return $this->AircraftData;
    }

    /**
     * @param AircraftMetaData $AircraftData
     * @return AircraftMetaDataRetrivalResponse
     */
    public function setAircraftData($AircraftData)
    {
      $this->AircraftData = $AircraftData;
      return $this;
    }

}
