<?php

class PerformanceProfileSummary
{

    /**
     * @var int $Id
     */
    protected $Id = null;

    /**
     * @var string $Name
     */
    protected $Name = null;

    /**
     * @var string $UUID
     */
    protected $UUID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getId()
    {
      return $this->Id;
    }

    /**
     * @param int $Id
     * @return PerformanceProfileSummary
     */
    public function setId($Id)
    {
      $this->Id = $Id;
      return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->Name;
    }

    /**
     * @param string $Name
     * @return PerformanceProfileSummary
     */
    public function setName($Name)
    {
      $this->Name = $Name;
      return $this;
    }

    /**
     * @return string
     */
    public function getUUID()
    {
      return $this->UUID;
    }

    /**
     * @param string $UUID
     * @return PerformanceProfileSummary
     */
    public function setUUID($UUID)
    {
      $this->UUID = $UUID;
      return $this;
    }

}
