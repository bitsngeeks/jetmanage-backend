<?php

class ArrayOfAircraftPerformanceProfileIdentifier implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var AircraftPerformanceProfileIdentifier[] $AircraftPerformanceProfileIdentifier
     */
    protected $AircraftPerformanceProfileIdentifier = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return AircraftPerformanceProfileIdentifier[]
     */
    public function getAircraftPerformanceProfileIdentifier()
    {
      return $this->AircraftPerformanceProfileIdentifier;
    }

    /**
     * @param AircraftPerformanceProfileIdentifier[] $AircraftPerformanceProfileIdentifier
     * @return ArrayOfAircraftPerformanceProfileIdentifier
     */
    public function setAircraftPerformanceProfileIdentifier(array $AircraftPerformanceProfileIdentifier = null)
    {
      $this->AircraftPerformanceProfileIdentifier = $AircraftPerformanceProfileIdentifier;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->AircraftPerformanceProfileIdentifier[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return AircraftPerformanceProfileIdentifier
     */
    public function offsetGet($offset)
    {
      return $this->AircraftPerformanceProfileIdentifier[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param AircraftPerformanceProfileIdentifier $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->AircraftPerformanceProfileIdentifier[] = $value;
      } else {
        $this->AircraftPerformanceProfileIdentifier[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->AircraftPerformanceProfileIdentifier[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return AircraftPerformanceProfileIdentifier Return the current element
     */
    public function current()
    {
      return current($this->AircraftPerformanceProfileIdentifier);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->AircraftPerformanceProfileIdentifier);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->AircraftPerformanceProfileIdentifier);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->AircraftPerformanceProfileIdentifier);
    }

    /**
     * Countable implementation
     *
     * @return AircraftPerformanceProfileIdentifier Return count of elements
     */
    public function count()
    {
      return count($this->AircraftPerformanceProfileIdentifier);
    }

}
