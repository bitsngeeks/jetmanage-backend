<?php

class RetrieveAircraftMetaDataResponse
{

    /**
     * @var AircraftMetaDataRetrivalResponse $RetrieveAircraftMetaDataResult
     */
    protected $RetrieveAircraftMetaDataResult = null;

    /**
     * @param AircraftMetaDataRetrivalResponse $RetrieveAircraftMetaDataResult
     */
    public function __construct($RetrieveAircraftMetaDataResult)
    {
      $this->RetrieveAircraftMetaDataResult = $RetrieveAircraftMetaDataResult;
    }

    /**
     * @return AircraftMetaDataRetrivalResponse
     */
    public function getRetrieveAircraftMetaDataResult()
    {
      return $this->RetrieveAircraftMetaDataResult;
    }

    /**
     * @param AircraftMetaDataRetrivalResponse $RetrieveAircraftMetaDataResult
     * @return RetrieveAircraftMetaDataResponse
     */
    public function setRetrieveAircraftMetaDataResult($RetrieveAircraftMetaDataResult)
    {
      $this->RetrieveAircraftMetaDataResult = $RetrieveAircraftMetaDataResult;
      return $this;
    }

}
