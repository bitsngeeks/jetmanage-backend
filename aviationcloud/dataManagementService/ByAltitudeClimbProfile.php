<?php

class ByAltitudeClimbProfile extends ClimbProfile
{

    /**
     * @var ArrayOfByAltitudeFuelFlow $FuelFlow
     */
    protected $FuelFlow = null;

    /**
     * @var string $ModelName
     */
    protected $ModelName = null;

    /**
     * @var ArrayOfByAltitudeClimbPerformance $Performance
     */
    protected $Performance = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return ArrayOfByAltitudeFuelFlow
     */
    public function getFuelFlow()
    {
      return $this->FuelFlow;
    }

    /**
     * @param ArrayOfByAltitudeFuelFlow $FuelFlow
     * @return ByAltitudeClimbProfile
     */
    public function setFuelFlow($FuelFlow)
    {
      $this->FuelFlow = $FuelFlow;
      return $this;
    }

    /**
     * @return string
     */
    public function getModelName()
    {
      return $this->ModelName;
    }

    /**
     * @param string $ModelName
     * @return ByAltitudeClimbProfile
     */
    public function setModelName($ModelName)
    {
      $this->ModelName = $ModelName;
      return $this;
    }

    /**
     * @return ArrayOfByAltitudeClimbPerformance
     */
    public function getPerformance()
    {
      return $this->Performance;
    }

    /**
     * @param ArrayOfByAltitudeClimbPerformance $Performance
     * @return ByAltitudeClimbProfile
     */
    public function setPerformance($Performance)
    {
      $this->Performance = $Performance;
      return $this;
    }

}
