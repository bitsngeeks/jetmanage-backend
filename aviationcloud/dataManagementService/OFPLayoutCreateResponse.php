<?php

class OFPLayoutCreateResponse
{

    /**
     * @var OFPLayout $Layout
     */
    protected $Layout = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return OFPLayout
     */
    public function getLayout()
    {
      return $this->Layout;
    }

    /**
     * @param OFPLayout $Layout
     * @return OFPLayoutCreateResponse
     */
    public function setLayout($Layout)
    {
      $this->Layout = $Layout;
      return $this;
    }

}
