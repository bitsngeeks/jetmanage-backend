<?php

class ClimbDescendDataPoint extends PerformanceDataPoint
{

    /**
     * @var ClimbDescendData $ClimbDescendPerformance
     */
    protected $ClimbDescendPerformance = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return ClimbDescendData
     */
    public function getClimbDescendPerformance()
    {
      return $this->ClimbDescendPerformance;
    }

    /**
     * @param ClimbDescendData $ClimbDescendPerformance
     * @return ClimbDescendDataPoint
     */
    public function setClimbDescendPerformance($ClimbDescendPerformance)
    {
      $this->ClimbDescendPerformance = $ClimbDescendPerformance;
      return $this;
    }

}
