<?php

class RegisterAircraft
{

    /**
     * @var AircraftRegistrationRequest $request
     */
    protected $request = null;

    /**
     * @param AircraftRegistrationRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return AircraftRegistrationRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param AircraftRegistrationRequest $request
     * @return RegisterAircraft
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
