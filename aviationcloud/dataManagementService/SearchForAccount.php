<?php

class SearchForAccount
{

    /**
     * @var AccountSearchRequest $request
     */
    protected $request = null;

    /**
     * @param AccountSearchRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return AccountSearchRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param AccountSearchRequest $request
     * @return SearchForAccount
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
