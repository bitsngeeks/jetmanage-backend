<?php

class OFPLayoutListResonse
{

    /**
     * @var ArrayOfOFPLayout $Layouts
     */
    protected $Layouts = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ArrayOfOFPLayout
     */
    public function getLayouts()
    {
      return $this->Layouts;
    }

    /**
     * @param ArrayOfOFPLayout $Layouts
     * @return OFPLayoutListResonse
     */
    public function setLayouts($Layouts)
    {
      $this->Layouts = $Layouts;
      return $this;
    }

}
