<?php

class ByAltitudeCruiseProfile extends CruiseProfile
{

    /**
     * @var float $FuelBias
     */
    protected $FuelBias = null;

    /**
     * @var string $ModelName
     */
    protected $ModelName = null;

    /**
     * @var ArrayOfByAltitudeCruisePerformance $Performance
     */
    protected $Performance = null;

    /**
     * @var float $SpeedBias
     */
    protected $SpeedBias = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return float
     */
    public function getFuelBias()
    {
      return $this->FuelBias;
    }

    /**
     * @param float $FuelBias
     * @return ByAltitudeCruiseProfile
     */
    public function setFuelBias($FuelBias)
    {
      $this->FuelBias = $FuelBias;
      return $this;
    }

    /**
     * @return string
     */
    public function getModelName()
    {
      return $this->ModelName;
    }

    /**
     * @param string $ModelName
     * @return ByAltitudeCruiseProfile
     */
    public function setModelName($ModelName)
    {
      $this->ModelName = $ModelName;
      return $this;
    }

    /**
     * @return ArrayOfByAltitudeCruisePerformance
     */
    public function getPerformance()
    {
      return $this->Performance;
    }

    /**
     * @param ArrayOfByAltitudeCruisePerformance $Performance
     * @return ByAltitudeCruiseProfile
     */
    public function setPerformance($Performance)
    {
      $this->Performance = $Performance;
      return $this;
    }

    /**
     * @return float
     */
    public function getSpeedBias()
    {
      return $this->SpeedBias;
    }

    /**
     * @param float $SpeedBias
     * @return ByAltitudeCruiseProfile
     */
    public function setSpeedBias($SpeedBias)
    {
      $this->SpeedBias = $SpeedBias;
      return $this;
    }

}
