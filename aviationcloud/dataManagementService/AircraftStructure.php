<?php

class AircraftStructure
{

    /**
     * @var Weight $MaxRampMass
     */
    protected $MaxRampMass = null;

    /**
     * @var Weight $MaxTakeOffMass
     */
    protected $MaxTakeOffMass = null;

    /**
     * @var Weight $MaxZeroFuelMass
     */
    protected $MaxZeroFuelMass = null;

    /**
     * @var FuelTankDefinition $FuelTankDefinition
     */
    protected $FuelTankDefinition = null;

    /**
     * @var ArrayOfMassAndBalanceConfiguration $Configurations
     */
    protected $Configurations = null;

    /**
     * @var Weight $MaxLandingMass
     */
    protected $MaxLandingMass = null;

    /**
     * @var float $OxygenPaxHours
     */
    protected $OxygenPaxHours = null;

    /**
     * @var AircraftEnvelope $Envelope
     */
    protected $Envelope = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Weight
     */
    public function getMaxRampMass()
    {
      return $this->MaxRampMass;
    }

    /**
     * @param Weight $MaxRampMass
     * @return AircraftStructure
     */
    public function setMaxRampMass($MaxRampMass)
    {
      $this->MaxRampMass = $MaxRampMass;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getMaxTakeOffMass()
    {
      return $this->MaxTakeOffMass;
    }

    /**
     * @param Weight $MaxTakeOffMass
     * @return AircraftStructure
     */
    public function setMaxTakeOffMass($MaxTakeOffMass)
    {
      $this->MaxTakeOffMass = $MaxTakeOffMass;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getMaxZeroFuelMass()
    {
      return $this->MaxZeroFuelMass;
    }

    /**
     * @param Weight $MaxZeroFuelMass
     * @return AircraftStructure
     */
    public function setMaxZeroFuelMass($MaxZeroFuelMass)
    {
      $this->MaxZeroFuelMass = $MaxZeroFuelMass;
      return $this;
    }

    /**
     * @return FuelTankDefinition
     */
    public function getFuelTankDefinition()
    {
      return $this->FuelTankDefinition;
    }

    /**
     * @param FuelTankDefinition $FuelTankDefinition
     * @return AircraftStructure
     */
    public function setFuelTankDefinition($FuelTankDefinition)
    {
      $this->FuelTankDefinition = $FuelTankDefinition;
      return $this;
    }

    /**
     * @return ArrayOfMassAndBalanceConfiguration
     */
    public function getConfigurations()
    {
      return $this->Configurations;
    }

    /**
     * @param ArrayOfMassAndBalanceConfiguration $Configurations
     * @return AircraftStructure
     */
    public function setConfigurations($Configurations)
    {
      $this->Configurations = $Configurations;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getMaxLandingMass()
    {
      return $this->MaxLandingMass;
    }

    /**
     * @param Weight $MaxLandingMass
     * @return AircraftStructure
     */
    public function setMaxLandingMass($MaxLandingMass)
    {
      $this->MaxLandingMass = $MaxLandingMass;
      return $this;
    }

    /**
     * @return float
     */
    public function getOxygenPaxHours()
    {
      return $this->OxygenPaxHours;
    }

    /**
     * @param float $OxygenPaxHours
     * @return AircraftStructure
     */
    public function setOxygenPaxHours($OxygenPaxHours)
    {
      $this->OxygenPaxHours = $OxygenPaxHours;
      return $this;
    }

    /**
     * @return AircraftEnvelope
     */
    public function getEnvelope()
    {
      return $this->Envelope;
    }

    /**
     * @param AircraftEnvelope $Envelope
     * @return AircraftStructure
     */
    public function setEnvelope($Envelope)
    {
      $this->Envelope = $Envelope;
      return $this;
    }

}
