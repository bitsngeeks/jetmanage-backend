<?php

class AircraftTypeDefinition
{

    /**
     * @var string $ICAO
     */
    protected $ICAO = null;

    /**
     * @var string $ModelName
     */
    protected $ModelName = null;

    /**
     * @var string $UUID
     */
    protected $UUID = null;

    /**
     * @var string $VariantName
     */
    protected $VariantName = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getICAO()
    {
      return $this->ICAO;
    }

    /**
     * @param string $ICAO
     * @return AircraftTypeDefinition
     */
    public function setICAO($ICAO)
    {
      $this->ICAO = $ICAO;
      return $this;
    }

    /**
     * @return string
     */
    public function getModelName()
    {
      return $this->ModelName;
    }

    /**
     * @param string $ModelName
     * @return AircraftTypeDefinition
     */
    public function setModelName($ModelName)
    {
      $this->ModelName = $ModelName;
      return $this;
    }

    /**
     * @return string
     */
    public function getUUID()
    {
      return $this->UUID;
    }

    /**
     * @param string $UUID
     * @return AircraftTypeDefinition
     */
    public function setUUID($UUID)
    {
      $this->UUID = $UUID;
      return $this;
    }

    /**
     * @return string
     */
    public function getVariantName()
    {
      return $this->VariantName;
    }

    /**
     * @param string $VariantName
     * @return AircraftTypeDefinition
     */
    public function setVariantName($VariantName)
    {
      $this->VariantName = $VariantName;
      return $this;
    }

}
