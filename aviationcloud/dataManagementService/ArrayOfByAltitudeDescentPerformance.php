<?php

class ArrayOfByAltitudeDescentPerformance implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var ByAltitudeDescentPerformance[] $ByAltitudeDescentPerformance
     */
    protected $ByAltitudeDescentPerformance = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ByAltitudeDescentPerformance[]
     */
    public function getByAltitudeDescentPerformance()
    {
      return $this->ByAltitudeDescentPerformance;
    }

    /**
     * @param ByAltitudeDescentPerformance[] $ByAltitudeDescentPerformance
     * @return ArrayOfByAltitudeDescentPerformance
     */
    public function setByAltitudeDescentPerformance(array $ByAltitudeDescentPerformance = null)
    {
      $this->ByAltitudeDescentPerformance = $ByAltitudeDescentPerformance;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->ByAltitudeDescentPerformance[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return ByAltitudeDescentPerformance
     */
    public function offsetGet($offset)
    {
      return $this->ByAltitudeDescentPerformance[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param ByAltitudeDescentPerformance $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->ByAltitudeDescentPerformance[] = $value;
      } else {
        $this->ByAltitudeDescentPerformance[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->ByAltitudeDescentPerformance[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return ByAltitudeDescentPerformance Return the current element
     */
    public function current()
    {
      return current($this->ByAltitudeDescentPerformance);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->ByAltitudeDescentPerformance);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->ByAltitudeDescentPerformance);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->ByAltitudeDescentPerformance);
    }

    /**
     * Countable implementation
     *
     * @return ByAltitudeDescentPerformance Return count of elements
     */
    public function count()
    {
      return count($this->ByAltitudeDescentPerformance);
    }

}
