<?php

class GetAccountsResponse
{

    /**
     * @var ArrayOfAccount $GetAccountsResult
     */
    protected $GetAccountsResult = null;

    /**
     * @param ArrayOfAccount $GetAccountsResult
     */
    public function __construct($GetAccountsResult)
    {
      $this->GetAccountsResult = $GetAccountsResult;
    }

    /**
     * @return ArrayOfAccount
     */
    public function getGetAccountsResult()
    {
      return $this->GetAccountsResult;
    }

    /**
     * @param ArrayOfAccount $GetAccountsResult
     * @return GetAccountsResponse
     */
    public function setGetAccountsResult($GetAccountsResult)
    {
      $this->GetAccountsResult = $GetAccountsResult;
      return $this;
    }

}
