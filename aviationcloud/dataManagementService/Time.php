<?php

class Time
{

    /**
     * @var TimeUNit $Unit
     */
    protected $Unit = null;

    /**
     * @var float $Value
     */
    protected $Value = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return TimeUNit
     */
    public function getUnit()
    {
      return $this->Unit;
    }

    /**
     * @param TimeUNit $Unit
     * @return Time
     */
    public function setUnit($Unit)
    {
      $this->Unit = $Unit;
      return $this;
    }

    /**
     * @return float
     */
    public function getValue()
    {
      return $this->Value;
    }

    /**
     * @param float $Value
     * @return Time
     */
    public function setValue($Value)
    {
      $this->Value = $Value;
      return $this;
    }

}
