<?php

class AircraftTypeConfigurationRegistrationRequest extends RequestBase
{

    /**
     * @var string $AicraftTypeName
     */
    protected $AicraftTypeName = null;

    /**
     * @var AircraftEngineSpecification $AircraftTypeConfiguration
     */
    protected $AircraftTypeConfiguration = null;

    /**
     * @var DataLevel $DataLevel
     */
    protected $DataLevel = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return string
     */
    public function getAicraftTypeName()
    {
      return $this->AicraftTypeName;
    }

    /**
     * @param string $AicraftTypeName
     * @return AircraftTypeConfigurationRegistrationRequest
     */
    public function setAicraftTypeName($AicraftTypeName)
    {
      $this->AicraftTypeName = $AicraftTypeName;
      return $this;
    }

    /**
     * @return AircraftEngineSpecification
     */
    public function getAircraftTypeConfiguration()
    {
      return $this->AircraftTypeConfiguration;
    }

    /**
     * @param AircraftEngineSpecification $AircraftTypeConfiguration
     * @return AircraftTypeConfigurationRegistrationRequest
     */
    public function setAircraftTypeConfiguration($AircraftTypeConfiguration)
    {
      $this->AircraftTypeConfiguration = $AircraftTypeConfiguration;
      return $this;
    }

    /**
     * @return DataLevel
     */
    public function getDataLevel()
    {
      return $this->DataLevel;
    }

    /**
     * @param DataLevel $DataLevel
     * @return AircraftTypeConfigurationRegistrationRequest
     */
    public function setDataLevel($DataLevel)
    {
      $this->DataLevel = $DataLevel;
      return $this;
    }

}
