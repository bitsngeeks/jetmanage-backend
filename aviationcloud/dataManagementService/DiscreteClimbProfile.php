<?php

class DiscreteClimbProfile extends ClimbProfile
{

    /**
     * @var ArrayOfClimbDescendDataSeries $ClimbPerformanceDataSet
     */
    protected $ClimbPerformanceDataSet = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return ArrayOfClimbDescendDataSeries
     */
    public function getClimbPerformanceDataSet()
    {
      return $this->ClimbPerformanceDataSet;
    }

    /**
     * @param ArrayOfClimbDescendDataSeries $ClimbPerformanceDataSet
     * @return DiscreteClimbProfile
     */
    public function setClimbPerformanceDataSet($ClimbPerformanceDataSet)
    {
      $this->ClimbPerformanceDataSet = $ClimbPerformanceDataSet;
      return $this;
    }

}
