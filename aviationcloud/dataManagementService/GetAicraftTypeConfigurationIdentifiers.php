<?php

class GetAicraftTypeConfigurationIdentifiers
{

    /**
     * @var AircaftTypeConfigurationRetrivalRequest $request
     */
    protected $request = null;

    /**
     * @param AircaftTypeConfigurationRetrivalRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return AircaftTypeConfigurationRetrivalRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param AircaftTypeConfigurationRetrivalRequest $request
     * @return GetAicraftTypeConfigurationIdentifiers
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
