<?php

class GetAircraftTypeConfiguration
{

    /**
     * @var AircaftTypeConfigurationRetrivalRequest $request
     */
    protected $request = null;

    /**
     * @param AircaftTypeConfigurationRetrivalRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return AircaftTypeConfigurationRetrivalRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param AircaftTypeConfigurationRetrivalRequest $request
     * @return GetAircraftTypeConfiguration
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
