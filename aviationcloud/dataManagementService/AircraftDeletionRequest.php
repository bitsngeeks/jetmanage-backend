<?php

class AircraftDeletionRequest extends RequestBase
{

    /**
     * @var string $AircraftTailNumber
     */
    protected $AircraftTailNumber = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return string
     */
    public function getAircraftTailNumber()
    {
      return $this->AircraftTailNumber;
    }

    /**
     * @param string $AircraftTailNumber
     * @return AircraftDeletionRequest
     */
    public function setAircraftTailNumber($AircraftTailNumber)
    {
      $this->AircraftTailNumber = $AircraftTailNumber;
      return $this;
    }

}
