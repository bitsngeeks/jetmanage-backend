<?php

class WeightLimitation
{

    /**
     * @var ISAPoint $ISAPoint
     */
    protected $ISAPoint = null;

    /**
     * @var Weight $WeightLimit
     */
    protected $WeightLimit = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ISAPoint
     */
    public function getISAPoint()
    {
      return $this->ISAPoint;
    }

    /**
     * @param ISAPoint $ISAPoint
     * @return WeightLimitation
     */
    public function setISAPoint($ISAPoint)
    {
      $this->ISAPoint = $ISAPoint;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getWeightLimit()
    {
      return $this->WeightLimit;
    }

    /**
     * @param Weight $WeightLimit
     * @return WeightLimitation
     */
    public function setWeightLimit($WeightLimit)
    {
      $this->WeightLimit = $WeightLimit;
      return $this;
    }

}
