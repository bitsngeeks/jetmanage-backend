<?php

class AircraftRetrivalRequest extends RequestBase
{

    /**
     * @var string $Tailnumber
     */
    protected $Tailnumber = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return string
     */
    public function getTailnumber()
    {
      return $this->Tailnumber;
    }

    /**
     * @param string $Tailnumber
     * @return AircraftRetrivalRequest
     */
    public function setTailnumber($Tailnumber)
    {
      $this->Tailnumber = $Tailnumber;
      return $this;
    }

}
