<?php

class CreateOFPLayoutResponse
{

    /**
     * @var OFPLayoutCreateResponse $CreateOFPLayoutResult
     */
    protected $CreateOFPLayoutResult = null;

    /**
     * @param OFPLayoutCreateResponse $CreateOFPLayoutResult
     */
    public function __construct($CreateOFPLayoutResult)
    {
      $this->CreateOFPLayoutResult = $CreateOFPLayoutResult;
    }

    /**
     * @return OFPLayoutCreateResponse
     */
    public function getCreateOFPLayoutResult()
    {
      return $this->CreateOFPLayoutResult;
    }

    /**
     * @param OFPLayoutCreateResponse $CreateOFPLayoutResult
     * @return CreateOFPLayoutResponse
     */
    public function setCreateOFPLayoutResult($CreateOFPLayoutResult)
    {
      $this->CreateOFPLayoutResult = $CreateOFPLayoutResult;
      return $this;
    }

}
