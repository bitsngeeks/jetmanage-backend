<?php

class GetAircraftTypesResponse
{

    /**
     * @var ArrayOfAircraftType $GetAircraftTypesResult
     */
    protected $GetAircraftTypesResult = null;

    /**
     * @param ArrayOfAircraftType $GetAircraftTypesResult
     */
    public function __construct($GetAircraftTypesResult)
    {
      $this->GetAircraftTypesResult = $GetAircraftTypesResult;
    }

    /**
     * @return ArrayOfAircraftType
     */
    public function getGetAircraftTypesResult()
    {
      return $this->GetAircraftTypesResult;
    }

    /**
     * @param ArrayOfAircraftType $GetAircraftTypesResult
     * @return GetAircraftTypesResponse
     */
    public function setGetAircraftTypesResult($GetAircraftTypesResult)
    {
      $this->GetAircraftTypesResult = $GetAircraftTypesResult;
      return $this;
    }

}
