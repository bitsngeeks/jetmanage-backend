<?php

class AircraftMetaData
{

    /**
     * @var AircraftTypeDefinition $AircraftType
     */
    protected $AircraftType = null;

    /**
     * @var \DateTime $AircraftTypeConfiguratinUpdateTime
     */
    protected $AircraftTypeConfiguratinUpdateTime = null;

    /**
     * @var \DateTime $AircraftUpdateTime
     */
    protected $AircraftUpdateTime = null;

    /**
     * @var AircraftATCInfo $AirraftEquipment
     */
    protected $AirraftEquipment = null;

    /**
     * @var ArrayOfClimbPerformanceProfileSummary $AvailableClimbProfiles
     */
    protected $AvailableClimbProfiles = null;

    /**
     * @var ArrayOfCruisePerformancePorifleSummary $AvailableCruiseProfiles
     */
    protected $AvailableCruiseProfiles = null;

    /**
     * @var ArrayOfDescentPerformanceProfileSummary $AvailableDescentProfiles
     */
    protected $AvailableDescentProfiles = null;

    /**
     * @var ArrayOfETOPSProfileSummary $AvailableETOPSProfiles
     */
    protected $AvailableETOPSProfiles = null;

    /**
     * @var boolean $CanUseInterpolatedCruiseProfiles
     */
    protected $CanUseInterpolatedCruiseProfiles = null;

    /**
     * @var Weight $DefaultTaxiFuel
     */
    protected $DefaultTaxiFuel = null;

    /**
     * @var ArrayOfMassAndBalanceProfileSummary $MassAndBalanceProfileSummaries
     */
    protected $MassAndBalanceProfileSummaries = null;

    /**
     * @var Weight $MaxLandingMass
     */
    protected $MaxLandingMass = null;

    /**
     * @var Weight $MaxRampMass
     */
    protected $MaxRampMass = null;

    /**
     * @var Weight $MaxTakeOffMass
     */
    protected $MaxTakeOffMass = null;

    /**
     * @var Weight $MaxZeroFuelMass
     */
    protected $MaxZeroFuelMass = null;

    /**
     * @var int $MaximumFlightlevel
     */
    protected $MaximumFlightlevel = null;

    /**
     * @var Weight $MaximumFuelCapacity
     */
    protected $MaximumFuelCapacity = null;

    /**
     * @var int $MaximumInterpolatedCruiseIndex
     */
    protected $MaximumInterpolatedCruiseIndex = null;

    /**
     * @var int $MinimumInterpolatedCruiseIndex
     */
    protected $MinimumInterpolatedCruiseIndex = null;

    /**
     * @var ATCWakeTurbulanceCategory $WakeTurbulenceCategory
     */
    protected $WakeTurbulenceCategory = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return AircraftTypeDefinition
     */
    public function getAircraftType()
    {
      return $this->AircraftType;
    }

    /**
     * @param AircraftTypeDefinition $AircraftType
     * @return AircraftMetaData
     */
    public function setAircraftType($AircraftType)
    {
      $this->AircraftType = $AircraftType;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getAircraftTypeConfiguratinUpdateTime()
    {
      if ($this->AircraftTypeConfiguratinUpdateTime == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->AircraftTypeConfiguratinUpdateTime);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $AircraftTypeConfiguratinUpdateTime
     * @return AircraftMetaData
     */
    public function setAircraftTypeConfiguratinUpdateTime(\DateTime $AircraftTypeConfiguratinUpdateTime = null)
    {
      if ($AircraftTypeConfiguratinUpdateTime == null) {
       $this->AircraftTypeConfiguratinUpdateTime = null;
      } else {
        $this->AircraftTypeConfiguratinUpdateTime = $AircraftTypeConfiguratinUpdateTime->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getAircraftUpdateTime()
    {
      if ($this->AircraftUpdateTime == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->AircraftUpdateTime);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $AircraftUpdateTime
     * @return AircraftMetaData
     */
    public function setAircraftUpdateTime(\DateTime $AircraftUpdateTime = null)
    {
      if ($AircraftUpdateTime == null) {
       $this->AircraftUpdateTime = null;
      } else {
        $this->AircraftUpdateTime = $AircraftUpdateTime->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return AircraftATCInfo
     */
    public function getAirraftEquipment()
    {
      return $this->AirraftEquipment;
    }

    /**
     * @param AircraftATCInfo $AirraftEquipment
     * @return AircraftMetaData
     */
    public function setAirraftEquipment($AirraftEquipment)
    {
      $this->AirraftEquipment = $AirraftEquipment;
      return $this;
    }

    /**
     * @return ArrayOfClimbPerformanceProfileSummary
     */
    public function getAvailableClimbProfiles()
    {
      return $this->AvailableClimbProfiles;
    }

    /**
     * @param ArrayOfClimbPerformanceProfileSummary $AvailableClimbProfiles
     * @return AircraftMetaData
     */
    public function setAvailableClimbProfiles($AvailableClimbProfiles)
    {
      $this->AvailableClimbProfiles = $AvailableClimbProfiles;
      return $this;
    }

    /**
     * @return ArrayOfCruisePerformancePorifleSummary
     */
    public function getAvailableCruiseProfiles()
    {
      return $this->AvailableCruiseProfiles;
    }

    /**
     * @param ArrayOfCruisePerformancePorifleSummary $AvailableCruiseProfiles
     * @return AircraftMetaData
     */
    public function setAvailableCruiseProfiles($AvailableCruiseProfiles)
    {
      $this->AvailableCruiseProfiles = $AvailableCruiseProfiles;
      return $this;
    }

    /**
     * @return ArrayOfDescentPerformanceProfileSummary
     */
    public function getAvailableDescentProfiles()
    {
      return $this->AvailableDescentProfiles;
    }

    /**
     * @param ArrayOfDescentPerformanceProfileSummary $AvailableDescentProfiles
     * @return AircraftMetaData
     */
    public function setAvailableDescentProfiles($AvailableDescentProfiles)
    {
      $this->AvailableDescentProfiles = $AvailableDescentProfiles;
      return $this;
    }

    /**
     * @return ArrayOfETOPSProfileSummary
     */
    public function getAvailableETOPSProfiles()
    {
      return $this->AvailableETOPSProfiles;
    }

    /**
     * @param ArrayOfETOPSProfileSummary $AvailableETOPSProfiles
     * @return AircraftMetaData
     */
    public function setAvailableETOPSProfiles($AvailableETOPSProfiles)
    {
      $this->AvailableETOPSProfiles = $AvailableETOPSProfiles;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getCanUseInterpolatedCruiseProfiles()
    {
      return $this->CanUseInterpolatedCruiseProfiles;
    }

    /**
     * @param boolean $CanUseInterpolatedCruiseProfiles
     * @return AircraftMetaData
     */
    public function setCanUseInterpolatedCruiseProfiles($CanUseInterpolatedCruiseProfiles)
    {
      $this->CanUseInterpolatedCruiseProfiles = $CanUseInterpolatedCruiseProfiles;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getDefaultTaxiFuel()
    {
      return $this->DefaultTaxiFuel;
    }

    /**
     * @param Weight $DefaultTaxiFuel
     * @return AircraftMetaData
     */
    public function setDefaultTaxiFuel($DefaultTaxiFuel)
    {
      $this->DefaultTaxiFuel = $DefaultTaxiFuel;
      return $this;
    }

    /**
     * @return ArrayOfMassAndBalanceProfileSummary
     */
    public function getMassAndBalanceProfileSummaries()
    {
      return $this->MassAndBalanceProfileSummaries;
    }

    /**
     * @param ArrayOfMassAndBalanceProfileSummary $MassAndBalanceProfileSummaries
     * @return AircraftMetaData
     */
    public function setMassAndBalanceProfileSummaries($MassAndBalanceProfileSummaries)
    {
      $this->MassAndBalanceProfileSummaries = $MassAndBalanceProfileSummaries;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getMaxLandingMass()
    {
      return $this->MaxLandingMass;
    }

    /**
     * @param Weight $MaxLandingMass
     * @return AircraftMetaData
     */
    public function setMaxLandingMass($MaxLandingMass)
    {
      $this->MaxLandingMass = $MaxLandingMass;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getMaxRampMass()
    {
      return $this->MaxRampMass;
    }

    /**
     * @param Weight $MaxRampMass
     * @return AircraftMetaData
     */
    public function setMaxRampMass($MaxRampMass)
    {
      $this->MaxRampMass = $MaxRampMass;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getMaxTakeOffMass()
    {
      return $this->MaxTakeOffMass;
    }

    /**
     * @param Weight $MaxTakeOffMass
     * @return AircraftMetaData
     */
    public function setMaxTakeOffMass($MaxTakeOffMass)
    {
      $this->MaxTakeOffMass = $MaxTakeOffMass;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getMaxZeroFuelMass()
    {
      return $this->MaxZeroFuelMass;
    }

    /**
     * @param Weight $MaxZeroFuelMass
     * @return AircraftMetaData
     */
    public function setMaxZeroFuelMass($MaxZeroFuelMass)
    {
      $this->MaxZeroFuelMass = $MaxZeroFuelMass;
      return $this;
    }

    /**
     * @return int
     */
    public function getMaximumFlightlevel()
    {
      return $this->MaximumFlightlevel;
    }

    /**
     * @param int $MaximumFlightlevel
     * @return AircraftMetaData
     */
    public function setMaximumFlightlevel($MaximumFlightlevel)
    {
      $this->MaximumFlightlevel = $MaximumFlightlevel;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getMaximumFuelCapacity()
    {
      return $this->MaximumFuelCapacity;
    }

    /**
     * @param Weight $MaximumFuelCapacity
     * @return AircraftMetaData
     */
    public function setMaximumFuelCapacity($MaximumFuelCapacity)
    {
      $this->MaximumFuelCapacity = $MaximumFuelCapacity;
      return $this;
    }

    /**
     * @return int
     */
    public function getMaximumInterpolatedCruiseIndex()
    {
      return $this->MaximumInterpolatedCruiseIndex;
    }

    /**
     * @param int $MaximumInterpolatedCruiseIndex
     * @return AircraftMetaData
     */
    public function setMaximumInterpolatedCruiseIndex($MaximumInterpolatedCruiseIndex)
    {
      $this->MaximumInterpolatedCruiseIndex = $MaximumInterpolatedCruiseIndex;
      return $this;
    }

    /**
     * @return int
     */
    public function getMinimumInterpolatedCruiseIndex()
    {
      return $this->MinimumInterpolatedCruiseIndex;
    }

    /**
     * @param int $MinimumInterpolatedCruiseIndex
     * @return AircraftMetaData
     */
    public function setMinimumInterpolatedCruiseIndex($MinimumInterpolatedCruiseIndex)
    {
      $this->MinimumInterpolatedCruiseIndex = $MinimumInterpolatedCruiseIndex;
      return $this;
    }

    /**
     * @return ATCWakeTurbulanceCategory
     */
    public function getWakeTurbulenceCategory()
    {
      return $this->WakeTurbulenceCategory;
    }

    /**
     * @param ATCWakeTurbulanceCategory $WakeTurbulenceCategory
     * @return AircraftMetaData
     */
    public function setWakeTurbulenceCategory($WakeTurbulenceCategory)
    {
      $this->WakeTurbulenceCategory = $WakeTurbulenceCategory;
      return $this;
    }

}
