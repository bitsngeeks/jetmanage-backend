<?php

class AircraftTypeRetrivalRequest extends RequestBase
{

    /**
     * @var string $AircraftTypeSearchString
     */
    protected $AircraftTypeSearchString = null;

    /**
     * @var DataLevel $DataLevel
     */
    protected $DataLevel = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return string
     */
    public function getAircraftTypeSearchString()
    {
      return $this->AircraftTypeSearchString;
    }

    /**
     * @param string $AircraftTypeSearchString
     * @return AircraftTypeRetrivalRequest
     */
    public function setAircraftTypeSearchString($AircraftTypeSearchString)
    {
      $this->AircraftTypeSearchString = $AircraftTypeSearchString;
      return $this;
    }

    /**
     * @return DataLevel
     */
    public function getDataLevel()
    {
      return $this->DataLevel;
    }

    /**
     * @param DataLevel $DataLevel
     * @return AircraftTypeRetrivalRequest
     */
    public function setDataLevel($DataLevel)
    {
      $this->DataLevel = $DataLevel;
      return $this;
    }

}
