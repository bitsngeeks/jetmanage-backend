<?php

class AircraftCruisePerformanceData
{

    /**
     * @var Weight $FuelFlow
     */
    protected $FuelFlow = null;

    /**
     * @var Length $Speed
     */
    protected $Speed = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Weight
     */
    public function getFuelFlow()
    {
      return $this->FuelFlow;
    }

    /**
     * @param Weight $FuelFlow
     * @return AircraftCruisePerformanceData
     */
    public function setFuelFlow($FuelFlow)
    {
      $this->FuelFlow = $FuelFlow;
      return $this;
    }

    /**
     * @return Length
     */
    public function getSpeed()
    {
      return $this->Speed;
    }

    /**
     * @param Length $Speed
     * @return AircraftCruisePerformanceData
     */
    public function setSpeed($Speed)
    {
      $this->Speed = $Speed;
      return $this;
    }

}
