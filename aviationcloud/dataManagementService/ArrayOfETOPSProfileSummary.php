<?php

class ArrayOfETOPSProfileSummary implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var ETOPSProfileSummary[] $ETOPSProfileSummary
     */
    protected $ETOPSProfileSummary = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ETOPSProfileSummary[]
     */
    public function getETOPSProfileSummary()
    {
      return $this->ETOPSProfileSummary;
    }

    /**
     * @param ETOPSProfileSummary[] $ETOPSProfileSummary
     * @return ArrayOfETOPSProfileSummary
     */
    public function setETOPSProfileSummary(array $ETOPSProfileSummary = null)
    {
      $this->ETOPSProfileSummary = $ETOPSProfileSummary;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->ETOPSProfileSummary[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return ETOPSProfileSummary
     */
    public function offsetGet($offset)
    {
      return $this->ETOPSProfileSummary[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param ETOPSProfileSummary $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->ETOPSProfileSummary[] = $value;
      } else {
        $this->ETOPSProfileSummary[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->ETOPSProfileSummary[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return ETOPSProfileSummary Return the current element
     */
    public function current()
    {
      return current($this->ETOPSProfileSummary);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->ETOPSProfileSummary);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->ETOPSProfileSummary);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->ETOPSProfileSummary);
    }

    /**
     * Countable implementation
     *
     * @return ETOPSProfileSummary Return count of elements
     */
    public function count()
    {
      return count($this->ETOPSProfileSummary);
    }

}
