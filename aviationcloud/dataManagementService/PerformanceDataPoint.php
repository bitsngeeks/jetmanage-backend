<?php

class PerformanceDataPoint
{

    /**
     * @var ISAPoint $ISAPoint
     */
    protected $ISAPoint = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ISAPoint
     */
    public function getISAPoint()
    {
      return $this->ISAPoint;
    }

    /**
     * @param ISAPoint $ISAPoint
     * @return PerformanceDataPoint
     */
    public function setISAPoint($ISAPoint)
    {
      $this->ISAPoint = $ISAPoint;
      return $this;
    }

}
