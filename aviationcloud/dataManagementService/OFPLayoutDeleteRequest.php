<?php

class OFPLayoutDeleteRequest extends RequestBase
{

    /**
     * @var int $LayoutNumber
     */
    protected $LayoutNumber = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return int
     */
    public function getLayoutNumber()
    {
      return $this->LayoutNumber;
    }

    /**
     * @param int $LayoutNumber
     * @return OFPLayoutDeleteRequest
     */
    public function setLayoutNumber($LayoutNumber)
    {
      $this->LayoutNumber = $LayoutNumber;
      return $this;
    }

}
