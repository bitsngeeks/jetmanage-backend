<?php

class ArrayOfClimbPerformanceProfileSummary implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var ClimbPerformanceProfileSummary[] $ClimbPerformanceProfileSummary
     */
    protected $ClimbPerformanceProfileSummary = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ClimbPerformanceProfileSummary[]
     */
    public function getClimbPerformanceProfileSummary()
    {
      return $this->ClimbPerformanceProfileSummary;
    }

    /**
     * @param ClimbPerformanceProfileSummary[] $ClimbPerformanceProfileSummary
     * @return ArrayOfClimbPerformanceProfileSummary
     */
    public function setClimbPerformanceProfileSummary(array $ClimbPerformanceProfileSummary = null)
    {
      $this->ClimbPerformanceProfileSummary = $ClimbPerformanceProfileSummary;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->ClimbPerformanceProfileSummary[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return ClimbPerformanceProfileSummary
     */
    public function offsetGet($offset)
    {
      return $this->ClimbPerformanceProfileSummary[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param ClimbPerformanceProfileSummary $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->ClimbPerformanceProfileSummary[] = $value;
      } else {
        $this->ClimbPerformanceProfileSummary[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->ClimbPerformanceProfileSummary[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return ClimbPerformanceProfileSummary Return the current element
     */
    public function current()
    {
      return current($this->ClimbPerformanceProfileSummary);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->ClimbPerformanceProfileSummary);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->ClimbPerformanceProfileSummary);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->ClimbPerformanceProfileSummary);
    }

    /**
     * Countable implementation
     *
     * @return ClimbPerformanceProfileSummary Return count of elements
     */
    public function count()
    {
      return count($this->ClimbPerformanceProfileSummary);
    }

}
