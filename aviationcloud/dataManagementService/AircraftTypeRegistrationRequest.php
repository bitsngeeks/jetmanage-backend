<?php

class AircraftTypeRegistrationRequest extends RequestBase
{

    /**
     * @var AircraftType $AircraftType
     */
    protected $AircraftType = null;

    /**
     * @var DataLevel $DataLevel
     */
    protected $DataLevel = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return AircraftType
     */
    public function getAircraftType()
    {
      return $this->AircraftType;
    }

    /**
     * @param AircraftType $AircraftType
     * @return AircraftTypeRegistrationRequest
     */
    public function setAircraftType($AircraftType)
    {
      $this->AircraftType = $AircraftType;
      return $this;
    }

    /**
     * @return DataLevel
     */
    public function getDataLevel()
    {
      return $this->DataLevel;
    }

    /**
     * @param DataLevel $DataLevel
     * @return AircraftTypeRegistrationRequest
     */
    public function setDataLevel($DataLevel)
    {
      $this->DataLevel = $DataLevel;
      return $this;
    }

}
