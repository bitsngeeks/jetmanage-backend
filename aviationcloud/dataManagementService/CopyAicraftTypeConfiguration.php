<?php

class CopyAicraftTypeConfiguration
{

    /**
     * @var AicraftTypeConfigurationCopyRequest $request
     */
    protected $request = null;

    /**
     * @param AicraftTypeConfigurationCopyRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return AicraftTypeConfigurationCopyRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param AicraftTypeConfigurationCopyRequest $request
     * @return CopyAicraftTypeConfiguration
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
