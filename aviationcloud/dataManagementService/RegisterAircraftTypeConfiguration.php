<?php

class RegisterAircraftTypeConfiguration
{

    /**
     * @var AircraftTypeConfigurationRegistrationRequest $request
     */
    protected $request = null;

    /**
     * @param AircraftTypeConfigurationRegistrationRequest $request
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * @return AircraftTypeConfigurationRegistrationRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param AircraftTypeConfigurationRegistrationRequest $request
     * @return RegisterAircraftTypeConfiguration
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
