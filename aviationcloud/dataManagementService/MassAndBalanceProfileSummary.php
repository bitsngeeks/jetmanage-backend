<?php

class MassAndBalanceProfileSummary
{

    /**
     * @var Weight $BasicMass
     */
    protected $BasicMass = null;

    /**
     * @var int $MaxPax
     */
    protected $MaxPax = null;

    /**
     * @var Weight $MaximumPayload
     */
    protected $MaximumPayload = null;

    /**
     * @var string $ProfileName
     */
    protected $ProfileName = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Weight
     */
    public function getBasicMass()
    {
      return $this->BasicMass;
    }

    /**
     * @param Weight $BasicMass
     * @return MassAndBalanceProfileSummary
     */
    public function setBasicMass($BasicMass)
    {
      $this->BasicMass = $BasicMass;
      return $this;
    }

    /**
     * @return int
     */
    public function getMaxPax()
    {
      return $this->MaxPax;
    }

    /**
     * @param int $MaxPax
     * @return MassAndBalanceProfileSummary
     */
    public function setMaxPax($MaxPax)
    {
      $this->MaxPax = $MaxPax;
      return $this;
    }

    /**
     * @return Weight
     */
    public function getMaximumPayload()
    {
      return $this->MaximumPayload;
    }

    /**
     * @param Weight $MaximumPayload
     * @return MassAndBalanceProfileSummary
     */
    public function setMaximumPayload($MaximumPayload)
    {
      $this->MaximumPayload = $MaximumPayload;
      return $this;
    }

    /**
     * @return string
     */
    public function getProfileName()
    {
      return $this->ProfileName;
    }

    /**
     * @param string $ProfileName
     * @return MassAndBalanceProfileSummary
     */
    public function setProfileName($ProfileName)
    {
      $this->ProfileName = $ProfileName;
      return $this;
    }

}
