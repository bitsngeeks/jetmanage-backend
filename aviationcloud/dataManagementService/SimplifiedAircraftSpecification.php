<?php

class SimplifiedAircraftSpecification extends AircraftSpecification
{

    /**
     * @var AircraftFuelType $AircraftFuelType
     */
    protected $AircraftFuelType = null;

    /**
     * @var string $AircraftICAOId
     */
    protected $AircraftICAOId = null;

    /**
     * @var SimpleClimbProfile $ClimbProfile
     */
    protected $ClimbProfile = null;

    /**
     * @var SimpleCruiseProfile $CruiseProfile
     */
    protected $CruiseProfile = null;

    /**
     * @var SimpleDescentProfile $DescentProfile
     */
    protected $DescentProfile = null;

    /**
     * @var float $EmptyWeight
     */
    protected $EmptyWeight = null;

    /**
     * @var AircraftATCInfo $Equipment
     */
    protected $Equipment = null;

    /**
     * @var float $MaxLanding
     */
    protected $MaxLanding = null;

    /**
     * @var float $MaxRampMass
     */
    protected $MaxRampMass = null;

    /**
     * @var float $MaxTakeoff
     */
    protected $MaxTakeoff = null;

    /**
     * @var float $MaxZeroFuel
     */
    protected $MaxZeroFuel = null;

    /**
     * @var DataLevel $PreferedDataLevel
     */
    protected $PreferedDataLevel = null;

    /**
     * @var string $Tailnumber
     */
    protected $Tailnumber = null;

    /**
     * @var float $UsableFuel
     */
    protected $UsableFuel = null;

    /**
     * @var ATCWakeTurbulanceCategory $WakeTurbulanceCategory
     */
    protected $WakeTurbulanceCategory = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return AircraftFuelType
     */
    public function getAircraftFuelType()
    {
      return $this->AircraftFuelType;
    }

    /**
     * @param AircraftFuelType $AircraftFuelType
     * @return SimplifiedAircraftSpecification
     */
    public function setAircraftFuelType($AircraftFuelType)
    {
      $this->AircraftFuelType = $AircraftFuelType;
      return $this;
    }

    /**
     * @return string
     */
    public function getAircraftICAOId()
    {
      return $this->AircraftICAOId;
    }

    /**
     * @param string $AircraftICAOId
     * @return SimplifiedAircraftSpecification
     */
    public function setAircraftICAOId($AircraftICAOId)
    {
      $this->AircraftICAOId = $AircraftICAOId;
      return $this;
    }

    /**
     * @return SimpleClimbProfile
     */
    public function getClimbProfile()
    {
      return $this->ClimbProfile;
    }

    /**
     * @param SimpleClimbProfile $ClimbProfile
     * @return SimplifiedAircraftSpecification
     */
    public function setClimbProfile($ClimbProfile)
    {
      $this->ClimbProfile = $ClimbProfile;
      return $this;
    }

    /**
     * @return SimpleCruiseProfile
     */
    public function getCruiseProfile()
    {
      return $this->CruiseProfile;
    }

    /**
     * @param SimpleCruiseProfile $CruiseProfile
     * @return SimplifiedAircraftSpecification
     */
    public function setCruiseProfile($CruiseProfile)
    {
      $this->CruiseProfile = $CruiseProfile;
      return $this;
    }

    /**
     * @return SimpleDescentProfile
     */
    public function getDescentProfile()
    {
      return $this->DescentProfile;
    }

    /**
     * @param SimpleDescentProfile $DescentProfile
     * @return SimplifiedAircraftSpecification
     */
    public function setDescentProfile($DescentProfile)
    {
      $this->DescentProfile = $DescentProfile;
      return $this;
    }

    /**
     * @return float
     */
    public function getEmptyWeight()
    {
      return $this->EmptyWeight;
    }

    /**
     * @param float $EmptyWeight
     * @return SimplifiedAircraftSpecification
     */
    public function setEmptyWeight($EmptyWeight)
    {
      $this->EmptyWeight = $EmptyWeight;
      return $this;
    }

    /**
     * @return AircraftATCInfo
     */
    public function getEquipment()
    {
      return $this->Equipment;
    }

    /**
     * @param AircraftATCInfo $Equipment
     * @return SimplifiedAircraftSpecification
     */
    public function setEquipment($Equipment)
    {
      $this->Equipment = $Equipment;
      return $this;
    }

    /**
     * @return float
     */
    public function getMaxLanding()
    {
      return $this->MaxLanding;
    }

    /**
     * @param float $MaxLanding
     * @return SimplifiedAircraftSpecification
     */
    public function setMaxLanding($MaxLanding)
    {
      $this->MaxLanding = $MaxLanding;
      return $this;
    }

    /**
     * @return float
     */
    public function getMaxRampMass()
    {
      return $this->MaxRampMass;
    }

    /**
     * @param float $MaxRampMass
     * @return SimplifiedAircraftSpecification
     */
    public function setMaxRampMass($MaxRampMass)
    {
      $this->MaxRampMass = $MaxRampMass;
      return $this;
    }

    /**
     * @return float
     */
    public function getMaxTakeoff()
    {
      return $this->MaxTakeoff;
    }

    /**
     * @param float $MaxTakeoff
     * @return SimplifiedAircraftSpecification
     */
    public function setMaxTakeoff($MaxTakeoff)
    {
      $this->MaxTakeoff = $MaxTakeoff;
      return $this;
    }

    /**
     * @return float
     */
    public function getMaxZeroFuel()
    {
      return $this->MaxZeroFuel;
    }

    /**
     * @param float $MaxZeroFuel
     * @return SimplifiedAircraftSpecification
     */
    public function setMaxZeroFuel($MaxZeroFuel)
    {
      $this->MaxZeroFuel = $MaxZeroFuel;
      return $this;
    }

    /**
     * @return DataLevel
     */
    public function getPreferedDataLevel()
    {
      return $this->PreferedDataLevel;
    }

    /**
     * @param DataLevel $PreferedDataLevel
     * @return SimplifiedAircraftSpecification
     */
    public function setPreferedDataLevel($PreferedDataLevel)
    {
      $this->PreferedDataLevel = $PreferedDataLevel;
      return $this;
    }

    /**
     * @return string
     */
    public function getTailnumber()
    {
      return $this->Tailnumber;
    }

    /**
     * @param string $Tailnumber
     * @return SimplifiedAircraftSpecification
     */
    public function setTailnumber($Tailnumber)
    {
      $this->Tailnumber = $Tailnumber;
      return $this;
    }

    /**
     * @return float
     */
    public function getUsableFuel()
    {
      return $this->UsableFuel;
    }

    /**
     * @param float $UsableFuel
     * @return SimplifiedAircraftSpecification
     */
    public function setUsableFuel($UsableFuel)
    {
      $this->UsableFuel = $UsableFuel;
      return $this;
    }

    /**
     * @return ATCWakeTurbulanceCategory
     */
    public function getWakeTurbulanceCategory()
    {
      return $this->WakeTurbulanceCategory;
    }

    /**
     * @param ATCWakeTurbulanceCategory $WakeTurbulanceCategory
     * @return SimplifiedAircraftSpecification
     */
    public function setWakeTurbulanceCategory($WakeTurbulanceCategory)
    {
      $this->WakeTurbulanceCategory = $WakeTurbulanceCategory;
      return $this;
    }

}
