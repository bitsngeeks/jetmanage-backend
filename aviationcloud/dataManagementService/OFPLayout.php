<?php

class OFPLayout
{

    /**
     * @var base64Binary $Content
     */
    protected $Content = null;

    /**
     * @var string $Description
     */
    protected $Description = null;

    /**
     * @var int $LayoutNumber
     */
    protected $LayoutNumber = null;

    /**
     * @var string $Name
     */
    protected $Name = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return base64Binary
     */
    public function getContent()
    {
      return $this->Content;
    }

    /**
     * @param base64Binary $Content
     * @return OFPLayout
     */
    public function setContent($Content)
    {
      $this->Content = $Content;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
      return $this->Description;
    }

    /**
     * @param string $Description
     * @return OFPLayout
     */
    public function setDescription($Description)
    {
      $this->Description = $Description;
      return $this;
    }

    /**
     * @return int
     */
    public function getLayoutNumber()
    {
      return $this->LayoutNumber;
    }

    /**
     * @param int $LayoutNumber
     * @return OFPLayout
     */
    public function setLayoutNumber($LayoutNumber)
    {
      $this->LayoutNumber = $LayoutNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->Name;
    }

    /**
     * @param string $Name
     * @return OFPLayout
     */
    public function setName($Name)
    {
      $this->Name = $Name;
      return $this;
    }

}
