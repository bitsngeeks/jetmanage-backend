<?php

class ArrayOfCargoCompartment implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var CargoCompartment[] $CargoCompartment
     */
    protected $CargoCompartment = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return CargoCompartment[]
     */
    public function getCargoCompartment()
    {
      return $this->CargoCompartment;
    }

    /**
     * @param CargoCompartment[] $CargoCompartment
     * @return ArrayOfCargoCompartment
     */
    public function setCargoCompartment(array $CargoCompartment = null)
    {
      $this->CargoCompartment = $CargoCompartment;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->CargoCompartment[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return CargoCompartment
     */
    public function offsetGet($offset)
    {
      return $this->CargoCompartment[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param CargoCompartment $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->CargoCompartment[] = $value;
      } else {
        $this->CargoCompartment[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->CargoCompartment[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return CargoCompartment Return the current element
     */
    public function current()
    {
      return current($this->CargoCompartment);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->CargoCompartment);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->CargoCompartment);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->CargoCompartment);
    }

    /**
     * Countable implementation
     *
     * @return CargoCompartment Return count of elements
     */
    public function count()
    {
      return count($this->CargoCompartment);
    }

}
