<?php

class GetAllRegisteredTailNumbersResponse
{

    /**
     * @var ArrayOfstring $GetAllRegisteredTailNumbersResult
     */
    protected $GetAllRegisteredTailNumbersResult = null;

    /**
     * @param ArrayOfstring $GetAllRegisteredTailNumbersResult
     */
    public function __construct($GetAllRegisteredTailNumbersResult)
    {
      $this->GetAllRegisteredTailNumbersResult = $GetAllRegisteredTailNumbersResult;
    }

    /**
     * @return ArrayOfstring
     */
    public function getGetAllRegisteredTailNumbersResult()
    {
      return $this->GetAllRegisteredTailNumbersResult;
    }

    /**
     * @param ArrayOfstring $GetAllRegisteredTailNumbersResult
     * @return GetAllRegisteredTailNumbersResponse
     */
    public function setGetAllRegisteredTailNumbersResult($GetAllRegisteredTailNumbersResult)
    {
      $this->GetAllRegisteredTailNumbersResult = $GetAllRegisteredTailNumbersResult;
      return $this;
    }

}
