<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LegApiTest extends TestCase
{
    use MakeLegTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateLeg()
    {
        $leg = $this->fakeLegData();
        $this->json('POST', '/api/v1/legs', $leg);

        $this->assertApiResponse($leg);
    }

    /**
     * @test
     */
    public function testReadLeg()
    {
        $leg = $this->makeLeg();
        $this->json('GET', '/api/v1/legs/'.$leg->id);

        $this->assertApiResponse($leg->toArray());
    }

    /**
     * @test
     */
    public function testUpdateLeg()
    {
        $leg = $this->makeLeg();
        $editedLeg = $this->fakeLegData();

        $this->json('PUT', '/api/v1/legs/'.$leg->id, $editedLeg);

        $this->assertApiResponse($editedLeg);
    }

    /**
     * @test
     */
    public function testDeleteLeg()
    {
        $leg = $this->makeLeg();
        $this->json('DELETE', '/api/v1/legs/'.$leg->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/legs/'.$leg->id);

        $this->assertResponseStatus(404);
    }
}
