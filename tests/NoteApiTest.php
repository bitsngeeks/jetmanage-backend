<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class NoteApiTest extends TestCase
{
    use MakeNoteTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateNote()
    {
        $note = $this->fakeNoteData();
        $this->json('POST', '/api/v1/notes', $note);

        $this->assertApiResponse($note);
    }

    /**
     * @test
     */
    public function testReadNote()
    {
        $note = $this->makeNote();
        $this->json('GET', '/api/v1/notes/'.$note->id);

        $this->assertApiResponse($note->toArray());
    }

    /**
     * @test
     */
    public function testUpdateNote()
    {
        $note = $this->makeNote();
        $editedNote = $this->fakeNoteData();

        $this->json('PUT', '/api/v1/notes/'.$note->id, $editedNote);

        $this->assertApiResponse($editedNote);
    }

    /**
     * @test
     */
    public function testDeleteNote()
    {
        $note = $this->makeNote();
        $this->json('DELETE', '/api/v1/notes/'.$note->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/notes/'.$note->id);

        $this->assertResponseStatus(404);
    }
}
