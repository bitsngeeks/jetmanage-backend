<?php

use App\Models\Airpors;
use App\Repositories\AirporsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AirporsRepositoryTest extends TestCase
{
    use MakeAirporsTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var AirporsRepository
     */
    protected $airporsRepo;

    public function setUp()
    {
        parent::setUp();
        $this->airporsRepo = App::make(AirporsRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateAirpors()
    {
        $airpors = $this->fakeAirporsData();
        $createdAirpors = $this->airporsRepo->create($airpors);
        $createdAirpors = $createdAirpors->toArray();
        $this->assertArrayHasKey('id', $createdAirpors);
        $this->assertNotNull($createdAirpors['id'], 'Created Airpors must have id specified');
        $this->assertNotNull(Airpors::find($createdAirpors['id']), 'Airpors with given id must be in DB');
        $this->assertModelData($airpors, $createdAirpors);
    }

    /**
     * @test read
     */
    public function testReadAirpors()
    {
        $airpors = $this->makeAirpors();
        $dbAirpors = $this->airporsRepo->find($airpors->id);
        $dbAirpors = $dbAirpors->toArray();
        $this->assertModelData($airpors->toArray(), $dbAirpors);
    }

    /**
     * @test update
     */
    public function testUpdateAirpors()
    {
        $airpors = $this->makeAirpors();
        $fakeAirpors = $this->fakeAirporsData();
        $updatedAirpors = $this->airporsRepo->update($fakeAirpors, $airpors->id);
        $this->assertModelData($fakeAirpors, $updatedAirpors->toArray());
        $dbAirpors = $this->airporsRepo->find($airpors->id);
        $this->assertModelData($fakeAirpors, $dbAirpors->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteAirpors()
    {
        $airpors = $this->makeAirpors();
        $resp = $this->airporsRepo->delete($airpors->id);
        $this->assertTrue($resp);
        $this->assertNull(Airpors::find($airpors->id), 'Airpors should not exist in DB');
    }
}
