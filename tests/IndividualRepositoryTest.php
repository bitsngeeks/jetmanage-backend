<?php

use App\Models\Individual;
use App\Repositories\IndividualRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class IndividualRepositoryTest extends TestCase
{
    use MakeIndividualTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var IndividualRepository
     */
    protected $individualRepo;

    public function setUp()
    {
        parent::setUp();
        $this->individualRepo = App::make(IndividualRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateIndividual()
    {
        $individual = $this->fakeIndividualData();
        $createdIndividual = $this->individualRepo->create($individual);
        $createdIndividual = $createdIndividual->toArray();
        $this->assertArrayHasKey('id', $createdIndividual);
        $this->assertNotNull($createdIndividual['id'], 'Created Individual must have id specified');
        $this->assertNotNull(Individual::find($createdIndividual['id']), 'Individual with given id must be in DB');
        $this->assertModelData($individual, $createdIndividual);
    }

    /**
     * @test read
     */
    public function testReadIndividual()
    {
        $individual = $this->makeIndividual();
        $dbIndividual = $this->individualRepo->find($individual->id);
        $dbIndividual = $dbIndividual->toArray();
        $this->assertModelData($individual->toArray(), $dbIndividual);
    }

    /**
     * @test update
     */
    public function testUpdateIndividual()
    {
        $individual = $this->makeIndividual();
        $fakeIndividual = $this->fakeIndividualData();
        $updatedIndividual = $this->individualRepo->update($fakeIndividual, $individual->id);
        $this->assertModelData($fakeIndividual, $updatedIndividual->toArray());
        $dbIndividual = $this->individualRepo->find($individual->id);
        $this->assertModelData($fakeIndividual, $dbIndividual->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteIndividual()
    {
        $individual = $this->makeIndividual();
        $resp = $this->individualRepo->delete($individual->id);
        $this->assertTrue($resp);
        $this->assertNull(Individual::find($individual->id), 'Individual should not exist in DB');
    }
}
