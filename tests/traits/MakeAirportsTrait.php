<?php

use Faker\Factory as Faker;
use App\Models\Airports;
use App\Repositories\AirportsRepository;

trait MakeAirportsTrait
{
    /**
     * Create fake instance of Airports and save it in database
     *
     * @param array $airportsFields
     * @return Airports
     */
    public function makeAirports($airportsFields = [])
    {
        /** @var AirportsRepository $airportsRepo */
        $airportsRepo = App::make(AirportsRepository::class);
        $theme = $this->fakeAirportsData($airportsFields);
        return $airportsRepo->create($theme);
    }

    /**
     * Get fake instance of Airports
     *
     * @param array $airportsFields
     * @return Airports
     */
    public function fakeAirports($airportsFields = [])
    {
        return new Airports($this->fakeAirportsData($airportsFields));
    }

    /**
     * Get fake data of Airports
     *
     * @param array $postFields
     * @return array
     */
    public function fakeAirportsData($airportsFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'airport_entry_id' => $fake->randomDigitNotNull,
            'code' => $fake->word,
            'city' => $fake->word,
            'country' => $fake->word,
            'faaCode' => $fake->word,
            'iata' => $fake->word,
            'icao' => $fake->word,
            'landingFacilityId' => $fake->word,
            'latitude' => $fake->randomDigitNotNull,
            'longitude' => $fake->randomDigitNotNull,
            'typicallyHasMetar' => $fake->word,
            'typicallyHasTaf' => $fake->word,
            'name' => $fake->word,
            'state' => $fake->word,
            'zip' => $fake->word,
            'fullCountryNameLowerCase' => $fake->word,
            'magVarWest' => $fake->randomDigitNotNull,
            'access' => $fake->word,
            'type' => $fake->word,
            'hasFuel' => $fake->word,
            'fuelTypes' => $fake->word,
            'longestRunwayLength' => $fake->randomDigitNotNull,
            'longestRunwaySurfaceType' => $fake->word,
            'nearestlandingFacilityIds' => $fake->word,
            'elevationFt' => $fake->randomDigitNotNull,
            'procedureTypes' => $fake->word,
            'towered' => $fake->word
        ], $airportsFields);
    }
}
