<?php

use Faker\Factory as Faker;
use App\Models\Individual;
use App\Repositories\IndividualRepository;

trait MakeIndividualTrait
{
    /**
     * Create fake instance of Individual and save it in database
     *
     * @param array $individualFields
     * @return Individual
     */
    public function makeIndividual($individualFields = [])
    {
        /** @var IndividualRepository $individualRepo */
        $individualRepo = App::make(IndividualRepository::class);
        $theme = $this->fakeIndividualData($individualFields);
        return $individualRepo->create($theme);
    }

    /**
     * Get fake instance of Individual
     *
     * @param array $individualFields
     * @return Individual
     */
    public function fakeIndividual($individualFields = [])
    {
        return new Individual($this->fakeIndividualData($individualFields));
    }

    /**
     * Get fake data of Individual
     *
     * @param array $postFields
     * @return array
     */
    public function fakeIndividualData($individualFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'first_name' => $fake->word,
            'middle_name' => $fake->word,
            'last_name' => $fake->word,
            'gender' => $fake->word,
            'dob' => $fake->date('Y-m-d H:i:s'),
            'weight' => $fake->randomDigitNotNull,
            'height' => $fake->randomDigitNotNull,
            'res_country_id' => $fake->randomDigitNotNull,
            'birth_country_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s'),
            'company_id' => $fake->randomDigitNotNull
        ], $individualFields);
    }
}
