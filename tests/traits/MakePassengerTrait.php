<?php

use Faker\Factory as Faker;
use App\Models\Passenger;
use App\Repositories\PassengerRepository;

trait MakePassengerTrait
{
    /**
     * Create fake instance of Passenger and save it in database
     *
     * @param array $passengerFields
     * @return Passenger
     */
    public function makePassenger($passengerFields = [])
    {
        /** @var PassengerRepository $passengerRepo */
        $passengerRepo = App::make(PassengerRepository::class);
        $theme = $this->fakePassengerData($passengerFields);
        return $passengerRepo->create($theme);
    }

    /**
     * Get fake instance of Passenger
     *
     * @param array $passengerFields
     * @return Passenger
     */
    public function fakePassenger($passengerFields = [])
    {
        return new Passenger($this->fakePassengerData($passengerFields));
    }

    /**
     * Get fake data of Passenger
     *
     * @param array $postFields
     * @return array
     */
    public function fakePassengerData($passengerFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'leg_id' => $fake->randomDigitNotNull,
            'trip_id' => $fake->randomDigitNotNull,
            'ind_id' => $fake->randomDigitNotNull,
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $passengerFields);
    }
}
