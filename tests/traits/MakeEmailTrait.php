<?php

use Faker\Factory as Faker;
use App\Models\Email;
use App\Repositories\EmailRepository;

trait MakeEmailTrait
{
    /**
     * Create fake instance of Email and save it in database
     *
     * @param array $emailFields
     * @return Email
     */
    public function makeEmail($emailFields = [])
    {
        /** @var EmailRepository $emailRepo */
        $emailRepo = App::make(EmailRepository::class);
        $theme = $this->fakeEmailData($emailFields);
        return $emailRepo->create($theme);
    }

    /**
     * Get fake instance of Email
     *
     * @param array $emailFields
     * @return Email
     */
    public function fakeEmail($emailFields = [])
    {
        return new Email($this->fakeEmailData($emailFields));
    }

    /**
     * Get fake data of Email
     *
     * @param array $postFields
     * @return array
     */
    public function fakeEmailData($emailFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'ind_id' => $fake->randomDigitNotNull,
            'email' => $fake->word,
            'is_verified' => $fake->word,
            'is_primary' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $emailFields);
    }
}
