<?php

use App\Models\Trip;
use App\Repositories\TripRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TripRepositoryTest extends TestCase
{
    use MakeTripTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var TripRepository
     */
    protected $tripRepo;

    public function setUp()
    {
        parent::setUp();
        $this->tripRepo = App::make(TripRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateTrip()
    {
        $trip = $this->fakeTripData();
        $createdTrip = $this->tripRepo->create($trip);
        $createdTrip = $createdTrip->toArray();
        $this->assertArrayHasKey('id', $createdTrip);
        $this->assertNotNull($createdTrip['id'], 'Created Trip must have id specified');
        $this->assertNotNull(Trip::find($createdTrip['id']), 'Trip with given id must be in DB');
        $this->assertModelData($trip, $createdTrip);
    }

    /**
     * @test read
     */
    public function testReadTrip()
    {
        $trip = $this->makeTrip();
        $dbTrip = $this->tripRepo->find($trip->id);
        $dbTrip = $dbTrip->toArray();
        $this->assertModelData($trip->toArray(), $dbTrip);
    }

    /**
     * @test update
     */
    public function testUpdateTrip()
    {
        $trip = $this->makeTrip();
        $fakeTrip = $this->fakeTripData();
        $updatedTrip = $this->tripRepo->update($fakeTrip, $trip->id);
        $this->assertModelData($fakeTrip, $updatedTrip->toArray());
        $dbTrip = $this->tripRepo->find($trip->id);
        $this->assertModelData($fakeTrip, $dbTrip->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteTrip()
    {
        $trip = $this->makeTrip();
        $resp = $this->tripRepo->delete($trip->id);
        $this->assertTrue($resp);
        $this->assertNull(Trip::find($trip->id), 'Trip should not exist in DB');
    }
}
