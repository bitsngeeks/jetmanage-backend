<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AirporsApiTest extends TestCase
{
    use MakeAirporsTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateAirpors()
    {
        $airpors = $this->fakeAirporsData();
        $this->json('POST', '/api/v1/airpors', $airpors);

        $this->assertApiResponse($airpors);
    }

    /**
     * @test
     */
    public function testReadAirpors()
    {
        $airpors = $this->makeAirpors();
        $this->json('GET', '/api/v1/airpors/'.$airpors->id);

        $this->assertApiResponse($airpors->toArray());
    }

    /**
     * @test
     */
    public function testUpdateAirpors()
    {
        $airpors = $this->makeAirpors();
        $editedAirpors = $this->fakeAirporsData();

        $this->json('PUT', '/api/v1/airpors/'.$airpors->id, $editedAirpors);

        $this->assertApiResponse($editedAirpors);
    }

    /**
     * @test
     */
    public function testDeleteAirpors()
    {
        $airpors = $this->makeAirpors();
        $this->json('DELETE', '/api/v1/airpors/'.$airpors->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/airpors/'.$airpors->id);

        $this->assertResponseStatus(404);
    }
}
