<?php

// Home
Breadcrumbs::register('home', function($breadcrumbs){
    $breadcrumbs->push(trans('breadcrumbs.home'), route('home'));
});

// Home
Breadcrumbs::register('dashboardIndex', function($breadcrumbs){
    $breadcrumbs->parent('home');
    $breadcrumbs->push(trans('breadcrumbs.dashboard'));
});

/**
 * Role route
 */
Breadcrumbs::register('roleIndex', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(trans('breadcrumbs.role'), action('Admin\RoleController@index'));
});
Breadcrumbs::register('roleCreate', function($breadcrumbs) {
    $breadcrumbs->parent('roleIndex');
    $breadcrumbs->push(trans('breadcrumbs.create'));
});
Breadcrumbs::register('roleEdit', function($breadcrumbs) {
    $breadcrumbs->parent('roleIndex');
    $breadcrumbs->push(trans('breadcrumbs.edit'));
});

/**
 * User route
 */
Breadcrumbs::register('userIndex', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(trans('breadcrumbs.user'), action('Admin\UserController@index'));
});
Breadcrumbs::register('userCreate', function($breadcrumbs) {
    $breadcrumbs->parent('userIndex');
    $breadcrumbs->push(trans('breadcrumbs.create'));
});
Breadcrumbs::register('userEdit', function($breadcrumbs) {
    $breadcrumbs->parent('userIndex');
    $breadcrumbs->push(trans('breadcrumbs.edit'));
});
Breadcrumbs::register('userRole', function($breadcrumbs) {
    $breadcrumbs->parent('userIndex');
    $breadcrumbs->push(trans('breadcrumbs.role'));
});