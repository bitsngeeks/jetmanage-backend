<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $this->call(UsersTableSeeder::class);
        $this->call(PermissionRoleSeeder::class);
        $this->call(CountriesTableSeeder::class);

        foreach (range(1,10) as $index) {
            DB::table('companies')->insert([
                'name' => $faker->word,
                'is_active' => 1,
                'hash' => md5(md5(rand(10000,99999))),
            ]);
        }

        foreach (range(1,10) as $index) {
            DB::table('individuals')->insert([
                'first_name' => $faker->firstNameMale,
                'middle_name' => $faker->firstNameMale,
                'last_name' => $faker->lastName,
                'gender' => 1,
                'dob' => $faker->dateTimeThisCentury,
                'weight' => $faker->numberBetween($min = 100, $max = 200),
                'height' => $faker->numberBetween($min = 60, $max = 72),
                'res_country_id' => 235,
                'birth_country_id' => 235,
                'company_id' => 1,
                'user_id' => $index
            ]);
        }

        foreach (range(1,10) as $index) {
            DB::table('addresses')->insert([
                'ind_id' => $index,
                'address_1' => $faker->streetAddress,
                'address_2' => '',
                'city' => $faker->city,
                'state' => $faker->stateAbbr,
                'zip' => $faker->postcode,
                'country' => 'US',
                'is_primary' => true,
            ]);
        }

        foreach (range(1,10) as $index) {
            DB::table('emails')->insert([
                'ind_id' => $index,
                'email' => $faker->userName . '@sked.dev',
                'is_verified' => 1,
                'is_primary' => 1,
            ]);
        }

        foreach (range(1,10) as $index) {
            DB::table('phones')->insert([
                'ind_id' => $index,
                'phone' => '5555555555',
                'phone_type' => 1,
                'is_textable' => 1,
                'is_verified' => 1,
                'is_primary' => 1,
            ]);
        }

        DB::table('aircrafts')->insert([
            'registration' => 'N807D',
            'type_id' => 1,
            'serial_number' => 'N807D',
            'name' => 'Pilatus PC 12',
        ]);

        DB::table('aircrafts')->insert([
            'registration' => 'N859PL',
            'type_id' => 1,
            'serial_number' => 'N859PL',
            'name' => 'Pilatus PC 12',
        ]);

        DB::table('trips')->insert([
            'aircraft_id' => 1,
        ]);


        DB::table('legs')->insert([
            'trip_id' => 1,
            'trip_leg_order' => 1,
            'from_airport_id' => 2561,
            'to_airport_id' => 2561,
            'depart_date' => '2017-10-31 07:00:00',
            'out_time' => '2017-10-31 07:00:00',
            'off_time' => '2017-10-31 07:10:00',
            'arrival_date' => '2017-10-31 09:00:00',
            'on_time' => '2017-10-31 09:00:00',
            'in_time' => '2017-10-31 09:20:00',
            'out_fuel' => 500,
            'in_fuel' => 300,
            'out_hobbs' => 500,
            'in_hobbs' => 300,
            'out_tach' => 1000,
            'in_tach' => 1300,
            'regs' => 1,
            'aircraft_id' => 1,
            'registration' => 1,
        ]);

        DB::table('legs')->insert([
            'trip_id' => 1,
            'trip_leg_order' => 1,
            'from_airport_id' => 2561,
            'to_airport_id' => 2561,
            'depart_date' => '2017-10-31 09:30:00',
            'out_time' => '2017-10-31 07:00:00',
            'off_time' => '2017-10-31 07:10:00',
            'arrival_date' => '2017-10-31 11:30:00',
            'on_time' => '2017-10-31 09:00:00',
            'in_time' => '2017-10-31 09:20:00',
            'out_fuel' => 500,
            'in_fuel' => 300,
            'out_hobbs' => 500,
            'in_hobbs' => 300,
            'out_tach' => 1000,
            'in_tach' => 1300,
            'regs' => 1,
            'aircraft_id' => 1,
            'registration' => 1,
        ]);
        DB::table('passengers')->insert([
            'leg_id' => 1,
            'trip_id' => 1,
            'ind_id' => 1,
        ]);

        DB::table('tags')->insertGetId([
            'name' => 'Crew',
            'company_id' => 1,
        ]);
        DB::table('tags')->insertGetId([
            'name' => 'Pilot',
            'company_id' => 1,
        ]);
        DB::table('tags')->insertGetId([
            'name' => 'Second',
            'company_id' => 1,
        ]);
        DB::table('tags')->insertGetId([
            'name' => 'Passenger',
            'company_id' => 1,
        ]);
        DB::table('tags')->insertGetId([
            'name' => 'Office',
            'company_id' => 1,
        ]);

        foreach (range(1,10) as $index) {
            DB::table('individual_tag')->insert([
                'tag_id' => rand(1,3),
                'individual_id' => $index,
            ]);
            DB::table('individual_tag')->insert([
                'tag_id' => rand(4,5),
                'individual_id' => $index,
            ]);
        }

    }
}

