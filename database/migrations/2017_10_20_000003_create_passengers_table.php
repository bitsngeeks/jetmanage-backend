<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePassengersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('passengers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('leg_id')->unsigned();
            $table->integer('trip_id')->unsigned();
            $table->integer('ind_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('passengers', function (Blueprint $table) {
            $table->foreign('leg_id')
                ->references('id')
                ->on('legs');
        });
        Schema::table('passengers', function (Blueprint $table) {
            $table->foreign('trip_id')
                ->references('id')
                ->on('trips');
        });
        Schema::table('passengers', function (Blueprint $table) {
            $table->foreign('ind_id')
                ->references('id')
                ->on('individuals');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('passengers');
    }
}

