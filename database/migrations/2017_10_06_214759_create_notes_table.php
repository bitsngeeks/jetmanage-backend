<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ind_id')->unsigned();
            $table->integer('note_type')->unsigned();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('notes', function (Blueprint $table) {
            $table->foreign('ind_id')
                ->references('id')
                ->on('individuals');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notes');
    }
}
