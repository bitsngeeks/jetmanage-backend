<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ind_id')->unsigned();
            $table->string('address_1', 50);
            $table->string('address_2', 50)->nullable();
            $table->string('city', 50);
            $table->string('state', 60);
            $table->string('zip', 11);
            $table->string('country', 11);
            $table->boolean('is_primary');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('addresses', function (Blueprint $table) {
            $table->foreign('ind_id')
                ->references('id')
                ->on('individuals');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
