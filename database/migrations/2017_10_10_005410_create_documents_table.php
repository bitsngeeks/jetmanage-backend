<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('type');
            $table->string('number')->nullable();
            $table->string('authority')->nullable();
            $table->dateTime('issue_date');
            $table->dateTime('exp_date');
            $table->integer('issue_country')->unsigned();
            $table->boolean('is_primary');
            $table->boolean('is_active');
            $table->timestamps();
        });

        Schema::table('documents', function (Blueprint $table) {
            $table->foreign('issue_country')
                ->references('id')
                ->on('countries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('documents');
    }
}
